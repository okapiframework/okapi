/*===========================================================================
  Copyright (C) 2008-2011 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
===========================================================================*/

package net.sf.okapi.applications.rainbow.lib;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import com.ibm.icu.impl.LocaleIDs;
import com.ibm.icu.util.ULocale;

/**
 * Manages language/locale data for Rainbow pipelines.
 * This class is immutable and threadsafe once it is instantiated.
 */
public class LanguageManager {

	final private List<LanguageItem> langs;

	public LanguageManager () {
		langs = Collections.unmodifiableList(initialize());
	}

	// If it is safe to remove the script, do it.
	// By "safe" it means the script is default for that language / lang + region
	// For example : zh-Hans  zh-Hans-CN  zh-Hant-TW  mn-Cyrl  mn-Cyrl-MN  mn-Mong-CN 
	// Safe, return: zh       zh-CN       zh-TW       mn       mn-MN       mn-CN 
	// But for something like zh-Hant it is not safe to remove the script.
	static private ULocale removeTheScript(ULocale loc) {
		String script = loc.getScript();
		if (!script.isEmpty()) {
			// To check if it is safe to remove the script we remove it
			// then add it back with addLikelySubtags, and compare with what we had before
			ULocale locNoScript = new ULocale.Builder().setLocale(loc).setScript("").build();
			ULocale fullLocale = ULocale.addLikelySubtags(locNoScript);
			if (script.equals(fullLocale.getScript())) {
				return locNoScript;
			}
		}
		return loc;
	}

	private boolean addLocale(Set<ULocale> allLocales, ULocale loc) {
		if (allLocales.contains(loc))
			return false;

		String lang = loc.getLanguage();
		if (lang.isEmpty()) // Unknown language
			return false;
		if ("zxx".equals(lang)) // No linguistic content
			return false;
		if ("ZZ".equals(loc.getCountry())) // Unknown region
			return false;

		// There are 67 ISO language ids for which ICU has no friendly name.
		// Should we show them as id, or drop them?
		// Or take the names (hard-coded) from somewhere else?
		if (lang.equals(loc.getDisplayLanguage(ULocale.US)))
			return false;

		allLocales.add(loc);
		return true;
	}

	private List<LanguageItem> initialize() {
		Set<ULocale> allLocales = new TreeSet<>();

		for (ULocale loc : ULocale.getAvailableLocales()) {
			addLocale(allLocales, loc);
			addLocale(allLocales, removeTheScript(loc));
		}

		// getAvailableLocales only list the locales for which CLDR has data.
		// But that is not a complete list. So we also collect from ISO languages / countries  
		for (String lang : LocaleIDs.getISOLanguages()) {
			ULocale loc = ULocale.forLanguageTag(lang); // lang
			ULocale fullLocale = ULocale.addLikelySubtags(loc); // lang-Script-Region
			addLocale(allLocales, loc); // lang
			addLocale(allLocales, removeTheScript(fullLocale)); // lang-Region
		}

		for (String ctry : LocaleIDs.getISOCountries()) {
			ULocale loc = ULocale.forLanguageTag("und-" + ctry);
			ULocale fullLocale = ULocale.addLikelySubtags(loc);
			addLocale(allLocales, removeTheScript(fullLocale));
		}

		List<LanguageItem> result = new ArrayList<>();
		allLocales.forEach(loc -> {
			result.add(new LanguageItem(
					loc.toLanguageTag(),
					loc.getDisplayName(ULocale.US)));
		});
		Collections.sort(result, (a, b) -> a.toString().compareTo(b.toString()));
		return result;
	}

	public int getCount () {
		return langs.size();
	}

	public LanguageItem getItem (int p_nIndex) {
		return langs.get(p_nIndex);
	}

	public LanguageItem GetItem (String p_sCode) {
		for ( int i=0; i<langs.size(); i++ ) {
			if ( p_sCode.equalsIgnoreCase(langs.get(i).code) )
				return langs.get(i);
		}
		return null;
	}

	public String GetNameFromCode (String p_sCode) {
		for ( int i=0; i<langs.size(); i++ ) {
			if ( p_sCode.equalsIgnoreCase(langs.get(i).code) )
				return langs.get(i).name;
		}
		return p_sCode; // Return code if not found
	}
	
	public int getIndexFromCode (String p_sCode) {
		if ( p_sCode == null ) return -1;
		for ( int i=0; i<langs.size(); i++ ) {
			if ( p_sCode.equalsIgnoreCase(langs.get(i).code) )
				return i;
		}
		return  -1; // Return -1 if not found
	}
}
