!!! note "This is a title"
    Some admonitions have titles.

!!! note
    Some admonitions do not have titles. "note" should be translated.


!!! note ""
    Admonition without header

??? type "Python generator example"
    A generator function that yields ints is secretly just a function that
    returns an iterator of ints, so that's how we annotate it
    ``` python
    def g(n: int) -> Iterator[int]:
        i = 0
        while i < n:
            yield i
            i += 1
    ```

??? note "Open styled details"
    ??? danger "Nested details!"
        And more content again.


---

:::note This is a title

Some **content** with _Markdown_ `syntax`. Check [this `api`](#).

:::

:::tip

Some **content** with _Markdown_ `syntax`. Check [this `api`](#).

:::

:::info

Some **content** with _Markdown_ `syntax`. Check [this `api`](#).

:::

:::caution

Some **content** with _Markdown_ `syntax`. Check [this `api`](#).

:::