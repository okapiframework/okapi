package net.sf.okapi.simplifier.integration;

import net.sf.okapi.common.integration.FileComparator;
import net.sf.okapi.common.integration.SimplifyRoundTripIT;
import net.sf.okapi.filters.regex.RegexFilter;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.FileNotFoundException;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;

@RunWith(JUnit4.class)
public class RoundTripSimplifyRegexIT extends SimplifyRoundTripIT {
	private static final String CONFIG_ID = "okf_regex";
	private static final String DIR_NAME = "/regex/";
	private static final List<String> EXTENSIONS = Arrays.asList(".txt", ".regex");
	private static final String XLIFF_EXTRACTED_EXTENSION = ".xliff";

	public RoundTripSimplifyRegexIT() {
		super(CONFIG_ID, DIR_NAME, EXTENSIONS, XLIFF_EXTRACTED_EXTENSION, RegexFilter::new);
	}

	@Test
	public void regexFiles() throws FileNotFoundException, URISyntaxException {
		setSerializedOutput(false);
		realTestFiles(false, new FileComparator.Utf8FilePerLineComparator());
	}

	@Test
	public void regexFilesSerialized() throws FileNotFoundException, URISyntaxException {
		setSerializedOutput(true);
		realTestFiles(false, new FileComparator.Utf8FilePerLineComparator());
	}
}
