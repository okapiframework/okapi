package net.sf.okapi.xliffcompare.integration;

import net.sf.okapi.common.FileLocation;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.integration.XliffCompareIT;
import net.sf.okapi.common.integration.FileComparator;
import net.sf.okapi.filters.xliff.XLIFFFilter;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.File;
import java.io.FileNotFoundException;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@RunWith(JUnit4.class)
public class XliffXliffCompareIT extends XliffCompareIT {
	private static final String CONFIG_ID = "okf_xliff";
	private static final String DIR_NAME = "/xliff/";
	private static final List<String> EXTENSIONS = Arrays.asList(".xliff", ".xlf", ".sdlxliff");
	final static FileLocation root = FileLocation.fromClass(XliffXliffCompareIT.class);

	public XliffXliffCompareIT() {
		super(CONFIG_ID, DIR_NAME, EXTENSIONS, LocaleId.EMPTY, XLIFFFilter::new);
		addKnownFailingFile("DE_CALC_PHASE1.xlsx.sdlxliff");
	}

	@Test
	public void sdlXliffXliffCompareFiles() throws FileNotFoundException, URISyntaxException {
		setExtensions(Collections.singletonList(".sdlxliff"));
		realTestFiles("okf_xliff-sdl", true, new FileComparator.XmlComparator());
	}

	@Test
	public void worldserverXliffXLiffCompareFiles() throws FileNotFoundException, URISyntaxException {
		setExtensions(Collections.singletonList(".iwsxliff"));
		realTestFiles("okf_xliff-iws", true, new FileComparator.XmlComparator());
	}

	@Test
	public void xliffXliffCompareFiles() throws FileNotFoundException, URISyntaxException {
		setExtensions(EXTENSIONS);
		realTestFiles(CONFIG_ID, true, new FileComparator.XmlComparator());
	}
}
