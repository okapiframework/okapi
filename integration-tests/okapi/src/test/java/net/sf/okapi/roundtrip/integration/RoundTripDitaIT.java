package net.sf.okapi.roundtrip.integration;

import net.sf.okapi.common.FileLocation;
import net.sf.okapi.common.filters.IFilter;
import net.sf.okapi.common.integration.EventRoundTripIT;
import net.sf.okapi.common.integration.FileComparator;
import net.sf.okapi.filters.xmlstream.XmlStreamFilter;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.File;
import java.io.FileNotFoundException;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;
import java.util.function.Supplier;

@RunWith(JUnit4.class)
public class RoundTripDitaIT extends EventRoundTripIT {
	private static final String CONFIG_ID = "okf_xmlstream-dita";
	private static final String DIR_NAME = "/dita/";
	private static final List<String> EXTENSIONS = Arrays.asList(".dita", ".ditamap");
	final static FileLocation root = FileLocation.fromClass(RoundTripDitaIT.class);
	private static final Supplier<IFilter> FILTER_CONSTRUCTOR = XmlStreamFilter::new;	

	public RoundTripDitaIT() {
		super(CONFIG_ID, DIR_NAME, EXTENSIONS, FILTER_CONSTRUCTOR);
	}

	@Ignore
	public void debug() throws FileNotFoundException, URISyntaxException {
		final File file = root.in("/dita/whitespacebug.dita").asFile();
		runTest(new TestJob(CONFIG_ID, true, file, "", null,
				new FileComparator.EventComparatorWithWhitespace(), FILTER_CONSTRUCTOR));
	}

	@Test
	public void ditaFiles() throws FileNotFoundException, URISyntaxException {
		setSerializedOutput(false);
		realTestFiles(CONFIG_ID, false, new FileComparator.EventComparator());
	}

	@Test
	public void ditaFilesSerialized() throws FileNotFoundException, URISyntaxException {
		setSerializedOutput(true);
		realTestFiles(CONFIG_ID, false, new FileComparator.EventComparator());
	}
}
