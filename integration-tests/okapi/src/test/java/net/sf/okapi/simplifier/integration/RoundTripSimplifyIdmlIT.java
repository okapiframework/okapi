package net.sf.okapi.simplifier.integration;

import net.sf.okapi.common.FileLocation;
import net.sf.okapi.common.filters.IFilter;
import net.sf.okapi.common.integration.SimplifyRoundTripIT;
import net.sf.okapi.common.integration.FileComparator;
import net.sf.okapi.filters.idml.IDMLFilter;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.File;
import java.io.FileNotFoundException;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;
import java.util.function.Supplier;

@RunWith(JUnit4.class)
public class RoundTripSimplifyIdmlIT extends SimplifyRoundTripIT {
	private static final String CONFIG_ID = "okf_idml";
	private static final String DIR_NAME = "/idml/";
	private static final List<String> EXTENSIONS = Arrays.asList(".idml");
	private static final String XLIFF_EXTRACTED_EXTENSION = ".simplify_xliff";
	private static final Supplier<IFilter> FILTER_CONSTRUCTOR = IDMLFilter::new;
	final static FileLocation root = FileLocation.fromClass(RoundTripSimplifyIdmlIT.class);

	public RoundTripSimplifyIdmlIT() {
		// IDML filter stores the original code id's in the TU skeleton and expects these to stay
		// exactly the same. Not sure it's always possible if a TextFragment re-balanced etc..
		super(CONFIG_ID, DIR_NAME, EXTENSIONS, XLIFF_EXTRACTED_EXTENSION, IDMLFilter::new);
		addKnownFailingFile("4_story_pointers.idml");
	}

	@Ignore
	public void debug() throws FileNotFoundException, URISyntaxException {
		setSerializedOutput(false);
		final File file = root.in("/idml/4_story_pointers.idml").asFile();
		runTest(new TestJob(CONFIG_ID, false, file, "", null,
				new FileComparator.ArchiveComparator(), FILTER_CONSTRUCTOR));
	}

	@Test
	public void idmlFiles() throws FileNotFoundException, URISyntaxException {
		setSerializedOutput(false);
		realTestFiles(false, new FileComparator.ArchiveComparator());
	}

	@Test
	public void idmlFilesSerialized() throws FileNotFoundException, URISyntaxException {
		setSerializedOutput(true);
		realTestFiles(false, new FileComparator.ArchiveComparator());
	}
}
