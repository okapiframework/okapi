package net.sf.okapi.roundtrip.integration;

import net.sf.okapi.common.integration.EventRoundTripIT;
import net.sf.okapi.common.integration.FileComparator;
import net.sf.okapi.filters.vtt.VTTFilter;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.FileNotFoundException;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;

@RunWith(JUnit4.class)
public class RoundTripVttIT extends EventRoundTripIT {
	private static final String CONFIG_ID = "okf_vtt";
	private static final String DIR_NAME = "/vtt/";
	private static final List<String> EXTENSIONS = Arrays.asList(".vtt", ".srt");

	public RoundTripVttIT() {
		super(CONFIG_ID, DIR_NAME, EXTENSIONS, VTTFilter::new);
	}

	@Test
	public void vttFiles() throws FileNotFoundException, URISyntaxException {
		setSerializedOutput(false);
		realTestFiles(false, new FileComparator.EventComparator());
	}

	@Test
	public void vttSerializedFiles() throws FileNotFoundException, URISyntaxException {
		setSerializedOutput(true);
		realTestFiles(false, new FileComparator.EventComparator());
	}
}
