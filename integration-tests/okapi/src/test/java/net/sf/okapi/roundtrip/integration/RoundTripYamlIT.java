package net.sf.okapi.roundtrip.integration;

import net.sf.okapi.common.FileLocation;
import net.sf.okapi.common.filters.IFilter;
import net.sf.okapi.common.integration.EventRoundTripIT;
import net.sf.okapi.common.integration.FileComparator;
import net.sf.okapi.filters.yaml.YamlFilter;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.File;
import java.io.FileNotFoundException;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;
import java.util.function.Supplier;

@RunWith(JUnit4.class)
public class RoundTripYamlIT extends EventRoundTripIT {
	private static final String CONFIG_ID = "okf_yaml";
	private static final String DIR_NAME = "/yaml/";
	private static final List<String> EXTENSIONS = Arrays.asList(".yml", ".yaml");
	final static FileLocation root = FileLocation.fromClass(RoundTripXmlStreamIT.class);
	private static final Supplier<IFilter> FILTER_CONSTRUCTOR = YamlFilter::new;

	public RoundTripYamlIT() {
		super(CONFIG_ID, DIR_NAME, EXTENSIONS, YamlFilter::new);
		addKnownFailingFile("unknown-tags-example.yaml");
		addKnownFailingFile("no-children-1-pretty.yaml");
		addKnownFailingFile("ios_emoji_surrogate.yaml");
		addKnownFailingFile("example2_17.yaml");
		addKnownFailingFile("example2_17_control.yaml");
		addKnownFailingFile("emoji1.yaml");
	}

	@Test
	public void debug() throws FileNotFoundException, URISyntaxException {
		setSerializedOutput(false);
		final File file = root.in("/yaml/spec_test/example2_15_dumped.yaml").asFile();
		runTest(new TestJob(CONFIG_ID, false, file, "", null, new FileComparator.EventComparator(), FILTER_CONSTRUCTOR));
	}

	@Test
	public void yamlFiles() throws FileNotFoundException, URISyntaxException {
		setSerializedOutput(false);
		realTestFiles(false, new FileComparator.EventComparator());
	}

	@Test
	public void yamlSerializedFiles() throws FileNotFoundException, URISyntaxException {
		setSerializedOutput(true);
		realTestFiles(false, new FileComparator.EventComparator());
	}
}
