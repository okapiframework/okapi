package net.sf.okapi.roundtrip.integration;

import net.sf.okapi.common.FileLocation;
import net.sf.okapi.common.filters.IFilter;
import net.sf.okapi.common.integration.EventRoundTripIT;
import net.sf.okapi.common.integration.FileComparator;
import net.sf.okapi.filters.po.POFilter;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.File;
import java.io.FileNotFoundException;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;
import java.util.function.Supplier;

@RunWith(JUnit4.class)
public class RoundTripPoIT extends EventRoundTripIT {
	private static final String CONFIG_ID = "okf_po";
	private static final String DIR_NAME = "/po/";
	private static final List<String> EXTENSIONS = Arrays.asList(".po");
	private static final Supplier<IFilter> FILTER_CONSTRUCTOR = POFilter::new;
	final static FileLocation root = FileLocation.fromClass(RoundTripPoIT.class);

	public RoundTripPoIT() {
		super(CONFIG_ID, DIR_NAME, EXTENSIONS, FILTER_CONSTRUCTOR);
	}

	@Ignore
	public void debug() throws FileNotFoundException, URISyntaxException {
		final File file = root.in("/po/escaping.po").asFile();
		runTest(new TestJob(CONFIG_ID, true, file, "", null,
				new FileComparator.EventComparatorWithWhitespace(), FILTER_CONSTRUCTOR));
	}

	@Test
	public void poFiles() throws FileNotFoundException, URISyntaxException {
		setSerializedOutput(false);
		realTestFiles(CONFIG_ID, false, new FileComparator.EventComparatorWithWhitespace());
	}

	@Test
	public void poSerializedFiles() throws FileNotFoundException, URISyntaxException {
		setSerializedOutput(true);
		realTestFiles(CONFIG_ID, false, new FileComparator.EventComparatorWithWhitespace());
	}
}
