package net.sf.okapi.roundtrip.integration;

import net.sf.okapi.common.FileLocation;
import net.sf.okapi.common.filters.IFilter;
import net.sf.okapi.common.integration.EventRoundTripIT;
import net.sf.okapi.common.integration.FileComparator;
import net.sf.okapi.filters.its.html5.HTML5Filter;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.File;
import java.io.FileNotFoundException;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;
import java.util.function.Supplier;

@RunWith(JUnit4.class)
public class RoundTripHtmlItsIT extends EventRoundTripIT {
	private static final String CONFIG_ID = "okf_itshtml5";
	private static final String DIR_NAME = "/htmlIts/";
	private static final List<String> EXTENSIONS = Arrays.asList(".html", ".html5");
	final static FileLocation root = FileLocation.fromClass(RoundTripHtmlItsIT.class);
	private static final Supplier<IFilter> FILTER_CONSTRUCTOR = HTML5Filter::new;

	public RoundTripHtmlItsIT() {
		super(CONFIG_ID, DIR_NAME, EXTENSIONS, HTML5Filter::new);
		addKnownFailingFile("test01.html");
		addKnownFailingFile("test2.html");
	}

	@Ignore
	public void debug() throws FileNotFoundException, URISyntaxException {
		final File file = root.in("/htmlIts/lqi-test1.html").asFile();
		runTest(new TestJob(CONFIG_ID, false, file, "", null,
				new FileComparator.EventComparator(), FILTER_CONSTRUCTOR));
	}

	@Test
	public void itsHtmlFiles() throws FileNotFoundException, URISyntaxException {
		setSerializedOutput(false);
		realTestFiles(false, new FileComparator.EventComparator());
	}

	@Test
	public void itsHtmlFilesSerialized() throws FileNotFoundException, URISyntaxException {
		setSerializedOutput(true);
		realTestFiles(CONFIG_ID, false, new FileComparator.EventComparator());
	}
}
