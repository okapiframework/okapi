package net.sf.okapi.roundtrip.integration;

import net.sf.okapi.common.FileLocation;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.filters.IFilter;
import net.sf.okapi.common.integration.EventRoundTripIT;
import net.sf.okapi.common.integration.FileComparator;
import net.sf.okapi.filters.json.JSONFilter;
import net.sf.okapi.filters.yaml.YamlFilter;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.File;
import java.io.FileNotFoundException;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;
import java.util.function.Supplier;

@RunWith(JUnit4.class)
public class RoundTripJSONMessageFormatIT extends EventRoundTripIT {
	private static final String CONFIG_ID = "okf_json";
	private static final String DIR_NAME = "/messageformat";
	private static final List<String> EXTENSIONS = Arrays.asList(".json");
	private static final Supplier<IFilter> FILTER_CONSTRUCTOR = JSONFilter::new;
	final static FileLocation root = FileLocation.fromClass(RoundTripJSONMessageFormatIT.class);

	public RoundTripJSONMessageFormatIT() {
		super(true, CONFIG_ID, DIR_NAME, EXTENSIONS, LocaleId.ARABIC, FILTER_CONSTRUCTOR);
	}


	@Test
	public void debug() throws FileNotFoundException, URISyntaxException {
		setSerializedOutput(true);
		final File file = root.in("/messageformat/JSON/expand/complex.json").asFile();
		runTest(new TestJob("okf_json@messageformat_expand", false, file, "", file.getParent(),
				new FileComparator.EventComparator(), FILTER_CONSTRUCTOR));
	}

	@Test
	public void messageFiles() throws FileNotFoundException, URISyntaxException {
		setSerializedOutput(false);
		realTestFiles(false, new FileComparator.EventComparator());
	}

	@Test
	public void messageSerializedFiles() throws FileNotFoundException, URISyntaxException {
		setSerializedOutput(true);
		realTestFiles(false, new FileComparator.EventComparator());
	}
}
