package net.sf.okapi.conversion.integration;

import net.sf.okapi.common.FileLocation;
import net.sf.okapi.common.filters.IFilter;
import net.sf.okapi.common.integration.ConversionCompareIT;
import net.sf.okapi.common.integration.FileComparator;
import net.sf.okapi.filters.tmx.TmxFilter;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.File;
import java.io.FileNotFoundException;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;
import java.util.function.Supplier;

@RunWith(JUnit4.class)
public class ConvertFromTmxIT extends ConversionCompareIT {
	private static final String CONFIG_ID = "okf_tmx";
	private static final String DIR_NAME = "/tmx/";
	private static final List<String> EXTENSIONS = Arrays.asList(".tmx");
	private static final Supplier<IFilter> FILTER_CONSTRUCTOR = TmxFilter::new;
	final static FileLocation root = FileLocation.fromClass(ConvertFromTmxIT.class);

	public ConvertFromTmxIT() {
		super(CONFIG_ID, DIR_NAME, EXTENSIONS, FILTER_CONSTRUCTOR);
		addKnownFailingFile("code_id_difference.tmx");
	}

	@Test
	public void debug() throws FileNotFoundException, URISyntaxException {
		final File file = root.in("/tmx/code_id_difference.tmx").asFile();
		runTest(new TestJob(CONFIG_ID, true, file, "", null,
				new FileComparator.EventComparatorTextUnitOnly(), FILTER_CONSTRUCTOR));
	}

	@Test
	public void tmxFiles() throws FileNotFoundException, URISyntaxException {
		realTestFiles(true, new FileComparator.EventComparatorTextUnitOnly());
	}
}
