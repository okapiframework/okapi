/**
 * 
 */
package net.sf.okapi.common.integration;

import net.sf.okapi.common.FileUtil;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.filters.IFilter;

import net.sf.okapi.common.filters.IFilterConfigurationMapper;
import org.junit.Rule;
import org.junit.rules.ErrorCollector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.nio.file.Paths;
import java.util.List;
import java.util.function.Supplier;

import static org.junit.Assert.assertTrue;

/**
 * @author jimh
 *
 */
public abstract class XmlOrTextRoundTripIT extends BaseRoundTripIT {
	private final Logger logger = LoggerFactory.getLogger(getClass());
	@Rule
	public ErrorCollector errCol = new ErrorCollector();

	public XmlOrTextRoundTripIT(final String configId, final String dirName,
			final List<String> extensions, Supplier<IFilter> filterConstructor) {
		super(/*parallel*/ true, configId, dirName, extensions, filterConstructor);
	}

	public XmlOrTextRoundTripIT(final String configId, final String dirName,
			final List<String> extensions, final LocaleId defaultTargetLocale, Supplier<IFilter> filterConstructor) {
		super(/*parallel*/ true, configId, dirName, extensions, defaultTargetLocale, filterConstructor);
	}

	@Override
	protected void runTest(final TestJob testJob)  {
		final String f = testJob.file.getName();
		final String root = testJob.file.getParent() + File.separator;
		final String original = root + f;
		final String tkitMerged = root + f + ".tkitMerged";

		String extractedOutput = root + f + xliffExtractedExtension;
		extractedOutput = isSerializedOutput() ? root + f + serializedExtractedExtension : extractedOutput;
		LocaleId source = LocaleId.ENGLISH;
		LocaleId target = defaultTargetLocale;
		if (testJob.detectLocales) {
			final List<String> locales = FileUtil.guessLanguages(testJob.file.getAbsolutePath());
			if (locales.size() >= 1) {
				source = LocaleId.fromString(locales.get(0));
			}
			if (locales.size() >= 2) {
				target = LocaleId.fromString(locales.get(1));
			}
		}

		try {
			logger.info(Paths.get(testJob.relativeSubDir, f).toString());
			IFilterConfigurationMapper mapper = RoundTripUtils.extract(source, target, original, extractedOutput, testJob.configId,
					testJob.customConfigPath, isSerializedOutput(), false);
			RoundTripUtils.merge(source, target, original, extractedOutput, tkitMerged,
					testJob.configId, isSerializedOutput());
			assertTrue("Compare XML: " + f, testJob.comparator.compare(Paths.get(tkitMerged), Paths.get(original)));
		} catch (final Throwable e) {
			if (!knownFailingFiles.contains(f)) {
				errCol.addError(new OkapiTestException(f, e));
				logger.error("Failing test: {}\n{}", f, e.getMessage());
			} else {
				logger.info("Ignored known failing file: {}", f);
			}
		}
	}
}
