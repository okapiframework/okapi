package net.sf.okapi.xliffcompare.integration;

import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.integration.XliffCompareIT;
import net.sf.okapi.common.integration.FileComparator;
import net.sf.okapi.filters.ts.TsFilter;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.FileNotFoundException;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;

@RunWith(JUnit4.class)
public class TsXliffCompareIT extends XliffCompareIT {
	private static final String CONFIG_ID = "okf_ts";
	private static final String DIR_NAME = "/ts/";
	private static final List<String> EXTENSIONS = Arrays.asList(".ts");

	public TsXliffCompareIT() {
		super(CONFIG_ID, DIR_NAME, EXTENSIONS, LocaleId.EMPTY, TsFilter::new);
		if ("\r\n".equals(System.lineSeparator())) {
			addKnownFailingFile("Test_nautilus.af.ts");
		}
	}

	@Test
	public void tsXliffCompareFiles() throws FileNotFoundException, URISyntaxException {
		realTestFiles(true, new FileComparator.XmlComparator());
	}
}
