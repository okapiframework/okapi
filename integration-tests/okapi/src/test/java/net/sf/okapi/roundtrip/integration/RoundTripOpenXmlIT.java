package net.sf.okapi.roundtrip.integration;

import net.sf.okapi.common.FileLocation;
import net.sf.okapi.common.filters.IFilter;
import net.sf.okapi.common.integration.EventRoundTripIT;
import net.sf.okapi.common.integration.FileComparator;
import net.sf.okapi.filters.openxml.OpenXMLFilter;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.File;
import java.io.FileNotFoundException;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;
import java.util.function.Supplier;

@RunWith(JUnit4.class)
public class RoundTripOpenXmlIT extends EventRoundTripIT {
	private static final String CONFIG_ID = "okf_openxml";
	private static final String DIR_NAME = "/openxml/";
	private static final List<String> EXTENSIONS = Arrays.asList(".docx", ".pptx", ".xlsx");
	private static final Supplier<IFilter> FILTER_CONSTRUCTOR = OpenXMLFilter::new;
	final static FileLocation root = FileLocation.fromClass(RoundTripOpenXmlIT.class);
	public RoundTripOpenXmlIT() {
		super(CONFIG_ID, DIR_NAME, EXTENSIONS, OpenXMLFilter::new);
	}

	@Ignore
	public void debug() throws FileNotFoundException, URISyntaxException {
		setSerializedOutput(true);
		setExtensions(EXTENSIONS);
		final File file =
				root.in("/openxml/docx/equation2.docx").asFile();
		runTest(new TestJob(CONFIG_ID, false, file, "", null,
				new FileComparator.EventComparator(), FILTER_CONSTRUCTOR));
	}

	@Ignore
	public void debug2() {
		setSerializedOutput(false);
		setExtensions(EXTENSIONS);
		final File file = root.in("/openxml/debug/code_error.docx").asFile();
		runTest(new TestJob(CONFIG_ID, false, file, "", null,
				new FileComparator.EventComparator(), FILTER_CONSTRUCTOR));
	}

	@Test
	public void openXmlFiles() throws FileNotFoundException, URISyntaxException {
		setSerializedOutput(false);
		realTestFiles(true, new FileComparator.EventComparator());
	}

	@Test
	public void openXmlSerializedFiles() throws FileNotFoundException, URISyntaxException {
		setSerializedOutput(true);
		realTestFiles(true, new FileComparator.EventComparator());
	}
}
