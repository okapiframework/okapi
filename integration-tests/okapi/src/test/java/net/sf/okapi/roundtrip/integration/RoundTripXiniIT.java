package net.sf.okapi.roundtrip.integration;

import java.io.File;
import java.io.FileNotFoundException;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;

import net.sf.okapi.common.FileLocation;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import net.sf.okapi.common.integration.FileComparator;
import net.sf.okapi.common.integration.XmlOrTextRoundTripIT;
import net.sf.okapi.filters.xini.XINIFilter;

@RunWith(JUnit4.class)
public class RoundTripXiniIT extends XmlOrTextRoundTripIT {
	private static final String CONFIG_ID = "okf_xini";
	private static final String DIR_NAME = "/xini/";
	private static final List<String> EXTENSIONS = Arrays.asList(".xini");
	final static FileLocation root = FileLocation.fromClass(RoundTripXliff2IT.class);

	public RoundTripXiniIT() {
		super(CONFIG_ID, DIR_NAME, EXTENSIONS, XINIFilter::new);
	}

	@Ignore
	public void debug() throws FileNotFoundException, URISyntaxException {
		setSerializedOutput(true);
		final File file = root.in("/xini/ascendingPhs.xini").asFile();
		runTest(new TestJob(CONFIG_ID, true, file, "", null,
				new FileComparator.XmlComparator(), XINIFilter::new));
	}

	@Test
	public void xiniFiles() throws FileNotFoundException, URISyntaxException {
		setSerializedOutput(false);
		realTestFiles(false, new FileComparator.XmlComparator());
	}

	@Test
	public void xiniSerializedFiles() throws FileNotFoundException, URISyntaxException {
		setSerializedOutput(true);
		realTestFiles(false, new FileComparator.XmlComparator());
	}
}
