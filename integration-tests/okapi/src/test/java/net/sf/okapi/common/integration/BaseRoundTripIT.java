/**
 * 
 */
package net.sf.okapi.common.integration;

import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.Util;
import net.sf.okapi.common.filters.IFilter;

import org.junit.Rule;
import org.junit.rules.ErrorCollector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileNotFoundException;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Supplier;

/**
 * @author jimh
 *
 */
public abstract class BaseRoundTripIT {
	private final Logger logger = LoggerFactory.getLogger(getClass());

	final String defaultConfigId;
	final boolean parallel;
	final String dirName;
	List<String> extensions;
	final LocaleId defaultTargetLocale;
	final Set<String> knownFailingFiles = new HashSet<>();
	final String xliffExtractedExtension;
	final Supplier<IFilter> filterConstructor;
	boolean serializedOutput = false;
	String serializedExtractedExtension = ".json";


	@Rule
	public ErrorCollector errCol = new ErrorCollector();

	public BaseRoundTripIT(boolean parallel, final String configId, final String dirName,
			final List<String> extensions, Supplier<IFilter> filterConstructor) {
		this(parallel, configId, dirName, extensions, LocaleId.FRENCH, filterConstructor);
	}

	public BaseRoundTripIT(boolean parallel, final String configId, final String dirName,
			final List<String> extensions, final LocaleId defaultTargetLocale, Supplier<IFilter> filterConstructor) {
		this.parallel = parallel;
		this.defaultConfigId = configId;
		this.dirName = dirName;
		this.extensions = extensions;
		this.defaultTargetLocale = defaultTargetLocale;
		xliffExtractedExtension = configId.startsWith("okf_xliff") ? ".xliff_extracted" : ".xliff";
		this.filterConstructor = filterConstructor;
		this.serializedOutput = false;
		this.serializedExtractedExtension = ".ser";
	}

	public void setExtensions(List<String> extensions) {
		this.extensions = extensions;
	}


	public boolean isSerializedOutput() {
		return serializedOutput;
	}
	public void setSerializedOutput(boolean serializedOutput) {
		this.serializedOutput = serializedOutput;
	}

	public void addKnownFailingFile(final String fileName) {
		knownFailingFiles.add(fileName);
	}

	public void realTestFiles(final boolean detectLocales, final IComparator comparator)
			throws FileNotFoundException, URISyntaxException {
		realTestFiles(defaultConfigId, detectLocales, comparator);
	}

	public void realTestFiles(final String cfgId, final boolean detectLocales, final IComparator comparator)
			throws FileNotFoundException, URISyntaxException {

		List<TestJob> testJobs = new ArrayList<>();
		// run top level files (without config)
		final Path path = IntegrationtestUtils.ROOT.in(dirName).asPath();
		final File dir = path.toFile();
		for (final File file : IntegrationtestUtils.getTestFilesNoRecurse(dir, extensions)) {
			testJobs.add(new TestJob(cfgId, detectLocales, file, "", null, comparator, filterConstructor));
		}
		if (parallel) {
			testJobs.parallelStream().forEach(this::runTest);
		} else {
			testJobs.forEach(this::runTest);
		}
		testJobs.clear();

		// run each subdirectory where we assume there is a custom config)
		for (final File subDir : IntegrationtestUtils.getSubDirs(dir)) {
			final String relativeSubDir = path.relativize(subDir.toPath()).toString();
			for (final File file : IntegrationtestUtils.getTestFilesNoRecurse(subDir, extensions)) {
				Collection<File> configs = IntegrationtestUtils.getConfigFiles(subDir);
				if (configs.isEmpty()) {
					testJobs.add(new TestJob(cfgId, detectLocales, file, relativeSubDir, null, comparator, filterConstructor));
				} else {
					for (final File c : configs) {
						String configId = Util.getFilename(c.getAbsolutePath(), false);
						final String customConfigPath = c.getParent();
						testJobs.add(new TestJob(configId, detectLocales, file, relativeSubDir, customConfigPath, comparator, filterConstructor));
					}
				}
			}
		}
		if (parallel) {
			testJobs.parallelStream().forEach(this::runTest);
		} else {
			testJobs.forEach(this::runTest);
		}
	}
	
	public static class TestJob {
		final String configId;
		final boolean detectLocales;
		final File file;
		final String relativeSubDir;
		final String customConfigPath;
		final IComparator comparator;
		final Supplier<IFilter> constructor;

		public TestJob(String configId, boolean detectLocales, File file, String relativeSubDir,
		    String customConfigPath, IComparator<?> comparator, Supplier<IFilter> constructor) {
			this.configId = configId;
			this.detectLocales = detectLocales;
			this.file = file;
			this.relativeSubDir = relativeSubDir;
			this.customConfigPath = customConfigPath;
			this.comparator = comparator;
			this.constructor = constructor;
		}

		@Override
		public String toString() {
			return "TestJob ["
					+ "configId=" + configId
					+ ", detectLocales=" + detectLocales
					+ ", file=" + file
					+ ", relativeSubDir=" + relativeSubDir
					+ ", customConfigPath=" + customConfigPath
					+ ", comparator=" + comparator
					+ ", constructor=" + constructor
					+ "]";
		}
	}
	
	abstract protected void runTest(final TestJob testJob);
}
