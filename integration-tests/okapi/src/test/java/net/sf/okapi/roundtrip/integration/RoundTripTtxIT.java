package net.sf.okapi.roundtrip.integration;

import net.sf.okapi.common.FileLocation;
import net.sf.okapi.common.filters.IFilter;
import net.sf.okapi.common.integration.FileComparator;
import net.sf.okapi.common.integration.XmlOrTextRoundTripIT;
import net.sf.okapi.filters.ttx.TTXFilter;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.File;
import java.io.FileNotFoundException;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;
import java.util.function.Supplier;

@RunWith(JUnit4.class)
public class RoundTripTtxIT extends XmlOrTextRoundTripIT {
	private static final String CONFIG_ID = "okf_ttx";
	private static final String DIR_NAME = "/ttx/";
	private static final List<String> EXTENSIONS = Arrays.asList(".ttx");
	private static final Supplier<IFilter> FILTER_CONSTRUCTOR = TTXFilter::new;
	final static FileLocation root = FileLocation.fromClass(RoundTripXliffIT.class);

	public RoundTripTtxIT() {
		super(CONFIG_ID, DIR_NAME, EXTENSIONS, FILTER_CONSTRUCTOR);
		addKnownFailingFile("Test02_noseg.html.ttx");
	}

	@Ignore
	public void debug() throws FileNotFoundException, URISyntaxException {
		final File file = root.in("/ttx/Test02_allseg.html.ttx").asFile();
		runTest(new TestJob(CONFIG_ID, true, file, "", null,
				new FileComparator.XmlComparator(), FILTER_CONSTRUCTOR));
	}

	@Test
	public void ttxFiles() throws FileNotFoundException, URISyntaxException {
		setSerializedOutput(false);
		realTestFiles(false, new FileComparator.XmlComparator());
	}

	@Test
	public void ttxSerializedFiles() throws FileNotFoundException, URISyntaxException {
		setSerializedOutput(true);
		realTestFiles(true, new FileComparator.XmlComparator());
	}
}
