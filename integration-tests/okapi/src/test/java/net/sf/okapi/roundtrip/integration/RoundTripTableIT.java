package net.sf.okapi.roundtrip.integration;

import net.sf.okapi.common.FileLocation;
import net.sf.okapi.common.filters.IFilter;
import net.sf.okapi.common.integration.EventRoundTripIT;
import net.sf.okapi.common.integration.FileComparator;
import net.sf.okapi.filters.table.TableFilter;
import net.sf.okapi.filters.xliff2.XLIFF2Filter;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.File;
import java.io.FileNotFoundException;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;
import java.util.function.Supplier;

@RunWith(JUnit4.class)
public class RoundTripTableIT extends EventRoundTripIT {
	private static final String CONFIG_ID = "okf_table";
	private static final String DIR_NAME = "/table/";
	private static final List<String> EXTENSIONS = Arrays.asList(".csv", ".tab");
	private static final Supplier<IFilter> FILTER_CONSTRUCTOR = TableFilter::new;
	final static FileLocation root = FileLocation.fromClass(RoundTripTableIT.class);

	public RoundTripTableIT() {
		super(CONFIG_ID, DIR_NAME, EXTENSIONS, TableFilter::new);
	}

	@Ignore
	public void debug() {
		setSerializedOutput(true);
		setExtensions(EXTENSIONS);
		final File file = root.in("/table/issue1128/strong.csv").asFile();
		runTest(new TestJob("okf_table@strong", true, file, "", file.getParent(),
				new FileComparator.EventComparator(), FILTER_CONSTRUCTOR));
	}

	@Test
	public void tableFiles() throws FileNotFoundException, URISyntaxException {
		setSerializedOutput(false);
		realTestFiles(false, new FileComparator.EventComparator());
	}

	@Test
	public void tableSerializedFiles() throws FileNotFoundException, URISyntaxException {
		setSerializedOutput(true);
		realTestFiles(false, new FileComparator.EventComparator());
	}
}
