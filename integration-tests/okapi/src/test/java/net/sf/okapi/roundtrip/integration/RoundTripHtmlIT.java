package net.sf.okapi.roundtrip.integration;

import net.sf.okapi.common.FileLocation;
import net.sf.okapi.common.filters.IFilter;
import net.sf.okapi.common.integration.EventRoundTripIT;
import net.sf.okapi.common.integration.FileComparator;
import net.sf.okapi.filters.html.HtmlFilter;
import net.sf.okapi.filters.html.Parameters;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.File;
import java.io.FileNotFoundException;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;
import java.util.function.Supplier;

@RunWith(JUnit4.class)
public class RoundTripHtmlIT extends EventRoundTripIT {
	private static final String CONFIG_ID = "okf_html";
	private static final String DIR_NAME = "/html/";
	private static final List<String> EXTENSIONS = Arrays.asList(".html", ".htm", ".xhtml");
	private static final Supplier<IFilter> FILTER_CONSTRUCTOR = HtmlFilter::new;
	final static FileLocation root = FileLocation.fromClass(RoundTripHtmlIT.class);

	public RoundTripHtmlIT() {
		super(CONFIG_ID, DIR_NAME, EXTENSIONS, FILTER_CONSTRUCTOR);
		addKnownFailingFile("98959751.html");
	}

	@Ignore
	public void debug() throws FileNotFoundException, URISyntaxException {
		final File file = root.in("/html/sanitizer.html").asFile();
		runTest(new TestJob("okf_html", false, file, "", null,
				new FileComparator.EventComparator(), FILTER_CONSTRUCTOR));
	}

	@Test
	public void htmlFiles() throws FileNotFoundException, URISyntaxException {
		setSerializedOutput(false);
		realTestFiles(false, new FileComparator.EventComparator());
	}

	@Test
	public void htmlFilesSerialized() throws FileNotFoundException, URISyntaxException {
		setSerializedOutput(true);
		realTestFiles(CONFIG_ID, false, new FileComparator.EventComparator());
	}
}
