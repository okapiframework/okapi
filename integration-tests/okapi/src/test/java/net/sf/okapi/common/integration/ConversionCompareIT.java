package net.sf.okapi.common.integration;

import net.sf.okapi.common.Event;
import net.sf.okapi.common.FileUtil;
import net.sf.okapi.common.IParameters;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.StreamUtil;
import net.sf.okapi.common.Util;
import net.sf.okapi.common.exceptions.OkapiException;
import net.sf.okapi.common.exceptions.OkapiFilterCreationException;
import net.sf.okapi.common.filters.IFilter;
import net.sf.okapi.common.filterwriter.TMXFilterWriter;
import net.sf.okapi.common.resource.ITextUnit;
import net.sf.okapi.common.resource.RawDocument;
import net.sf.okapi.filters.po.POFilter;
import net.sf.okapi.filters.table.TableFilter;
import net.sf.okapi.filters.tmx.TmxFilter;
import net.sf.okapi.filters.xliff.XLIFFFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Paths;
import java.util.List;
import java.util.function.Supplier;

import static org.junit.Assert.assertTrue;

public class ConversionCompareIT extends EventRoundTripIT {
	public static final String XLIFF_CONFIG = StreamUtil.streamUtf8AsString(
			ConversionCompareIT.class.getClassLoader().getResourceAsStream("bilingual/okf_xliff@tm-import.fprm"));
	public static final String TMX_CONFIG = StreamUtil.streamUtf8AsString(
			ConversionCompareIT.class.getClassLoader().getResourceAsStream("bilingual/okf_tmx@tm-import.fprm"));
	public static final String TABLE_CONFIG = StreamUtil.streamUtf8AsString(
			ConversionCompareIT.class.getClassLoader().getResourceAsStream("bilingual/okf_table@tm-import.fprm"));
	private final Logger logger = LoggerFactory.getLogger(getClass());

	public ConversionCompareIT(String configId, String dirName,
			List<String> extensions, Supplier<IFilter> filterConstructor) {
		super(configId, dirName, extensions, filterConstructor);
	}

	public ConversionCompareIT(String configId, String dirName,
			List<String> extensions, LocaleId defaultTargetLocale, Supplier<IFilter> filterConstructor) {
		super(configId, dirName, extensions, defaultTargetLocale, filterConstructor);
	}

	@Override
	public void runTest(final TestJob testJob) {
		final String f = testJob.file.getName();
		final String root = testJob.file.getParent() + File.separator;
		final String original = root + f;
		final String tmxConverted = root + f + ".converted_tmx";
		final String xliffConverted = root + f + ".converted_xliff";
		LocaleId source = LocaleId.ENGLISH;
		LocaleId target = defaultTargetLocale;
		if (testJob.detectLocales) {
			final List<String> locales = FileUtil.guessLanguages(testJob.file.getAbsolutePath());
			if (locales.size() >= 1) {
				source = LocaleId.fromString(locales.get(0));
			}
			if (locales.size() >= 2) {
				target = LocaleId.fromString(locales.get(1));
			}
		}

		try {
			logger.info(Paths.get(testJob.relativeSubDir, f).toString());
			IFilter bilingualFilter = createBilingualFilter(testJob.configId);
			IFilter tmxFilter = createBilingualFilter("okf_tmx");

			if (bilingualFilter == null || tmxFilter == null) {
				throw new OkapiFilterCreationException(String.format("Cannot create okapi bilingual filter: %s", testJob.configId));
			}

			// convert to tmx and xliff
			convert(original, source, target, tmxConverted, bilingualFilter, testJob.configId);

			// compare TextUnit events with original
			try (RawDocument ord = new RawDocument(Util.toURI(original), "UTF-8", source, target);
					RawDocument trd = new RawDocument(Util.toURI(tmxConverted), "UTF-8", source, target);) {
				final List<ITextUnit> o = IntegrationtestUtils.getTextUnitEvents(bilingualFilter, ord, null);
				final List<ITextUnit> t = IntegrationtestUtils.getTextUnitEvents(tmxFilter, trd, null);
				assertTrue("Compare Events (TMX): " + f, testJob.comparator.compare(t, o));
			}
		} catch (final Throwable e) {
			if (!knownFailingFiles.contains(f)) {
				errCol.addError(new OkapiTestException(f, e));
				logger.error("Failing test: {}\n{}", f, e.getMessage());
			} else {
				logger.info("Ignored known failing file: {}", f);
			}
		}
	}

	/**
	 * Convert a bilingual input file to a corresponding TMX file. Preserve as much
	 * of the original formats metadata as possible (normally as attributes or prop
	 * elements)
	 *
	 * @param original     original bilingual file (i.e., xliff, po, cvs etc..)
	 * @param sourceLocale source locale of bilingualOriginal.
	 * @param targetLocale target locale of bilingualOriginal.
	 */
	public void convert(String original, LocaleId sourceLocale, LocaleId targetLocale, String tmxConverted,
			IFilter bilingualFilter, String configId) throws OkapiTestException, IOException {

		try (TMXFilterWriter tmxWriter = new TMXFilterWriter()) {
			bilingualFilter.open(new RawDocument(Util.toURI(original), StandardCharsets.UTF_8.name(), sourceLocale, targetLocale));

			// tmx writer
			net.sf.okapi.common.filterwriter.Parameters tmxWriterParams = tmxWriter.getParameters();
			tmxWriter.setOptions(targetLocale, StandardCharsets.UTF_8.name());
			tmxWriter.setOutput(tmxConverted);
			tmxWriterParams.setWriteAllPropertiesAsAttributes(true);
			tmxWriterParams.setAutoGenerateTuId(false);
			tmxWriter.setParameters(tmxWriterParams);

			while (bilingualFilter.hasNext()) {
				Event event = bilingualFilter.next();
				tmxWriter.handleEvent(event);
			}
		} catch (OkapiException e) {
			String m = String.format("Error converting file type %s to TMX", configId);
			throw new OkapiTestException(m, e);
		}
	}

	private IFilter createBilingualFilter(String configId) {
		IFilter bilingualFilter = null;
		IParameters p;
		switch (configId) {
		case "okf_xliff":
			bilingualFilter = new XLIFFFilter();
			p = bilingualFilter.getParameters();
			p.fromString(XLIFF_CONFIG);
			break;
		case "okf_po":
			bilingualFilter = new POFilter();
			break;
		case "okf_table":
			bilingualFilter = new TableFilter();
			p = bilingualFilter.getParameters();
			p.fromString(TABLE_CONFIG);
			break;
		case "okf_tmx":
			bilingualFilter = new TmxFilter();
			p = bilingualFilter.getParameters();
			p.fromString(TMX_CONFIG);
			break;
		default:
			break;
		}

		return bilingualFilter;
	}
}
