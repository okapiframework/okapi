package net.sf.okapi.roundtrip.integration;

import net.sf.okapi.common.FileLocation;
import net.sf.okapi.common.filters.IFilter;
import net.sf.okapi.common.integration.EventRoundTripIT;
import net.sf.okapi.common.integration.FileComparator;
import net.sf.okapi.filters.xliff2.XLIFF2Filter;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.File;
import java.io.FileNotFoundException;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.function.Supplier;

@RunWith(JUnit4.class)
public class RoundTripXliff2IT extends EventRoundTripIT {
	private static final String CONFIG_ID = "okf_xliff2";
	private static final String DIR_NAME = "/xliff2/";
	private static final List<String> EXTENSIONS = Arrays.asList(".xliff", ".xlf", "xlf2");
	private static final Supplier<IFilter> FILTER_CONSTRUCTOR = XLIFF2Filter::new;

	final static FileLocation root = FileLocation.fromClass(RoundTripXliff2IT.class);

	public RoundTripXliff2IT() {
		super(true, CONFIG_ID, DIR_NAME, EXTENSIONS, FILTER_CONSTRUCTOR);
	}

	@Test
	public void debug() throws FileNotFoundException, URISyntaxException {
		setSerializedOutput(true);
		final File file = root.in("/xliff2/code_id_mismatch.xlf").asFile();
		runTest(new TestJob(CONFIG_ID, true, file, "", null,
				new FileComparator.EventComparatorWithWhitespace(), FILTER_CONSTRUCTOR));
	}

	@Test
	public void debug2() {
		setSerializedOutput(true);
		setExtensions(Collections.singletonList(".deepen_xlf"));
		final File file = root.in("/xliff2/deepenSegmentation/paragraphs-source-only.deepen_xlf").asFile();
		runTest(new TestJob("okf_xliff2@deepen-segmentation", true, file, "", file.getParent(),
				new FileComparator.EventComparatorIgnoreSegmentation(), FILTER_CONSTRUCTOR));
	}

	@Ignore
	public void debug3() throws FileNotFoundException, URISyntaxException {
		setSerializedOutput(false);
		final File file = root.in("/xliff2/test3.xlf").asFile();
		runTest(new TestJob(CONFIG_ID, true, file, "", null, new FileComparator.EventComparatorWithWhitespace(), FILTER_CONSTRUCTOR));
	}


	@Test
	public void debug4() throws FileNotFoundException, URISyntaxException {
		setSerializedOutput(true);
		final File file = root.in("/xliff2/subfilter_json/subfilter_json.xlf").asFile();
		runTest(new TestJob("okf_xliff2@json.fprm", true, file, "", file.getParent(), new FileComparator.EventComparatorWithWhitespace(), FILTER_CONSTRUCTOR));
	}

	@Test
	public void deepenXliff2() throws FileNotFoundException, URISyntaxException {
		setExtensions(Collections.singletonList(".deepen_xlf"));
		// these tests only fully work with serialized output
		setSerializedOutput(true);
		realTestFiles(true, new FileComparator.EventComparatorIgnoreSegmentation());
	}

	@Test
	public void xliff2Files() throws FileNotFoundException, URISyntaxException {		
		setSerializedOutput(false);
		realTestFiles(true, new FileComparator.EventComparatorWithWhitespace());
	}

	@Test
	public void xliff2SerializedFiles() throws FileNotFoundException, URISyntaxException {
		setSerializedOutput(true);
		realTestFiles(true, new FileComparator.EventComparatorWithWhitespace());
	}
}
