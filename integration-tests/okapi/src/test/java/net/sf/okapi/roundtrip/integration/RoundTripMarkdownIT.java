package net.sf.okapi.roundtrip.integration;

import net.sf.okapi.common.FileLocation;
import net.sf.okapi.common.filters.IFilter;
import net.sf.okapi.common.integration.EventRoundTripIT;
import net.sf.okapi.common.integration.FileComparator;
import net.sf.okapi.filters.markdown.MarkdownFilter;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.File;
import java.io.FileNotFoundException;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;
import java.util.function.Supplier;

@RunWith(JUnit4.class)
public class RoundTripMarkdownIT extends EventRoundTripIT {
	private static final String CONFIG_ID = "okf_markdown";
	private static final String DIR_NAME = "/markdown/";
	private static final List<String> EXTENSIONS = Arrays.asList(".md");
	private static final Supplier<IFilter> FILTER_CONSTRUCTOR = MarkdownFilter::new;	
	final static FileLocation root = FileLocation.fromClass(RoundTripMarkdownIT.class);

	public RoundTripMarkdownIT() {
		super(CONFIG_ID, DIR_NAME, EXTENSIONS, FILTER_CONSTRUCTOR);
		// FIXME: These fail with Windows (possibly MAC) newlines
		// MarkdownFilter has problems when handling non-Linux newlines
		addKnownFailingFile("test-html-block-newline.md");
		addKnownFailingFile("html_list_original.md");
		addKnownFailingFile("html_table_changed.md");
		addKnownFailingFile("admonitions.md");
		addKnownFailingFile("html_list_changed.md");
		addKnownFailingFile("html-table-w-empty-lines.md");
		addKnownFailingFile("html_table1_original.md");
	}

	@Test
	public void debug() throws FileNotFoundException, URISyntaxException {
		setSerializedOutput(true);
		final File file = root.in("/markdown/html_table_changed.md").asFile();
		runTest(new TestJob(CONFIG_ID, false, file, "", null,
				new FileComparator.EventComparator(), FILTER_CONSTRUCTOR));
	}

	@Test
	public void markdownFiles() throws FileNotFoundException, URISyntaxException {
		setSerializedOutput(false);
		realTestFiles(false, new FileComparator.EventComparator());
	}

	@Test
	public void markdownSerializedFiles() throws FileNotFoundException, URISyntaxException {
		setSerializedOutput(true);
		realTestFiles(false, new FileComparator.EventComparator());
	}
}
