package net.sf.okapi.common.integration;

import net.sf.okapi.common.*;
import net.sf.okapi.common.exceptions.OkapiException;
import net.sf.okapi.common.filters.*;
import net.sf.okapi.common.filterwriter.XLIFFWriter;
import net.sf.okapi.common.filterwriter.XLIFFWriterParameters;
import net.sf.okapi.common.pipeline.IPipelineStep;
import net.sf.okapi.common.resource.ITextUnit;
import net.sf.okapi.common.resource.RawDocument;
import net.sf.okapi.filters.archive.ArchiveFilter;
import net.sf.okapi.filters.doxygen.DoxygenFilter;
import net.sf.okapi.filters.dtd.DTDFilter;
import net.sf.okapi.filters.html.HtmlFilter;
import net.sf.okapi.filters.icml.ICMLFilter;
import net.sf.okapi.filters.idml.IDMLFilter;
import net.sf.okapi.filters.its.html5.HTML5Filter;
import net.sf.okapi.filters.json.JSONFilter;
import net.sf.okapi.filters.markdown.MarkdownFilter;
import net.sf.okapi.filters.messageformat.MessageFormatFilter;
import net.sf.okapi.filters.mif.MIFFilter;
import net.sf.okapi.filters.openoffice.ODFFilter;
import net.sf.okapi.filters.openoffice.OpenOfficeFilter;
import net.sf.okapi.filters.openxml.OpenXMLFilter;
import net.sf.okapi.filters.plaintext.PlainTextFilter;
import net.sf.okapi.filters.po.POFilter;
import net.sf.okapi.filters.properties.PropertiesFilter;
import net.sf.okapi.filters.regex.RegexFilter;
import net.sf.okapi.filters.table.TableFilter;
import net.sf.okapi.filters.tex.TEXFilter;
import net.sf.okapi.filters.tmx.TmxFilter;
import net.sf.okapi.filters.transtable.TransTableFilter;
import net.sf.okapi.filters.ts.TsFilter;
import net.sf.okapi.filters.ttml.TTMLFilter;
import net.sf.okapi.filters.ttx.TTXFilter;
import net.sf.okapi.filters.txml.TXMLFilter;
import net.sf.okapi.filters.vtt.VTTFilter;
import net.sf.okapi.filters.wiki.WikiFilter;
import net.sf.okapi.filters.xini.XINIFilter;
import net.sf.okapi.filters.xliff.XLIFFFilter;
import net.sf.okapi.filters.xliff2.XLIFF2Filter;
import net.sf.okapi.filters.xml.XMLFilter;
import net.sf.okapi.filters.xmlstream.XmlStreamFilter;
import net.sf.okapi.filters.yaml.YamlFilter;
import net.sf.okapi.lib.merge.step.OriginalDocumentXliffMergerStep;
import net.sf.okapi.lib.segmentation.SRXSegmenter;
import net.sf.okapi.lib.serialization.filter.ProtoFilter;
import net.sf.okapi.lib.serialization.step.OriginalDocumentProtoMergerStep;
import net.sf.okapi.lib.serialization.writer.ProtoBufferWriter;
import net.sf.okapi.steps.common.FilterEventsWriterStep;
import net.sf.okapi.steps.common.codesimplifier.PostSegmentationCodeSimplifierStep;
import net.sf.okapi.steps.segmentation.Parameters;
import net.sf.okapi.steps.segmentation.SegmentationStep;
import net.sf.okapi.steps.whitespacecorrection.WhitespaceCorrectionStep;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Stream;

public final class RoundTripUtils {
    private static final String sourceSrx =
            Objects.requireNonNull(StreamUtil.streamUtf8AsString(SRXSegmenter.class.getClassLoader().getResourceAsStream("net/sf/okapi/lib/segmentation/defaultSegmentation.srx")));
    private static final String targetSrx =
            Objects.requireNonNull(StreamUtil.streamUtf8AsString(SRXSegmenter.class.getClassLoader().getResourceAsStream("net/sf/okapi/lib/segmentation/defaultSegmentation.srx")));
    private static final IFilterConfigurationMapper mapper;

    private static Map<String, FilterConfiguration> loadDefaultConfigs() {
        var builder = new ThreadSafeFilterConfigurationMapper.ConfigBuilder();
        builder.addConfigurations(OpenXMLFilter.class);
        builder.addConfigurations(HtmlFilter.class);
        builder.addConfigurations(HTML5Filter.class);
        builder.addConfigurations(JSONFilter.class);
        builder.addConfigurations(XmlStreamFilter.class);
        builder.addConfigurations(PlainTextFilter.class);
        builder.addConfigurations(DTDFilter.class);
        builder.addConfigurations(XLIFFFilter.class);
        builder.addConfigurations(OpenOfficeFilter.class);
        builder.addConfigurations(ODFFilter.class);
        builder.addConfigurations(PropertiesFilter.class);
        builder.addConfigurations(POFilter.class);
        builder.addConfigurations(RegexFilter.class);
        builder.addConfigurations(TsFilter.class);
        builder.addConfigurations(TableFilter.class);
        builder.addConfigurations(XMLFilter.class);
        builder.addConfigurations(IDMLFilter.class);
        builder.addConfigurations(ArchiveFilter.class);
        builder.addConfigurations(YamlFilter.class);
        builder.addConfigurations(MIFFilter.class);
        builder.addConfigurations(TXMLFilter.class);
        builder.addConfigurations(DoxygenFilter.class);
        builder.addConfigurations(WikiFilter.class);
        builder.addConfigurations(ICMLFilter.class);
        builder.addConfigurations(XLIFF2Filter.class);
        builder.addConfigurations(MarkdownFilter.class);
        builder.addConfigurations(TEXFilter.class);
        builder.addConfigurations(TransTableFilter.class);
        builder.addConfigurations(TmxFilter.class);
        builder.addConfigurations(XINIFilter.class);
        builder.addConfigurations(TTXFilter.class);
        builder.addConfigurations(MessageFormatFilter.class);
        builder.addConfigurations(VTTFilter.class);
        builder.addConfigurations(TTMLFilter.class);

        String p = StreamUtil.streamUtf8AsString(Objects.requireNonNull(RoundTripUtils.class.getResourceAsStream(
                "/messageformat/YAML/expand/okf_messageformat@expand.secondary.fprm")));
        builder.addCustomConfiguration("okf_messageformat@expand.secondary", p);
        return builder.build();
    }

    static {
        mapper = new ThreadSafeFilterConfigurationMapper(RoundTripUtils::loadDefaultConfigs);
    }

    public static IFilterConfigurationMapper extract(final LocaleId source, final LocaleId target,
                                                     final String originalPath, final String outputPath,
                                                     final String configId, final String customConfigPath,
                                                     final boolean serializedOutput, final boolean simplify) throws URISyntaxException, FileNotFoundException {
        final List<IPipelineStep> steps = new LinkedList<>();
        addCustomConfigs(customConfigPath, configId);
        try (RawDocument originalDoc = new RawDocument(Util.toURI(originalPath), StandardCharsets.UTF_8.name(),
                source, target); IFilter filter = mapper.createFilter(configId)) {
            steps.addAll(createInitialPipeline(target, configId, simplify));
            if (serializedOutput) {
                try (ProtoBufferWriter writer = new ProtoBufferWriter()) {
                    // Filter events to raw document final step (using the defined writer)
                    final FilterEventsWriterStep fewStep = new FilterEventsWriterStep();
                    fewStep.setFilterWriter(writer);
                    final net.sf.okapi.lib.serialization.filter.Parameters params = writer.getParameters();
                    params.setCopySource(true);
                    fewStep.setDocumentRoots(Util.getDirectoryName(originalPath));
                    fewStep.setOutputURI(new File(outputPath).toURI());
                    fewStep.setOutputEncoding(StandardCharsets.UTF_8.name());
                    fewStep.setFilterConfigurationMapper(mapper);
                    fewStep.setLastOutputStep(true);
                    steps.add(fewStep);
                }
            } else {
                try (XLIFFWriter writer = new XLIFFWriter()) {
                    // Filter events to raw document final step (using the defined writer)
                    final FilterEventsWriterStep fewStep = new FilterEventsWriterStep();
                    fewStep.setFilterWriter(writer);
                    final XLIFFWriterParameters xliffParams = writer.getParameters();
                    // set non-default values
                    xliffParams.setPlaceholderMode(false);
                    xliffParams.setEscapeGt(true);
                    xliffParams.setIncludeCodeAttrs(true);
                    xliffParams.setCopySource(true);
                    xliffParams.setToolId("okapi");
                    xliffParams.setToolName("okapi-tests");
                    xliffParams.setToolCompany("okapi");
                    xliffParams.setToolVersion("M29");

                    fewStep.setOutputURI(new File(outputPath).toURI());
                    fewStep.setOutputEncoding(StandardCharsets.UTF_8.name());
                    fewStep.setDocumentRoots(Util.getDirectoryName(originalPath));
                    fewStep.setFilterConfigurationMapper(mapper);
                    fewStep.setLastOutputStep(true);
                    steps.add(fewStep);
                }
            }

            // do general config and initialization
            for (IPipelineStep s : steps) {
                s.setSourceLocale(source);
                s.setTargetLocale(target);
                s.handleEvent(Event.createStartBatchEvent());
                s.handleEvent(Event.createStartBatchItemEvent());
            }

            final List<Function<Stream<Event>, Stream<Event>>> stepsAsFunctions = new ArrayList<>(steps);
            try (filter) {
                filter.open(originalDoc);
                filter.stream()
                        .flatMap(e -> IPipelineStep.runPipelineSteps(stepsAsFunctions, Stream.of(e)))
                        .count();
            } finally {
                steps.forEach(IPipelineStep::close);
            }
        } catch (Exception e) {
            throw new OkapiException(e);
        } finally {
            steps.forEach(IPipelineStep::close);
        }

        return mapper;
    }

    private static void addCustomConfigs(final String customConfigPath, final String configId) throws FileNotFoundException {
        if (customConfigPath != null) {
            var cp = new File(customConfigPath);
            var primaryConfigs = IntegrationtestUtils.getConfigFiles(cp);
            // normally should be only one by add all of them
            for (File c : primaryConfigs) {
                mapper.addCustomConfiguration(configId, StreamUtil.streamUtf8AsString(new FileInputStream(c)));
            }

            // secondary config
            var secondaryConfig = IntegrationtestUtils.getSecondaryConfigFile(cp, configId);
            if (secondaryConfig != null) {
                mapper.addCustomConfiguration(Util.getFilename(secondaryConfig.getName(), false),
                        StreamUtil.streamUtf8AsString(new FileInputStream(secondaryConfig)));
            }
        }
    }

    private static List<IPipelineStep> createInitialPipeline(final LocaleId target, final String configId,
                                                             final boolean simplify) {

        var pipeline = new LinkedList<IPipelineStep>();
        final SegmentationStep ss = new SegmentationStep();
        final List<LocaleId> tl = new LinkedList<>();
        tl.add(target);
        ss.setTargetLocales(tl);
        final Parameters params = ss.getParameters();
        params.setSegmentSource(true);
        params.setSegmentTarget(true);
        params.setDoNotSegmentIfHasTarget(true);
        params.setSourceSrx(sourceSrx);
        params.setTargetSrx(targetSrx);
        ss.setParameters(params);
        pipeline.add(ss);

        if (simplify) {
            net.sf.okapi.steps.common.codesimplifier.Parameters p =
                    new net.sf.okapi.steps.common.codesimplifier.Parameters();
            ISimplifierRulesParameters fp = (ISimplifierRulesParameters) mapper.getConfiguration(configId).parameters;
            if (fp != null && null != fp.getSimplifierRules()) {
                p.setRules(fp.getSimplifierRules());
            }

            PostSegmentationCodeSimplifierStep simplifier = new PostSegmentationCodeSimplifierStep();
            simplifier.setParameters(p);
            pipeline.add(simplifier);
        }

        return pipeline;
    }

    public static void merge(final LocaleId source, final LocaleId target, final String originalPath,
                             final String extractedPath, final String outputPath, final String configId,
                             final boolean serializedOutput) throws URISyntaxException {
        try (RawDocument originalDoc = new RawDocument(Util.toURI(originalPath), StandardCharsets.UTF_8.name(),
                source, target); RawDocument extractedDoc = new RawDocument(Util.toURI(extractedPath),
                StandardCharsets.UTF_8.name(), source, target)) {
            originalDoc.setFilterConfigId(configId);

            final List<IPipelineStep> steps = new LinkedList<>();
            steps.add(new WhitespaceCorrectionStep());
            if (serializedOutput) {
                try (IFilter f = new ProtoFilter()) {
                    OriginalDocumentProtoMergerStep originalDocumentProtoMergerStep = new OriginalDocumentProtoMergerStep();
                    originalDocumentProtoMergerStep.getSkelMergerWriter().setOutput(outputPath);
                    originalDocumentProtoMergerStep.setFilterConfigurationMapper(mapper);
                    originalDocumentProtoMergerStep.setOutputEncoding(StandardCharsets.UTF_8.name());
                    originalDocumentProtoMergerStep.setSecondInput(originalDoc);
                    net.sf.okapi.lib.serialization.step.Parameters params = new net.sf.okapi.lib.serialization.step.Parameters();
                    params.setPreserveWhiteSpaceByDefault(true);
                    steps.add(originalDocumentProtoMergerStep);
                    f.open(extractedDoc);
                    mergePipeline(f, steps, source, target);
                }
            } else {
                try (IFilter f = new XLIFFFilter()) {
                    // Make sure that whitespace is preserved
                    OriginalDocumentXliffMergerStep originalDocumentXliffMergerStep = new OriginalDocumentXliffMergerStep();
                    net.sf.okapi.lib.merge.step.Parameters parameters = new net.sf.okapi.lib.merge.step.Parameters();
                    parameters.setPreserveWhitespace(true);
                    originalDocumentXliffMergerStep.getSkelMergerWriter().setOutput(outputPath);
                    originalDocumentXliffMergerStep.setParameters(parameters);
                    originalDocumentXliffMergerStep.setFilterConfigurationMapper(mapper);
                    originalDocumentXliffMergerStep.setOutputEncoding(StandardCharsets.UTF_8.name());
                    originalDocumentXliffMergerStep.setSecondInput(originalDoc);
                    steps.add(originalDocumentXliffMergerStep);
                    net.sf.okapi.filters.xliff.Parameters xliffParams = new net.sf.okapi.filters.xliff.Parameters();
                    xliffParams.setPreserveSpaceByDefault(true);
                    f.setParameters(xliffParams);
                    f.open(extractedDoc);
                    mergePipeline(f, steps, source,target);
                }
            }
        }
    }

    private static void mergePipeline(IFilter filter, List<IPipelineStep> steps, LocaleId source, LocaleId target) {
        // do general last config and initialization
        for (IPipelineStep s : steps) {
            s.setSourceLocale(source);
            s.setTargetLocale(target);
            s.handleEvent(Event.createStartBatchEvent());
            s.handleEvent(Event.createStartBatchItemEvent());
        }

        final List<Function<Stream<Event>, Stream<Event>>> stepsAsFunctions = new ArrayList<>(steps);
        try (filter) {
            long c = filter.stream()
                    .flatMap(e -> IPipelineStep.runPipelineSteps(stepsAsFunctions, Stream.of(e)))
                    .count();
        } finally {
            steps.forEach(IPipelineStep::close);
        }
    }

    /**
     * Assumes merge has been called to set source and target locales
     */
    public static boolean compareEvents(final List<Event> actual, final List<Event> expected,
                                        final boolean includeSkeleton, final boolean ignoreSkelWhitespace,
                                        final boolean ignoreFragmentWhitespace, boolean ignoreSegmentation) {
        return FilterTestDriver.compareEvents(actual, expected, includeSkeleton, ignoreSkelWhitespace,
                ignoreFragmentWhitespace, ignoreSegmentation);
    }

    /**
     * Assumes merge has been called to set source and target locales
     */
    public static boolean compareTextUnits(final List<ITextUnit> actual, final List<ITextUnit> expected,
                                           final boolean ignoreFragmentWhitespace) {
        return FilterTestDriver.compareTextUnits(actual, expected, ignoreFragmentWhitespace);
    }
}
