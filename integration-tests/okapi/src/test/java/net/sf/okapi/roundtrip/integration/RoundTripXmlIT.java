package net.sf.okapi.roundtrip.integration;

import net.sf.okapi.common.filters.IFilter;
import net.sf.okapi.common.integration.FileComparator;
import net.sf.okapi.common.integration.IComparator;
import net.sf.okapi.common.integration.IntegrationtestUtils;
import net.sf.okapi.common.integration.XmlOrTextRoundTripIT;
import net.sf.okapi.filters.xml.XMLFilter;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.FileNotFoundException;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;
import java.util.function.Supplier;

@RunWith(JUnit4.class)
public class RoundTripXmlIT extends XmlOrTextRoundTripIT {
	private static final String CONFIG_ID = "okf_xml";
	private static final String DIR_NAME = "/xml/";
	private static final List<String> EXTENSIONS = Arrays.asList(".xml", ".htm");
	private static final IComparator<?> FILE_COMPARATOR = new FileComparator.XmlComparator<>();
	private static final Supplier<IFilter> FILTER_CONSTRUCTOR = XMLFilter::new;

	public RoundTripXmlIT() {
		super(CONFIG_ID, DIR_NAME, EXTENSIONS, FILTER_CONSTRUCTOR);
	}

	@Test
	public void xmlFiles() throws FileNotFoundException, URISyntaxException {
		setSerializedOutput(false);
		realTestFiles(false, RoundTripXmlIT.FILE_COMPARATOR);
	}

	@Test
	public void xmlSerializedFiles() throws FileNotFoundException, URISyntaxException {
		setSerializedOutput(true);
		realTestFiles(false, RoundTripXmlIT.FILE_COMPARATOR);
	}

	@Test
	public void issue591Resolved() {
		run2Tests(
			IntegrationtestUtils.ROOT.in("/xml/custom-configs/591").asPath(),
			"okf_xml@ibxlf1",
			"simple_with_simple_codes.xml"
		);
	}

	@Test
	public void issue1384Resolved() {
		run2Tests(
			IntegrationtestUtils.ROOT.in("/xml/custom-configs/1384").asPath(),
			"okf_xml@translatable-and-untranslatable",
			"translatable-and-untranslatable.xml"
		);
	}

	private void run2Tests(final Path rootDir, final String configId, final String documentName) {
		final boolean detectLocales = false;
		final String relativeSubDir = IntegrationtestUtils.ROOT.in(DIR_NAME).asPath().relativize(rootDir).toString();
		setSerializedOutput(false);
		runTest(new TestJob(
			configId,
			detectLocales,
			rootDir.resolve(documentName).toFile(),
			relativeSubDir,
			rootDir.toString(),
			RoundTripXmlIT.FILE_COMPARATOR,
			RoundTripXmlIT.FILTER_CONSTRUCTOR
		));
		setSerializedOutput(true);
		runTest(new TestJob(
			configId,
			detectLocales,
			rootDir.resolve(documentName).toFile(),
			relativeSubDir,
			rootDir.toString(),
			RoundTripXmlIT.FILE_COMPARATOR,
			RoundTripXmlIT.FILTER_CONSTRUCTOR
		));
	}
}
