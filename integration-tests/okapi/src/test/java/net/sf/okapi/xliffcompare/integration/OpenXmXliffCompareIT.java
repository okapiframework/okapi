package net.sf.okapi.xliffcompare.integration;

import net.sf.okapi.common.integration.XliffCompareIT;
import net.sf.okapi.common.integration.FileComparator;
import net.sf.okapi.filters.openxml.OpenXMLFilter;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.FileNotFoundException;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;

@RunWith(JUnit4.class)
public class OpenXmXliffCompareIT extends XliffCompareIT {
	private static final String CONFIG_ID = "okf_openxml";
	private static final String DIR_NAME = "/openxml/";
	private static final List<String> EXTENSIONS = Arrays.asList(".docx", ".pptx", ".xlsx");

	public OpenXmXliffCompareIT() {
		super(CONFIG_ID, DIR_NAME, EXTENSIONS, OpenXMLFilter::new);
	}

	@Test
	public void openXmlXliffCompareFiles() throws FileNotFoundException, URISyntaxException {
		realTestFiles(true, new FileComparator.XmlComparator());
	}
}
