#!/usr/bin/env bash

# ===== Helper functions =====

function showSectionTitle() {
  echo
  echo =============================================
  echo $*
  echo =============================================
  echo
}

function showHelp () {
  showSectionTitle Help
  echo Unknown option \"$*\"
  echo
  echo Usage: build one_or_more_options
  echo
  echo Options for mvn install:
  echo " " js : -P with_javadoc,with_sources with mvn install
  echo " " nt : -DskipITs -DskipTests with mvn install
  echo " " mt : -T2C with mvn install
  echo Build options:
  echo " " bo : Build okapi only, then exit
  echo " " ba : Build okapi and applications, then exit
  echo " " ai : Build okapi, applications, integration test applications, then exit
  echo " " fi : Build okapi, applications, full integration test, then exit
  echo
  echo The mvn install options \"accumulate\"
  echo If more than one build options is specified \(bo, ba, ai, fi\), the last one wins
  echo
  echo Example: build mt js ba
  echo   Will build Okapi proper \(mvn install\) with -T2C -P with_javadoc,with_sources
  echo   Will build the applications
  echo   And will end
  echo
}

function setStepParameters() {
  export OKAPI_BUILD_OKAPI=$1
  export OKAPI_BUILD_APPS=$2
  export OKAPI_IT_APPS=$3
  export OKAPI_IT_ALL=$4
}

function defaultParameters() {
  export OKAPI_BUILD_TYPE=
  export OKAPI_BUILD_PARALLEL=
  setStepParameters true true true false
}

function parseArgument() {
  if [ $1 == "js" ]; then
    export OKAPI_BUILD_TYPE="$OKAPI_BUILD_TYPE -P with_javadoc,with_sources"
  elif [ $1 == "nt" ]; then
    export OKAPI_BUILD_TYPE="$OKAPI_BUILD_TYPE -DskipITs -DskipTests"
  elif [ $1 == "mt" ]; then
    export OKAPI_BUILD_PARALLEL=-T2C
  elif [ $1 == "11" ]; then
    export MAVEN_OPTS="$MAVEN_OPTS -Dmaven.compiler.source=11 -Dmaven.compiler.target=11"
  elif [ $1 == "17" ]; then
    export MAVEN_OPTS="$MAVEN_OPTS -Dmaven.compiler.source=17 -Dmaven.compiler.target=17"
  elif [ $1 == "bo" ]; then
    setStepParameters true false false false
  elif [ $1 == "ba" ]; then
    setStepParameters true true false false
  elif [ $1 == "ai" ]; then
    setStepParameters true true true false
  elif [ $1 == "fi" ]; then
    setStepParameters true true false true
  else
    showHelp $*
    exit
  fi
}

function error() {
  showSectionTitle ERROR!
  exit 1
}

# ===== Main script =====

[ ! -d "./superpom" ] && cd ..
[ ! -d "./superpom" ] && cd ..

defaultParameters
for x in $@; do parseArgument $x; done

if [ $OKAPI_BUILD_OKAPI == true ]; then
  showSectionTitle Build Okapi
  # The javacc maven plugin does not work well when multithreaded.
  # This is a workaround, we generate sources first, then build parallel.
  mvn clean generate-sources compile $MAVEN_CLI_OPTS $OKAPI_BUILD_TYPE
  if [ $? -ne 0 ]; then error ; fi
  mvn install $OKAPI_BUILD_PARALLEL $MAVEN_CLI_OPTS $OKAPI_BUILD_TYPE
  if [ $? -ne 0 ]; then error ; fi
fi

if [ $OKAPI_BUILD_APPS == true ]; then
  showSectionTitle Build Applications
  mvn dependency:resolve -f okapi-ui/swt/core-ui/pom.xml -PWIN_64_SWT -PCOCOA_64_SWT -PCOCOA_AARCH64_SWT -PLinux_x86_64_swt
  if [ $? -ne 0 ]; then error ; fi
  mvn initialize -f okapi-ui/swt/dependencies-ui/pom.xml -PFreeBSD_x86_64_swt
  if [ $? -ne 0 ]; then error ; fi
  ant clean -f deployment/maven
  if [ $? -ne 0 ]; then error ; fi
  ant -f deployment/maven
  if [ $? -ne 0 ]; then error ; fi
fi

if [ $OKAPI_IT_APPS == true ]; then
  showSectionTitle Integration tests, applications only
  mvn clean verify $MAVEN_CLI_OPTS -f integration-tests/applications -P integration
  if [ $? -ne 0 ]; then error ; fi
fi

if [ $OKAPI_IT_ALL == true ]; then
  showSectionTitle Integration tests, everything
  mvn clean verify $MAVEN_CLI_OPTS -f integration-tests -P integration
  if [ $? -ne 0 ]; then error ; fi
fi

showSectionTitle SUCCESS!
