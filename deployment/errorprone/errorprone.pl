#!/usr/bin/perl -w

use Cwd 'abs_path';

my $HTML = 1;
my %results;
my %counts;

my %errorLevels = (
    'AlwaysThrows' => ERROR,
    'AndroidInjectionBeforeSuper' => ERROR,
    'ArrayEquals' => ERROR,
    'ArrayFillIncompatibleType' => ERROR,
    'ArrayHashCode' => ERROR,
    'ArrayToString' => ERROR,
    'ArraysAsListPrimitiveArray' => ERROR,
    'AsyncCallableReturnsNull' => ERROR,
    'AsyncFunctionReturnsNull' => ERROR,
    'AutoValueBuilderDefaultsInConstructor' => ERROR,
    'AutoValueConstructorOrderChecker' => ERROR,
    'BadAnnotationImplementation' => ERROR,
    'BadShiftAmount' => ERROR,
    'BanJNDI' => ERROR,
    'BoxedPrimitiveEquality' => ERROR,
    'BundleDeserializationCast' => ERROR,
    'ChainingConstructorIgnoresParameter' => ERROR,
    'CheckNotNullMultipleTimes' => ERROR,
    'CheckReturnValue' => ERROR,
    'CollectionIncompatibleType' => ERROR,
    'CollectionToArraySafeParameter' => ERROR,
    'ComparableType' => ERROR,
    'ComparingThisWithNull' => ERROR,
    'ComparisonOutOfRange' => ERROR,
    'CompatibleWithAnnotationMisuse' => ERROR,
    'CompileTimeConstant' => ERROR,
    'ComputeIfAbsentAmbiguousReference' => ERROR,
    'ConditionalExpressionNumericPromotion' => ERROR,
    'ConstantOverflow' => ERROR,
    'DaggerProvidesNull' => ERROR,
    'DangerousLiteralNull' => ERROR,
    'DeadException' => ERROR,
    'DeadThread' => ERROR,
    'DereferenceWithNullBranch' => ERROR,
    'DiscardedPostfixExpression' => ERROR,
    'DoNotCall' => ERROR,
    'DoNotMock' => ERROR,
    'DoubleBraceInitialization' => ERROR,
    'DuplicateMapKeys' => ERROR,
    'DurationFrom' => ERROR,
    'DurationGetTemporalUnit' => ERROR,
    'DurationTemporalUnit' => ERROR,
    'DurationToLongTimeUnit' => ERROR,
    'EqualsHashCode' => ERROR,
    'EqualsNaN' => ERROR,
    'EqualsNull' => ERROR,
    'EqualsReference' => ERROR,
    'EqualsWrongThing' => ERROR,
    'FloggerFormatString' => ERROR,
    'FloggerLogString' => ERROR,
    'FloggerLogVarargs' => ERROR,
    'FloggerSplitLogStatement' => ERROR,
    'ForOverride' => ERROR,
    'FormatString' => ERROR,
    'FormatStringAnnotation' => ERROR,
    'FromTemporalAccessor' => ERROR,
    'FunctionalInterfaceMethodChanged' => ERROR,
    'FuturesGetCheckedIllegalExceptionType' => ERROR,
    'FuzzyEqualsShouldNotBeUsedInEqualsMethod' => ERROR,
    'GetClassOnAnnotation' => ERROR,
    'GetClassOnClass' => ERROR,
    'GuardedBy' => ERROR,
    'GuiceAssistedInjectScoping' => ERROR,
    'GuiceAssistedParameters' => ERROR,
    'GuiceInjectOnFinalField' => ERROR,
    'HashtableContains' => ERROR,
    'IdentityBinaryExpression' => ERROR,
    'IdentityHashMapBoxing' => ERROR,
    'Immutable' => ERROR,
    'ImpossibleNullComparison' => ERROR,
    'Incomparable' => ERROR,
    'IncompatibleArgumentType' => ERROR,
    'IncompatibleModifiers' => ERROR,
    'IndexOfChar' => ERROR,
    'InexactVarargsConditional' => ERROR,
    'InfiniteRecursion' => ERROR,
    'InjectMoreThanOneScopeAnnotationOnClass' => ERROR,
    'InjectOnMemberAndConstructor' => ERROR,
    'InlineMeValidator' => ERROR,
    'InstantTemporalUnit' => ERROR,
    'InvalidJavaTimeConstant' => ERROR,
    'InvalidPatternSyntax' => ERROR,
    'InvalidTimeZoneID' => ERROR,
    'InvalidZoneId' => ERROR,
    'IsInstanceIncompatibleType' => ERROR,
    'IsInstanceOfClass' => ERROR,
    'IsLoggableTagLength' => ERROR,
    'JUnit3TestNotRun' => ERROR,
    'JUnit4ClassAnnotationNonStatic' => ERROR,
    'JUnit4SetUpNotRun' => ERROR,
    'JUnit4TearDownNotRun' => ERROR,
    'JUnit4TestNotRun' => ERROR,
    'JUnit4TestsNotRunWithinEnclosed' => ERROR,
    'JUnitAssertSameCheck' => ERROR,
    'JUnitParameterMethodNotFound' => ERROR,
    'JavaxInjectOnAbstractMethod' => ERROR,
    'JodaToSelf' => ERROR,
    'LenientFormatStringValidation' => ERROR,
    'LiteByteStringUtf8' => ERROR,
    'LocalDateTemporalAmount' => ERROR,
    'LockOnBoxedPrimitive' => ERROR,
    'LoopConditionChecker' => ERROR,
    'LossyPrimitiveCompare' => ERROR,
    'MathRoundIntLong' => ERROR,
    'MislabeledAndroidString' => ERROR,
    'MisplacedScopeAnnotations' => ERROR,
    'MissingSuperCall' => ERROR,
    'MissingTestCall' => ERROR,
    'MisusedDayOfYear' => ERROR,
    'MisusedWeekYear' => ERROR,
    'MixedDescriptors' => ERROR,
    'MockitoUsage' => ERROR,
    'ModifyingCollectionWithItself' => ERROR,
    'MoreThanOneInjectableConstructor' => ERROR,
    'MustBeClosedChecker' => ERROR,
    'NCopiesOfChar' => ERROR,
    'NoCanIgnoreReturnValueOnClasses' => ERROR,
    'NonCanonicalStaticImport' => ERROR,
    'NonFinalCompileTimeConstant' => ERROR,
    'NonRuntimeAnnotation' => ERROR,
    'NullArgumentForNonNullParameter' => ERROR,
    'NullTernary' => ERROR,
    'NullableOnContainingClass' => ERROR,
    'OptionalEquality' => ERROR,
    'OptionalMapUnusedValue' => ERROR,
    'OptionalOfRedundantMethod' => ERROR,
    'OverlappingQualifierAndScopeAnnotation' => ERROR,
    'OverridesJavaxInjectableMethod' => ERROR,
    'PackageInfo' => ERROR,
    'ParametersButNotParameterized' => ERROR,
    'ParcelableCreator' => ERROR,
    'PeriodFrom' => ERROR,
    'PeriodGetTemporalUnit' => ERROR,
    'PeriodTimeMath' => ERROR,
    'PreconditionsInvalidPlaceholder' => ERROR,
    'PrivateSecurityContractProtoAccess' => ERROR,
    'ProtoBuilderReturnValueIgnored' => ERROR,
    'ProtoStringFieldReferenceEquality' => ERROR,
    'ProtoTruthMixedDescriptors' => ERROR,
    'ProtocolBufferOrdinal' => ERROR,
    'ProvidesMethodOutsideOfModule' => ERROR,
    'RandomCast' => ERROR,
    'RandomModInteger' => ERROR,
    'RectIntersectReturnValueIgnored' => ERROR,
    'RequiredModifiers' => ERROR,
    'RestrictedApi' => ERROR,
    'ReturnValueIgnored' => ERROR,
    'SelfAssignment' => ERROR,
    'SelfComparison' => ERROR,
    'SelfEquals' => ERROR,
    'ShouldHaveEvenArgs' => ERROR,
    'SizeGreaterThanOrEqualsZero' => ERROR,
    'StreamToString' => ERROR,
    'StringBuilderInitWithChar' => ERROR,
    'SubstringOfZero' => ERROR,
    'SuppressWarningsDeprecated' => ERROR,
    'TemporalAccessorGetChronoField' => ERROR,
    'TestParametersNotInitialized' => ERROR,
    'TheoryButNoTheories' => ERROR,
    'ThrowIfUncheckedKnownChecked' => ERROR,
    'ThrowNull' => ERROR,
    'TreeToString' => ERROR,
    'TruthSelfEquals' => ERROR,
    'TryFailThrowable' => ERROR,
    'TypeParameterQualifier' => ERROR,
    'UnicodeDirectionalityCharacters' => ERROR,
    'UnicodeInCode' => ERROR,
    'UnnecessaryCheckNotNull' => ERROR,
    'UnnecessaryTypeArgument' => ERROR,
    'UnsafeWildcard' => ERROR,
    'UnusedAnonymousClass' => ERROR,
    'UnusedCollectionModifiedInPlace' => ERROR,
    'VarTypeName' => ERROR,
    'WrongOneof' => ERROR,
    'XorPower' => ERROR,
    'ZoneIdOfZ' => ERROR,
    'AndroidJdkLibsChecker' => ExpERROR,
    'AutoFactoryAtInject' => ExpERROR,
    'BanClassLoader' => ExpERROR,
    'BanSerializableRead' => ExpERROR,
    'ClassName' => ExpERROR,
    'ComparisonContractViolated' => ExpERROR,
    'DeduplicateConstants' => ExpERROR,
    'DepAnn' => ExpERROR,
    'EmptyIf' => ExpERROR,
    'ExtendsAutoValue' => ExpERROR,
    'InjectMoreThanOneQualifier' => ExpERROR,
    'InjectScopeOrQualifierAnnotationRetention' => ExpERROR,
    'InsecureCryptoUsage' => ExpERROR,
    'IterablePathParameter' => ExpERROR,
    'Java7ApiChecker' => ExpERROR,
    'Java8ApiChecker' => ExpERROR,
    'LongLiteralLowerCaseSuffix' => ExpERROR,
    'NoAllocation' => ExpERROR,
    'RefersToDaggerCodegen' => ExpERROR,
    'StaticOrDefaultInterfaceMethod' => ExpERROR,
    'StaticQualifiedUsingExpression' => ExpERROR,
    'SystemExitOutsideMain' => ExpERROR,
    'ThreadSafe' => ExpERROR,
    'UseCorrectAssertInTests' => ExpERROR,
);

sub getCurrentTime() {
	# The `undef`s stand for wday, yday, isdst
	($sec,$min,$hour,$mday,$mon,$year,undef,undef,undef) = localtime();
	return sprintf "%04d/%02d/%02d at %02d:%02d:%02d", $year + 1900, $mon + 1, $mday, $hour, $min, $sec;
}

sub getCounts() {
	foreach my $errType (keys %results) {
		my $totalCount = 0;
		my $sublevel = $results{$errType};
		foreach my $subl (values %$sublevel) {
			my $count = @$subl;
			$totalCount += $count;
		}
		$counts{$errType} = $totalCount;
	}
}

sub startReport() {
	my $ts = getCurrentTime();
	getCounts();
	if ($HTML) {
		print "<html>\n";
		print "<head>\n";
		print "<style>\n";
		print "  body { font-family: Arial, Helvetica, sans-serif; }\n";
		print "  h2 { padding-left: 1em; } \n";
		print "  h3 { padding-left: 3em; }\n";
		print "  p, li { padding-left: 6em; }\n";
		print "  h2, code, .error, .warning { font-family: Consolas, 'Courier New', Courier, monospace; }\n";
		print "  .error { color: red; }\n";
		print "</style>\n";
		print "</head>\n";
		print "<h1>Errorprone Report, $ts</h1>\n";
		print "<body>\n";
		foreach my $errType (sort keys %results) {
			$style = $errorLevels{$errType} ? "class='error'" : "warning";
			my $count = $counts{$errType};
			print "&#x2022; <a href='#$errType' $style>$errType [$count]</a> \n";
		}
	} else {
		print "\n";
		print "\x{1b}[97;100m                             \x{1b}[m\n";
		print "\x{1b}[97;100m      Errorprone Report      \x{1b}[m\n";
		print "\x{1b}[97;100m                             \x{1b}[m\n";
	}
}

sub endReport() {
	if ($HTML) {
		print "</body>\n";
		print "</html>\n";
	} else {
		print "\n==========================\n";
	}
}

sub htmlEscape($) {
	my ($msg) = @_;
	$msg =~ s/&/&amp;/g;
	$msg =~ s/</&lt;/g;
	$msg =~ s/>/&gt;/g;
	$msg =~ s/"/&quot;/g;
	return $msg;
}

sub level1($) {
	my ($msg) = @_;
	my $isError = $errorLevels{$msg};
	my $count = $counts{$msg};
	my $textMsg = $msg . " [$count]";
	if ($HTML) {
		$style = $isError ? "error" : "warning";
		print "\n  <h2><a name='$msg'></a><a href='http://errorprone.info/bugpattern/$msg' class='$style'>$textMsg</a></h2>\n";
	} else {
        if ($isError) {
			print "\n\x{1b}[91m" . $textMsg . "\x{1b}[m\n";
		} else {
			print "\n\x{1b}[93m" . $textMsg . "\x{1b}[m\n";
		}
	}
}

sub level2($) {
	my ($msg) = @_;
	$msg = htmlEscape($msg);
	if ($HTML) {
		print "\n    <h3>" . $msg . "</h3>\n";
	} else {
		print "\n\x{1b}[97m    " . $msg . "\x{1b}[m\n";
	}
}

sub level3($) {
	my ($msg) = @_;
	$msg = htmlEscape($msg);
	if ($HTML) {
		print "      <li><code>" . $msg . "</code></li>\n";
	} else {
		print "        $msg\n";
	}
}

# The work proper...

$rootDir = abs_path(__FILE__);
$rootDir =~ s/\\/\//g; # Normalize to UNIX path separator (`/`)
$rootDir =~ s/\/deployment\/errorprone\/.*/\//g;
$rootDir =~ s/\/errorprone.pl/\//g;

# Scan the output of mvn install and collect the interesting lines
while (<>) {
	if (($path, $errType, $errMsg) = /^\[WARNING\] (.+\.java:\[\d+,\d+\]) \[(.+?)\] (.+)/) {
		$path =~ s/$rootDir//g;
		$path =~ s/^\///g;
		$errMsg =~ s/; did you mean '.*'\?$//g;
		$foo = $results{$errType}{$errMsg};
		if (not defined $foo) {
			$foo = [];
			$results{$errType}{$errMsg} = $foo;
		}
		push @$foo, $path;
	}
}

startReport();
foreach my $errType (sort keys %results) {
	level1($errType);
	my $hashLevel2 = $results{$errType};
	foreach my $errMsg (sort keys %$hashLevel2) {
		level2($errMsg);
		my $hashLevel3 = $hashLevel2->{$errMsg};
		foreach my $path (@$hashLevel3) {
			level3($path);
		}
	}
}
endReport();
