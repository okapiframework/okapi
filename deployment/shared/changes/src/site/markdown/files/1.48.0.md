# Changes from 1.47.0 to 1.48.0

<!-- MACRO{toc} -->

## Core

* Changed: TextFragmet renumberCodes now assigns a new code id for closing tags without a corresponding opening tag.
* Removed: XLIFF Writer: includeNoTranslate parameter.

## Connectors

* ABC Connector

    * Fixed X
    * Improved Y

## Filters

* CSV Filter

  * Fixed: escape qualifiers in unqualified fields.

* IDML Filter

  * Fixed: correctly display codefinder rules in the filter config UI
  
* JSON Filter

  * Added: an option to set the size-unit property when specifying maxwidth.

* OpenXML Filter

  * Fixed: PPTX: a line break prepended by a run with empty text (extended cases):
    [issue#1373](https://bitbucket.org/okapiframework/okapi/issues/1373).
  * Fixed: XLSX: tinted colors handling clarified:
    [issue#1378](https://gitlab.com/okapiframework/okapi/-/issues/1378).
  * Improved: PPTX: graphic metadata translation supported:
    [issue#1380](https://gitlab.com/okapiframework/okapi/-/issues/1380).
  * Improved: whitespace styles ignorance supported:
    [issue#1385](https://gitlab.com/okapiframework/okapi/-/issues/1385).
  * Added: XLSX: non-empty maxwidth and size-unit properties become available per worksheet and 
    target column:
    [issue#1386](https://gitlab.com/okapiframework/okapi/-/issues/1386).

* PDF Filter

  * Added: Option to use [SortByPosition](https://pdfbox.apache.org/docs/2.0.7/javadocs/org/apache/pdfbox/text/PDFTextStripper.html#setSortByPosition(boolean)) to order results by position on page

* Subtitle Filter

  * Added: Option to not merge captions
  * Added: Codefinder support

## Libraries

* Merge Library

  * Added: preserveWhitespace parameter.

## Steps

* Merging Step

  * Improved: preserveWhiteSpaceByDefault replaced by preserveWhitespace parameter to align the 
    names throughout the codebase.

* Rainbow Kit Step

  * Added: preserveWhitespace parameter.

## TMs

* ABC TM

    * Fixed X
    * Improved Y
    * Deprecated Z

## Applications

* Tikal

    * Fixed X
    * Improved Y
    * Deprecated Z

* CheckMate

    * Fixed X

* Rainbow

    * Fixed X

* Ratel

    * Fixed X

* Serval

    * Fixed X

## OSes

* macos

    * Did something for macos

* All

    * Did something for all systems

## Installation

* Did something 1
* Did something 2

## General

* Did something 1
* Did something 2

## Plugins

* OmegaT

    * Did something

* Trados Utilities

    * Did something even better
