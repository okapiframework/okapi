# Changes from M32 to M33

<!-- MACRO{toc} -->

## Applications

* CheckMate

    * Updated LanguageTool integration to support the new JSON API ([Issue #582](https://gitlab.com/okapiframework/okapi/-/issues/582)). \
      **Note:** As a result of this change, CheckMate will no longer work
      with LanguageTool version 3.3 or earlier. For more information, see the
      [LanguageTool API Migration page](https://languagetool.org/http-api/migration.php).

* Tikal

    * You can now process several files at once with the `-xm`
      and `-lm` commands. Note that in that case you cannot use
      the `-to` and `-from` options. This resolves [issue #598](https://gitlab.com/okapiframework/okapi/-/issues/598).

## Filters

* Markdown Filter

    * There is a new filter for Markdown (`.md`) files.

* XLIFF Filter

    * Fixed missing prefix on output for some prefixed XLIFF inline elements.
    * Improved support for `equiv-text` attribute.
    * [Issue #466](https://gitlab.com/okapiframework/okapi/-/issues/466): Add a new `skipNoMrkSegSource` option that
      will cause `trans-units` containing `seg-source`
      but no internal `mrk` data to be treated as skeleton.
      This behavior (which is consistent with SDL Studio) is enabled in
      the `okf_xliff-sdl` configuration, but disabled by
      default in `okf_xliff`.
    * [Issue #551](https://gitlab.com/okapiframework/okapi/-/issues/551): XLIFF files containing entities that are invalid in
      XML 1.0 (for example, `&#x1f;`) will no longer break the filter.
      (These entities, and their corresponding characters, will be
      stripped before parsing.)
    * [Issue #602](https://gitlab.com/okapiframework/okapi/-/issues/602): the `maxwidth`, `maxheight`,
      and `size-unit` attributes are now parsed and exposed as
      resource properties. Updates to these properties will be reflected
      in the merged XLIFF file.

* XML Filter

    * The handling of files with UTF-16LE and UTF-16BE declaration has
      been improved. Output to these encodings is treated as an output to
      UTF-16 with a BOM.
    * Add the `inlineCdata` option, which will cause `CDATA`
      markup to appear as inline codes rather than being stripped.

* OpenXML Filter

    * Support for `.dotx`, `.dotm`, `.ppsx`, `.ppsm`, `.potx`, `.potm`, `.xltx`,
      `.xltm` files has been added.
    * Fix a bug where certain XLSX files would cause an infinite loop in parsing.
    * Fix a bug where certain XLSX files would fail to extract.
    * Fix a bug where paragraph spacing properties were incorrectly
      stripped when using "aggressive" cleanup mode.

* PO Filter

    * [Issue #584](https://gitlab.com/okapiframework/okapi/-/issues/584): Add "Include `msgctxt` in note" option to
      include context data in trans-unit notes.

* JSON Filter

    * A new option to not have the leading slash in the full key path
      has been added. This resolves [issue #603](https://gitlab.com/okapiframework/okapi/-/issues/603).

## Libraries

* lib-verification

    * Updated the LanguageTool integration to support the new JSON API, as
      described above.
    * Updated the BlacklistChecker to do case sensitive validation if the
      blaclist terms are identical except for the case.

## Connectors

* SimpleTM

    * Fixed the `setPenalizeSourceWithDifferentCodes()`
      method so it sets the value correctly.

* Google MT

    * Update the connector to support NMT models, when available.

## General

    * The code is now under Apache License version 2.0.
    * The `XLIFFWriter` class now supports serializing the `maxwidth`,
      `maxheight`, and `size-unit` attributes on `<group>`
      and `<trans-unit>` elements, by attaching the
      corresponding `net.sf.okapi.common.resource.Property` key
      to the appropriate resource.
    * The displayText field of `Code` objects is now stored in
      a field on the object, rather than in an annotation.
    * The `equals` and `compareTo` methods on the
      `TextFragment` class will no longer indicate equality when
      compared to non-`TextFragment` instances.
    * Improved error message in `GenericSkeletonWriter`. This
      resolves [issue #593](https://gitlab.com/okapiframework/okapi/-/issues/593).
