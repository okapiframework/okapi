# Changes from M14 to M15

## Applications

* Tikal

    * Added the option `-gg` for the Google MT v2 conector.
      This fixes [issue #189](https://gitlab.com/okapiframework/okapi/-/issues/189).
    * Mapped the `-google` option to `-gg` and
      removed references to the Google MT v1 connector which is not
      available anymore.

* Rainbow

    * Update the ID-Based Alignment utility to use the GT v2 API for the
      MT services instead of v1.
    * Moved old utilities at the bottom of the Utilities menu.

## Filters Plugin for OmegaT

* Existing segmentation is auto-detected.
* Fixed code re-construction issues for some cases.
* Improved backward compatibility, and implemented support for
  alternate translation of repeated source segments for OmegaT 2.5.
* Now if output encoding is not defined for a TTX file, UTF-16 is
  used (instead of UTF-8).

## Steps

* Text Modification Step

    * Added the option for text expansion.
    * Added the option to replace ASCII letters with Cyrillic,
      Arabic and Chinese characters.

* Rainbow Kit Creation Step

    * Fixed incorrectly mapped inline code that resulted in invalid
      merged files.
    * The Translation Table format is now available as an package
      type 9allows extraction and merge of tab-delimited files)

* Format Conversion Step

    * For the parallel corpus output, the source entries are also
      now always output even when there is no corresponding target (in
      that case the target entry is output as an empty line).

* Id-Based Aligner Step

    * Implemented support for bilingual files as the reference file.

* Microsoft Batch Translation Step

    * Fixed issue where leveraged text was not copied into the
      target in some cases when it should.

* XML validation Step

    * Fixed the schema/DTD selection error.

* Term Extraction Step

    * The `TermsAnnotation` annotations are taken into account
      and are extracted.

## Connectors

* Google MT v2 Connector

    * Fixed error when list of segments to translate is empty or
      made of segments with only codes.
    * Removed query length limit for single query mode to let Google
      Translate to deal with it in case it changes)

## Filters

* IDML Filter

    * Fixed issue of not merging last cell of a table that is the
      last element of a story.
    * Fixed TextPath-attached stories not being extracted. This
      fixes [issue #194](https://gitlab.com/okapiframework/okapi/-/issues/194).

* HTML Filter

    * Files with no encoding declaration detected within the fisrt 1
      KB have an encoding declaration added on output (only if the
      file has a `<head>` element).
    * Added support for HTML5 charset declaration.

* XLIFF Filter

    * Fixed issue where the `datatype` attribute for `<file>`
      was not read properly.

* RTF Filter

    * Improved warning messages for decoding error (e.g. when a font
      name is corrupted). dangerous cases have now an extra message
      warning about the lost of the character.

* TTX Filter

    * Replace the "include un-segmented part" option by a new
      `segmentMode` option: \
      Mode 0: auto-detect, if a segment is detected mode 1 is used.
      If no segment is detected mode 2 is used. \
      Mode 1: only existing segments are extracted. \
      Mode 2: all text is extracted, segmented or not.
    * Added support for Catalyst-generated TTX (that have no `<Raw>`
      nor `<ut>` elements)

* Properties Filter

    * Added inline code support for HTML tags in default

* PO Filter

    * The translator's comments are now extracted into the common `transNote`
      read-only property.

* TS Filter

    * Implemented the support for translator's note and simple
      notes.

* Regex Filter

    * Added a pre-defined configuration for Apple iOS `.strings`
      files.

* OpenXML Filter

    * Fixed the issue where a ZIP stream was not closed properly.
    * Fixed [issue #143](https://gitlab.com/okapiframework/okapi/-/issues/143) (Excel parameters not working)
    * Fixed [issue #144](https://gitlab.com/okapiframework/okapi/-/issues/144) (Color not save for Excel options))

* XML Filter

    * Fixed issue of missing start tag in case of non-translatable
      inline with no translatable text before.
    * Changed unwrapping default: ends are not trimmed anymore (was
      causing lost of spaces in fragment extraction).
    * Fixed issue where translation context was not reset after an
      embedded structural translatable element was extracted (e.g.
      para inside para case)
    * `TermsAnnotation` annotations are now generated for
      entries matching the ITS Terminology datacategory.

## Libraries

* Added support for multiple monitors when centering a dialog.
* Fixed case where tuid was written twice with `TMXWriter`.
* Fixed the link of the "Patterns" button in Inline Code panel.
* Continued to improve the XLIFF 2.0 experimental library.
* Fixed the case of batch configuration with no plugins.
* Removed Google MT v1 connector from the list of default
  connectors.
* Fixed icon and cancel command in Filter Configuration common
  dialog.
* Improved cloning of skeleton parts.
* The XLIFFWriter output the common `transNote` property as a
  `<note from="translator">`. This fixes [issue #195](https://gitlab.com/okapiframework/okapi/-/issues/195).
* Added `changeFontSize()` to `InputDialog` UI.

