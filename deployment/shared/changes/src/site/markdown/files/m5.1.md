# Changes from M5 (0.5) to M5 (0.5.1)

## Applications

* Rainbow

    * Translation Package Creation:
        * Fixed the bug where the encoder manager for RTF output was not
          properly set and cause some formats like HTML, TMX, etc. to have
          un-escaped characters.
        * Changed the RTF writer to allow other skeleton writers than
          GenericSkeletonWriter.
    * Replaced the Search and Replace utility by the "Search and Replace
      with Filter" and the "Search and Replace without Filter" pre-defined
      pipelines.
    * Replaced the Text Rewriting utility by the "Text Rewriting"
      pre-defined pipeline.

* Tikal

    * Fixed the issue of not having the HTML filter mapped when using
      the Vignette filter.
    * Added support for accessing Microsoft MT engine (`-ms` option).

## Translation resources

* Added a connector for Microsoft MT Web services (http://api.microsofttranslator.com/V1/SOAP.svc),
  a Microsoft Bing AppID is needed to use it. You can obtain one at
  http://www.bing.com/developers/appids.aspx.
* Google MT: made it consistent with other connector when result is
  same as target, now the result is returned.
* SimpleTM: Added made the feature "penalize exact matches when
  target has different codes than the query" an option. (default is
  true, backward compatible).

## Libraries

* Fixed issue with `GenericSkeletonWriter` and in-line codes in
  segmented text unit that were outside any segment.
* Fixed issue with `GenericFilterWriter` output stream not nullified
  in `close()` (causing for example no output using
  `FilterEventsToRawDocument`).

## Steps / Pipeline

* Added `MULTI_EVENT` (new resource and Event) handling to pipeline.
* Changed step handlers to return Event by default.
* Fixed the parameters setting bug preventing to save the parameters
  for pre-defined pipeline from one session to the next.

* Leveraging Step

    * Fixed the bug preventing to enter a TMX path.
    * Made adding an `MT!` prefix to the TMX entries an
      option.
    * Added an option to enabled/disable the step.

* Search and Replace Step

    * Improved the behavior of the dialog box for add/edit item.

* Format Conversion Step

    * Fixed bug where the table-delimited output
      was not closed properly for "one output per input" use case.

* Added Text Modification Step

## Filters

* PHP Content Filter

    * Added UI for the localization directives options (default behaviour is the same).

* OpenXML Filter

    * Changed the parameters editor to use `GridLayout`
      instead of `BorderLayout`.

* TMX Filter

    * Fixed losing original line-breaks between `<tu>` when re-writing.

* Vignette Filter
    * Fixed bug of un-escaped and non-`CDATA` RTF output.

* Properties Filter
    * Added the option "Convert \n and \t to line-break and tab".

* Table Filter

    * Fixed [issue #119](https://gitlab.com/okapiframework/okapi/-/issues/119) where csv action "Exclude
      leading/trailing..." was not updated properly in the parameters
      editor
    * Fixed [issue #118](https://gitlab.com/okapiframework/okapi/-/issues/118) where some csv cases were not extracted properly

## Installation

* Updated licence information for third-party packages.
* Removed all the dependencies to swing2swt.

