# <span style="color:red">The Okapi Framework moved to GitLab</span>

**Okapi proper:**
https://gitlab.com/okapiframework/Okapi

**Related projects:**
https://gitlab.com/okapiframework

---

Any pull requests or issues you might file here will ignored.

The history and all issues were preserved, so there is no good reason to dig here.

But if you want to do it you can switch to the [`dev` branch](https://bitbucket.org/okapiframework/okapi/src/dev/).

