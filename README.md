# About Okapi Framework

The **Okapi Framework** is a set of interface specifications, format definitions,
components and applications that provides a cross-platform environment to build
interoperable tools for the different steps of the translation and localization process.

The goal of the **Okapi Framework** is to allow tools developers and localizers
to build new localization processes or enhance existing ones to best meet their needs,
while preserving a level of compatibility and interoperability.
It also provides them with a way to share (and re-use) components across different solutions.

Okapi code is developed under the [Apache 2.0 License](https://www.apache.org/licenses/LICENSE-2.0).

## Useful links

* **[Latest release of the distributions](https://okapiframework.org/binaries/main/1.47.0)**
* [Latest snapshot of the distributions (nightly build)](https://gitlab.com/okapiframework/okapi/-/jobs/artifacts/main/browse/deployment/maven/done?job=verification:jdk17)
* [The main Okapi Framework Web site](http://okapiframework.org/wiki/index.php?title=Main_Page)
* [Users group](https://groups.google.com/forum/#!forum/okapi-users)
* [A word about using open standards](http://okapiframework.org/wiki/index.php?title=Open_Standards)
* [How to Contribute](https://gitlab.com/okapiframework/okapi/-/wikis/How-to-Contribute)
* [Contributor License Agreement](https://gitlab.com/okapiframework/okapi/-/wikis/Contributor-License-Agreement)
* [Consultancy](https://gitlab.com/okapiframework/okapi/-/wikis/Consultancy)

## Build status on GitLab

Pipeline | Status (`release`) | Status (`main`)
-------- | --------------- | ------------
Okapi (this) | [![pipeline status](https://gitlab.com/okapiframework/okapi/badges/release/pipeline.svg)](https://gitlab.com/okapiframework/okapi/commits/release) | [![pipeline status](https://gitlab.com/okapiframework/okapi/badges/main/pipeline.svg)](https://gitlab.com/okapiframework/okapi/commits/main)
[Longhorn](https://gitlab.com/okapiframework/longhorn/) | [![pipeline status](https://gitlab.com/okapiframework/longhorn/badges/release/pipeline.svg)](https://gitlab.com/okapiframework/longhorn/commits/release) | [![pipeline status](https://gitlab.com/okapiframework/longhorn/badges/main/pipeline.svg)](https://gitlab.com/okapiframework/longhorn/commits/main)
[Longhorn JS Client](https://gitlab.com/okapiframework/longhorn-js-client/) | [![pipeline status](https://gitlab.com/okapiframework/longhorn-js-client/badges/release/pipeline.svg)](https://gitlab.com/okapiframework/longhorn-js-client/commits/release) | [![pipeline status](https://gitlab.com/okapiframework/longhorn-js-client/badges/main/pipeline.svg)](https://gitlab.com/okapiframework/longhorn-js-client/commits/main)
[OmegaT Plugin](https://gitlab.com/okapiframework/omegat-plugin/) | [![pipeline status](https://gitlab.com/okapiframework/omegat-plugin/badges/release/pipeline.svg)](https://gitlab.com/okapiframework/omegat-plugin/commits/release) | [![pipeline status](https://gitlab.com/okapiframework/omegat-plugin/badges/main/pipeline.svg)](https://gitlab.com/okapiframework/omegat-plugin/commits/main)

## Maven repositories

Okapi is available in Maven Central, so unless you want to use the snapshot version
there is no reason to do anything in you `pom.xml` other than add dependencies.

But just in case, here it is:

* **Okapi artifacts releases:**
  <https://search.maven.org/search?q=net.sf.okapi>
* **Okapi artifacts snapshots:**
  <https://oss.sonatype.org/content/repositories/snapshots/>

## Example of tools and applications using Okapi

* [Ratel](http://okapiframework.org/wiki/index.php?title=Ratel) -
  an editor for [SRX (segmentation rules)](http://okapiframework.org/wiki/index.php?title=SRX).
* [Pangolin](https://github.com/davidmason/Pangolin) -
  an on-line editor for SRX files.
* [Rainbow](http://okapiframework.org/wiki/index.php?title=Rainbow) -
  a localization toolbox that makes use of standards like [XLIFF](http://okapiframework.org/wiki/index.php?title=Open_Standards#XLIFF)
  and [TMX](http://okapiframework.org/wiki/index.php?title=Open_Standards#TMX),
  supports a wide range of [formats](http://okapiframework.org/wiki/index.php?title=Filters)
  and offers many [features](http://okapiframework.org/wiki/index.php?title=Steps).
* [CheckMate](http://okapiframework.org/wiki/index.php?title=CheckMate) -
  an application to perform various quality checks on bilingual translated documents.
* [Tikal](http://okapiframework.org/wiki/index.php?title=Tikal) -
  a command-line tool for extracting/merging XLIFF files and do many other tasks.
* [Filters plugin for OmegaT](http://okapiframework.org/wiki/index.php?title=Okapi_Filters_Plugin_for_OmegaT) -
  an [OmegaT](http://www.omegat.org/) plugin to use Okapi filters.
* [Longhorn](http://okapiframework.org/wiki/index.php?title=Longhorn) -
  a batch processing server.
* [Okapi-ant](https://github.com/tingley/okapi-ant) -
  several Ant tasks to process files with Okapi components.
* See some [screenshots](http://okapiframework.org/wiki/index.php?title=Screenshots)
