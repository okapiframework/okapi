/*===========================================================================
  Copyright (C) 2008-2009 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
===========================================================================*/

package net.sf.okapi.filters.openxml.ui;

import net.sf.okapi.common.EditorFor;
import net.sf.okapi.common.IContext;
import net.sf.okapi.common.IHelp;
import net.sf.okapi.common.IParameters;
import net.sf.okapi.common.IParametersEditor;
import net.sf.okapi.common.ui.Dialogs;
import net.sf.okapi.common.ui.OKCancelPanel;
import net.sf.okapi.common.ui.ResponsiveTable;
import net.sf.okapi.common.ui.UIUtil;
import net.sf.okapi.common.ui.filters.ResponsiveTableFontMappingsInput;
import net.sf.okapi.common.ui.filters.ResponsiveTableFontMappingsOutput;
import net.sf.okapi.filters.openxml.Color;
import net.sf.okapi.filters.openxml.ConditionalParameters;
import net.sf.okapi.filters.openxml.Excell;
import net.sf.okapi.filters.openxml.HighlightColorValues;
import net.sf.okapi.filters.openxml.ParseType;
import net.sf.okapi.filters.openxml.PresetColorValues;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.Text;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.ResourceBundle;
import java.util.TreeSet;

@EditorFor(ConditionalParameters.class)
@SuppressWarnings({"rawtypes", "unused"})
public class Editor implements IParametersEditor {
	private static final String[] WORD_SYLES = {
		"Emphasis", "ExcludeCharacterStyle", "ExcludeParagraphStyle", "Heading1", "Heading2",
		"Normal", "Title", "Strong", "Subtitle", "tw4winExternal"
	};

	private Shell shell;
	private IContext context;
	private boolean readOnly = false;
	private boolean result = false;
	private ConditionalParameters params;
	private ResourceBundle resourceBundle;
	private PresetColorValues presetColorValues;
	private PresetColorValues highlightColorValues;
	private IHelp help;	
	private Button btnHelp;
	private Button btnOk;
	private Button btnCancel;
	private Button btnTranslateDocumentProperties;
	private Button translatePowerpointDocumentProperties;
	private Button reorderPowerpointDocumentPropertiesButton;
	private Button reorderPowerpointRelationshipsButton;
	private Button translatePowerpointDiagramDataButton;
	private Button reorderPowerpointDiagramDataButton;
	private Button translatePowerpointChartsButton;
	private Button reorderPowerpointChartsButton;
	private Button translateCommentsButton;
	private Button translatePowerpointCommentsButton;
	private Button reorderPowerpointCommentsButton;
	private Button btnCleanAggressively;
	private Button ignoreWhitespaceSttyles;
	private Button btnTreatTabAsChar;
	private Button btnTreatLineBreakAsChar;
	private ResponsiveTable fontMappingsTable;
	private Button btnTranslateHeadersAndFooters;
	private Button translateWordNumberingLevelText;
	private Button translateWordHidden;
	private Button btnExcludeWordGraphicMetadata;
	private Button translatePowerpointHidden;
	private Button translatePowerpointGraphicMetadata;
	private Button btnAutomaticallyAcceptRevisions;
	private Button btnIgnoreSoftHyphen;
    private Button btnReplaceNonBreakingHyphen;
	private Button allowWordStyleOptimisation;
	private Button preserveExcelStylesInTargetColumns;
	private Button btnStylesFromDocument;
	private Button btnColorsFromDocument;
	private Button translateExcelHidden;
	private Button btnTranslateSheetNames;
	private Button translateCellsCopied;
	private Button btnTranslateDiagramData;
	private Button btnTranslateDrawings;
	private Button btnExtractExternalHyperlinks;
	private Button ignoreFontColors;
	private Text fontColorsMinIgnoranceThreshold;
	private Text fontColorsMaxIgnoranceThreshold;
	private List listExcludedWordStyles;
	private List listTranslatableFields;
	private List listExcelColorsToExclude;
	private Text edSubfilter;
	private ResponsiveTable worksheetConfigurationsTable;
	private Button translatePowerpointNotesButton;
	private Button reorderPowerpointNotesButton;
	private Button btnTranslateMasters;
	private Button btnIgnorePlaceholdersInMasters;
	private Button btnIncludedSlideNumbersOnly;
	private List listPowerpointIncludedSlideNumbers;
	private Button rdExcludeStyles;
	private Button rdIncludeStyles;
	private List listWordHightlightColorsToExclude;
	private Button rdExcludeHighlightStyles;
	private Button rdIncludeHighlightStyles;
	private List listWordColorsToExclude;

	public boolean edit (IParameters options,
		boolean readOnly,
		IContext context)
	{
		this.context = context;
		this.readOnly = readOnly;
		help = (IHelp)context.getObject("help");
		boolean bRes = false;
		shell = null;
		params = (ConditionalParameters)options;
		this.resourceBundle = ResourceBundle.getBundle("net.sf.okapi.filters.openxml.ui.Editor");
		this.presetColorValues = new PresetColorValues.Default();
		this.highlightColorValues = new HighlightColorValues();
		try {
			createContents();			
			return showDialog();
		}
		catch ( Exception E ) {
			Dialogs.showError(shell, E.getLocalizedMessage(), null);
			bRes = false;
		}
		finally {
			// Dispose of the shell, but not of the display
			if ( shell != null ) shell.dispose();
		}
		return bRes;
	}
	
	public IParameters createParameters () {
		return new ConditionalParameters();
	}
	
	/**
	 * @wbp.parser.entryPoint
	 * Create contents of the dialog.
	 */	
	protected void createContents() { // DWH 6-17-09 was private
		Shell parent = (Shell)context.getObject("shell");
		shell = new Shell(parent, SWT.CLOSE | SWT.TITLE | SWT.RESIZE | SWT.APPLICATION_MODAL);
		shell.setText("Office 2007 Filter Parameters");
		if ( parent != null ) UIUtil.inheritIcon(shell, parent);
		GridLayout layTmp = new GridLayout();
		layTmp.marginBottom = 0;
		layTmp.verticalSpacing = 0;
		shell.setLayout(layTmp);

		TabFolder tabFolder = new TabFolder(shell, SWT.NONE);
		//tabFolder.setLayoutData(BorderLayout.CENTER);
		GridData gdTmp = new GridData(GridData.FILL_BOTH);
		tabFolder.setLayoutData(gdTmp);
		{
			TabItem tbtmGeneralOptions_1 = new TabItem(tabFolder, SWT.NONE);
			tbtmGeneralOptions_1.setText("General Options");
			{
				Composite composite = new Composite(tabFolder, SWT.NONE);
				tbtmGeneralOptions_1.setControl(composite);
				composite.setLayout(new GridLayout(2, false));
				Composite leftOptions = new Composite(composite, SWT.NONE);
				leftOptions.setLayout(new GridLayout());
				leftOptions.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
				{
					btnTranslateDocumentProperties = new Button(leftOptions, SWT.CHECK);
					btnTranslateDocumentProperties.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
					btnTranslateDocumentProperties.setSelection(true);
					btnTranslateDocumentProperties.setText(this.resourceBundle.getString("translate-document-properties"));
				}
				{
					this.translateCommentsButton = new Button(leftOptions, SWT.CHECK);
					this.translateCommentsButton.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
					this.translateCommentsButton.setSelection(true);
					this.translateCommentsButton.setText(this.resourceBundle.getString("translate-comments"));
				}
				{
					btnCleanAggressively = new Button(leftOptions, SWT.CHECK);
					btnCleanAggressively.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
					btnCleanAggressively.setSelection(true);
					btnCleanAggressively.setText("Clean Tags Aggressively");
				}
				{
					this.ignoreWhitespaceSttyles = new Button(leftOptions, SWT.CHECK);
					this.ignoreWhitespaceSttyles.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
					this.ignoreWhitespaceSttyles.setSelection(true);
					this.ignoreWhitespaceSttyles.setText(this.resourceBundle.getString("ignore-whitespace-styles"));
				}
				{
					btnTreatTabAsChar = new Button(leftOptions, SWT.CHECK);
					btnTreatTabAsChar.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
					btnTreatTabAsChar.setSelection(false);
					btnTreatTabAsChar.setText("Treat Tab as Character");
				}
				{
					btnTreatLineBreakAsChar = new Button(leftOptions, SWT.CHECK);
					btnTreatLineBreakAsChar.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
					btnTreatLineBreakAsChar.setSelection(false);
					btnTreatLineBreakAsChar.setText("Treat Line Break as Character");
				}
				{
					btnExtractExternalHyperlinks = new Button(leftOptions, SWT.CHECK);
					btnExtractExternalHyperlinks.setLayoutData(new GridData(SWT.LEFT, SWT.TOP, false, false, 1, 1));
					btnExtractExternalHyperlinks.setSelection(false);
					btnExtractExternalHyperlinks.setText(this.resourceBundle.getString("extract-external-hyperlinks"));
				}
				Composite rightOptions = new Composite(composite, SWT.NONE);
				rightOptions.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
				rightOptions.setLayout(new GridLayout());

				Composite wideOptions = new Composite(composite, SWT.NONE);
				wideOptions.setLayout(new GridLayout());
				wideOptions.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 2, 1));
				{
					final Label l = new Label(wideOptions, SWT.NONE);
					l.setText(this.resourceBundle.getString("font-mappings"));
					this.fontMappingsTable = this.params.fontMappings().writtenTo(
						new ResponsiveTableFontMappingsOutput(
							new ResponsiveTable.Default(
								new Table(wideOptions, SWT.MULTI | SWT.BORDER | SWT.FULL_SELECTION),
								new Menu(this.shell, SWT.POP_UP),
								new Menu(this.shell, SWT.POP_UP)
							)
						)
					);
				}
			}
		}
		{
			TabItem tbtmWordOptions = new TabItem(tabFolder, SWT.NONE);
			tbtmWordOptions.setText("Word Options");
			{
				Composite composite = new Composite(tabFolder, SWT.NONE);
				tbtmWordOptions.setControl(composite);
				composite.setLayout(new GridLayout(2, false));
				Composite leftOptions = new Composite(composite, SWT.NONE);
				leftOptions.setLayout(new GridLayout());
				leftOptions.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
				{
					btnTranslateHeadersAndFooters = new Button(leftOptions, SWT.CHECK);
					btnTranslateHeadersAndFooters.setSelection(true);
					btnTranslateHeadersAndFooters.setText("Translate Headers and Footers");
				}
				{
					translateWordNumberingLevelText = new Button(leftOptions, SWT.CHECK);
					translateWordNumberingLevelText.setSelection(true);
					translateWordNumberingLevelText.setText(this.resourceBundle.getString("translate-numbering-level-text"));
				}
				{
					this.translateWordHidden = new Button(leftOptions, SWT.CHECK);
					this.translateWordHidden.setLayoutData(new GridData(SWT.LEFT, SWT.TOP, false, false, 1, 1));
					this.translateWordHidden.setSelection(true);
					this.translateWordHidden.setText(this.resourceBundle.getString("translate-hidden"));
				}
				{
					btnExcludeWordGraphicMetadata = new Button(leftOptions, SWT.CHECK);
					btnExcludeWordGraphicMetadata.setLayoutData(new GridData(SWT.LEFT, SWT.TOP, false, false, 1, 1));
					btnExcludeWordGraphicMetadata.setSelection(false);
					btnExcludeWordGraphicMetadata.setText("Exclude Graphical Metadata");
				}
				{
				    btnAutomaticallyAcceptRevisions = new Button(leftOptions, SWT.CHECK);
				    btnAutomaticallyAcceptRevisions.setLayoutData(new GridData(SWT.LEFT, SWT.TOP, false, false, 1, 1));
				    btnAutomaticallyAcceptRevisions.setSelection(true);
				    btnAutomaticallyAcceptRevisions.setText("Automatically Accept Revisions");
                }
				{
                    btnIgnoreSoftHyphen = new Button(leftOptions, SWT.CHECK);
                    btnIgnoreSoftHyphen.setLayoutData(new GridData(SWT.LEFT, SWT.TOP, false, false, 1, 1));
                    btnIgnoreSoftHyphen.setSelection(false);
                    btnIgnoreSoftHyphen.setText("Ignore Soft Hyphens");
                }
                {
                    btnReplaceNonBreakingHyphen = new Button(leftOptions, SWT.CHECK);
                    btnReplaceNonBreakingHyphen.setLayoutData(new GridData(SWT.LEFT, SWT.TOP, false, false, 1, 1));
                    btnReplaceNonBreakingHyphen.setSelection(false);
                    btnReplaceNonBreakingHyphen.setText("Replace Non-Breaking Hyphen with Regular Hyphen");
                }
				{
					allowWordStyleOptimisation = new Button(leftOptions, SWT.CHECK);
					allowWordStyleOptimisation.setLayoutData(new GridData(SWT.LEFT, SWT.TOP, false, false, 1, 1));
					allowWordStyleOptimisation.setSelection(true);
					allowWordStyleOptimisation.setText(this.resourceBundle.getString("allow-style-optimisation"));
				}
				{
					Group g = new Group(leftOptions, SWT.NONE);
					g.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
					g.setLayout(new GridLayout());
					g.setText(this.resourceBundle.getString("translatable-fields"));
					listTranslatableFields = new List(g,  SWT.V_SCROLL | SWT.MULTI);
					GridData gd_listTranslatableFields = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
					gd_listTranslatableFields.heightHint = 40;
					listTranslatableFields.setLayoutData(gd_listTranslatableFields);
					listTranslatableFields.setItems(new String[]{"HYPERLINK", "FORMTEXT", "TOC"});
				}
				{
					Group g = new Group(leftOptions, SWT.NONE);
					g.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
					g.setLayout(new GridLayout());
					g.setText(this.resourceBundle.getString("ignored-styles"));
					ignoreFontColors = new Button(g, SWT.CHECK);
					ignoreFontColors.setLayoutData(new GridData(SWT.LEFT, SWT.TOP, false, false, 1, 1));
					ignoreFontColors.setSelection(false);
					ignoreFontColors.setText(this.resourceBundle.getString("ignore-font-colours"));
					Label l = new Label(g, SWT.NONE);
					l.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
					l.setText(this.resourceBundle.getString("font-colours-minimum-ignorance-threshold"));
					fontColorsMinIgnoranceThreshold = new Text(g, SWT.BORDER);
					fontColorsMinIgnoranceThreshold.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1));
					l = new Label(g, SWT.NONE);
					l.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
					l.setText(this.resourceBundle.getString("font-colours-maximum-ignorance-threshold"));
					fontColorsMaxIgnoranceThreshold = new Text(g, SWT.BORDER);
					fontColorsMaxIgnoranceThreshold.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1));
				}
				Composite rightOptions = new Composite(composite, SWT.NONE);
				rightOptions.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
				rightOptions.setLayout(new GridLayout());
				{
					Group g = new Group(rightOptions, SWT.NONE);
					g.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
					g.setLayout(new GridLayout());
					g.setText(this.resourceBundle.getString("excluded-included-styles"));
					Composite radiosInLine = new Composite(g, SWT.NONE);
					radiosInLine.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false));
					radiosInLine.setLayout(new GridLayout(2, true));
					rdExcludeStyles = new Button(radiosInLine, SWT.RADIO);
					rdExcludeStyles.setText(this.resourceBundle.getString("exclude"));
					rdIncludeStyles = new Button(radiosInLine, SWT.RADIO);
					rdIncludeStyles.setText(this.resourceBundle.getString("include"));
					listExcludedWordStyles = new List(g, SWT.V_SCROLL | SWT.MULTI);
					GridData gd_listExcludedWordStyles = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
					gd_listExcludedWordStyles.heightHint = 40;
					listExcludedWordStyles.setLayoutData(gd_listExcludedWordStyles);
					listExcludedWordStyles.setItems(WORD_SYLES);
				}
				{
					Group g = new Group(rightOptions, SWT.NONE);
					g.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
					g.setLayout(new GridLayout());
					g.setText(this.resourceBundle.getString("excluded-included-highlight-colours"));
					Composite radiosInLine = new Composite(g, SWT.NONE);
					radiosInLine.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false));
					radiosInLine.setLayout(new GridLayout(2, true));
					rdExcludeHighlightStyles = new Button(radiosInLine, SWT.RADIO);
					rdExcludeHighlightStyles.setText(this.resourceBundle.getString("exclude"));
					rdIncludeHighlightStyles = new Button(radiosInLine, SWT.RADIO);
					rdIncludeHighlightStyles.setText(this.resourceBundle.getString("include"));
					listWordHightlightColorsToExclude = new List(g, SWT.V_SCROLL | SWT.MULTI);
					GridData gd_listWordHighlightColorsToExclude = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
					gd_listWordHighlightColorsToExclude.heightHint = 40;
					listWordHightlightColorsToExclude.setLayoutData(gd_listWordHighlightColorsToExclude);
					listWordHightlightColorsToExclude.setItems(this.highlightColorValues.externalNames().toArray(new String[0]));
				}
				{
					Group g = new Group(rightOptions, SWT.NONE);
					g.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
					g.setLayout(new GridLayout());
					g.setText(this.resourceBundle.getString("excluded-font-colours"));
					listWordColorsToExclude = new List(g, SWT.V_SCROLL | SWT.MULTI);
					GridData gd_listWordColorsToExclude = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
					gd_listWordColorsToExclude.heightHint = 40;
					listWordColorsToExclude.setLayoutData(gd_listWordColorsToExclude);
					listWordColorsToExclude.setItems(this.presetColorValues.externalNames().toArray(new String[0]));
				}
			}
		}
		{
			TabItem tbtmPowerpointOptions = new TabItem(tabFolder, SWT.NONE);
			tbtmPowerpointOptions.setText("Powerpoint Options");
			{
				Composite composite = new Composite(tabFolder, SWT.NONE);
				tbtmPowerpointOptions.setControl(composite);
				composite.setLayout(new GridLayout(2, false));
				Composite leftOptions = new Composite(composite, SWT.NONE);
				leftOptions.setLayout(new GridLayout());
				leftOptions.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
				{
					this.translatePowerpointDocumentProperties = new Button(leftOptions, SWT.CHECK);
					this.translatePowerpointDocumentProperties.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
					this.translatePowerpointDocumentProperties.setSelection(true);
					this.translatePowerpointDocumentProperties.setText(this.resourceBundle.getString("translate-document-properties"));
				}
				{
					this.translatePowerpointDiagramDataButton = new Button(leftOptions, SWT.CHECK);
					this.translatePowerpointDiagramDataButton.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
					this.translatePowerpointDiagramDataButton.setSelection(true);
					this.translatePowerpointDiagramDataButton.setText(this.resourceBundle.getString("translate-diagram-data"));
				}
				{
					this.translatePowerpointChartsButton = new Button(leftOptions, SWT.CHECK);
					this.translatePowerpointChartsButton.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
					this.translatePowerpointChartsButton.setSelection(true);
					this.translatePowerpointChartsButton.setText(this.resourceBundle.getString("translate-charts"));
				}
				{
					this.translatePowerpointNotesButton = new Button(leftOptions, SWT.CHECK);
					this.translatePowerpointNotesButton.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
					this.translatePowerpointNotesButton.setSelection(true);
					this.translatePowerpointNotesButton.setText(this.resourceBundle.getString("translate-notes"));
				}
				{
					this.translatePowerpointCommentsButton = new Button(leftOptions, SWT.CHECK);
					this.translatePowerpointCommentsButton.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
					this.translatePowerpointCommentsButton.setSelection(true);
					this.translatePowerpointCommentsButton.setText(this.resourceBundle.getString("translate-comments"));
				}
				{
					btnTranslateMasters = new Button(leftOptions, SWT.CHECK);
					btnTranslateMasters.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
					btnTranslateMasters.setSelection(true);
					btnTranslateMasters.setText("Translate Masters");
				}
				{
					btnIgnorePlaceholdersInMasters = new Button(leftOptions, SWT.CHECK);
					btnIgnorePlaceholdersInMasters.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
					btnIgnorePlaceholdersInMasters.setSelection(true);
					btnIgnorePlaceholdersInMasters.setText("Ignore Placeholder Text in Masters");
				}
				{
					this.translatePowerpointHidden = new Button(leftOptions, SWT.CHECK);
					this.translatePowerpointHidden.setLayoutData(new GridData(SWT.LEFT, SWT.TOP, false, false, 1, 1));
					this.translatePowerpointHidden.setSelection(true);
					this.translatePowerpointHidden.setText(this.resourceBundle.getString("translate-hidden"));
				}
				{
					this.translatePowerpointGraphicMetadata = new Button(leftOptions, SWT.CHECK);
					this.translatePowerpointGraphicMetadata.setLayoutData(new GridData(SWT.LEFT, SWT.TOP, false, false, 1, 1));
					this.translatePowerpointGraphicMetadata.setSelection(false);
					this.translatePowerpointGraphicMetadata.setText(this.resourceBundle.getString("translate-graphic-metadata"));
				}
				Composite rightOptions = new Composite(composite, SWT.NONE);
				rightOptions.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
				rightOptions.setLayout(new GridLayout());
				{
					this.reorderPowerpointDocumentPropertiesButton = new Button(rightOptions, SWT.CHECK);
					this.reorderPowerpointDocumentPropertiesButton.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
					this.reorderPowerpointDocumentPropertiesButton.setSelection(false);
					this.reorderPowerpointDocumentPropertiesButton.setText(this.resourceBundle.getString("reorder-document-properties"));
				}
				{
					this.reorderPowerpointDiagramDataButton = new Button(rightOptions, SWT.CHECK);
					this.reorderPowerpointDiagramDataButton.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
					this.reorderPowerpointDiagramDataButton.setSelection(false);
					this.reorderPowerpointDiagramDataButton.setText(this.resourceBundle.getString("reorder-diagram-data"));
				}
				{
					this.reorderPowerpointChartsButton = new Button(rightOptions, SWT.CHECK);
					this.reorderPowerpointChartsButton.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
					this.reorderPowerpointChartsButton.setSelection(false);
					this.reorderPowerpointChartsButton.setText(this.resourceBundle.getString("reorder-charts"));
				}
				{
					this.reorderPowerpointNotesButton = new Button(rightOptions, SWT.CHECK);
					this.reorderPowerpointNotesButton.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
					this.reorderPowerpointNotesButton.setSelection(true);
					this.reorderPowerpointNotesButton.setText(this.resourceBundle.getString("reorder-notes"));
				}
				{
					this.reorderPowerpointCommentsButton = new Button(rightOptions, SWT.CHECK);
					this.reorderPowerpointCommentsButton.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
					this.reorderPowerpointCommentsButton.setSelection(true);
					this.reorderPowerpointCommentsButton.setText(this.resourceBundle.getString("reorder-comments"));
				}
				{
					this.reorderPowerpointRelationshipsButton = new Button(rightOptions, SWT.CHECK);
					this.reorderPowerpointRelationshipsButton.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
					this.reorderPowerpointRelationshipsButton.setSelection(false);
					this.reorderPowerpointRelationshipsButton.setText(this.resourceBundle.getString("reorder-relationships"));
				}
				{
					btnIncludedSlideNumbersOnly = new Button(rightOptions, SWT.CHECK);
					btnIncludedSlideNumbersOnly.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
					btnIncludedSlideNumbersOnly.setSelection(false);
					btnIncludedSlideNumbersOnly.setText("Translate included slide numbers only");
				}
				{
					listPowerpointIncludedSlideNumbers = new List(rightOptions, SWT.BORDER | SWT.V_SCROLL | SWT.MULTI);
					GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
					gridData.heightHint = 40;
					listPowerpointIncludedSlideNumbers.setLayoutData(gridData);
					java.util.List<String> slideNumbers = new ArrayList<>();
					for (int i = 1; i < 100; i++) {
						slideNumbers.add(String.valueOf(i));
					}
					listPowerpointIncludedSlideNumbers.setItems(slideNumbers.toArray(new String[0]));
				}
			}
		}
		{
			TabItem tbtmExcelOptions = new TabItem(tabFolder, SWT.NONE);
			tbtmExcelOptions.setText("Excel Options");
			{
				Composite composite = new Composite(tabFolder, SWT.NONE);
				tbtmExcelOptions.setControl(composite);
				composite.setLayout(new GridLayout(2, false));
				Composite leftOptions = new Composite(composite, SWT.NONE);
				leftOptions.setLayout(new GridLayout());
				leftOptions.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
				{
					translateExcelHidden = new Button(leftOptions, SWT.CHECK);
					translateExcelHidden.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
					translateExcelHidden.setSelection(false);
					translateExcelHidden.setText(this.resourceBundle.getString("translate-hidden"));
				}
				{
					btnTranslateSheetNames = new Button(leftOptions, SWT.CHECK);
					btnTranslateSheetNames.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
					btnTranslateSheetNames.setSelection(true);
					btnTranslateSheetNames.setText("Translate Sheet Names");
				}
				{
					translateCellsCopied = new Button(leftOptions, SWT.CHECK);
					translateCellsCopied.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
					translateCellsCopied.setSelection(true);
					translateCellsCopied.setText(this.resourceBundle.getString("translate-cells-copied"));
				}
				{
					btnTranslateDiagramData = new Button(leftOptions, SWT.CHECK);
					btnTranslateDiagramData.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
					btnTranslateDiagramData.setSelection(true);
					btnTranslateDiagramData.setText("Translate Diagram Data (e.g. Smart Art)");
				}
				{
					btnTranslateDrawings = new Button(leftOptions, SWT.CHECK);
					btnTranslateDrawings.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
					btnTranslateDrawings.setSelection(true);
					btnTranslateDrawings.setText("Translate Drawings (e.g. Text fields)");
				}
				{
					preserveExcelStylesInTargetColumns = new Button(leftOptions, SWT.CHECK);
					preserveExcelStylesInTargetColumns.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
					preserveExcelStylesInTargetColumns.setSelection(false);
					preserveExcelStylesInTargetColumns.setText(this.resourceBundle.getString("preserve-styles-in-target-columns"));
				}
				{
					Label lblSubfilter = new Label(leftOptions, SWT.NONE);
					lblSubfilter.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
					lblSubfilter.setText("Name of subfilter for cell content:");
				}
				{
					edSubfilter = new Text(leftOptions, SWT.BORDER);
					GridData gd_subfilter = new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1);
					edSubfilter.setLayoutData(gd_subfilter);
				}
				{
					final Label l = new Label(leftOptions, SWT.NONE);
					l.setText(this.resourceBundle.getString("worksheet-configurations"));
					this.worksheetConfigurationsTable = this.params.worksheetConfigurations().writtenTo(
						new ResponsiveTableWorksheetConfigurationsOutput(
							new ResponsiveTable.Default(
								new Table(leftOptions, SWT.MULTI | SWT.BORDER | SWT.FULL_SELECTION),
								new Menu(this.shell, SWT.POP_UP),
								new Menu(this.shell, SWT.POP_UP)
							)
						)
					);
				}
				Composite rightOptions = new Composite(composite, SWT.NONE);
				rightOptions.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
				rightOptions.setLayout(new GridLayout());
				{
					Group g = new Group(rightOptions, SWT.NONE);
					g.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
					g.setLayout(new GridLayout());
					g.setText(this.resourceBundle.getString("excluded-font-colours"));
					listExcelColorsToExclude = new List(g, SWT.V_SCROLL | SWT.MULTI);
					GridData gd_listExcelColorsToExclude = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
					gd_listExcelColorsToExclude.heightHint = 40;
					listExcelColorsToExclude.setLayoutData(gd_listExcelColorsToExclude);
					listExcelColorsToExclude.setItems(this.presetColorValues.externalNames().toArray(new String[0]));
				}
			}
		}
		//--- Dialog-level buttons

		SelectionAdapter OKCancelActions = new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				result = false;
				if ( e.widget.getData().equals("h") ) {
					if ( help != null ) help.showWiki("OpenXML Filter");
					return;
				}
				if ( e.widget.getData().equals("o") ) {
					if ( !saveData() ) return;
					result = true;
				}
				shell.close();
			};
		};
		
		OKCancelPanel pnlActions = new OKCancelPanel(shell, SWT.NONE, OKCancelActions, true);
		gdTmp = new GridData(GridData.FILL_HORIZONTAL);
		pnlActions.setLayoutData(gdTmp);
		pnlActions.btOK.setEnabled(!readOnly);
		if ( !readOnly ) {
			shell.setDefaultButton(pnlActions.btOK);
		}

		shell.pack();
		Rectangle Rect = shell.getBounds();
		shell.setMinimumSize(Rect.width, Rect.height);
		Point startSize = shell.getMinimumSize();
		if ( startSize.x < 300 ) startSize.x = 300; 
		if ( startSize.y < 200 ) startSize.y = 200; 
		shell.setSize(new Point(541, 367));
		Dialogs.centerWindow(shell, parent);
		setData();
	}
	
	private boolean showDialog () {
		shell.open();
		while ( !shell.isDisposed() ) {
			if ( !shell.getDisplay().readAndDispatch() )
				shell.getDisplay().sleep();
		}
		return result;
	}
	
	protected void setData ()
	{		
		Iterator it;
		String sYmphony;
		String sRGB;
		Excell eggshell;
		String sDuraCell;
		String sMulti[];
		TreeSet<String> tsColors;
		Object o[];
		int ndx;
		int siz;
		btnTranslateDocumentProperties.setSelection(params.getTranslateDocProperties());
		this.translatePowerpointDocumentProperties.setSelection(params.getTranslatePowerpointDocProperties());
		this.reorderPowerpointDocumentPropertiesButton.setSelection(params.getReorderPowerpointDocProperties());
		this.reorderPowerpointRelationshipsButton.setSelection(params.getReorderPowerpointRelationships());
		this.translatePowerpointDiagramDataButton.setSelection(params.getTranslatePowerpointDiagramData());
		this.reorderPowerpointDiagramDataButton.setSelection(params.getReorderPowerpointDiagramData());
		this.translatePowerpointChartsButton.setSelection(params.getTranslatePowerpointCharts());
		this.reorderPowerpointChartsButton.setSelection(params.getReorderPowerpointCharts());
		this.translateCommentsButton.setSelection(params.getTranslateComments());
		this.translatePowerpointCommentsButton.setSelection(params.getTranslatePowerpointComments());
		this.reorderPowerpointCommentsButton.setSelection(params.getReorderPowerpointComments());
		btnCleanAggressively.setSelection(params.getCleanupAggressively());
		this.ignoreWhitespaceSttyles.setSelection(params.getIgnoreWhitespaceStyles());
		btnTreatTabAsChar.setSelection(params.getAddTabAsCharacter());
		btnTreatLineBreakAsChar.setSelection(params.getAddLineSeparatorCharacter());
		btnTranslateHeadersAndFooters.setSelection(params.getTranslateWordHeadersFooters());
		translateWordNumberingLevelText.setSelection(params.getTranslateWordNumberingLevelText());
		translateWordHidden.setSelection(params.getTranslateWordHidden());
		btnExcludeWordGraphicMetadata.setSelection(params.getTranslateWordExcludeGraphicMetaData());
		translatePowerpointHidden.setSelection(params.getTranslatePowerpointHidden());
		translatePowerpointGraphicMetadata.setSelection(params.getTranslatePowerpointGraphicMetadata());
		btnAutomaticallyAcceptRevisions.setSelection(params.getAutomaticallyAcceptRevisions());
		btnExtractExternalHyperlinks.setSelection(params.getExtractExternalHyperlinks());
		btnIgnoreSoftHyphen.setSelection(params.getIgnoreSoftHyphenTag());
        btnReplaceNonBreakingHyphen.setSelection(params.getReplaceNoBreakHyphenTag());
		this.allowWordStyleOptimisation.setSelection((params.getAllowWordStyleOptimisation()));
		this.preserveExcelStylesInTargetColumns.setSelection((params.getPreserveExcelStylesInTargetColumns()));
		this.translatePowerpointNotesButton.setSelection(params.getTranslatePowerpointNotes());
		this.reorderPowerpointNotesButton.setSelection(params.getReorderPowerpointNotes());
		btnTranslateMasters.setSelection(params.getTranslatePowerpointMasters());
		btnIgnorePlaceholdersInMasters.setSelection(params.getIgnorePlaceholdersInPowerpointMasters());
		btnTranslateSheetNames.setSelection(params.getTranslateExcelSheetNames());
		translateCellsCopied.setSelection(params.getTranslateExcelCellsCopied());
		btnTranslateDiagramData.setSelection(params.getTranslateExcelDiagramData());
		btnTranslateDrawings.setSelection(params.getTranslateExcelDrawings());
		this.ignoreFontColors.setSelection(params.getIgnoreWordFontColors());
		this.fontColorsMinIgnoranceThreshold.setText(params.getWordFontColorsMinIgnoranceThreshold());
		this.fontColorsMaxIgnoranceThreshold.setText(params.getWordFontColorsMaxIgnoranceThreshold());
		rdExcludeStyles.setSelection(params.getTranslateWordInExcludeStyleMode());
		rdIncludeStyles.setSelection(!params.getTranslateWordInExcludeStyleMode());
		rdExcludeHighlightStyles.setSelection(params.getTranslateWordInExcludeHighlightMode());
		rdIncludeHighlightStyles.setSelection(!params.getTranslateWordInExcludeHighlightMode());
		if (params.getTranslateWordInExcludeStyleMode()
			&& params.tsExcludeWordStyles!=null && !params.tsExcludeWordStyles.isEmpty()) {
			it = params.tsExcludeWordStyles.iterator();
			siz = params.tsExcludeWordStyles.size();
			if (siz>0)
			{
				sMulti = new String[siz];
				ndx = 0;
				while(it.hasNext())
				{
					sMulti[ndx++] = (String)it.next();
				}
				listExcludedWordStyles.setSelection(sMulti);
			}
		}
		if (params.getTranslateWordInExcludeHighlightMode()
			&& params.tsWordHighlightColors !=null && !params.tsWordHighlightColors.isEmpty()) {
			it = params.tsWordHighlightColors.iterator();
			siz = params.tsWordHighlightColors.size();
			if (siz>0)
			{
				sMulti = new String[siz];
				ndx = 0;
				while(it.hasNext())
				{
					final String v = (String) it.next();
                    final Color.Value cv = this.highlightColorValues.valueFor(v);
                    final String name = cv.asExternalName();
					if (!name.isEmpty()) {
						sMulti[ndx++] = name;
					}
				}
				listWordHightlightColorsToExclude.setSelection(sMulti);
			}
		}
		if (params.getTranslateWordExcludeColors() &&
			params.tsWordExcludedColors!=null && !params.tsWordExcludedColors.isEmpty())
		{
			setOfficeColors(params.tsWordExcludedColors, listWordColorsToExclude);
		}
		if (params.tsComplexFieldDefinitionsToExtract != null && !params.tsComplexFieldDefinitionsToExtract.isEmpty()) {
			it = params.tsComplexFieldDefinitionsToExtract.iterator();
			siz = params.tsComplexFieldDefinitionsToExtract.size();
			if (siz > 0)
			{
				sMulti = new String[siz];
				ndx = 0;
				while (it.hasNext())
				{
					sMulti[ndx++] = (String) it.next();
				}
				listTranslatableFields.setSelection(sMulti);
			}
		}
		if (params.getTranslateExcelExcludeColors() &&
			params.tsExcelExcludedColors!=null && !params.tsExcelExcludedColors.isEmpty())
		{
			setOfficeColors(params.tsExcelExcludedColors, listExcelColorsToExclude);
		}
		translateExcelHidden.setSelection(params.getTranslateExcelHidden());
		edSubfilter.setText(params.getSubfilter());
		btnIncludedSlideNumbersOnly.setSelection(params.getPowerpointIncludedSlideNumbersOnly());
		java.util.List<String> selectedSlideNumbers = new ArrayList<>();
		for (Integer slideNumber : params.tsPowerpointIncludedSlideNumbers) {
			selectedSlideNumbers.add(slideNumber.toString());
		}
		listPowerpointIncludedSlideNumbers.setSelection(selectedSlideNumbers.toArray(new String[0]));
	}
	
	private boolean saveData () {
		String sArray[];
		String sRGB;
		int len;
		params.reset();
		params.setTranslateDocProperties(btnTranslateDocumentProperties.getSelection());
		params.setTranslatePowerpointDocProperties(this.translatePowerpointDocumentProperties.getSelection());
		params.setReorderPowerpointDocProperties(this.reorderPowerpointDocumentPropertiesButton.getSelection());
		params.setReorderPowerpointRelationships(this.reorderPowerpointRelationshipsButton.getSelection());
		params.setTranslatePowerpointDiagramData(this.translatePowerpointDiagramDataButton.getSelection());
		params.setReorderPowerpointDiagramData(this.reorderPowerpointDiagramDataButton.getSelection());
		params.setTranslatePowerpointCharts(this.translatePowerpointChartsButton.getSelection());
		params.setReorderPowerpointCharts(this.reorderPowerpointChartsButton.getSelection());
		params.setTranslateComments(this.translateCommentsButton.getSelection());
		params.setTranslatePowerpointComments(this.translatePowerpointCommentsButton.getSelection());
		params.setReorderPowerpointComments(this.reorderPowerpointCommentsButton.getSelection());
		params.setCleanupAggressively(btnCleanAggressively.getSelection());
		params.setIgnoreWhitespaceStyles(this.ignoreWhitespaceSttyles.getSelection());
		params.setAddTabAsCharacter(btnTreatTabAsChar.getSelection());
		params.setAddLineSeparatorCharacter(btnTreatLineBreakAsChar.getSelection());
		this.params.fontMappings().addFrom(
			new ResponsiveTableFontMappingsInput(this.fontMappingsTable)
		);
		params.setTranslateWordHeadersFooters(btnTranslateHeadersAndFooters.getSelection());
		params.setTranslateWordNumberingLevelText(translateWordNumberingLevelText.getSelection());
		params.setTranslateWordHidden(translateWordHidden.getSelection());
		params.setTranslateWordExcludeGraphicMetaData(btnExcludeWordGraphicMetadata.getSelection());
		params.setTranslatePowerpointHidden(this.translatePowerpointHidden.getSelection());
		params.setTranslatePowerpointGraphicMetadata(this.translatePowerpointGraphicMetadata.getSelection());
		params.setAutomaticallyAcceptRevisions(btnAutomaticallyAcceptRevisions.getSelection());
		params.setIgnoreSoftHyphenTag(btnIgnoreSoftHyphen.getSelection());
        params.setReplaceNoBreakHyphenTag(btnReplaceNonBreakingHyphen.getSelection());
		params.setAllowWordStyleOptimisation(this.allowWordStyleOptimisation.getSelection());
		params.setPreserveExcelStylesInTargetColumns(this.preserveExcelStylesInTargetColumns.getSelection());
		params.setTranslatePowerpointNotes(this.translatePowerpointNotesButton.getSelection());
		params.setReorderPowerpointNotes(this.reorderPowerpointNotesButton.getSelection());
		params.setTranslatePowerpointMasters(btnTranslateMasters.getSelection());
		params.setIgnorePlaceholdersInPowerpointMasters(
				btnIgnorePlaceholdersInMasters.getSelection());
		params.setTranslateExcelHidden(this.translateExcelHidden.getSelection());
		params.setTranslateExcelSheetNames(btnTranslateSheetNames.getSelection());
		params.setTranslateExcelCellsCopied(translateCellsCopied.getSelection());
		params.setTranslateExcelDiagramData(btnTranslateDiagramData.getSelection());
		params.setTranslateExcelDrawings(btnTranslateDrawings.getSelection());
		params.setExtractExternalHyperlinks(btnExtractExternalHyperlinks.getSelection());
		this.params.setIgnoreWordFontColors(this.ignoreFontColors.getSelection());
		if (!this.fontColorsMinIgnoranceThreshold.getText().isEmpty()) {
			this.params.setWordFontColorsMinIgnoranceThreshold(this.fontColorsMinIgnoranceThreshold.getText());
		}
		if (!this.fontColorsMaxIgnoranceThreshold.getText().isEmpty()) {
			this.params.setWordFontColorsMaxIgnoranceThreshold(this.fontColorsMaxIgnoranceThreshold.getText());
		}
		params.setTranslateWordInExcludeStyleMode(rdExcludeStyles.getSelection());
		params.setTranslateWordInExcludeHighlightMode(rdExcludeHighlightStyles.getSelection());

		// Exclude text in certain styles from translation in Word
		sArray = listExcludedWordStyles.getSelection(); // selected items
		if (params.tsExcludeWordStyles==null)
			params.tsExcludeWordStyles = new TreeSet<>();
		else
			params.tsExcludeWordStyles.clear();
		len = sArray.length;
		if (len>0)
		{
			for(int i=0;i<len;i++)
			params.tsExcludeWordStyles.add(sArray[i]);
		}

		// Exclude text in certain colors from translation in Excel
		sArray = listWordHightlightColorsToExclude.getSelection(); // selected items
		if (params.tsWordHighlightColors == null)
			params.tsWordHighlightColors = new TreeSet<>();
		else
			params.tsWordHighlightColors.clear();
		len = sArray.length;
		if (len>0)
		{
			for(int i=0;i<len;i++)
			{
				params.tsWordHighlightColors.add(sArray[i]);
			}
		} else {
			params.setTranslateWordInExcludeHighlightMode(false);
		}

		// Exclude text in certain colors from translation in Word
		sArray = listWordColorsToExclude.getSelection(); // selected items
		if (params.tsWordExcludedColors==null)
			params.tsWordExcludedColors = new TreeSet<>();
		else
			params.tsWordExcludedColors.clear();
		len = sArray.length;
		if (len>0)
		{
			params.setTranslateWordExcludeColors(true);
			for(int i=0;i<len;i++)
			{
                final String name = this.presetColorValues.valueFor(sArray[i]).asExternalName();
				if (name.isEmpty()) {
					params.tsWordExcludedColors.add(sArray[i]);  // specific RGB value
				} else {
					params.tsWordExcludedColors.add(name);
				}
			}
		}
		else
			params.setTranslateWordExcludeColors(false);

		// Translate specific field types in Word
		sArray = listTranslatableFields.getSelection(); // selected items
		if (params.tsComplexFieldDefinitionsToExtract == null)
			params.tsComplexFieldDefinitionsToExtract = new TreeSet<>();
		else
			params.tsComplexFieldDefinitionsToExtract.clear();
		len = sArray.length;
		if (len>0)
		{
			for(int i=0;i<len;i++)
			params.tsComplexFieldDefinitionsToExtract.add(sArray[i]);
		}

		params.setTranslateExcelHidden(translateExcelHidden.getSelection());

		if (!edSubfilter.getText().isEmpty()) {
			params.setSubfilter(edSubfilter.getText());
		}
		this.params.worksheetConfigurations().addFrom(
			new ResponsiveTableWorksheetConfigurationsInput(this.worksheetConfigurationsTable)
		);

		// Exclude text in certain colors from translation in Excel
		sArray = listExcelColorsToExclude.getSelection(); // selected items
		if (params.tsExcelExcludedColors==null)
			params.tsExcelExcludedColors = new TreeSet<>();
		else
			params.tsExcelExcludedColors.clear();
		len = sArray.length;
		if (len>0)
		{
			params.setTranslateExcelExcludeColors(true);
			for(int i=0;i<len;i++)
			{
                final String name = this.presetColorValues.valueFor(sArray[i]).asExternalName();
				if (name.isEmpty()) {
					params.tsExcelExcludedColors.add(sArray[i]); // specific RGB value
				} else {
					params.tsExcelExcludedColors.add(name);
				}
			}
		}
		else
			params.setTranslateExcelExcludeColors(false);

		params.setPowerpointIncludedSlideNumbersOnly(btnIncludedSlideNumbersOnly.getSelection());
		if (params.tsPowerpointIncludedSlideNumbers == null) {
			params.tsPowerpointIncludedSlideNumbers = new TreeSet<>();
		}
		else {
			params.tsPowerpointIncludedSlideNumbers.clear();
		}
		if (btnIncludedSlideNumbersOnly.getSelection()) {
			java.util.List<Integer> slideNumbers = new ArrayList<>();
			for (String s : listPowerpointIncludedSlideNumbers.getSelection()) {
				slideNumbers.add(Integer.valueOf(s));
			}
			params.tsPowerpointIncludedSlideNumbers.addAll(slideNumbers);
		}
		params.nFileType = ParseType.MSWORD;
		return true;
	}

	/**
	 * Handle the Office Color code mapping.
	 *
	 * @param values the excluded colors from the parameter details.
	 * @param nameableRgbValues the list to update with the appropriate color name (or value if
	 * updated by hand).
	 */
	private void setOfficeColors(TreeSet<String> values, List nameableRgbValues) {
		TreeSet<String> tsColors;
		Iterator it;
		int siz;
		String[] sMulti;
		int ndx;
		tsColors = new TreeSet<>();
		it = values.iterator();

		while (it.hasNext()) {
			final String v = (String) it.next();
            final Color.Value cv = this.presetColorValues.valueFor(v);
			String name = cv.asExternalName();
			if (name.isEmpty()) {
				nameableRgbValues.add(v);
				tsColors.add(v);
			} else {
				tsColors.add(name);
			}
		}
		siz = tsColors.size();
		if (siz > 0) {
			sMulti = new String[siz];
			it = tsColors.iterator();
			ndx = 0;
			while (it.hasNext())
				sMulti[ndx++] = (String) it.next();
			nameableRgbValues.setSelection(sMulti);
		}
	}

}

