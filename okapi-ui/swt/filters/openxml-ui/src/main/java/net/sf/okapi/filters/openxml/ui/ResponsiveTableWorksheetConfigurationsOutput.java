/*
 * =============================================================================
 * Copyright (C) 2010-2022 by the Okapi Framework contributors
 * -----------------------------------------------------------------------------
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =============================================================================
 */
package net.sf.okapi.filters.openxml.ui;

import net.sf.okapi.common.ui.ResponsiveTable;
import net.sf.okapi.filters.openxml.WorksheetConfiguration;
import net.sf.okapi.filters.openxml.WorksheetConfigurations;

import java.util.Iterator;
import java.util.ResourceBundle;

final class ResponsiveTableWorksheetConfigurationsOutput implements WorksheetConfigurations.Output<ResponsiveTable> {
    private final ResponsiveTable responsiveTable;

    ResponsiveTableWorksheetConfigurationsOutput(final ResponsiveTable responsiveTable) {
        this.responsiveTable = responsiveTable;
    }

    @Override
    public ResponsiveTable writtenWith(final Iterator<WorksheetConfiguration> worksheetConfigurationsIterator) {
        final ResourceBundle rb = ResourceBundle.getBundle("net.sf.okapi.filters.openxml.ui.ResponsiveTableWorksheetConfigurationsOutput");
        this.responsiveTable.configureHeader(
            new String[] {
                rb.getString("name-pattern"),
                rb.getString("source-columns"),
                rb.getString("target-columns"),
                rb.getString("target-columns-max-characters"),
                rb.getString("excluded-rows"),
                rb.getString("excluded-columns"),
                rb.getString("metadata-rows"),
                rb.getString("metadata-columns")
            }
        );
        while (worksheetConfigurationsIterator.hasNext()) {
            worksheetConfigurationsIterator.next().writtenTo(
                new ResponsiveTableWorksheetConfigurationOutput(this.responsiveTable)
            );
        }
        this.responsiveTable.configureBody();
        return this.responsiveTable;
    }
}
