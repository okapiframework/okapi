/*
 * =============================================================================
 * Copyright (C) 2010-2022 by the Okapi Framework contributors
 * -----------------------------------------------------------------------------
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =============================================================================
 */
package net.sf.okapi.filters.idml.ui;

import net.sf.okapi.common.ui.ResponsiveTable;
import net.sf.okapi.common.ui.filters.InlineCodeFinderPanel;
import net.sf.okapi.common.ui.filters.ResponsiveTableFontMappingsInput;
import net.sf.okapi.common.ui.filters.ResponsiveTableFontMappingsOutput;
import net.sf.okapi.filters.idml.Parameters;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.Text;

import java.util.ResourceBundle;

interface Inputs {
    void configureFrom(final Parameters parameters);
    void saveTo(final Parameters parameters);

    final class Default implements Inputs {
        private final ResourceBundle resourceBundle;
        private final Shell shell;
        private Text maxAttributeSizeText;
        private Text specialCharacterPattern;
        private Button untagXmlStructuresButton;
        private Button extractNotesButton;
        private Button extractMasterSpreadsButton;
        private Button extractHiddenLayersButton;
        private Button extractHiddenPasteboardItemsButton;
        private Button skipDiscretionaryHyphensButton;
        private Button extractBreaksInlineButton;
        private Button extractHyperlinkTextSourcesInlineButton;
        private Button extractCustomTextVariablesButton;
        private Button extractIndexTopicsButton;
        private Button extractExternalHyperlinksButton;
        private ResponsiveTable fontMappingsTable;
        private Button ignoreCharacterKerningButton;
        private Text characterKerningMinIgnoranceThresholdText;
        private Text characterKerningMaxIgnoranceThresholdText;
        private Button ignoreCharacterTrackingButton;
        private Text characterTrackingMinIgnoranceThresholdText;
        private Text characterTrackingMaxIgnoranceThresholdText;
        private Button ignoreCharacterLeadingButton;
        private Text characterLeadingMinIgnoranceThresholdText;
        private Text characterLeadingMaxIgnoranceThresholdText;
        private Button ignoreCharacterBaselineShiftButton;
        private Text characterBaselineShiftMinIgnoranceThresholdText;
        private Text characterBaselineShiftMaxIgnoranceThresholdText;
        private Button useCodefinderButton;
        private InlineCodeFinderPanel pnlCodeFinder;

        Default(final ResourceBundle resourceBundle, final Shell shell) {
            this.resourceBundle = resourceBundle;
            this.shell = shell;
        }

        @Override
        public void configureFrom(final Parameters parameters) {
            final Composite c = new Composite(this.shell, SWT.NONE);
            c.setLayout(new GridLayout(2, false));
            final Composite lc = new Composite(c, SWT.NONE);
            lc.setLayout(new GridLayout());
            lc.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
            this.maxAttributeSizeText = labelAndTextFor(lc, "maximum-attribute-size", Integer.toUnsignedString(parameters.getMaxAttributeSize()));
            this.specialCharacterPattern = labelAndTextFor(lc, "special-character-pattern", parameters.specialCharacterPattern().toString());
            this.untagXmlStructuresButton = buttonFor(lc, "untag-xml-structures", parameters.getUntagXmlStructures());
            this.extractNotesButton = buttonFor(lc, "extract-notes", parameters.getExtractNotes());
            this.extractMasterSpreadsButton = buttonFor(lc, "extract-master-spreads", parameters.getExtractMasterSpreads());
            this.extractHiddenLayersButton = buttonFor(lc, "extract-hidden-layers", parameters.getExtractHiddenLayers());
            this.extractHiddenPasteboardItemsButton = buttonFor(lc, "extract-hidden-pasteboard-items", parameters.getExtractHiddenPasteboardItems());
            this.skipDiscretionaryHyphensButton = buttonFor(lc, "skip-discretionary-hyphens", parameters.getSkipDiscretionaryHyphens());
            this.extractBreaksInlineButton = buttonFor(lc, "extract-breaks-inline", parameters.getExtractBreaksInline());
            this.extractHyperlinkTextSourcesInlineButton = buttonFor(lc, "extract-hyperlink-text-sources-inline", parameters.getExtractHyperlinkTextSourcesInline());
            this.extractCustomTextVariablesButton = buttonFor(lc, "extract-custom-text-variables", parameters.getExtractCustomTextVariables());
            this.extractIndexTopicsButton = buttonFor(lc, "extract-index-topics", parameters.getExtractIndexTopics());
            this.extractExternalHyperlinksButton = buttonFor(lc, "extract-external-hyperlinks", parameters.getExtractExternalHyperlinks());
            labelFor(lc, "font-mappings");
            this.fontMappingsTable = parameters.fontMappings().writtenTo(
                new ResponsiveTableFontMappingsOutput(
                    new ResponsiveTable.Default(
                        new Table(lc, SWT.MULTI | SWT.BORDER | SWT.FULL_SELECTION),
                        new Menu(this.shell, SWT.POP_UP),
                        new Menu(this.shell, SWT.POP_UP)
                    )
                )
            );
            this.useCodefinderButton = buttonFor(lc, "use-codefinder", parameters.getUseCodeFinder());
            this.pnlCodeFinder = new InlineCodeFinderPanel(lc, SWT.NONE);
            this.pnlCodeFinder.setLayoutData(new GridData(GridData.FILL_BOTH));
            SelectionAdapter listener = new SelectionAdapter() {
                public void widgetSelected(SelectionEvent e) {
                    updateInlineCodes();
                };
            };
            this.pnlCodeFinder.setRules(parameters.getCodeFinder().toString());
            updateInlineCodes();
            this.pnlCodeFinder.updateDisplay();

            this.useCodefinderButton.addSelectionListener(listener);
            final Composite rc = new Composite(c, SWT.NONE);
            rc.setLayout(new GridLayout());
            rc.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
            labelFor(rc, "ignored-styles");
            this.ignoreCharacterKerningButton = buttonFor(rc, "ignore-character-kerning", parameters.getIgnoreCharacterKerning());
            this.characterKerningMinIgnoranceThresholdText = labelAndTextFor(rc, "character-kerning-minimum-ignorance-threshold", parameters.getCharacterKerningMinIgnoranceThreshold());
            this.characterKerningMaxIgnoranceThresholdText = labelAndTextFor(rc, "character-kerning-maximum-ignorance-threshold", parameters.getCharacterKerningMaxIgnoranceThreshold());
            this.ignoreCharacterTrackingButton = buttonFor(rc, "ignore-character-tracking", parameters.getIgnoreCharacterTracking());
            this.characterTrackingMinIgnoranceThresholdText = labelAndTextFor(rc, "character-tracking-minimum-ignorance-threshold", parameters.getCharacterTrackingMinIgnoranceThreshold());
            this.characterTrackingMaxIgnoranceThresholdText = labelAndTextFor(rc, "character-tracking-maximum-ignorance-threshold", parameters.getCharacterTrackingMaxIgnoranceThreshold());
            this.ignoreCharacterLeadingButton = buttonFor(rc, "ignore-character-leading", parameters.getIgnoreCharacterLeading());
            this.characterLeadingMinIgnoranceThresholdText = labelAndTextFor(rc, "character-leading-minimum-ignorance-threshold", parameters.getCharacterLeadingMinIgnoranceThreshold());
            this.characterLeadingMaxIgnoranceThresholdText = labelAndTextFor(rc, "character-leading-maximum-ignorance-threshold", parameters.getCharacterLeadingMaxIgnoranceThreshold());
            this.ignoreCharacterBaselineShiftButton = buttonFor(rc, "ignore-character-baseline-shift", parameters.getIgnoreCharacterBaselineShift());
            this.characterBaselineShiftMinIgnoranceThresholdText = labelAndTextFor(rc, "character-baseline-shift-minimum-ignorance-threshold", parameters.getCharacterBaselineShiftMinIgnoranceThreshold());
            this.characterBaselineShiftMaxIgnoranceThresholdText = labelAndTextFor(rc, "character-baseline-shift-maximum-ignorance-threshold", parameters.getCharacterBaselineShiftMaxIgnoranceThreshold());
        }

        private Button buttonFor(final Composite composite, final String key, final boolean selection) {
            final Button b = new Button(composite, SWT.CHECK);
            b.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
            b.setText(this.resourceBundle.getString(key));
            b.setSelection(selection);
            return b;
        }

        private Label labelFor(final Composite composite, final String key) {
            final Label l = new Label(composite, SWT.NONE);
            l.setText(this.resourceBundle.getString(key));
            return l;
        }

        private Text textFor(final Composite composite, final String text) {
            final Text t = new Text(composite, SWT.BORDER);
            t.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1));
            t.setText(text);
            return t;
        }

        private Text labelAndTextFor(final Composite composite, final String labelKey, final String text) {
            labelFor(composite, labelKey);
            return textFor(composite, text);
        }

        @Override
        public void saveTo(final Parameters parameters) {
            parameters.setMaxAttributeSize(Integer.parseUnsignedInt(this.maxAttributeSizeText.getText()));
            parameters.specialCharacterPattern(this.specialCharacterPattern.getText());
            parameters.setUntagXmlStructures(this.untagXmlStructuresButton.getSelection());
            parameters.setExtractNotes(this.extractNotesButton.getSelection());
            parameters.setExtractMasterSpreads(this.extractMasterSpreadsButton.getSelection());
            parameters.setExtractHiddenLayers(this.extractHiddenLayersButton.getSelection());
            parameters.setExtractHiddenPasteboardItems(this.extractHiddenPasteboardItemsButton.getSelection());
            parameters.setSkipDiscretionaryHyphens(this.skipDiscretionaryHyphensButton.getSelection());
            parameters.setExtractBreaksInline(this.extractBreaksInlineButton.getSelection());
            parameters.setExtractHyperlinkTextSourcesInline(this.extractHyperlinkTextSourcesInlineButton.getSelection());
            parameters.setExtractCustomTextVariables(this.extractCustomTextVariablesButton.getSelection());
            parameters.setExtractIndexTopics(this.extractIndexTopicsButton.getSelection());
            parameters.setExtractExternalHyperlinks(this.extractExternalHyperlinksButton.getSelection());
            parameters.fontMappings().addFrom(
                new ResponsiveTableFontMappingsInput(this.fontMappingsTable)
            );
            parameters.setIgnoreCharacterKerning(this.ignoreCharacterKerningButton.getSelection());
            parameters.setCharacterKerningMinIgnoranceThreshold(this.characterKerningMinIgnoranceThresholdText.getText());
            parameters.setCharacterKerningMaxIgnoranceThreshold(this.characterKerningMaxIgnoranceThresholdText.getText());
            parameters.setIgnoreCharacterTracking(this.ignoreCharacterTrackingButton.getSelection());
            parameters.setCharacterTrackingMinIgnoranceThreshold(this.characterTrackingMinIgnoranceThresholdText.getText());
            parameters.setCharacterTrackingMaxIgnoranceThreshold(this.characterTrackingMaxIgnoranceThresholdText.getText());
            parameters.setIgnoreCharacterLeading(this.ignoreCharacterLeadingButton.getSelection());
            parameters.setCharacterLeadingMinIgnoranceThreshold(this.characterLeadingMinIgnoranceThresholdText.getText());
            parameters.setCharacterLeadingMaxIgnoranceThreshold(this.characterLeadingMaxIgnoranceThresholdText.getText());
            parameters.setIgnoreCharacterBaselineShift(this.ignoreCharacterBaselineShiftButton.getSelection());
            parameters.setCharacterBaselineShiftMinIgnoranceThreshold(this.characterBaselineShiftMinIgnoranceThresholdText.getText());
            parameters.setCharacterBaselineShiftMaxIgnoranceThreshold(this.characterBaselineShiftMaxIgnoranceThresholdText.getText());
            parameters.setUseCodeFinder(this.useCodefinderButton.getSelection());
            if (this.useCodefinderButton.getSelection()) {
                parameters.setCodeFinderData(pnlCodeFinder.getRules());
            }
        }

        private void updateInlineCodes() {
            this.pnlCodeFinder.setEnabled(useCodefinderButton.getSelection());
        }
    }
}
