/*
 * =============================================================================
 * Copyright (C) 2010-2022 by the Okapi Framework contributors
 * -----------------------------------------------------------------------------
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =============================================================================
 */
package net.sf.okapi.common.ui.filters;

import net.sf.okapi.common.filters.fontmappings.FontMapping;
import net.sf.okapi.common.filters.fontmappings.FontMappings;
import net.sf.okapi.common.ui.ResponsiveTable;

import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public final class ResponsiveTableFontMappingsInput implements FontMappings.Input {
    private final ResponsiveTable responsiveTable;
    private List<FontMapping> fontMappings;

    public ResponsiveTableFontMappingsInput(final ResponsiveTable responsiveTable) {
        this.responsiveTable = responsiveTable;
    }

    @Override
    public Iterator<FontMapping> read() {
        if (null != this.fontMappings) {
            return Collections.emptyIterator();
        }
        this.fontMappings = new LinkedList<>();
        Arrays.stream(this.responsiveTable.rows()).forEach(i -> {
            final TableItemFontMapping fm = new TableItemFontMapping(i);
            fm.targetFont(); // force reading
            this.fontMappings.add(fm);
        });
        return this.fontMappings.iterator();
    }
}
