# Useful info on CI setup, integration with other services

## Overview

By adding a `.gitlab-ci.yml` file to the root directory of the source
repository and configuring the GitLab project to use
[a Runner](https://docs.gitlab.com/ee/ci/runners/README.html) you are
activating [GitLab's continuous integration service](https://about.gitlab.com/product/continuous-integration),
which in its turn will give you an ability to automatically trigger
your CI [pipeline](https://docs.gitlab.com/ee/ci/pipelines.html) for
each push to the repository. For more general information please refer
to [the getting started guide](https://docs.gitlab.com/ee/ci/quick_start/README.html).

## Triggering the Okapi project pipeline from the XLIFF Toolkit project pipeline

When you get done with setting up the Okapi project, make sure a trigger
token is available for sharing with the XLIFF Toolkit project.

You can add a new trigger by going to the project’s **Settings > CI/CD**
under **Triggers**. The **Add trigger** button will create a new token
which you can then use to trigger a rerun of this particular project’s
pipeline. For more information please refer to the corresponding section
of GitLab's documentation
[here](https://docs.gitlab.com/ee/ci/triggers/#adding-a-new-trigger).

## Sonatype integration

The following secret variables have to be declared under
**Settings > CI/CD > Variables**:

`MAVEN_REPO_USER`: sonatype user

`MAVEN_REPO_PASS`: sonatype user's password

`OPENSSL_ENC_KEY`: the OpenSSL key for decoding the code signing key

`OPENSSL_ENC_IV`: the OpenSSL initialisation vector for decoding the code signing key

`GPG_PASSPHRASE`: the pass-phrase for the code signing key

## Docker image creation & publishing

See https://gitlab.com/okapiframework/okapi/container_registry

But the "TLDR" flow is:

```sh
docker login registry.gitlab.com
# use okapiframework.robot with password or a Personal Access Token for login

docker build -t registry.gitlab.com/okapiframework/okapi:latest .
docker push registry.gitlab.com/okapiframework/okapi:latest
```

Real example:

```sh
docker build -t registry.gitlab.com/okapiframework/okapi:openjdk_17 .
docker push registry.gitlab.com/okapiframework/okapi:openjdk_17

# WARNING: note how the push uses `_`, not `.`
docker build -t registry.gitlab.com/okapiframework/okapi:openjdk_21 . -f Dockerfile_jdk21
docker push registry.gitlab.com/okapiframework/okapi:openjdk_21
```
