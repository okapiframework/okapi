package net.sf.okapi.filters.cascadingfilter;

import net.sf.okapi.common.Event;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.filters.FilterConfiguration;
import net.sf.okapi.common.filters.FilterTestDriver;
import net.sf.okapi.common.filters.ThreadSafeFilterConfigurationMapper;
import net.sf.okapi.common.resource.ITextUnit;
import net.sf.okapi.common.resource.RawDocument;
import net.sf.okapi.filters.html.HtmlFilter;
import net.sf.okapi.filters.json.JSONFilter;
import net.sf.okapi.filters.xmlstream.XmlStreamFilter;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.InputStream;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RunWith(JUnit4.class)

public class CascadingFilterTest {
    private CascadingFilter cascadingFilter;
    private FilterParameters maximal;
    private FilterParameters minimal;
    private ThreadSafeFilterConfigurationMapper mapper;
    // XML, containing pcdata JSON, containing HTML.  A feast!
    final String XML_JSON_HTML = "<json>{ \"key\" : \"&lt;p&gt;value&lt;\\/p&gt;\" }</json>";
    private FilterParameters feast;

    static Map<String, FilterConfiguration> loadConfigs() {
        var builder = new ThreadSafeFilterConfigurationMapper.ConfigBuilder();
        builder.addConfigurations(HtmlFilter.class);
        builder.addConfigurations(JSONFilter.class);
        builder.addConfigurations(XmlStreamFilter.class);
        return builder.build();
    }

    @Before
    public void setUp() {
        maximal = new FilterParameters(
                "okf_json",
                List.of("okf_html"),
                new JSONFilter().getParameters().toString(),
                Collections.singletonList(new HtmlFilter().getParameters().toString()));
        minimal = new FilterParameters(
                "okf_json",
                null,
                null,
                null);
        String XML_CONFIG =
                "assumeWellformed: true\n" +
                        "preserve_whitespace: true\n" +
                        "attributes:\n" +
                        "  xml:lang:\n" +
                        "    ruleTypes: [ATTRIBUTE_WRITABLE]\n" +
                        "  xml:id:\n" +
                        "    ruleTypes: [ATTRIBUTE_ID]\n" +
                        "  id:\n" +
                        "    ruleTypes: [ATTRIBUTE_ID]\n" +
                        "  xml:space:\n" +
                        "    ruleTypes: [ATTRIBUTE_PRESERVE_WHITESPACE]\n" +
                        "    preserve: ['xml:space', EQUALS, preserve]\n" +
                        "    default: ['xml:space', EQUALS, default]\n" +
                        "elements:\n" +
                        "  json:\n" +
                        "    ruleTypes: [TEXTUNIT]\n";
        feast = new FilterParameters(
                "okf_xmlstream",
                List.of("okf_json", "okf_html"),
                XML_CONFIG,
                List.of(new JSONFilter().getParameters().toString(), new HtmlFilter().getParameters().toString()));
        mapper = new ThreadSafeFilterConfigurationMapper(CascadingFilterTest::loadConfigs);
    }

    @Test
    public void extractMinimalWithStream() {
        cascadingFilter = CascadingFilter.create(minimal, mapper);
        final String original = "/test.json";
        InputStream originalDocument = getClass().getResourceAsStream(original);
        assert originalDocument != null;
        cascadingFilter.open(new RawDocument(originalDocument, "UTF-8",
                LocaleId.fromString("en-US"),
                LocaleId.fromString("es-ES")));
        List<Event> tus = cascadingFilter.stream().filter(Event::isTextUnit).collect(Collectors.toList());
        Assert.assertNotNull(tus);
        Assert.assertEquals(2, tus.size());

        ITextUnit tu = tus.get(0).getTextUnit();
        compareMinimalTu1(tu);
        tu = tus.get(1).getTextUnit();
        compareMinimalTu2(tu);

        cascadingFilter.close();
    }

    @Test
    public void extractMinimalNormally() {
        cascadingFilter = CascadingFilter.create(minimal, mapper);
        final String original = "/test.json";
        InputStream originalDocument = getClass().getResourceAsStream(original);
        assert originalDocument != null;
        List<Event> tus = FilterTestDriver.getTextUnitEvents(cascadingFilter,
                new RawDocument(originalDocument, "UTF-8",
                        LocaleId.fromString("en-US"),
                        LocaleId.fromString("es-ES")));

        Assert.assertNotNull(tus);
        Assert.assertEquals(2, tus.size());

        ITextUnit tu = tus.get(0).getTextUnit();
        compareMinimalTu1(tu);
        tu = tus.get(1).getTextUnit();
        compareMinimalTu2(tu);

        cascadingFilter.close();
    }

    @Test
    public void extractWithStream() {
        cascadingFilter = CascadingFilter.create(maximal, mapper);
        final String original = "/test.json";
        InputStream originalDocument = getClass().getResourceAsStream(original);
        assert originalDocument != null;
        cascadingFilter.open(new RawDocument(originalDocument, "UTF-8",
                LocaleId.fromString("en-US"),
                LocaleId.fromString("es-ES")));
        List<Event> tus = cascadingFilter.stream().filter(Event::isTextUnit).collect(Collectors.toList());
        Assert.assertNotNull(tus);
        Assert.assertEquals(3, tus.size());

        ITextUnit tu = tus.get(0).getTextUnit();
        compareMaximalTu1(tu);
        tu = tus.get(1).getTextUnit();
        compareMaximalTu2(tu);
        tu = tus.get(2).getTextUnit();
        compareMaximalTu3(tu);

        cascadingFilter.close();
    }

    @Test
    public void extractNormally() {
        final String original = "/test.json";
        InputStream originalDocument = getClass().getResourceAsStream(original);
        assert originalDocument != null;

        cascadingFilter = CascadingFilter.create(maximal, mapper);
        List<Event> tus = FilterTestDriver.getTextUnitEvents(cascadingFilter,
                new RawDocument(originalDocument, "UTF-8",
                LocaleId.fromString("en-US"),
                LocaleId.fromString("es-ES")));

        Assert.assertNotNull(tus);
        Assert.assertEquals(3, tus.size());

        ITextUnit tu = tus.get(0).getTextUnit();
        compareMaximalTu1(tu);
        tu = tus.get(1).getTextUnit();
        compareMaximalTu2(tu);
        tu = tus.get(2).getTextUnit();
        compareMaximalTu3(tu);

        cascadingFilter.close();
    }

    @Test
    public void extractFeastWithStream() {
        cascadingFilter = CascadingFilter.create(feast, mapper);
        cascadingFilter.open(new RawDocument(XML_JSON_HTML,
                LocaleId.fromString("en-US"),
                LocaleId.fromString("es-ES")));
        List<Event> tus = cascadingFilter.stream().filter(Event::isTextUnit).collect(Collectors.toList());
        Assert.assertNotNull(tus);
        Assert.assertEquals(1, tus.size());

        ITextUnit tu = tus.get(0).getTextUnit();
        compareFeastTu(tu);

        cascadingFilter.close();
    }

    private void compareMaximalTu1(ITextUnit tu) {
        Assert.assertEquals("tu1_sf1_tu1", tu.getId());
        Assert.assertEquals("one_1", tu.getName());
        Assert.assertEquals("text/html", tu.getMimeType());
        Assert.assertEquals("HTML <b><font>transitional</font></b> \"<mode\".", tu.getSource().toString());
    }

    private void compareMaximalTu2(ITextUnit tu) {
        Assert.assertEquals("tu1_sf1_tu2", tu.getId());
        Assert.assertEquals("one_2", tu.getName());
        Assert.assertEquals("text/html", tu.getMimeType());
        Assert.assertEquals("More text.", tu.getSource().toString());
    }

    private void compareMaximalTu3(ITextUnit tu) {
        Assert.assertEquals("tu2_sf2_tu1", tu.getId());
        Assert.assertEquals("two_1", tu.getName());
        Assert.assertEquals("text/html", tu.getMimeType());
        Assert.assertEquals("<i><u>emphasis&</u></i>", tu.getSource().toString());
    }

    private void compareMinimalTu1(ITextUnit tu) {
        Assert.assertEquals("tu1", tu.getId());
        Assert.assertEquals("one", tu.getName());
        Assert.assertEquals("application/json", tu.getMimeType());
        Assert.assertEquals("HTML <b><font>transitional</font></b> \"&lt;mode\".<p> More text.", tu.getSource().toString());
    }

    private void compareMinimalTu2(ITextUnit tu) {
        Assert.assertEquals("tu2", tu.getId());
        Assert.assertEquals("two", tu.getName());
        Assert.assertEquals("application/json", tu.getMimeType());
        Assert.assertEquals("<i><u>emphasis&amp;</u></i>", tu.getSource().toString());
    }

    private void compareFeastTu(ITextUnit tu) {
        Assert.assertEquals("tu1_sf1_tu1_sf1_tu1", tu.getId());
        Assert.assertEquals("key_1", tu.getName());
        Assert.assertEquals("text/html", tu.getMimeType());
        Assert.assertEquals("value", tu.getSource().toString());
    }
}
