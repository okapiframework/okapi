package net.sf.okapi.filters.vtt;

import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.encoder.EncoderManager;
import net.sf.okapi.common.resource.StartDocument;
import net.sf.okapi.common.resource.TextUnit;
import net.sf.okapi.common.skeleton.GenericSkeleton;
import net.sf.okapi.common.skeleton.ISkeletonWriter;
import net.sf.okapi.filters.subtitles.CaptionAnnotation;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(JUnit4.class)
public class VTTSkeletonWriterTest {
    private VTTFilter filter;

    @Before
    public void setUp() {
        filter = new VTTFilter();
        assertNotNull(filter);
    }

    public static VTTParameters getDefaultParams() {
        VTTParameters params = new VTTParameters();

        params.setTimeFormat("HH:mm:ss.SSS");
        params.setMaxLinesPerCaption(2);
        params.setMaxCharsPerLine(20);
        params.setSplitWords(true);

        return params;
    }

    @Test
    public void testProcessTextUnit() {
        VTTParameters params = getDefaultParams();
        params.setMaxLinesPerCaption(1);
        filter.setParameters(params);

        StartDocument sd = new StartDocument("sd");
        sd.setEncoding("UTF-8", false);
        sd.setName("docName");
        sd.setLineBreak("\n");
        sd.setLocale(LocaleId.ENGLISH);
        sd.setMultilingual(false);
        sd.setFilterParameters(filter.getParameters());

        TextUnit tu1 = new TextUnit("tu1", "This is an orange.");
        CaptionAnnotation annotation = makeCaptionAnnotation();
        annotation.add("00:00:02.680", "00:00:04.720");
        tu1.setAnnotation(annotation);
        GenericSkeleton skeleton = new GenericSkeleton();
        skeleton.add(new VTTSkeletonPart("00:00:02.680 --> 00:00:04.720", true));
        tu1.setSkeleton(skeleton);

        ISkeletonWriter sw = new VTTSkeletonWriter();
        sw.processStartDocument(LocaleId.FRENCH, "UTF-8", new EncoderManager(), sd);
        assertEquals("00:00:02.680 --> 00:00:04.720\nThis is an orange.\n", sw.processTextUnit(tu1));
    }

    @Test
    public void testProcessTextUnitSplitLines() {
        VTTParameters params = getDefaultParams();
        params.setMaxCharsPerLine(10);
        filter.setParameters(params);

        StartDocument sd = new StartDocument("sd");
        sd.setEncoding("UTF-8", false);
        sd.setName("docName");
        sd.setLineBreak("\n");
        sd.setLocale(LocaleId.ENGLISH);
        sd.setMultilingual(false);
        sd.setFilterParameters(filter.getParameters());

        TextUnit tu1 = new TextUnit("tu1", "This is an orange.");
        CaptionAnnotation annotation = makeCaptionAnnotation();
        annotation.add("00:00:02.680", "00:00:04.720");
        tu1.setAnnotation(annotation);
        GenericSkeleton skeleton = new GenericSkeleton();
        skeleton.add(new VTTSkeletonPart("00:00:02.680 --> 00:00:04.720", true));
        tu1.setSkeleton(skeleton);

        ISkeletonWriter sw = new VTTSkeletonWriter();
        sw.processStartDocument(LocaleId.FRENCH, "UTF-8", new EncoderManager(), sd);
        assertEquals("00:00:02.680 --> 00:00:04.720\nThis is an\norange.\n", sw.processTextUnit(tu1));
    }

    @Test
    public void testProcessTextUnitSplitCaptionsLines() {
        VTTParameters params = getDefaultParams();
        params.setMaxCharsPerLine(10);
        filter.setParameters(params);

        StartDocument sd = new StartDocument("sd");
        sd.setEncoding("UTF-8", false);
        sd.setName("docName");
        sd.setLineBreak("\n");
        sd.setLocale(LocaleId.ENGLISH);
        sd.setMultilingual(false);
        sd.setFilterParameters(filter.getParameters());

        TextUnit tu1 = new TextUnit("tu1", "This is an orange.");
        CaptionAnnotation annotation = makeCaptionAnnotation();
        annotation.add("00:00:02.680", "00:00:04.720");
        annotation.add("00:00:04.800", "00:00:06.960");
        tu1.setAnnotation(annotation);
        GenericSkeleton skeleton = new GenericSkeleton();
        skeleton.add(new VTTSkeletonPart("00:00:02.680 --> 00:00:04.720", true));
        skeleton.add(new VTTSkeletonPart("", false));
        skeleton.add(new VTTSkeletonPart("00:00:04.800 --> 00:00:06.960", true));
        tu1.setSkeleton(skeleton);

        ISkeletonWriter sw = new VTTSkeletonWriter();
        sw.processStartDocument(LocaleId.FRENCH, "UTF-8", new EncoderManager(), sd);
        assertEquals("00:00:02.680 --> 00:00:04.820\nThis is\nan\n\n00:00:04.820 --> 00:00:06.960\norange.\n", sw.processTextUnit(tu1));
    }

    @Test
    public void testProcessTextUnitSplitCaptionsLineOverflow() {
        VTTParameters params = getDefaultParams();
        params.setMaxCharsPerLine(5);
        filter.setParameters(params);

        StartDocument sd = new StartDocument("sd");
        sd.setEncoding("UTF-8", false);
        sd.setName("docName");
        sd.setLineBreak("\n");
        sd.setLocale(LocaleId.ENGLISH);
        sd.setMultilingual(false);
        sd.setFilterParameters(filter.getParameters());

        TextUnit tu1 = new TextUnit("tu1", "This is an orange.");
        CaptionAnnotation annotation = makeCaptionAnnotation();
        annotation.add("00:00:02.680", "00:00:04.720");
        annotation.add("00:00:04.800", "00:00:06.960");
        tu1.setAnnotation(annotation);
        GenericSkeleton skeleton = new GenericSkeleton();
        skeleton.add(new VTTSkeletonPart("00:00:02.680 --> 00:00:04.720", true));
        skeleton.add(new VTTSkeletonPart("", false));
        skeleton.add(new VTTSkeletonPart("00:00:04.800 --> 00:00:06.960", true));
        tu1.setSkeleton(skeleton);

        ISkeletonWriter sw = new VTTSkeletonWriter();
        sw.processStartDocument(LocaleId.FRENCH, "UTF-8", new EncoderManager(), sd);
        assertEquals("00:00:02.680 --> 00:00:04.820\nThis\nis an\n\n00:00:04.820 --> 00:00:06.960\noran\nge.\n", sw.processTextUnit(tu1));
    }

    @Test
    public void testProcessTextUnitSplitCaptionsCaptionOverflow() {
        VTTParameters params = getDefaultParams();
        params.setMaxCharsPerLine(5);
        filter.setParameters(params);

        StartDocument sd = new StartDocument("sd");
        sd.setEncoding("UTF-8", false);
        sd.setName("docName");
        sd.setLineBreak("\n");
        sd.setLocale(LocaleId.ENGLISH);
        sd.setMultilingual(false);
        sd.setFilterParameters(filter.getParameters());

        TextUnit tu1 = new TextUnit("tu1", "This is a hippopotamus.");
        CaptionAnnotation annotation = makeCaptionAnnotation();
        annotation.add("00:00:02.680", "00:00:04.720");
        annotation.add("00:00:04.800", "00:00:06.960");
        annotation.add("00:00:06.960", "00:00:09.100");
        tu1.setAnnotation(annotation);
        GenericSkeleton skeleton = new GenericSkeleton();
        skeleton.add(new VTTSkeletonPart("00:00:02.680 --> 00:00:04.720", true));
        skeleton.add(new VTTSkeletonPart("", false));
        skeleton.add(new VTTSkeletonPart("00:00:04.800 --> 00:00:06.960", true));
        skeleton.add(new VTTSkeletonPart("", false));
        skeleton.add(new VTTSkeletonPart("00:00:06.960 --> 00:00:09.100", true));
        tu1.setSkeleton(skeleton);

        ISkeletonWriter sw = new VTTSkeletonWriter();
        sw.processStartDocument(LocaleId.FRENCH, "UTF-8", new EncoderManager(), sd);
        assertEquals("00:00:02.680 --> 00:00:04.820\nThis\nis\n\n00:00:04.820 --> 00:00:06.960\na hi\nppop\n\n00:00:06.960 --> 00:00:09.100\notam\nus.\n", sw.processTextUnit(tu1));
    }

    @Test
    public void testProcessTextUnitWithCues() {
        VTTParameters params = getDefaultParams();
        params.setMaxLinesPerCaption(1);
        filter.setParameters(params);

        StartDocument sd = new StartDocument("sd");
        sd.setEncoding("UTF-8", false);
        sd.setName("docName");
        sd.setLineBreak("\n");
        sd.setLocale(LocaleId.ENGLISH);
        sd.setMultilingual(false);
        sd.setFilterParameters(filter.getParameters());

        TextUnit tu1 = new TextUnit("tu1", "This is an orange.");
        CaptionAnnotation annotation = makeCaptionAnnotation();
        annotation.add("00:00:02.680", "00:00:04.720");
        annotation.add("00:00:04.800", "00:00:06.960");
        tu1.setAnnotation(annotation);
        GenericSkeleton skeleton = new GenericSkeleton();
        skeleton.add(new VTTSkeletonPart("00:00:02.680 --> 00:00:04.720 align:middle line:84%", true));
        skeleton.add(new VTTSkeletonPart("", false));
        skeleton.add(new VTTSkeletonPart("00:00:04.800 --> 00:00:06.960 align:middle line:94%", true));
        tu1.setSkeleton(skeleton);

        ISkeletonWriter sw = new VTTSkeletonWriter();
        sw.processStartDocument(LocaleId.FRENCH, "UTF-8", new EncoderManager(), sd);
        assertEquals("00:00:02.680 --> 00:00:04.820 align:middle line:84%\nThis is an\n\n00:00:04.820 --> 00:00:06.960 align:middle line:94%\norange.\n", sw.processTextUnit(tu1));
    }

    @Test
    public void testProcessTextUnitDiscardCues() {
        VTTParameters params = getDefaultParams();
        params.setMaxLinesPerCaption(1);
        params.setDiscardCues(true);
        filter.setParameters(params);

        StartDocument sd = new StartDocument("sd");
        sd.setEncoding("UTF-8", false);
        sd.setName("docName");
        sd.setLineBreak("\n");
        sd.setLocale(LocaleId.ENGLISH);
        sd.setMultilingual(false);
        sd.setFilterParameters(filter.getParameters());

        TextUnit tu1 = new TextUnit("tu1", "This is an orange.");
        CaptionAnnotation annotation = makeCaptionAnnotation();
        annotation.add("00:00:02.680", "00:00:04.720");
        annotation.add("00:00:04.800", "00:00:06.960");
        tu1.setAnnotation(annotation);
        GenericSkeleton skeleton = new GenericSkeleton();
        skeleton.add(new VTTSkeletonPart("00:00:02.680 --> 00:00:04.720 align:middle line:84%", true));
        skeleton.add(new VTTSkeletonPart("", false));
        skeleton.add(new VTTSkeletonPart("00:00:04.800 --> 00:00:06.960 align:middle line:94%", true));
        tu1.setSkeleton(skeleton);

        ISkeletonWriter sw = new VTTSkeletonWriter();
        sw.processStartDocument(LocaleId.FRENCH, "UTF-8", new EncoderManager(), sd);
        assertEquals("00:00:02.680 --> 00:00:04.820\nThis is an\n\n00:00:04.820 --> 00:00:06.960\norange.\n", sw.processTextUnit(tu1));
    }

    @Test
    public void testProcessTextUnitOverwriteCues() {
        VTTParameters params = getDefaultParams();
        params.setMaxLinesPerCaption(1);
        params.setCueSettings("align:middle line:100%");
        filter.setParameters(params);

        StartDocument sd = new StartDocument("sd");
        sd.setEncoding("UTF-8", false);
        sd.setName("docName");
        sd.setLineBreak("\n");
        sd.setLocale(LocaleId.ENGLISH);
        sd.setMultilingual(false);
        sd.setFilterParameters(filter.getParameters());

        TextUnit tu1 = new TextUnit("tu1", "This is an orange.");
        CaptionAnnotation annotation = makeCaptionAnnotation();
        annotation.add("00:00:02.680", "00:00:04.720");
        annotation.add("00:00:04.800", "00:00:06.960");
        tu1.setAnnotation(annotation);
        GenericSkeleton skeleton = new GenericSkeleton();
        skeleton.add(new VTTSkeletonPart("00:00:02.680 --> 00:00:04.720 align:middle line:84%", true));
        skeleton.add(new VTTSkeletonPart("", false));
        skeleton.add(new VTTSkeletonPart("00:00:04.800 --> 00:00:06.960 align:middle line:94%", true));
        tu1.setSkeleton(skeleton);

        ISkeletonWriter sw = new VTTSkeletonWriter();
        sw.processStartDocument(LocaleId.FRENCH, "UTF-8", new EncoderManager(), sd);
        assertEquals("00:00:02.680 --> 00:00:04.820 align:middle line:100%\nThis is an\n\n00:00:04.820 --> 00:00:06.960 align:middle line:100%\norange.\n", sw.processTextUnit(tu1));
    }

    @Test
    public void testProcessTextUnitKeepTimings() {
        VTTParameters params = getDefaultParams();
        params.setMaxLinesPerCaption(1);
        params.setKeepTimecodes(true);
        filter.setParameters(params);

        StartDocument sd = new StartDocument("sd");
        sd.setEncoding("UTF-8", false);
        sd.setName("docName");
        sd.setLineBreak("\n");
        sd.setLocale(LocaleId.ENGLISH);
        sd.setMultilingual(false);
        sd.setFilterParameters(filter.getParameters());

        TextUnit tu1 = new TextUnit("tu1", "This is an orange.");
        CaptionAnnotation annotation = makeCaptionAnnotation();
        annotation.add("00:00:02.680", "00:00:04.720");
        annotation.add("00:00:04.800", "00:00:06.960");
        tu1.setAnnotation(annotation);
        GenericSkeleton skeleton = new GenericSkeleton();
        skeleton.add(new VTTSkeletonPart("00:00:02.680 --> 00:00:04.720", true));
        skeleton.add(new VTTSkeletonPart("", false));
        skeleton.add(new VTTSkeletonPart("00:00:04.800 --> 00:00:06.960", true));
        tu1.setSkeleton(skeleton);

        ISkeletonWriter sw = new VTTSkeletonWriter();
        sw.processStartDocument(LocaleId.FRENCH, "UTF-8", new EncoderManager(), sd);
        assertEquals("00:00:02.680 --> 00:00:04.720\nThis is an\n\n00:00:04.800 --> 00:00:06.960\norange.\n", sw.processTextUnit(tu1));
    }

    @Test
    public void testProcessTextUnitWithChapters() {
        VTTParameters params = getDefaultParams();
        params.setMaxLinesPerCaption(1);
        filter.setParameters(params);

        StartDocument sd = new StartDocument("sd");
        sd.setEncoding("UTF-8", false);
        sd.setName("docName");
        sd.setLineBreak("\n");
        sd.setLocale(LocaleId.ENGLISH);
        sd.setMultilingual(false);
        sd.setFilterParameters(filter.getParameters());

        TextUnit tu1 = new TextUnit("tu1", "This is an orange.");
        CaptionAnnotation annotation = makeCaptionAnnotation();
        annotation.add("00:00:02.680", "00:00:04.720");
        annotation.add("00:00:04.800", "00:00:06.960");
        tu1.setAnnotation(annotation);
        GenericSkeleton skeleton = new GenericSkeleton();
        skeleton.add(new VTTSkeletonPart("1", false));
        skeleton.add(new VTTSkeletonPart("00:00:02.680 --> 00:00:04.720", true));
        skeleton.add(new VTTSkeletonPart("", false));
        skeleton.add(new VTTSkeletonPart("2", false));
        skeleton.add(new VTTSkeletonPart("00:00:04.800 --> 00:00:06.960", true));
        tu1.setSkeleton(skeleton);

        ISkeletonWriter sw = new VTTSkeletonWriter();
        sw.processStartDocument(LocaleId.FRENCH, "UTF-8", new EncoderManager(), sd);
        assertEquals("1\n00:00:02.680 --> 00:00:04.820\nThis is an\n\n2\n00:00:04.820 --> 00:00:06.960\norange.\n", sw.processTextUnit(tu1));
    }

    @Test
    public void testProcessTextUnitSplitCaptionsChineseNoSpaces() {
        VTTParameters params = getDefaultParams();
        params.setMaxCharsPerLine(10);
        filter.setParameters(params);

        StartDocument sd = new StartDocument("sd");
        sd.setEncoding("UTF-8", false);
        sd.setName("docName");
        sd.setLineBreak("\n");
        sd.setLocale(LocaleId.ENGLISH);
        sd.setMultilingual(false);
        sd.setFilterParameters(filter.getParameters());

        TextUnit tu1 = new TextUnit("tu1", "在本系列迄今为止的会议中，我们专注于以善意的方式注意当下正在发生的事情。");
        CaptionAnnotation annotation = makeCaptionAnnotation();
        annotation.add("00:00:02.680", "00:00:04.720");
        annotation.add("00:00:04.800", "00:00:06.960");
        annotation.add("00:00:06.960", "00:00:09.100");
        tu1.setAnnotation(annotation);
        GenericSkeleton skeleton = new GenericSkeleton();
        skeleton.add(new VTTSkeletonPart("00:00:02.680 --> 00:00:04.720", true));
        skeleton.add(new VTTSkeletonPart("", false));
        skeleton.add(new VTTSkeletonPart("00:00:04.800 --> 00:00:06.960", true));
        skeleton.add(new VTTSkeletonPart("", false));
        skeleton.add(new VTTSkeletonPart("00:00:06.960 --> 00:00:09.100", true));
        tu1.setSkeleton(skeleton);

        ISkeletonWriter sw = new VTTSkeletonWriter();
        sw.processStartDocument(LocaleId.CHINA_CHINESE, "UTF-8", new EncoderManager(), sd);
        assertEquals("00:00:02.680 --> 00:00:04.820\n在本系列迄今\n为止的会议中\n\n00:00:04.820 --> 00:00:06.960\n，我们专注于\n以善意的方式\n\n00:00:06.960 --> 00:00:09.100\n注意当下正在\n发生的事情。\n", sw.processTextUnit(tu1));
    }

    @Test
    public void testProcessTextUnitSplitCaptionsKoreanWithSpaces() {
        VTTParameters params = getDefaultParams();
        params.setMaxCharsPerLine(5);
        filter.setParameters(params);

        StartDocument sd = new StartDocument("sd");
        sd.setEncoding("UTF-8", false);
        sd.setName("docName");
        sd.setLineBreak("\n");
        sd.setLocale(LocaleId.ENGLISH);
        sd.setMultilingual(false);
        sd.setFilterParameters(filter.getParameters());

        TextUnit tu1 = new TextUnit("tu1", "걸 환영합니다");
        CaptionAnnotation annotation = makeCaptionAnnotation();
        annotation.add("00:00:02.680", "00:00:04.720");
        tu1.setAnnotation(annotation);
        GenericSkeleton skeleton = new GenericSkeleton();
        skeleton.add(new VTTSkeletonPart("00:00:02.680 --> 00:00:04.720", true));
        tu1.setSkeleton(skeleton);

        ISkeletonWriter sw = new VTTSkeletonWriter();
        sw.processStartDocument(LocaleId.KOREAN, "UTF-8", new EncoderManager(), sd);
        assertEquals("00:00:02.680 --> 00:00:04.720\n걸\n환영합니다\n", sw.processTextUnit(tu1));
    }

    @Test
    public void testProcessTextUnitOverflow() {
        VTTParameters params = getDefaultParams();
        params.setMaxCharsPerLine(5);
        filter.setParameters(params);

        StartDocument sd = new StartDocument("sd");
        sd.setEncoding("UTF-8", false);
        sd.setName("docName");
        sd.setLineBreak("\n");
        sd.setLocale(LocaleId.ENGLISH);
        sd.setMultilingual(false);
        sd.setFilterParameters(filter.getParameters());

        TextUnit tu1 = new TextUnit("tu1", "This is an orange.");
        CaptionAnnotation annotation = makeCaptionAnnotation();
        annotation.add("00:00:02.680", "00:00:04.720");
        tu1.setAnnotation(annotation);
        GenericSkeleton skeleton = new GenericSkeleton();
        skeleton.add(new VTTSkeletonPart("00:00:02.680 --> 00:00:04.720", true));
        tu1.setSkeleton(skeleton);

        ISkeletonWriter sw = new VTTSkeletonWriter();
        sw.processStartDocument(LocaleId.FRENCH, "UTF-8", new EncoderManager(), sd);
        assertEquals("00:00:02.680 --> 00:00:04.720\nThis is a\nn orange.\n", sw.processTextUnit(tu1));
    }

    @Test
    public void testProcessTextUnitWithLongWords() {
        VTTParameters params = getDefaultParams();
        params.setMaxCharsPerLine(50);
        filter.setParameters(params);

        StartDocument sd = new StartDocument("sd");
        sd.setEncoding("UTF-8", false);
        sd.setName("docName");
        sd.setLineBreak("\n");
        sd.setLocale(LocaleId.ENGLISH);
        sd.setMultilingual(false);
        sd.setFilterParameters(filter.getParameters());

        TextUnit tu1 = new TextUnit("tu1",
                "Lo rem ip sumdolo rsi tamet cons extetur adipiscing elit seddo eiusmodtempor, " +
                        "incid idu ntutla boreetd oloremagnaaliq, uau ten imad mi nimv eniamqu.");
        CaptionAnnotation annotation = makeCaptionAnnotation();
        annotation.add("00:00:02.680", "00:00:04.820");
        annotation.add("00:00:04.820", "00:00:06.960");
        annotation.add("00:00:06.960", "00:00:09.100");
        annotation.add("00:00:09.100", "00:00:11.240");
        annotation.add("00:00:11.240", "00:00:13.380");
        tu1.setAnnotation(annotation);
        GenericSkeleton skeleton = new GenericSkeleton();
        skeleton.add(new VTTSkeletonPart("00:00:02.680 --> 00:00:04.820", true));
        skeleton.add(new VTTSkeletonPart("", false));
        skeleton.add(new VTTSkeletonPart("00:00:04.820 --> 00:00:06.960", true));
        skeleton.add(new VTTSkeletonPart("", false));
        skeleton.add(new VTTSkeletonPart("00:00:06.960 --> 00:00:09.100", true));
        skeleton.add(new VTTSkeletonPart("", false));
        skeleton.add(new VTTSkeletonPart("00:00:09.100 --> 00:00:11.240", true));
        skeleton.add(new VTTSkeletonPart("", false));
        skeleton.add(new VTTSkeletonPart("00:00:11.240 --> 00:00:13.380", true));
        tu1.setSkeleton(skeleton);

        ISkeletonWriter sw = new VTTSkeletonWriter();
        sw.processStartDocument(LocaleId.FRENCH, "UTF-8", new EncoderManager(), sd);
        assertEquals("00:00:02.680 --> 00:00:04.820\n" +
                "Lo rem ip sumdolo\n" +
                "rsi tamet cons\n" +
                "\n" +
                "00:00:04.820 --> 00:00:06.960\n" +
                "extetur adipiscing\n" +
                "elit seddo\n" +
                "\n" +
                "00:00:06.960 --> 00:00:09.100\n" +
                "eiusmodtempor, incid\n" +
                "idu ntutla\n" +
                "\n" +
                "00:00:09.100 --> 00:00:11.240\n" +
                "boreetd oloremagnaaliq,\n" +
                "uau ten\n" +
                "\n" +
                "00:00:11.240 --> 00:00:13.380\n" +
                "imad mi nimv\n" +
                "eniamqu.\n", sw.processTextUnit(tu1));
    }

    @Test
    public void testProcessTextUnitWithFewWords() {
        VTTParameters params = getDefaultParams();
        params.setMaxCharsPerLine(50);
        filter.setParameters(params);

        StartDocument sd = new StartDocument("sd");
        sd.setEncoding("UTF-8", false);
        sd.setName("docName");
        sd.setLineBreak("\n");
        sd.setLocale(LocaleId.ENGLISH);
        sd.setMultilingual(false);
        sd.setFilterParameters(filter.getParameters());

        TextUnit tu1 = new TextUnit("tu1",
                "Lorem ipsumdolor sit amet consec tetur.");
        CaptionAnnotation annotation = makeCaptionAnnotation();
        annotation.add("00:00:02.680", "00:00:04.820");
        annotation.add("00:00:04.820", "00:00:06.960");
        annotation.add("00:00:06.960", "00:00:09.100");
        annotation.add("00:00:09.100", "00:00:11.240");
        tu1.setAnnotation(annotation);
        GenericSkeleton skeleton = new GenericSkeleton();
        skeleton.add(new VTTSkeletonPart("00:00:02.680 --> 00:00:04.820", true));
        skeleton.add(new VTTSkeletonPart("", false));
        skeleton.add(new VTTSkeletonPart("00:00:04.820 --> 00:00:06.960", true));
        skeleton.add(new VTTSkeletonPart("", false));
        skeleton.add(new VTTSkeletonPart("00:00:06.960 --> 00:00:09.100", true));
        skeleton.add(new VTTSkeletonPart("", false));
        skeleton.add(new VTTSkeletonPart("00:00:09.100 --> 00:00:11.240", true));
        tu1.setSkeleton(skeleton);

        ISkeletonWriter sw = new VTTSkeletonWriter();
        sw.processStartDocument(LocaleId.FRENCH, "UTF-8", new EncoderManager(), sd);
        assertEquals("00:00:02.680 --> 00:00:04.820\n" +
                "Lorem\n" +
                "\n" +
                "00:00:04.820 --> 00:00:06.960\n" +
                "ipsumdolor\n" +
                "\n" +
                "00:00:06.960 --> 00:00:09.100\n" +
                "sit amet\n" +
                "\n" +
                "00:00:09.100 --> 00:00:11.240\n" +
                "consec tetur.\n", sw.processTextUnit(tu1));
    }

    @Test
    public void testProcessTextUnitFewWordsJapanese() {
        VTTParameters params = getDefaultParams();
        params.setMaxCharsPerLine(50);
        params.setKeepTimecodes(true);
        filter.setParameters(params);

        StartDocument sd = new StartDocument("sd");
        sd.setEncoding("UTF-8", false);
        sd.setName("docName");
        sd.setLineBreak("\n");
        sd.setLocale(LocaleId.JAPANESE);
        sd.setMultilingual(false);
        sd.setFilterParameters(filter.getParameters());

        TextUnit tu1 = new TextUnit("tu1",
                "いろはにほてと いろはに ちりぬるを わかよたれそ");
        CaptionAnnotation annotation = makeCaptionAnnotation();
        annotation.add("00:00:02.680", "00:00:04.820");
        annotation.add("00:00:04.820", "00:00:06.960");
        annotation.add("00:00:06.960", "00:00:09.100");
        annotation.add("00:00:09.100", "00:00:11.240");
        annotation.add("00:00:09.100", "00:00:11.240");
        annotation.add("00:00:09.100", "00:00:11.240");
        tu1.setAnnotation(annotation);
        GenericSkeleton skeleton = new GenericSkeleton();
        skeleton.add(new VTTSkeletonPart("00:47:45.480 --> 00:47:48.480", true));
        skeleton.add(new VTTSkeletonPart("", false));
        skeleton.add(new VTTSkeletonPart("00:47:48.560 --> 00:47:50.240", true));
        skeleton.add(new VTTSkeletonPart("", false));
        skeleton.add(new VTTSkeletonPart("00:47:50.320 --> 00:47:52.760", true));
        skeleton.add(new VTTSkeletonPart("", false));
        skeleton.add(new VTTSkeletonPart("00:47:52.840 --> 00:47:54.240", true));
        skeleton.add(new VTTSkeletonPart("", false));
        skeleton.add(new VTTSkeletonPart("00:47:54.320 --> 00:47:57.200", true));
        skeleton.add(new VTTSkeletonPart("", false));
        skeleton.add(new VTTSkeletonPart("00:47:57.280 --> 00:47:59.440", true));
        tu1.setSkeleton(skeleton);

        ISkeletonWriter sw = new VTTSkeletonWriter();
        sw.processStartDocument(LocaleId.JAPANESE, "UTF-8", new EncoderManager(), sd);
        assertEquals("00:00:02.680 --> 00:00:04.820\n" +
                "いろはにほ\n" +
                "\n" +
                "00:00:04.820 --> 00:00:06.960\n" +
                "てと い\n" +
                "\n" +
                "00:00:06.960 --> 00:00:09.100\n" +
                "ろはに\n" +
                "\n" +
                "00:00:09.100 --> 00:00:11.240\n" +
                "ちりぬる\n" +
                "\n" +
                "00:00:09.100 --> 00:00:11.240\n" +
                "を わか\n" +
                "\n" +
                "00:00:09.100 --> 00:00:11.240\n" +
                "よたれそ\n", sw.processTextUnit(tu1));
    }

    @Test
    public void testProcessTextUnitDontSplitWords() {
        VTTParameters params = getDefaultParams();
        params.setMaxCharsPerLine(5);
        params.setSplitWords(false);
        filter.setParameters(params);

        StartDocument sd = new StartDocument("sd");
        sd.setEncoding("UTF-8", false);
        sd.setName("docName");
        sd.setLineBreak("\n");
        sd.setLocale(LocaleId.ENGLISH);
        sd.setMultilingual(false);
        sd.setFilterParameters(filter.getParameters());

        TextUnit tu1 = new TextUnit("tu1", "This is a hippopotamus.");
        CaptionAnnotation annotation = makeCaptionAnnotation();
        annotation.add("00:00:02.680", "00:00:04.720");
        tu1.setAnnotation(annotation);
        GenericSkeleton skeleton = new GenericSkeleton();
        skeleton.add(new VTTSkeletonPart("00:00:02.680 --> 00:00:04.720", true));
        tu1.setSkeleton(skeleton);

        ISkeletonWriter sw = new VTTSkeletonWriter();
        sw.processStartDocument(LocaleId.FRENCH, "UTF-8", new EncoderManager(), sd);
        assertEquals("00:00:02.680 --> 00:00:04.720\nThis is\na hippopotamus.\n", sw.processTextUnit(tu1));
    }

    @Test
    public void testWrongParameters() {
        VTTParameters params = getDefaultParams();
        params.setMaxLinesPerCaption(0);
        params.setMaxCharsPerLine(0);
        params.setTimeFormat("");
        params.setKeepTimecodes(true);
        filter.setParameters(params);

        StartDocument sd = new StartDocument("sd");
        sd.setEncoding("UTF-8", false);
        sd.setName("docName");
        sd.setLineBreak("\n");
        sd.setLocale(LocaleId.ENGLISH);
        sd.setMultilingual(false);
        sd.setFilterParameters(filter.getParameters());

        TextUnit tu1 = new TextUnit("tu1", "This is an orange.");
        CaptionAnnotation annotation = makeCaptionAnnotation();
        annotation.add("00:00:02.680", "00:00:04.720");
        tu1.setAnnotation(annotation);
        GenericSkeleton skeleton = new GenericSkeleton();
        skeleton.add(new VTTSkeletonPart("00:00:02.680 --> 00:00:04.720", true));
        tu1.setSkeleton(skeleton);

        ISkeletonWriter sw = new VTTSkeletonWriter();
        sw.processStartDocument(LocaleId.FRENCH, "UTF-8", new EncoderManager(), sd);
        assertEquals("00:00:02.680 --> 00:00:04.720\nThis is an\norange.\n", sw.processTextUnit(tu1));
    }

    private CaptionAnnotation makeCaptionAnnotation() {
        CaptionAnnotation annotation = filter.makeCaptionAnnotation();
        annotation.setMaxLine(filter.getParameters().getMaxLinesPerCaption());
        annotation.setMaxChar(filter.getParameters().getMaxCharsPerLine());
        return annotation;
    }
}