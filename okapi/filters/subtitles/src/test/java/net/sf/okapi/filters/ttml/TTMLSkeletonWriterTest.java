package net.sf.okapi.filters.ttml;

import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.encoder.EncoderManager;
import net.sf.okapi.common.filters.InlineCodeFinder;
import net.sf.okapi.common.resource.StartDocument;
import net.sf.okapi.common.resource.TextUnit;
import net.sf.okapi.common.skeleton.GenericSkeleton;
import net.sf.okapi.common.skeleton.ISkeletonWriter;
import net.sf.okapi.filters.subtitles.CaptionAnnotation;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(JUnit4.class)
public class TTMLSkeletonWriterTest {
    private TTMLFilter filter;

    @Before
    public void setUp() {
        filter = new TTMLFilter();
        assertNotNull(filter);
    }

    public static TTMLParameters getDefaultParams() {
        TTMLParameters params = new TTMLParameters();

        params.setTimeFormat("HH:mm:ss.SSS");
        params.setMaxLinesPerCaption(2);
        params.setMaxCharsPerLine(20);
        params.setSplitWords(true);
        params.setUseCodeFinder(false);

        return params;
    }

    @Test
    public void testProcessTextUnit() {
        TTMLParameters params = getDefaultParams();
        params.setMaxLinesPerCaption(1);
        filter.setParameters(params);

        StartDocument sd = new StartDocument("sd");
        sd.setEncoding("UTF-8", false);
        sd.setName("docName");
        sd.setLineBreak("\n");
        sd.setLocale(LocaleId.ENGLISH);
        sd.setMultilingual(false);
        sd.setFilterParameters(filter.getParameters());

        TextUnit tu1 = new TextUnit("tu1", "This is an orange.");
        CaptionAnnotation annotation = makeCaptionAnnotation();
        annotation.add("00:00:02.680", "00:00:04.720");
        tu1.setAnnotation(annotation);
        GenericSkeleton skeleton = new GenericSkeleton();
        skeleton.add(new TTMLSkeletonPart(
                "<p xml:id=\"subtitle1\" begin=\"00:00:00.897\" end=\"00:00:05.263\" region=\"bottom\" tts:textAlign=\"center\">",
                22, 42, 43, 61));
        skeleton.add(new TTMLSkeletonPart("</p>"));
        tu1.setSkeleton(skeleton);

        ISkeletonWriter sw = new TTMLSkeletonWriter();
        sw.processStartDocument(LocaleId.FRENCH, "UTF-8", new EncoderManager(), sd);
        assertEquals("<p xml:id=\"subtitle1\" begin=\"00:00:02.680\" end=\"00:00:04.720\" region=\"bottom\" tts:textAlign=\"center\">This is an orange.</p>", sw.processTextUnit(tu1));
    }

    @Test
    public void testProcessTextUnitSplitLines() {
        TTMLParameters params = getDefaultParams();
        params.setMaxCharsPerLine(10);
        filter.setParameters(params);

        StartDocument sd = new StartDocument("sd");
        sd.setEncoding("UTF-8", false);
        sd.setName("docName");
        sd.setLineBreak("\n");
        sd.setLocale(LocaleId.ENGLISH);
        sd.setMultilingual(false);
        sd.setFilterParameters(filter.getParameters());

        TextUnit tu1 = new TextUnit("tu1", "This is an orange.");
        CaptionAnnotation annotation = makeCaptionAnnotation();
        annotation.add("00:00:02.680", "00:00:04.720");
        tu1.setAnnotation(annotation);
        GenericSkeleton skeleton = new GenericSkeleton();
        skeleton.add(new TTMLSkeletonPart(
                "<p xml:id=\"subtitle1\" begin=\"00:00:00.897\" end=\"00:00:05.263\" region=\"bottom\" tts:textAlign=\"center\">",
                22, 42, 43, 61));
        skeleton.add(new TTMLSkeletonPart("</p>"));
        tu1.setSkeleton(skeleton);

        ISkeletonWriter sw = new TTMLSkeletonWriter();
        sw.processStartDocument(LocaleId.FRENCH, "UTF-8", new EncoderManager(), sd);
        assertEquals("<p xml:id=\"subtitle1\" begin=\"00:00:02.680\" end=\"00:00:04.720\" region=\"bottom\" tts:textAlign=\"center\">This is an<br/>orange.</p>", sw.processTextUnit(tu1));
    }

    @Test
    public void testProcessTextUnitSplitCaptionsLines() {
        TTMLParameters params = getDefaultParams();
        params.setMaxCharsPerLine(10);
        filter.setParameters(params);

        StartDocument sd = new StartDocument("sd");
        sd.setEncoding("UTF-8", false);
        sd.setName("docName");
        sd.setLineBreak("\n");
        sd.setLocale(LocaleId.ENGLISH);
        sd.setMultilingual(false);
        sd.setFilterParameters(filter.getParameters());

        TextUnit tu1 = new TextUnit("tu1", "This is an orange.");
        CaptionAnnotation annotation = makeCaptionAnnotation();
        annotation.add("00:00:02.680", "00:00:04.720");
        annotation.add("00:00:04.800", "00:00:06.960");
        tu1.setAnnotation(annotation);
        GenericSkeleton skeleton = new GenericSkeleton();
        skeleton.add(new TTMLSkeletonPart(
                "<p xml:id=\"subtitle1\" begin=\"00:00:00.897\" end=\"00:00:05.263\" region=\"bottom\" tts:textAlign=\"center\">",
                22, 42, 43, 61));
        skeleton.add(new TTMLSkeletonPart("</p>"));
        skeleton.add(new TTMLSkeletonPart("\n"));
        skeleton.add(new TTMLSkeletonPart(
                "<p xml:id=\"subtitle1\" begin=\"00:00:05.263\" end=\"00:00:06.263\" region=\"bottom\" tts:textAlign=\"center\">",
                22, 42, 43, 61));
        skeleton.add(new TTMLSkeletonPart("</p>"));
        tu1.setSkeleton(skeleton);

        ISkeletonWriter sw = new TTMLSkeletonWriter();
        sw.processStartDocument(LocaleId.FRENCH, "UTF-8", new EncoderManager(), sd);
        assertEquals("<p xml:id=\"subtitle1\" begin=\"00:00:02.680\" end=\"00:00:04.820\" region=\"bottom\" tts:textAlign=\"center\">This is<br/>an</p>\n"
                + "<p xml:id=\"subtitle1\" begin=\"00:00:04.820\" end=\"00:00:06.960\" region=\"bottom\" tts:textAlign=\"center\">orange.</p>", sw.processTextUnit(tu1));
    }

    @Test
    public void testProcessTextUnitSplitCaptionsLineOverflow() {
        TTMLParameters params = getDefaultParams();
        params.setMaxCharsPerLine(5);
        filter.setParameters(params);

        StartDocument sd = new StartDocument("sd");
        sd.setEncoding("UTF-8", false);
        sd.setName("docName");
        sd.setLineBreak("\n");
        sd.setLocale(LocaleId.ENGLISH);
        sd.setMultilingual(false);
        sd.setFilterParameters(filter.getParameters());

        TextUnit tu1 = new TextUnit("tu1", "This is an orange.");
        CaptionAnnotation annotation = makeCaptionAnnotation();
        annotation.add("00:00:02.680", "00:00:04.720");
        annotation.add("00:00:04.800", "00:00:06.960");
        tu1.setAnnotation(annotation);
        GenericSkeleton skeleton = new GenericSkeleton();
        skeleton.add(new TTMLSkeletonPart(
                "<p xml:id=\"subtitle1\" begin=\"00:00:00.897\" end=\"00:00:05.263\" region=\"bottom\" tts:textAlign=\"center\">",
                22, 42, 43, 61));
        skeleton.add(new TTMLSkeletonPart("</p>"));
        skeleton.add(new TTMLSkeletonPart("\n"));
        skeleton.add(new TTMLSkeletonPart(
                "<p xml:id=\"subtitle1\" begin=\"00:00:05.263\" end=\"00:00:06.263\" region=\"bottom\" tts:textAlign=\"center\">",
                22, 42, 43, 61));
        skeleton.add(new TTMLSkeletonPart("</p>"));
        tu1.setSkeleton(skeleton);

        ISkeletonWriter sw = new TTMLSkeletonWriter();
        sw.processStartDocument(LocaleId.FRENCH, "UTF-8", new EncoderManager(), sd);
        assertEquals("<p xml:id=\"subtitle1\" begin=\"00:00:02.680\" end=\"00:00:04.820\" region=\"bottom\" tts:textAlign=\"center\">This<br/>is an</p>\n"
                + "<p xml:id=\"subtitle1\" begin=\"00:00:04.820\" end=\"00:00:06.960\" region=\"bottom\" tts:textAlign=\"center\">oran<br/>ge.</p>", sw.processTextUnit(tu1));
    }

    @Test
    public void testProcessTextUnitSplitCaptionsCaptionOverflow() {
        TTMLParameters params = getDefaultParams();
        params.setMaxCharsPerLine(5);
        filter.setParameters(params);

        StartDocument sd = new StartDocument("sd");
        sd.setEncoding("UTF-8", false);
        sd.setName("docName");
        sd.setLineBreak("\n");
        sd.setLocale(LocaleId.ENGLISH);
        sd.setMultilingual(false);
        sd.setFilterParameters(filter.getParameters());

        TextUnit tu1 = new TextUnit("tu1", "This is a hippopotamus.");
        CaptionAnnotation annotation = makeCaptionAnnotation();
        annotation.add("00:00:02.680", "00:00:04.720");
        annotation.add("00:00:04.800", "00:00:06.960");
        annotation.add("00:00:06.960", "00:00:09.100");
        tu1.setAnnotation(annotation);
        GenericSkeleton skeleton = new GenericSkeleton();
        skeleton.add(new TTMLSkeletonPart(
                "<p xml:id=\"subtitle1\" begin=\"00:00:00.897\" end=\"00:00:05.263\" region=\"bottom\" tts:textAlign=\"center\">",
                22, 42, 43, 61));
        skeleton.add(new TTMLSkeletonPart("</p>"));
        skeleton.add(new TTMLSkeletonPart("\n"));
        skeleton.add(new TTMLSkeletonPart(
                "<p xml:id=\"subtitle1\" begin=\"00:00:05.263\" end=\"00:00:06.263\" region=\"bottom\" tts:textAlign=\"center\">",
                22, 42, 43, 61));
        skeleton.add(new TTMLSkeletonPart("</p>"));
        skeleton.add(new TTMLSkeletonPart("\n"));
        skeleton.add(new TTMLSkeletonPart(
                "<p xml:id=\"subtitle1\" begin=\"00:00:06.263\" end=\"00:00:09.263\" region=\"bottom\" tts:textAlign=\"center\">",
                22, 42, 43, 61));
        skeleton.add(new TTMLSkeletonPart("</p>"));
        tu1.setSkeleton(skeleton);

        ISkeletonWriter sw = new TTMLSkeletonWriter();
        sw.processStartDocument(LocaleId.FRENCH, "UTF-8", new EncoderManager(), sd);
        assertEquals("<p xml:id=\"subtitle1\" begin=\"00:00:02.680\" end=\"00:00:04.820\" region=\"bottom\" tts:textAlign=\"center\">This<br/>is</p>\n"
                + "<p xml:id=\"subtitle1\" begin=\"00:00:04.820\" end=\"00:00:06.960\" region=\"bottom\" tts:textAlign=\"center\">a hi<br/>ppop</p>\n"
                + "<p xml:id=\"subtitle1\" begin=\"00:00:06.960\" end=\"00:00:09.100\" region=\"bottom\" tts:textAlign=\"center\">otam<br/>us.</p>", sw.processTextUnit(tu1));
    }

    @Test
    public void testProcessTextUnitWithCodes() {
        TTMLParameters params = getDefaultParams();
        params.setMaxCharsPerLine(10);
        params.setUseCodeFinder(true);
        params.setCodeFinderData("count.i=1\n" +
                "rule0=</?([A-Z0-9a-z]*)\\b[^>]*>\n" +
                "sample=&name; <tag><\\/at><tag\\/> <tag attr='val'> <\\/tag=\"val\">\n" +
                "useAllRulesWhenTesting.b=true\n");
        params.getCodeFinder().compile();
        filter.setParameters(params);

        StartDocument sd = new StartDocument("sd");
        sd.setEncoding("UTF-8", false);
        sd.setName("docName");
        sd.setLineBreak("\n");
        sd.setLocale(LocaleId.ENGLISH);
        sd.setMultilingual(false);
        sd.setFilterParameters(filter.getParameters());

        InlineCodeFinder codeFinder = new InlineCodeFinder();
        codeFinder.setSample("&name; <tag></at><tag/> <tag attr='val'> </tag=\"val\">");
        codeFinder.setUseAllRulesWhenTesting(true);
        codeFinder.addRule("</?([A-Z0-9a-z]*)\\b[^>]*>");
        codeFinder.compile();

        TextUnit tu1 = new TextUnit("tu1", "<span>This</span> is an orange.");
        params.getCodeFinder().process(tu1.getSource().getFirstContent());

        CaptionAnnotation annotation = makeCaptionAnnotation();
        annotation.add("00:00:02.680", "00:00:04.720");
        tu1.setAnnotation(annotation);
        GenericSkeleton skeleton = new GenericSkeleton();
        skeleton.add(new TTMLSkeletonPart(
                "<p xml:id=\"subtitle1\" begin=\"00:00:00.897\" end=\"00:00:05.263\" region=\"bottom\" tts:textAlign=\"center\">",
                22, 42, 43, 61));
        skeleton.add(new TTMLSkeletonPart("</p>"));
        tu1.setSkeleton(skeleton);

        ISkeletonWriter sw = new TTMLSkeletonWriter();
        sw.processStartDocument(LocaleId.FRENCH, "UTF-8", new EncoderManager(), sd);
        assertEquals("<p xml:id=\"subtitle1\" begin=\"00:00:02.680\" end=\"00:00:04.720\" region=\"bottom\" tts:textAlign=\"center\"><span>This</span> is<br/>an orange.</p>", sw.processTextUnit(tu1));
    }

    private CaptionAnnotation makeCaptionAnnotation() {
        CaptionAnnotation annotation = filter.makeCaptionAnnotation();
        annotation.setMaxLine(filter.getParameters().getMaxLinesPerCaption());
        annotation.setMaxChar(filter.getParameters().getMaxCharsPerLine());
        return annotation;
    }
}
