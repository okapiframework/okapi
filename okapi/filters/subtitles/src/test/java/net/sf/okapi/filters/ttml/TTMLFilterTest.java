package net.sf.okapi.filters.ttml;

import net.sf.okapi.common.Event;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.filters.FilterTestDriver;
import net.sf.okapi.common.resource.ITextUnit;
import net.sf.okapi.common.skeleton.GenericSkeleton;
import net.sf.okapi.common.skeleton.GenericSkeletonPart;
import net.sf.okapi.filters.subtitles.CaptionAnnotation;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.time.format.DateTimeFormatter;
import java.util.Iterator;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(JUnit4.class)
public class TTMLFilterTest {
    private static final String TIME_FORMAT = "HH:mm:ss.SSS";
    private static final DateTimeFormatter TIME_FORMATTER = DateTimeFormatter.ofPattern(TIME_FORMAT);

    private TTMLFilter filter;

    private LocaleId locEN = LocaleId.fromString("en");
    private LocaleId locFR = LocaleId.fromString("fr");

    @Before
    public void setUp() {
        filter = new TTMLFilter();
        assertNotNull(filter);

        setDefaultParams(filter.getParameters());
    }

    public static void setDefaultParams(TTMLParameters params) {
        if (params == null) return;

        params.setTimeFormat(TIME_FORMAT);
        params.setMaxLinesPerCaption(2);
        params.setMaxCharsPerLine(42);
    }

    public static TTMLParameters getDefaultParams() {
        TTMLParameters params = new TTMLParameters();

        params.setTimeFormat(TIME_FORMAT);
        params.setMaxLinesPerCaption(2);
        params.setMaxCharsPerLine(42);

        return params;
    }

    private static void assertCaptionTiming(String begin, String end, CaptionAnnotation.CaptionTiming timing) {
        assertEquals(begin, timing.getBeginTime().format(TIME_FORMATTER));
        assertEquals(end, timing.getEndTime().format(TIME_FORMATTER));
    }

    @Test
    public void testProcessTextUnit() {
        String snippet = "<p xml:id=\"subtitle1\" begin=\"00:00:00.897\" end=\"00:00:05.263\" region=\"bottom\" tts:textAlign=\"center\">Thanks everyone <br/>for joining us today.</p>\n"
                + "<p xml:id=\"subtitle2\" begin=\"00:00:05.430\" end=\"00:00:08.730\" region=\"bottom\" tts:textAlign=\"center\">I am so excited<br/> to be with you.</p>";

        List<Event> events = FilterTestDriver.getEvents(filter, snippet, locEN, locFR);

        ITextUnit tu = FilterTestDriver.getTextUnit(events, 1);
        assertNotNull(tu);
        assertEquals("Thanks everyone for joining us today.", tu.getSource().getFirstContent().getText());
        TTMLSkeletonPart part = (TTMLSkeletonPart) ((GenericSkeleton) tu.getSkeleton()).getFirstPart();
        assertEquals("<p xml:id=\"subtitle1\" begin=\"00:00:00.897\" end=\"00:00:05.263\" region=\"bottom\" tts:textAlign=\"center\">", part.toString());
        assertTrue(part.isHeader());
        assertEquals(22, part.startBeginTime);
        assertEquals(42, part.endBeginTime);
        assertEquals(43, part.startEndTime);
        assertEquals(61, part.endEndTime);
        CaptionAnnotation annotation = tu.getAnnotation(CaptionAnnotation.class);
        assertNotNull(annotation);
        assertEquals(2, annotation.getMaxLine());
        assertEquals(42, annotation.getMaxChar());
        assertEquals(1, annotation.getSize());
        assertCaptionTiming("00:00:00.897", "00:00:05.263", annotation.iterator().next());

        tu = FilterTestDriver.getTextUnit(events, 2);
        assertNotNull(tu);
        assertEquals("I am so excited to be with you.", tu.getSource().getFirstContent().getText());
        part = (TTMLSkeletonPart) ((GenericSkeleton) tu.getSkeleton()).getFirstPart();
        assertEquals("<p xml:id=\"subtitle2\" begin=\"00:00:05.430\" end=\"00:00:08.730\" region=\"bottom\" tts:textAlign=\"center\">", part.toString());
        assertTrue(part.isHeader());
        assertEquals(22, part.startBeginTime);
        assertEquals(42, part.endBeginTime);
        assertEquals(43, part.startEndTime);
        assertEquals(61, part.endEndTime);
        annotation = tu.getAnnotation(CaptionAnnotation.class);
        assertNotNull(annotation);
        assertEquals(2, annotation.getMaxLine());
        assertEquals(42, annotation.getMaxChar());
        assertEquals(1, annotation.getSize());
        assertCaptionTiming("00:00:05.430", "00:00:08.730", annotation.iterator().next());
    }

    @Test
    public void testMergeCaptions() {
        String snippet = "<p xml:id=\"subtitle1\" begin=\"00:00:00.897\" end=\"00:00:05.263\" region=\"bottom\" tts:textAlign=\"center\">Thanks everyone<br/>for joining us today,</p>\n"
                + "<p xml:id=\"subtitle2\" begin=\"00:00:05.430\" end=\"00:00:08.730\" region=\"bottom\" tts:textAlign=\"center\">I am so excited<br/>to be with you.</p>";

        List<Event> events = FilterTestDriver.getEvents(filter, snippet, locEN, locFR);

        ITextUnit tu = FilterTestDriver.getTextUnit(events, 1);
        assertNotNull(tu);
        assertEquals("Thanks everyone for joining us today, I am so excited to be with you.", tu.getSource().getFirstContent().getText());

        List<GenericSkeletonPart> parts = ((GenericSkeleton) tu.getSkeleton()).getParts();
        assertEquals(5, parts.size());

        TTMLSkeletonPart part = (TTMLSkeletonPart) parts.get(0);
        assertEquals("<p xml:id=\"subtitle1\" begin=\"00:00:00.897\" end=\"00:00:05.263\" region=\"bottom\" tts:textAlign=\"center\">", part.toString());
        assertTrue(part.isHeader());
        assertEquals(22, part.startBeginTime);
        assertEquals(42, part.endBeginTime);
        assertEquals(43, part.startEndTime);
        assertEquals(61, part.endEndTime);

        part = (TTMLSkeletonPart) parts.get(1);
        assertEquals("</p>", part.toString());
        assertFalse(part.isHeader());

        part = (TTMLSkeletonPart) parts.get(2);
        assertEquals("\n", part.toString());
        assertFalse(part.isHeader());

        part = (TTMLSkeletonPart) parts.get(3);
        assertEquals("<p xml:id=\"subtitle2\" begin=\"00:00:05.430\" end=\"00:00:08.730\" region=\"bottom\" tts:textAlign=\"center\">", part.toString());
        assertTrue(part.isHeader());
        assertEquals(22, part.startBeginTime);
        assertEquals(42, part.endBeginTime);
        assertEquals(43, part.startEndTime);
        assertEquals(61, part.endEndTime);

        part = (TTMLSkeletonPart) parts.get(4);
        assertEquals("</p>", part.toString());
        assertFalse(part.isHeader());

        CaptionAnnotation annotation = tu.getAnnotation(CaptionAnnotation.class);
        assertNotNull(annotation);
        assertEquals(2, annotation.getMaxLine());
        assertEquals(42, annotation.getMaxChar());
        assertEquals(2, annotation.getSize());
        Iterator<CaptionAnnotation.CaptionTiming> iterator = annotation.iterator();
        assertCaptionTiming("00:00:00.897", "00:00:05.263", iterator.next());
        assertCaptionTiming("00:00:05.430", "00:00:08.730", iterator.next());
    }

    @Test
    public void testDontMergeCaptions() {
        String snippet = "<p xml:id=\"subtitle1\" begin=\"00:00:00.897\" end=\"00:00:05.263\" region=\"bottom\" tts:textAlign=\"center\">Thanks everyone <br/>for joining us today,</p>\n"
                + "<p xml:id=\"subtitle2\" begin=\"00:00:05.430\" end=\"00:00:08.730\" region=\"bottom\" tts:textAlign=\"center\">I am so excited<br/> to be with you.</p>";

        filter.getParameters().setMergeCaptions(false);

        List<Event> events = FilterTestDriver.getEvents(filter, snippet, locEN, locFR);

        ITextUnit tu = FilterTestDriver.getTextUnit(events, 1);
        assertNotNull(tu);
        assertEquals("Thanks everyone for joining us today,", tu.getSource().getFirstContent().getText());
        TTMLSkeletonPart part = (TTMLSkeletonPart) ((GenericSkeleton) tu.getSkeleton()).getFirstPart();
        assertEquals("<p xml:id=\"subtitle1\" begin=\"00:00:00.897\" end=\"00:00:05.263\" region=\"bottom\" tts:textAlign=\"center\">", part.toString());
        assertTrue(part.isHeader());
        assertEquals(22, part.startBeginTime);
        assertEquals(42, part.endBeginTime);
        assertEquals(43, part.startEndTime);
        assertEquals(61, part.endEndTime);
        CaptionAnnotation annotation = tu.getAnnotation(CaptionAnnotation.class);
        assertNotNull(annotation);
        assertEquals(2, annotation.getMaxLine());
        assertEquals(42, annotation.getMaxChar());
        assertEquals(1, annotation.getSize());
        assertCaptionTiming("00:00:00.897", "00:00:05.263", annotation.iterator().next());

        tu = FilterTestDriver.getTextUnit(events, 2);
        assertNotNull(tu);
        assertEquals("I am so excited to be with you.", tu.getSource().getFirstContent().getText());
        part = (TTMLSkeletonPart) ((GenericSkeleton) tu.getSkeleton()).getFirstPart();
        assertEquals("<p xml:id=\"subtitle2\" begin=\"00:00:05.430\" end=\"00:00:08.730\" region=\"bottom\" tts:textAlign=\"center\">", part.toString());
        assertTrue(part.isHeader());
        assertEquals(22, part.startBeginTime);
        assertEquals(42, part.endBeginTime);
        assertEquals(43, part.startEndTime);
        assertEquals(61, part.endEndTime);
        annotation = tu.getAnnotation(CaptionAnnotation.class);
        assertNotNull(annotation);
        assertEquals(2, annotation.getMaxLine());
        assertEquals(42, annotation.getMaxChar());
        assertEquals(1, annotation.getSize());
        assertCaptionTiming("00:00:05.430", "00:00:08.730", annotation.iterator().next());
    }

    @Test
    public void testQuoteCaptions() {
        String snippet = "<p xml:id=\"subtitle1\" begin=\"00:00:00.897\" end=\"00:00:05.263\" region=\"bottom\" tts:textAlign=\"center\">'Thanks everyone <br/>for joining us today.'</p>\n"
                + "<p xml:id=\"subtitle2\" begin=\"00:00:05.430\" end=\"00:00:08.730\" region=\"bottom\" tts:textAlign=\"center\">'I am so excited<br/> to be with you.'</p>";

        List<Event> events = FilterTestDriver.getEvents(filter, snippet, locEN, locFR);

        ITextUnit tu = FilterTestDriver.getTextUnit(events, 1);
        assertNotNull(tu);
        assertEquals("'Thanks everyone for joining us today.'", tu.getSource().getFirstContent().getText());
        TTMLSkeletonPart part = (TTMLSkeletonPart) ((GenericSkeleton) tu.getSkeleton()).getFirstPart();
        assertEquals("<p xml:id=\"subtitle1\" begin=\"00:00:00.897\" end=\"00:00:05.263\" region=\"bottom\" tts:textAlign=\"center\">", part.toString());
        assertTrue(part.isHeader());
        assertEquals(22, part.startBeginTime);
        assertEquals(42, part.endBeginTime);
        assertEquals(43, part.startEndTime);
        assertEquals(61, part.endEndTime);
        CaptionAnnotation annotation = tu.getAnnotation(CaptionAnnotation.class);
        assertNotNull(annotation);
        assertEquals(2, annotation.getMaxLine());
        assertEquals(42, annotation.getMaxChar());
        assertEquals(1, annotation.getSize());
        assertCaptionTiming("00:00:00.897", "00:00:05.263", annotation.iterator().next());

        tu = FilterTestDriver.getTextUnit(events, 2);
        assertNotNull(tu);
        assertEquals("'I am so excited to be with you.'", tu.getSource().getFirstContent().getText());
        part = (TTMLSkeletonPart) ((GenericSkeleton) tu.getSkeleton()).getFirstPart();
        assertEquals("<p xml:id=\"subtitle2\" begin=\"00:00:05.430\" end=\"00:00:08.730\" region=\"bottom\" tts:textAlign=\"center\">", part.toString());
        assertTrue(part.isHeader());
        assertEquals(22, part.startBeginTime);
        assertEquals(42, part.endBeginTime);
        assertEquals(43, part.startEndTime);
        assertEquals(61, part.endEndTime);
        annotation = tu.getAnnotation(CaptionAnnotation.class);
        assertNotNull(annotation);
        assertEquals(2, annotation.getMaxLine());
        assertEquals(42, annotation.getMaxChar());
        assertEquals(1, annotation.getSize());
        assertCaptionTiming("00:00:05.430", "00:00:08.730", annotation.iterator().next());
    }

    @Test
    public void testEmptyCaptions() {
        String snippet = "<p xml:id=\"subtitle1\" begin=\"00:00:00.897\" end=\"00:00:05.263\" region=\"bottom\" tts:textAlign=\"center\"></p>\n"
                + "<p xml:id=\"subtitle2\" begin=\"00:00:05.430\" end=\"00:00:08.730\" region=\"bottom\" tts:textAlign=\"center\">I am so excited<br/> to be with you.</p>";

        List<Event> events = FilterTestDriver.getEvents(filter, snippet, locEN, locFR);

        ITextUnit tu = FilterTestDriver.getTextUnit(events, 1);
        assertNotNull(tu);
        assertEquals("", tu.getSource().getFirstContent().getText());
        TTMLSkeletonPart part = (TTMLSkeletonPart) ((GenericSkeleton) tu.getSkeleton()).getFirstPart();
        assertEquals("<p xml:id=\"subtitle1\" begin=\"00:00:00.897\" end=\"00:00:05.263\" region=\"bottom\" tts:textAlign=\"center\">", part.toString());
        assertTrue(part.isHeader());
        assertEquals(22, part.startBeginTime);
        assertEquals(42, part.endBeginTime);
        assertEquals(43, part.startEndTime);
        assertEquals(61, part.endEndTime);
        CaptionAnnotation annotation = tu.getAnnotation(CaptionAnnotation.class);
        assertNotNull(annotation);
        assertEquals(2, annotation.getMaxLine());
        assertEquals(42, annotation.getMaxChar());
        assertEquals(1, annotation.getSize());
        assertCaptionTiming("00:00:00.897", "00:00:05.263", annotation.iterator().next());

        tu = FilterTestDriver.getTextUnit(events, 2);
        assertNotNull(tu);
        assertEquals("I am so excited to be with you.", tu.getSource().getFirstContent().getText());
        part = (TTMLSkeletonPart) ((GenericSkeleton) tu.getSkeleton()).getFirstPart();
        assertEquals("<p xml:id=\"subtitle2\" begin=\"00:00:05.430\" end=\"00:00:08.730\" region=\"bottom\" tts:textAlign=\"center\">", part.toString());
        assertTrue(part.isHeader());
        assertEquals(22, part.startBeginTime);
        assertEquals(42, part.endBeginTime);
        assertEquals(43, part.startEndTime);
        assertEquals(61, part.endEndTime);
        annotation = tu.getAnnotation(CaptionAnnotation.class);
        assertNotNull(annotation);
        assertEquals(2, annotation.getMaxLine());
        assertEquals(42, annotation.getMaxChar());
        assertEquals(1, annotation.getSize());
        assertCaptionTiming("00:00:05.430", "00:00:08.730", annotation.iterator().next());
    }

    @Test
    public void testReadMaxCharMaxLine() {
        String snippet = "<metadata>\n" +
                "<okp:maximum_character_count>20</okp:maximum_character_count>\n" +
                "<okp:maximum_line_count>3</okp:maximum_line_count>\n" +
                "</metadata>\n" +
                "<p xml:id=\"subtitle1\" begin=\"00:00:00.897\" end=\"00:00:05.263\" region=\"bottom\" tts:textAlign=\"center\">Thanks everyone <br/>for joining us today.</p>\n"
                + "<p xml:id=\"subtitle2\" begin=\"00:00:05.430\" end=\"00:00:08.730\" region=\"bottom\" tts:textAlign=\"center\">I am so excited<br/> to be with you.</p>";

        List<Event> events = FilterTestDriver.getEvents(filter, snippet, locEN, locFR);

        ITextUnit tu = FilterTestDriver.getTextUnit(events, 1);
        assertNotNull(tu);
        assertEquals("Thanks everyone for joining us today.", tu.getSource().getFirstContent().getText());
        TTMLSkeletonPart part = (TTMLSkeletonPart) ((GenericSkeleton) tu.getSkeleton()).getFirstPart();
        assertEquals("<p xml:id=\"subtitle1\" begin=\"00:00:00.897\" end=\"00:00:05.263\" region=\"bottom\" tts:textAlign=\"center\">", part.toString());
        assertTrue(part.isHeader());
        assertEquals(22, part.startBeginTime);
        assertEquals(42, part.endBeginTime);
        assertEquals(43, part.startEndTime);
        assertEquals(61, part.endEndTime);
        CaptionAnnotation annotation = tu.getAnnotation(CaptionAnnotation.class);
        assertNotNull(annotation);
        assertEquals(3, annotation.getMaxLine());
        assertEquals(20, annotation.getMaxChar());
        assertEquals(1, annotation.getSize());
        assertCaptionTiming("00:00:00.897", "00:00:05.263", annotation.iterator().next());

        tu = FilterTestDriver.getTextUnit(events, 2);
        assertNotNull(tu);
        assertEquals("I am so excited to be with you.", tu.getSource().getFirstContent().getText());
        part = (TTMLSkeletonPart) ((GenericSkeleton) tu.getSkeleton()).getFirstPart();
        assertEquals("<p xml:id=\"subtitle2\" begin=\"00:00:05.430\" end=\"00:00:08.730\" region=\"bottom\" tts:textAlign=\"center\">", part.toString());
        assertTrue(part.isHeader());
        assertEquals(22, part.startBeginTime);
        assertEquals(42, part.endBeginTime);
        assertEquals(43, part.startEndTime);
        assertEquals(61, part.endEndTime);
        annotation = tu.getAnnotation(CaptionAnnotation.class);
        assertNotNull(annotation);
        assertEquals(3, annotation.getMaxLine());
        assertEquals(20, annotation.getMaxChar());
        assertEquals(1, annotation.getSize());
        assertCaptionTiming("00:00:05.430", "00:00:08.730", annotation.iterator().next());
    }

    @Test
    public void testCodeFinder() {
        String snippet = "<p xml:id=\"subtitle1\" begin=\"00:00:00.897\" end=\"00:00:05.263\" region=\"bottom\" tts:textAlign=\"center\"><span ns2:fontStyle=\"italic\">Thanks everyone <br/>for joining us today.</span></p>\n"
                + "<p xml:id=\"subtitle2\" begin=\"00:00:05.430\" end=\"00:00:08.730\" region=\"bottom\" tts:textAlign=\"center\">I am so <span ns2:fontStyle=\"italic\">excited</span><br/> to be with you.</p>";

        filter.getParameters().setMergeCaptions(false);
        filter.getParameters().setUseCodeFinder(true);
        filter.getParameters().setCodeFinderData("count.i=1\n" +
                "rule0=</?([A-Z0-9a-z]*)\\b[^>]*>\n" +
                "sample=&name; <tag><\\/at><tag\\/> <tag attr='val'> <\\/tag=\"val\">\n" +
                "useAllRulesWhenTesting.b=true\n");
        List<Event> events = FilterTestDriver.getEvents(filter, snippet, locEN, locFR);

        ITextUnit tu = FilterTestDriver.getTextUnit(events, 1);
        assertNotNull(tu);
        assertEquals("Thanks everyone for joining us today.", tu.getSource().getFirstContent().getText());
        assertEquals("<span ns2:fontStyle=\"italic\">", tu.getSource().getFirstContent().getCode(0).getData());
        assertEquals("</span>", tu.getSource().getFirstContent().getCode(1).getData());
        TTMLSkeletonPart part = (TTMLSkeletonPart) ((GenericSkeleton) tu.getSkeleton()).getFirstPart();
        assertEquals("<p xml:id=\"subtitle1\" begin=\"00:00:00.897\" end=\"00:00:05.263\" region=\"bottom\" tts:textAlign=\"center\">", part.toString());
        assertTrue(part.isHeader());
        assertEquals(22, part.startBeginTime);
        assertEquals(42, part.endBeginTime);
        assertEquals(43, part.startEndTime);
        assertEquals(61, part.endEndTime);
        CaptionAnnotation annotation = tu.getAnnotation(CaptionAnnotation.class);
        assertNotNull(annotation);
        assertEquals(2, annotation.getMaxLine());
        assertEquals(42, annotation.getMaxChar());
        assertEquals(1, annotation.getSize());
        assertCaptionTiming("00:00:00.897", "00:00:05.263", annotation.iterator().next());

        tu = FilterTestDriver.getTextUnit(events, 2);
        assertNotNull(tu);
        assertEquals("I am so excited to be with you.", tu.getSource().getFirstContent().getText());
        assertEquals("<span ns2:fontStyle=\"italic\">", tu.getSource().getFirstContent().getCode(0).getData());
        assertEquals("</span>", tu.getSource().getFirstContent().getCode(1).getData());
        part = (TTMLSkeletonPart) ((GenericSkeleton) tu.getSkeleton()).getFirstPart();
        assertEquals("<p xml:id=\"subtitle2\" begin=\"00:00:05.430\" end=\"00:00:08.730\" region=\"bottom\" tts:textAlign=\"center\">", part.toString());
        assertTrue(part.isHeader());
        assertEquals(22, part.startBeginTime);
        assertEquals(42, part.endBeginTime);
        assertEquals(43, part.startEndTime);
        assertEquals(61, part.endEndTime);
        annotation = tu.getAnnotation(CaptionAnnotation.class);
        assertNotNull(annotation);
        assertEquals(2, annotation.getMaxLine());
        assertEquals(42, annotation.getMaxChar());
        assertEquals(1, annotation.getSize());
        assertCaptionTiming("00:00:05.430", "00:00:08.730", annotation.iterator().next());
    }
}
