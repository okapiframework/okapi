package net.sf.okapi.filters.ttml;

import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.encoder.EncoderContext;
import net.sf.okapi.common.encoder.EncoderManager;
import net.sf.okapi.common.resource.ITextUnit;
import net.sf.okapi.common.resource.StartDocument;
import net.sf.okapi.common.resource.TextFragment;
import net.sf.okapi.common.skeleton.GenericSkeleton;
import net.sf.okapi.common.skeleton.GenericSkeletonPart;
import net.sf.okapi.common.skeleton.GenericSkeletonWriter;
import net.sf.okapi.filters.subtitles.CaptionAnnotation;
import net.sf.okapi.filters.subtitles.SplitCaption;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Text;

import java.time.Duration;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Iterator;

public class TTMLSkeletonWriter extends GenericSkeletonWriter {
    private final Logger LOGGER = LoggerFactory.getLogger(getClass());

    private DateTimeFormatter formatter;

    private TTMLParameters params;

    @Override
    public String processStartDocument(LocaleId outputLocale, String outputEncoding, EncoderManager encoderManager, StartDocument resource) {
        params = (TTMLParameters) resource.getFilterParameters();
        formatter = TTMLFilter.getDateTimeFormatter(params.getTimeFormat());
        return super.processStartDocument(outputLocale, outputEncoding, encoderManager, resource);
    }

    @Override
    public String processTextUnit(ITextUnit resource) {
        CaptionAnnotation annotation = resource.getAnnotation(CaptionAnnotation.class);
        String wholeCaption = super.getContent(resource, outputLoc, EncoderContext.TEXT);
        SplitCaption decodedCaptions;
        if (params.getUseCodeFinder()) {
            TextFragment tf = new TextFragment(wholeCaption);
            params.getCodeFinder().process(tf);
            SplitCaption codedCaptions = SplitCaption.splitCaption(tf.getCodedText(),
                    annotation.getSize(), annotation.getMaxLine(), annotation.getMaxChar(), false, false, !params.getSplitWords());
            ArrayList<ArrayList<String>> newCaptions = new ArrayList<>();
            for (ArrayList<String> captions : codedCaptions.getCaptions()) {
                ArrayList<String> newCaption = new ArrayList<>();
                for (String caption: captions) {
                    TextFragment captionFragment = new TextFragment(tf);
                    captionFragment.setCodedText(caption, true);
                    newCaption.add(super.getContent(captionFragment, outputLoc, EncoderContext.TEXT));
                }
                newCaptions.add(newCaption);
            }
            decodedCaptions = new SplitCaption(newCaptions);
        } else {
            decodedCaptions = SplitCaption.splitCaption(wholeCaption,
                    annotation.getSize(), annotation.getMaxLine(), annotation.getMaxChar(), false, false, !params.getSplitWords());
        }
        Iterator<String> startTagIterator = normalizeCaptionTimes(annotation, (GenericSkeleton) resource.getSkeleton()).iterator();
        Iterator<ArrayList<String>> captionIterator = decodedCaptions.iterator();
        StringBuilder tmp = new StringBuilder();
        for (GenericSkeletonPart part : ((GenericSkeleton) resource.getSkeleton()).getParts()) {
            TTMLSkeletonPart ttmlPart = (TTMLSkeletonPart) part;
            if (ttmlPart.isHeader()) {
                tmp.append(startTagIterator.next());
                tmp.append(mergeCaption(captionIterator.next()));
            } else {
                tmp.append(ttmlPart.getData());
            }
        }
        return tmp.toString();
    }

    private String mergeCaption(ArrayList<String> caption) {
        StringBuilder tmp = new StringBuilder();
        for (String line : caption) {
            tmp.append(line);
            tmp.append("<br/>");
        }
        tmp.setLength(tmp.length() - 5);
        return tmp.toString();
    }

    private ArrayList<String> normalizeCaptionTimes(CaptionAnnotation annotation, GenericSkeleton skeleton) {
        ArrayList<String> normalizedCaptionTimes = new ArrayList<>();

        LocalTime beginTime = annotation.getFirst().getBeginTime();
        LocalTime endTime = annotation.getLast().getEndTime();

        if (params.getKeepTimecodes() || beginTime == null || endTime == null) {
            for (GenericSkeletonPart part : skeleton.getParts()) {
                normalizedCaptionTimes.add(part.toString());
            }
            return normalizedCaptionTimes;
        }

        Duration duration = Duration.between(beginTime, endTime);
        duration = duration.dividedBy(annotation.getSize());

        LocalTime prevTime = beginTime;
        LocalTime nextTime;

        for (GenericSkeletonPart part : skeleton.getParts()) {
            TTMLSkeletonPart ttmlPart = (TTMLSkeletonPart) part;
            if (ttmlPart.isHeader()) {
                nextTime = prevTime.plus(duration);
                normalizedCaptionTimes.add(ttmlPart.reconstruct(formatter.format(prevTime), formatter.format(nextTime)));
                prevTime = nextTime;
            }
        }

        return normalizedCaptionTimes;
    }
}
