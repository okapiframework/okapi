package net.sf.okapi.filters.subtitles;

import net.sf.okapi.common.*;
import net.sf.okapi.common.filters.*;
import net.sf.okapi.common.resource.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.format.DateTimeFormatter;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@UsingParameters(SubtitleParameters.class)
public abstract class SubtitleFilter extends AbstractFilter {

	protected static final String DEFAULT_TIME_FORMAT = "HH:mm:ss.SSS";

	protected final Logger LOGGER = LoggerFactory.getLogger(getClass());

	protected SubtitleEventBuilder eventBuilder;

	protected SubtitleParameters params;

	protected boolean hasUtf8Bom;
	protected boolean hasUtf8Encoding;

	protected boolean skipNextWhiteSpace = false;

	protected int maxChar = 0;
	protected int maxLine = 0;

	protected DateTimeFormatter formatter;

	protected Pattern terminalPattern;

	public SubtitleFilter() {
		super();
		terminalPattern = Pattern.compile(getTerminalRegex());
	};

	@Override
	public void setParameters(IParameters params) {
		this.params = (SubtitleParameters) params;
	}

	@Override
	public SubtitleParameters getParameters() { return params; }

	@Override
	public void open(RawDocument input) {
		open(input, true);
		LOGGER.debug("{} has opened an input document", getName());
	}

	@Override
	public void open(RawDocument input, boolean generateSkeleton) {
		super.open(input, generateSkeleton);

		formatter = getDateTimeFormatter(params.getTimeFormat());

		if (input.getInputURI() != null) {
			setDocumentName(input.getInputURI().getPath());
		}

		detectEncoding(input);
		setOptions(input.getSourceLocale(), input.getTargetLocale(), getEncoding(),
				generateSkeleton);

		if (eventBuilder == null) {
			eventBuilder = new SubtitleEventBuilder(getParentId(), this);
			eventBuilder.setMimeType(getMimeType());
		} else {
			eventBuilder.reset(getParentId(), this);
		}

		// Compile code finder rules
		if (params.getUseCodeFinder()) {
			params.getCodeFinder().compile();
			eventBuilder.setCodeFinder(params.getCodeFinder());
		}

		eventBuilder.addFilterEvent(createStartFilterEvent());
	}

	public static DateTimeFormatter getDateTimeFormatter(String timeFormat) {
		if (timeFormat.isEmpty()) {
			return DateTimeFormatter.ofPattern(DEFAULT_TIME_FORMAT);
		}
		return DateTimeFormatter.ofPattern(timeFormat);
	}

	/**
	 * Is the input encoded as UTF-8?
	 *
	 * @return true if the document is in utf8 encoding.
	 */
	@Override
	protected boolean isUtf8Encoding() {
		return hasUtf8Encoding;
	}

	/**
	 * Does the input have a UTF-8 Byte Order Mark?
	 *
	 * @return true if the document has a utf-8 byte order mark.
	 */
	@Override
	protected boolean isUtf8Bom() {
		return hasUtf8Bom;
	}

	private void detectEncoding(RawDocument input) {
		BOMNewlineEncodingDetector detector = new BOMNewlineEncodingDetector(input.getStream(),
				input.getEncoding());
		// string input has a default BOM defined by java
		// do not remove it
		if (input.getInputCharSequence() != null) {
			detector.detectBom();
		} else {
			detector.detectAndRemoveBom();
		}

		setEncoding(detector.getEncoding());
		hasUtf8Bom = detector.hasUtf8Bom();
		hasUtf8Encoding = detector.hasUtf8Encoding();
		setNewlineType(detector.getNewlineType().toString());
	}

	public static boolean isCJK(LocaleId localeId) {
		return localeId.sameLanguageAs(LocaleId.CHINA_CHINESE) // sameLanguageAs "zh" for any region
				|| localeId.sameLanguageAs(LocaleId.KOREAN)
				|| localeId.sameLanguageAs(LocaleId.JAPANESE);
	}

	public int getMaxChar() {
		if (maxChar == 0) {
			if (isCJK(getTrgLoc())) {
				return params.getCjkCharsPerLine();
			}
			return params.getMaxCharsPerLine();
		}
		return maxChar;
	}

	public int getMaxLine() {
		if (maxLine == 0) {
			return params.getMaxLinesPerCaption();
		}
		return maxLine;
	}

	@Override
	public boolean hasNext() {
		return eventBuilder.hasNext();
	}

	protected void endFilter() {
		// clear out all unended temp events
		eventBuilder.flushRemainingTempEvents();

		// add the final endDocument event
		eventBuilder.addFilterEvent(createEndFilterEvent());
	}

	protected void addString(String string) {
		if (isInsideTextRun()) { addToTextUnit(string); }
		else { addToDocumentPart(string); }
	}

	protected boolean isInsideTextRun() {
		return eventBuilder.isInsideTextRun();
	}

	protected void addToDocumentPart(String part) {
		if (Util.isEmpty(part)) return;
		eventBuilder.addToDocumentPart(part);
	}

	protected void addToTextUnit(String text) {
		if (Util.isEmpty(text)) return;
		if (text.trim().isEmpty()) return;

		ITextUnit tu = eventBuilder.peekMostRecentTextUnit();
		if (tu == null) return;
		if (skipNextWhiteSpace) {
			skipNextWhiteSpace = false;
		} else if (!isCJK(getSrcLoc()) && needToAppendWhitespace(tu.getSource().getFirstContent().getText()) && needToPrependWhitespace(text)) {
			eventBuilder.addToTextUnit(" ");
		}
		eventBuilder.addToTextUnit(text);
	}

	protected static boolean needToAppendWhitespace(String str) {
		if (str.isEmpty()) {
			return false; // if string is empty, we don't need to append whitespace
		}
		return !Character.isWhitespace(str.charAt(str.length() - 1));
	}


	protected static boolean needToPrependWhitespace(String str) {
		if (str.isEmpty()) {
			return false;
		}
		return !Character.isWhitespace(str.charAt(0));
	}

	protected void addCaptionAnnotation(String startTime, String endTime) {
		Event tempEvent = eventBuilder.peekTempEvent();
		CaptionAnnotation annotation = tempEvent.getResource().getAnnotation(CaptionAnnotation.class);
		if (annotation == null) {
			annotation = makeCaptionAnnotation();
			tempEvent.getResource().setAnnotation(annotation);
		}
		annotation.add(startTime, endTime);
	}

	public CaptionAnnotation makeCaptionAnnotation() {
		return new CaptionAnnotation(formatter);
	}

	protected abstract String getTerminalRegex();

	protected boolean endsWithPunctuation(String str)	{
		if (str.trim().isEmpty()) {
			return false;
		}
		Matcher matcher = terminalPattern.matcher(str);
		return matcher.find();
	}
}
