package net.sf.okapi.filters.vtt;

import net.sf.okapi.common.ParametersDescription;
import net.sf.okapi.filters.subtitles.SubtitleParameters;
import net.sf.okapi.common.uidescription.CheckboxPart;
import net.sf.okapi.common.uidescription.EditorDescription;
import net.sf.okapi.common.uidescription.TextInputPart;

/**
 * VTT Filter parameters
 * 
 * @version 0.1, 07.06.23
 */

public class VTTParameters extends SubtitleParameters {
	private static final String CUESETTINGS = "cueSettings";
	private static final String DISCARDCUES = "discardCues";

	public VTTParameters() {
		super();
	}

	public String getCueSettings() { return getString(CUESETTINGS); }

	public void setCueSettings(String cueSettings) { setString(CUESETTINGS, cueSettings); }

	public boolean getDiscardCues() { return getBoolean(DISCARDCUES); }

	public void setDiscardCues(boolean discardCues) { setBoolean(DISCARDCUES, discardCues); }

	@Override
	public ParametersDescription getParametersDescription () {
		ParametersDescription desc = super.getParametersDescription();
		desc.add(CUESETTINGS, "Replace existing cue settings with text", null);
		desc.add(DISCARDCUES, "Discard existing cue settings", null);
		return desc;
	}

	@Override
	public EditorDescription createEditorDescription(ParametersDescription paramDesc) {
		EditorDescription desc = createEditorDescription("VTT Filter Parameters", paramDesc);

		CheckboxPart cp = desc.addCheckboxPart(paramDesc.get(DISCARDCUES));

		TextInputPart tip = desc.addTextInputPart(paramDesc.get(CUESETTINGS));
		tip.setAllowEmpty(true);
		tip.setMasterPart(cp, false);

		return desc;
	}
}
