package net.sf.okapi.filters.subtitles;

import net.sf.okapi.common.IResource;
import net.sf.okapi.common.annotation.GenericAnnotation;
import net.sf.okapi.common.annotation.GenericAnnotationType;
import net.sf.okapi.common.annotation.GenericAnnotations;
import net.sf.okapi.common.encoder.EncoderContext;
import net.sf.okapi.common.encoder.EncoderManager;
import net.sf.okapi.common.filters.EventBuilder;
import net.sf.okapi.common.filters.IFilter;
import net.sf.okapi.common.filters.InlineCodeFinder;
import net.sf.okapi.common.resource.Code;
import net.sf.okapi.common.resource.ITextUnit;
import net.sf.okapi.common.resource.Property;
import net.sf.okapi.common.resource.TextFragment;
import net.sf.okapi.filters.vtt.VTTSkeletonPart;

public class SubtitleEventBuilder extends EventBuilder {
    private SubtitleFilter filter;
    private InlineCodeFinder codeFinder;

    public SubtitleEventBuilder(String rootId, IFilter filter) {
        super(rootId, filter);
        setMimeType(filter.getMimeType());
        codeFinder = null;
        this.filter = (SubtitleFilter) filter;
    }

    private void addMaxWidth(IResource resource) {
        if (filter.getMaxChar() > 0 && filter.getMaxLine() > 0) {
            CaptionAnnotation annotation = resource.getAnnotation(CaptionAnnotation.class);
            int numCaptions = 0;
            if (annotation != null) {
                numCaptions = annotation.getSize();
                annotation.setMaxChar(filter.getMaxChar());
                annotation.setMaxLine(filter.getMaxLine());
            }
            Property maxWidth = new Property(Property.MAX_WIDTH, Integer.toString(filter.getMaxChar() * filter.getMaxLine() * numCaptions), Property.DISPLAY_ONLY);
            resource.setProperty(maxWidth);
            Property sizeUnit = new Property(Property.SIZE_UNIT, "char", Property.DISPLAY_ONLY);
            resource.setProperty(sizeUnit);
            if (filter.getParameters().getWriteCharAnnotations()) {
                GenericAnnotations anns = new GenericAnnotations();
                anns.add(new GenericAnnotation(GenericAnnotationType.MISC_METADATA, "max_chars_per_line", Integer.toString(filter.getMaxChar())));
                anns.add(new GenericAnnotation(GenericAnnotationType.MISC_METADATA, "max_lines_count", Integer.toString(filter.getMaxLine()*numCaptions)));
                GenericAnnotations.addAnnotations(resource, anns);
            }
        }
    }

    @Override
    protected ITextUnit postProcessTextUnit(ITextUnit textUnit) {
        TextFragment text = textUnit.getSource().getFirstContent();
        if (codeFinder != null) {
            codeFinder.process(text);
        }
        addMaxWidth(textUnit);
        return textUnit;
    }

    public void setCodeFinder(InlineCodeFinder codeFinder) {
        this.codeFinder = codeFinder;
    }
}
