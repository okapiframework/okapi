package net.sf.okapi.filters.vtt;

import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.encoder.EncoderContext;
import net.sf.okapi.common.encoder.EncoderManager;
import net.sf.okapi.common.resource.ITextUnit;
import net.sf.okapi.common.resource.StartDocument;
import net.sf.okapi.common.resource.TextFragment;
import net.sf.okapi.common.skeleton.GenericSkeleton;
import net.sf.okapi.common.skeleton.GenericSkeletonPart;
import net.sf.okapi.common.skeleton.GenericSkeletonWriter;
import net.sf.okapi.filters.subtitles.CaptionAnnotation;
import net.sf.okapi.filters.subtitles.SplitCaption;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Duration;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.regex.Matcher;

public class VTTSkeletonWriter extends GenericSkeletonWriter {
    private final Logger LOGGER = LoggerFactory.getLogger(getClass());

    private String newLineType;

    private DateTimeFormatter formatter;

    private VTTParameters params;

    @Override
    public String processStartDocument(LocaleId outputLocale, String outputEncoding, EncoderManager encoderManager, StartDocument resource) {
        newLineType = resource.getLineBreak();
        params = (VTTParameters) resource.getFilterParameters();
        formatter = VTTFilter.getDateTimeFormatter(params.getTimeFormat());
        return super.processStartDocument(outputLocale, outputEncoding, encoderManager, resource);
    }

    @Override
    public String processTextUnit(ITextUnit resource) {
        CaptionAnnotation annotation = resource.getAnnotation(CaptionAnnotation.class);
        String wholeCaption = super.getContent(resource, outputLoc, EncoderContext.TEXT);
        SplitCaption decodedCaptions;
        if (params.getUseCodeFinder()) {
            TextFragment tf = new TextFragment(wholeCaption);
            params.getCodeFinder().process(tf);
            SplitCaption codedCaptions = SplitCaption.splitCaption(tf.getCodedText(),
                    annotation.getSize(), annotation.getMaxLine(), annotation.getMaxChar(), false, false, !params.getSplitWords());
            ArrayList<ArrayList<String>> newCaptions = new ArrayList<>();
            for (ArrayList<String> captions : codedCaptions.getCaptions()) {
                ArrayList<String> newCaption = new ArrayList<>();
                for (String caption: captions) {
                    TextFragment captionFragment = new TextFragment(tf);
                    captionFragment.setCodedText(caption, true);
                    newCaption.add(super.getContent(captionFragment, outputLoc, EncoderContext.TEXT));
                }
                newCaptions.add(newCaption);
            }
            decodedCaptions = new SplitCaption(newCaptions);
        } else {
            decodedCaptions = SplitCaption.splitCaption(wholeCaption,
                    annotation.getSize(), annotation.getMaxLine(), annotation.getMaxChar(), false, false, !params.getSplitWords());
        }
        Iterator<ArrayList<String>> captionIterator = decodedCaptions.iterator();
        Iterator<String> headerIterator = normalizeCaptionTimes(annotation).iterator();
        StringBuilder tmp = new StringBuilder();
        for (GenericSkeletonPart part : ((GenericSkeleton) resource.getSkeleton()).getParts()) {
            VTTSkeletonPart vttPart = (VTTSkeletonPart) part;
            if (vttPart.isHeader()) {
                tmp.append(headerIterator.next());
                tmp.append(getCueSettings(vttPart));
                tmp.append(newLineType);
                tmp.append(mergeCaption(captionIterator.next()));
            } else {
                if (!vttPart.isEmpty()) {
                    tmp.append(vttPart.getData());
                }
                tmp.append(newLineType);
            }
        }
        return tmp.toString();
    }

    private String mergeCaption(ArrayList<String> caption) {
        StringBuilder tmp = new StringBuilder();
        for (String line : caption) {
            tmp.append(line);
            tmp.append(newLineType);
        }
        return tmp.toString();
    }

    private String getCueSettings(VTTSkeletonPart part) {
        if (!params.getCueSettings().isEmpty()) {
            return " " + params.getCueSettings();
        } else if (params.getDiscardCues()) {
            return "";
        }

        Matcher matcher = VTTFilter.HEADER_PATTERN.matcher(part.getData());
        if (!matcher.find() || matcher.group(4).isEmpty()) {
            return "";
        }
        return " " + matcher.group(4);
    }

    private ArrayList<String> normalizeCaptionTimes(CaptionAnnotation annotation) {
        ArrayList<String> normalizedCaptionTimes = new ArrayList<>();

        LocalTime beginTime = annotation.getFirst().getBeginTime();
        LocalTime endTime = annotation.getLast().getEndTime();

        if (params.getKeepTimecodes() || beginTime == null || endTime == null) {
            for (CaptionAnnotation.CaptionTiming timing: annotation) {
                normalizedCaptionTimes.add(formatAsHeader(timing.getBeginString(), timing.getEndString()));
            }
            return normalizedCaptionTimes;
        }

        Duration duration = Duration.between(beginTime, endTime);
        duration = duration.dividedBy(annotation.getSize());

        LocalTime prevTime = beginTime;
        LocalTime nextTime;

        for (CaptionAnnotation.CaptionTiming timing : annotation) {
            nextTime = prevTime.plus(duration);
            normalizedCaptionTimes.add(formatAsHeader(prevTime, nextTime));
            prevTime = nextTime;
        }

        return normalizedCaptionTimes;
    }

    private String formatAsHeader(LocalTime begin, LocalTime end) {
        return formatAsHeader(formatter.format(begin), formatter.format(end));
    }

    private String formatAsHeader(String begin, String end) {
        return begin + " --> " + end;
    }
}
