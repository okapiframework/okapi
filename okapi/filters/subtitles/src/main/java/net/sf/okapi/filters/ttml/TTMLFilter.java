package net.sf.okapi.filters.ttml;

import net.htmlparser.jericho.Attribute;
import net.htmlparser.jericho.EndTag;
import net.htmlparser.jericho.Segment;
import net.htmlparser.jericho.StartTag;
import net.htmlparser.jericho.StreamedSource;
import net.sf.okapi.common.*;
import net.sf.okapi.common.exceptions.OkapiIOException;
import net.sf.okapi.common.filters.*;
import net.sf.okapi.common.resource.*;
import net.sf.okapi.common.skeleton.ISkeletonWriter;
import net.sf.okapi.filters.subtitles.SubtitleFilter;

import java.io.IOException;
import java.util.*;

/**
 * TTML Filter
 * 
 */
@UsingParameters(TTMLParameters.class)
public class TTMLFilter extends SubtitleFilter {

	public static final String FILTER_NAME	= "okf_ttml";
	public static final String FILTER_MIME	= MimeTypeMapper.TTML_MIME_TYPE;

	private static final String ELEM_METADATA = "metadata";
	private static final String ELEM_MAX_CHAR = "okp:maximum_character_count";
	private static final String ELEM_MAX_LINE = "okp:maximum_line_count";
	private static final String ELEM_CAPTION = "p";
	private static final String ELEM_LINE_BREAK = "br";

	public static final String ATTR_TIME_BEGIN = "begin";
	public static final String ATTR_TIME_END = "end";

	private static final String TERMINAL_REGEX = "[.．。!！?？؟？][^a-zA-Z0-9]*$";

	private StreamedSource document;
	private Iterator<Segment> nodeIterator;

	private boolean isInsideMetadata = false;

	private boolean isInsideCaption = false;
	
	public TTMLFilter() {
		super();
		setName(FILTER_NAME);
		setDisplayName("TTML Filter");
		setMimeType(FILTER_MIME);
		setParameters(new TTMLParameters());
		addConfiguration(new FilterConfiguration(getName(), FILTER_MIME, getClass().getName(),
				"TTML", "TTML Documents", "okf_ttml.fprm", ".ttml"));
	}

	@Override
	public void setParameters(IParameters params) {
		this.params = (TTMLParameters) params;
	}

	@Override
	public TTMLParameters getParameters() { return (TTMLParameters) params; }

	@Override
	public ISkeletonWriter createSkeletonWriter() {
		return new TTMLSkeletonWriter();
	}

	@Override
	public void close() {
		super.close();
		try {
			if (document != null) {
				document.close();
			}
		} catch (IOException e) {
			throw new OkapiIOException("Could not close " + getDocumentName(), e);
		}
		this.document = null; // help Java GC

		LOGGER.debug("{} has been closed", getDocumentName());
	}

	@Override
	public void open(RawDocument input, boolean generateSkeleton) {
		super.open(input, generateSkeleton);

		try {
			if (document != null) {
				document.close();
			}
			document = new StreamedSource(input.getReader());
			nodeIterator = document.iterator();
		} catch (IOException e) {
			throw new OkapiIOException(e);
		}

		loadMaxWidthFromConfig();
	}

	private void loadMaxWidthFromConfig() {
		maxChar = params.getMaxCharsPerLine(); // 0 by default
		maxLine = params.getMaxLinesPerCaption(); // 0 by default
	}

	private boolean updateMaxChar(String n) {
		try {
			maxChar = Integer.parseInt(n);
			return true;
		} catch (NumberFormatException e) {
			return false;
		}
	}

	private boolean updateMaxLine(String n) {
		try {
			maxLine = Integer.parseInt(n);
			return true;
		} catch (NumberFormatException e) {
			return false;
		}
	}

	@Override
	public boolean hasNext() {
		return eventBuilder.hasNext();
	}

	@Override
	public Event next() {
		if (eventBuilder.hasQueuedEvents()) {
			return eventBuilder.next();
		}
		while (nodeIterator.hasNext() && !isCanceled()) {
			Segment segment = nodeIterator.next();
			if (segment instanceof StartTag) {
				final StartTag tag = (StartTag) segment;
				handleStartTag(tag);
			} else if (segment instanceof EndTag) {
				final EndTag tag = (EndTag) segment;
				handleEndTag(tag);
			} else {
				addString(segment.toString());
			}
			if (eventBuilder.hasQueuedEvents()) {
				break;
			}
		}

		if (!nodeIterator.hasNext()) {
			endFilter();
		}

		return eventBuilder.next();
	}

	private void handleStartTag(StartTag tag) {
		switch (tag.getName()) {
			case ELEM_METADATA:
				if (!isInsideTextRun()) {
					isInsideMetadata = true;
				}
				addString(tag.toString());
				break;
			case ELEM_MAX_CHAR:
				addString(tag.toString());
				if (isInsideMetadata) {
					String maxChar = nodeIterator.next().toString();
					updateMaxChar(maxChar);
					addString(maxChar);
				}
				break;
			case ELEM_MAX_LINE:
				addString(tag.toString());
				if (isInsideMetadata) {
					String maxLine = nodeIterator.next().toString();
					updateMaxLine(maxLine);
					addString(maxLine);
				}
				break;
			case ELEM_CAPTION:
				if (!isInsideTextRun()) {
					eventBuilder.startTextUnit();
				}
				Attribute begin = tag.getAttributes().get(ATTR_TIME_BEGIN);
				Attribute end = tag.getAttributes().get(ATTR_TIME_END);
				appendToSkeleton(tag.toString(),
						begin.getBegin() - tag.getBegin(), begin.getEnd() - tag.getBegin(),
						end.getBegin() - tag.getBegin(), end.getEnd() - tag.getBegin());
				addCaptionAnnotation(begin.getValue(), end.getValue());
				isInsideCaption = true;
				break;
			case ELEM_LINE_BREAK:
				if (!isInsideTextRun()) {
					addString(tag.toString());
				}
				break;
			default:
				if (isInsideTextRun() && isInsideCaption) {
					super.skipNextWhiteSpace = true;
				}
				addString(tag.toString());
				if (isInsideTextRun() && isInsideCaption) {
					super.skipNextWhiteSpace = true;
				}
		}
	}

	private void handleEndTag(EndTag tag) {
		switch (tag.getName()) {
			case ELEM_METADATA:
				if (!isInsideTextRun()) {
					isInsideMetadata = false;
				}
				addString(tag.toString());
				break;
			case ELEM_CAPTION:
				isInsideCaption = false;
				addString(tag.toString());
				if (isInsideTextRun()) {
					// Consider empty captions as complete text units
					String captionText = eventBuilder.peekMostRecentTextUnit().getSource().getFirstContent().getText();
					if (captionText.isEmpty() || endsWithPunctuation(captionText) || !params.getMergeCaptions()) {
						eventBuilder.endTextUnit();
					}
				}
				break;
			default:
				if (isInsideTextRun() && isInsideCaption) {
					super.skipNextWhiteSpace = true;
				}
				addString(tag.toString());
				if (isInsideTextRun() && isInsideCaption) {
					super.skipNextWhiteSpace = true;
				}
		}
	}

	private void appendToSkeleton(String text, int startBeginTime, int endBeginTime, int startEndTime, int endEndTime) {
		eventBuilder.appendToSkeleton(new TTMLSkeletonPart(text, startBeginTime, endBeginTime, startEndTime, endEndTime));
	}

	private void appendToSkeleton(String text) {
		eventBuilder.appendToSkeleton(new TTMLSkeletonPart(text));
	}

	@Override
	protected void addString(String string) {
		if (isInsideTextRun()) {
			if (isInsideCaption) {
				addToTextUnit(string);
			} else {
				appendToSkeleton(string);
			}
		} else {
			addToDocumentPart(string);
		}
	}

	protected String getTerminalRegex() { return TERMINAL_REGEX; }
}
