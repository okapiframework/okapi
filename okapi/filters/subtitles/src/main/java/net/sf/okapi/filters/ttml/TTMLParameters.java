package net.sf.okapi.filters.ttml;

import net.sf.okapi.common.ParametersDescription;
import net.sf.okapi.common.filters.InlineCodeFinder;
import net.sf.okapi.common.uidescription.EditorDescription;
import net.sf.okapi.filters.subtitles.SubtitleParameters;

/**
 * TTML Filter parameters
 * 
 * @version 0.1, 07.27.2023
 */

public class TTMLParameters extends SubtitleParameters {

    @Override
    public void reset() {
        super.reset();
        setUseCodeFinder(true);
    }

    @Override
    public EditorDescription createEditorDescription(ParametersDescription paramDesc) {
        return createEditorDescription("TTML Filter Parameters", paramDesc);
    }
}
