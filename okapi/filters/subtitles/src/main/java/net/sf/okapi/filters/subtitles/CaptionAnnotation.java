package net.sf.okapi.filters.subtitles;

import net.sf.okapi.common.annotation.IAnnotation;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class CaptionAnnotation implements IAnnotation, Iterable<CaptionAnnotation.CaptionTiming> {
    private ArrayList<CaptionTiming> timings;

    private DateTimeFormatter formatter;

    private int maxChar = 0;
    private int maxLine = 0;

    public CaptionAnnotation(DateTimeFormatter formatter) {
        this.formatter = formatter;
        this.timings = new ArrayList<>(2);
    }

    public void setMaxChar(int maxChar) {
        this.maxChar = maxChar;
    }

    public void setMaxLine(int maxLine) {
        this.maxLine = maxLine;
    }

    public int getMaxChar() {
        return maxChar;
    }

    public int getMaxLine() {
        return maxLine;
    }

    public void add(CaptionTiming timing) {
        timings.add(timing);
    }

    public void add(String beginTime, String endTime) {
        add(new CaptionTiming(beginTime, endTime));
    }

    public void add(LocalTime beginTime, LocalTime endTime) { add(new CaptionTiming(beginTime, endTime)); }

    public int getSize() { return timings.size(); }

    public CaptionTiming getFirst() {
        return timings.get(0);
    }

    public CaptionTiming getLast() {
        return timings.get(getSize() - 1);
    }

    @Override
    public Iterator<CaptionTiming> iterator () {
        return new Iterator<>() {
            private int current = 0;

            @Override
            public void remove() {
                throw new UnsupportedOperationException("The method remove() not supported.");
            }

            @Override
            public CaptionTiming next() {
                if (current >= timings.size()) {
                    throw new NoSuchElementException("No more content parts.");
                }
                return timings.get(current++);
            }

            @Override
            public boolean hasNext() {
                return (current < timings.size());
            }
        };
    }

    public class CaptionTiming {
        private LocalTime begin;
        private LocalTime end;

        private String beginString;
        private String endString;

        private CaptionTiming(String begin, String end) {
            this.beginString = begin;
            this.endString = end;
            try {
                this.begin = parseTime(begin);
            } catch (DateTimeParseException e) {}
            try {
                this.end = parseTime(end);
            } catch (DateTimeParseException e) {}
        }

        private CaptionTiming(LocalTime begin, LocalTime end) {
            this.begin = begin;
            this.end = end;
        }

        public String getBeginString() { return beginString; }

        public String getEndString() { return endString; }

        public LocalTime getBeginTime() {
            return begin;
        }

        public LocalTime getEndTime() {
            return end;
        }

        private LocalTime parseTime(String time) {
            if (formatter == null) {
                return LocalTime.parse(time);
            }
            return LocalTime.parse(time, formatter);
        }
    }
}
