/*===========================================================================
  Copyright (C) 2016-2017 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
===========================================================================*/

package net.sf.okapi.filters.xml;

import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.List;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import net.sf.okapi.common.Event;
import net.sf.okapi.common.FileLocation;
import net.sf.okapi.common.IParameters;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.filters.FilterTestDriver;
import net.sf.okapi.common.resource.RawDocument;

import static org.junit.Assert.*;

@RunWith(JUnit4.class)
public class BundledConfigsTest {

	private XMLFilter filter;
	private FileLocation root;
	private LocaleId locEN = LocaleId.fromString("en");

	@Before
	public void setUp() {
		filter = new XMLFilter();
		root = FileLocation.fromClass(this.getClass());
	}

	@Test
	public void testAndroidUntranslatable() {
		IParameters parameters = filter.getParameters();
		URL fprmUrl = getClass().getClassLoader().getResource("net/sf/okapi/filters/xml/AndroidStrings.fprm");
		parameters.load(fprmUrl, false);
		RawDocument rd = new RawDocument(root.in("/AndroidTest2.xml").asUri(), "UTF-8", locEN);
		List<Event> events = FilterTestDriver.getEvents(filter, rd, parameters);

		// Test file has 3 TUs but one is marked translatable=false, so make
		// sure it isn't extracted.
		assertTrue(events.get(0).isStartDocument());
		assertEquals("Hello, Android! I am a string resource!", events.get(1).getTextUnit().getSource().toString());
		assertEquals("Hello, Android", events.get(2).getTextUnit().getSource().toString());
		assertTrue(events.get(3).isEndDocument());
		assertEquals(4, events.size());
	}

	@Test
	public void testDocBookSimpleInline() {
		IParameters parameters = filter.getParameters();
		URL fprmUrl = getClass().getClassLoader().getResource("net/sf/okapi/filters/xml/okf_xml-docbook.fprm");
		parameters.load(fprmUrl, false);
		RawDocument rd = new RawDocument(root.in("/docbook-emphasis-example.xml").asUri(), "UTF-8", locEN);
		List<Event> events = FilterTestDriver.getEvents(filter, rd, parameters);

		assertEquals(4, events.size());
		assertTrue(events.get(0).isStartDocument());
		assertTrue(events.get(events.size()-1).isEndDocument());
		assertTrue(events.get(1).isTextUnit());
		assertEquals("Example emphasis", events.get(1).getTextUnit().getSource().toString());
		assertTrue(events.get(2).isTextUnit());
		assertEquals("The <emphasis>most</emphasis> important example of this phenomenon occurs in A. Nonymous's book <citetitle>Power Snacking</citetitle>. ",
					events.get(2).getTextUnit().getSource().toString());
	}

	@Ignore("Issue #1041")
	@Test
	public void testDocBookFootnote() {
		IParameters parameters = filter.getParameters();
		URL fprmUrl = getClass().getClassLoader().getResource("net/sf/okapi/filters/xml/okf_xml-docbook.fprm");
		parameters.load(fprmUrl, false);
		RawDocument rd = new RawDocument(root.in("/docbook-footnote-example.xml").asUri(), "UTF-8", locEN);
		List<Event> events = FilterTestDriver.getEvents(filter, rd, parameters);

		assertEquals(5, events.size());
		assertTrue(events.get(0).isStartDocument());
		assertTrue(events.get(events.size()-1).isEndDocument());
		assertTrue(events.get(1).isTextUnit());
		assertEquals("Example footnote", events.get(1).getTextUnit().getSource().toString());
		assertTrue(events.get(2).isTextUnit());
		assertEquals("An annual percentage rate (<abbrev>APR</abbrev>) of 13.9%<footnote></footnote>will be charged on all balances carried forward.",
				events.get(2).getTextUnit().getSource().toString());
		assertEquals("The prime rate, as published in the <citetitle>Wall Street Journal</citetitle> on the first business day of the month, plus 7.0%.",
				events.get(3).getTextUnit().getSource().toString());
	}

	@Test
	public void translatableContentExtracted() {
		final URL url = getClass().getClassLoader().getResource(
			"okf_xml@translatable-and-untranslatable.fprm"
		);
		final IParameters parameters = this.filter.getParameters();
		parameters.load(url, false);
		final RawDocument rd = new RawDocument(
			this.root.in("/translatable-and-untranslatable.xml").asUri(),
			StandardCharsets.UTF_8.name(),
			this.locEN
		);
		parameters.setBoolean("extractUntranslatable", false);
		final List<Event> events = FilterTestDriver.getEvents(this.filter, rd, parameters);
		assertEquals(5, events.size());
		assertTrue(events.get(0).isStartDocument());
		assertEquals("Translatable ", events.get(1).getTextUnit().getSource().toString());
		assertTrue(events.get(1).getTextUnit().isTranslatable());
		assertEquals(" part.", events.get(2).getTextUnit().getSource().toString());
		assertTrue(events.get(2).getTextUnit().isTranslatable());
		assertEquals("This has to be extracted and translated.", events.get(3).getTextUnit().getSource().toString());
		assertTrue(events.get(3).getTextUnit().isTranslatable());
		assertTrue(events.get(4).isEndDocument());
	}

	@Test
	public void untranslatableContentExtracted() {
		final URL url = getClass().getClassLoader().getResource(
			"okf_xml@translatable-and-untranslatable.fprm"
		);
		final IParameters parameters = this.filter.getParameters();
		parameters.load(url, false);
		final RawDocument rd = new RawDocument(
			this.root.in("/translatable-and-untranslatable.xml").asUri(),
			StandardCharsets.UTF_8.name(),
			this.locEN
		);
		List<Event> events = FilterTestDriver.getEvents(this.filter, rd, parameters);
		assertEquals(8, events.size());
		assertTrue(events.get(0).isStartDocument());
		assertEquals("Translatable ", events.get(1).getTextUnit().getSource().toString());
		assertTrue(events.get(1).getTextUnit().isTranslatable());
		assertEquals("embedded untranslatable", events.get(2).getTextUnit().getSource().toString());
		assertFalse(events.get(2).getTextUnit().isTranslatable());
		assertEquals(" part.", events.get(3).getTextUnit().getSource().toString());
		assertTrue(events.get(3).getTextUnit().isTranslatable());
		assertEquals("This has to be extracted but not translated 1.", events.get(4).getTextUnit().getSource().toString());
		assertFalse(events.get(4).getTextUnit().isTranslatable());
		assertEquals("This has to be extracted but not translated 2.", events.get(5).getTextUnit().getSource().toString());
		assertFalse(events.get(5).getTextUnit().isTranslatable());
		assertEquals("This has to be extracted and translated.", events.get(6).getTextUnit().getSource().toString());
		assertTrue(events.get(6).getTextUnit().isTranslatable());
		assertTrue(events.get(7).isEndDocument());
	}
}
