/*===========================================================================
  Copyright (C) 2018-2018 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
===========================================================================*/

package net.sf.okapi.filters.markdown.parser;

import static net.sf.okapi.filters.markdown.parser.MarkdownTokenType.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.InputStream;
import java.util.Scanner;

import net.sf.okapi.common.*;
import net.sf.okapi.common.filters.FilterConfiguration;
import net.sf.okapi.common.filters.FilterConfigurationMapper;
import net.sf.okapi.filters.html.HtmlFilter;
import net.sf.okapi.filters.markdown.MarkdownFilter;
import net.sf.okapi.filters.yaml.YamlFilter;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import net.sf.okapi.filters.markdown.Parameters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@RunWith(JUnit4.class)
public class MarkdownParserTest {
    private static final String NEWLINE = "\n";
    private MarkdownParser parser;
    private final Logger LOGGER = LoggerFactory.getLogger(getClass());

    @Before
    public void setup() {
        parser = new MarkdownParser(new Parameters(), NEWLINE);
    }

    @Test
    public void testAutoLink() {
        parser.parse("<https://www.google.com>");
        assertNextToken(parser, "<https://www.google.com>", false, AUTO_LINK);
    }

    @Test
    public void testBlockQuote1() {
        parser.parse("> Blockquote");
        assertNextToken(parser, "> ", false, LINE_PREFIX);
        assertNextToken(parser, "Blockquote", true, TEXT);
        assertNextToken(parser, "", false, LINE_PREFIX);
        assertFalse(parser.hasNextToken());
    }

    @Test
    public void testBlockQuote2() {
        parser.parse("> Blockquote" + NEWLINE + "across multiple lines");
        assertNextToken(parser, "> ", false, LINE_PREFIX);
        assertNextToken(parser, "Blockquote" + NEWLINE + "across multiple lines", true, TEXT);
        assertNextToken(parser, "", false, LINE_PREFIX);
        assertFalse(parser.hasNextToken());
    }

    @Test
    public void testBulletList1() {
        parser.parse("* First" + NEWLINE + "* Second" + NEWLINE + "* Third" + NEWLINE);
        assertNextToken(parser, "", false, LINE_PREFIX);
        assertNextToken(parser, "* ", false, BULLET_LIST_ITEM);
        assertNextToken(parser, "First", true, TEXT);
        assertNextToken(parser, NEWLINE, false, SOFT_LINE_BREAK);
        assertNextToken(parser, "", false, LINE_PREFIX);

        assertNextToken(parser, "", false, LINE_PREFIX);
        assertNextToken(parser, "* ", false, BULLET_LIST_ITEM);
        assertNextToken(parser, "Second", true, TEXT);
        assertNextToken(parser, NEWLINE, false, SOFT_LINE_BREAK);
        assertNextToken(parser, "", false, LINE_PREFIX);

        assertNextToken(parser, "", false, LINE_PREFIX);
        assertNextToken(parser, "* ", false, BULLET_LIST_ITEM);
        assertNextToken(parser, "Third", true, TEXT);
        assertNextToken(parser, NEWLINE, false, SOFT_LINE_BREAK);
        assertNextToken(parser, "", false, LINE_PREFIX);
        assertFalse(parser.hasNextToken());
    }

    @Test
    public void testBulletList2() {
        parser.parse("* First" + NEWLINE + "element" + NEWLINE
                + "* Second element" + NEWLINE + NEWLINE + NEWLINE + NEWLINE
                + "* Third element");
        assertNextToken(parser, "", false, LINE_PREFIX);
        assertNextToken(parser, "* ", false, BULLET_LIST_ITEM);
        assertNextToken(parser, "First" + NEWLINE + "element", true, TEXT);
        assertNextToken(parser, NEWLINE, false, SOFT_LINE_BREAK);
        assertNextToken(parser, "", false, LINE_PREFIX);

        assertNextToken(parser, "", false, LINE_PREFIX);
        assertNextToken(parser, "* ", false, BULLET_LIST_ITEM);
        assertNextToken(parser, "Second element", true, TEXT);
        assertNextToken(parser, NEWLINE, false, SOFT_LINE_BREAK);
        assertNextToken(parser, "", false, LINE_PREFIX);

        assertNextToken(parser, NEWLINE, false, BLANK_LINE);
        assertNextToken(parser, NEWLINE, false, BLANK_LINE);
        assertNextToken(parser, NEWLINE, false, BLANK_LINE);

        assertNextToken(parser, "", false, LINE_PREFIX);
        assertNextToken(parser, "* ", false, BULLET_LIST_ITEM);
        assertNextToken(parser, "Third element", true, TEXT);
        assertNextToken(parser, "", false, LINE_PREFIX);

        assertFalse(parser.hasNextToken());
    }

    @Test
    public void testBulletListWithWhitespace() {
        parser.parse("*  First" + NEWLINE + "*   Second" + NEWLINE);
        assertNextToken(parser, "", false, LINE_PREFIX);
        assertNextToken(parser, "*  ", false, BULLET_LIST_ITEM);
        assertNextToken(parser, "First", true, TEXT);
        assertNextToken(parser, NEWLINE, false, SOFT_LINE_BREAK);
        assertNextToken(parser, "", false, LINE_PREFIX);

        assertNextToken(parser, "", false, LINE_PREFIX);
        assertNextToken(parser, "*   ", false, BULLET_LIST_ITEM);
        assertNextToken(parser, "Second", true, TEXT);
        assertNextToken(parser, NEWLINE, false, SOFT_LINE_BREAK);
        assertNextToken(parser, "", false, LINE_PREFIX);
        assertFalse(parser.hasNextToken());
    }

    @Test
    public void testCode1() {
        parser.parse("`Code content`");
        assertNextToken(parser, "`", false, CODE);
        assertNextToken(parser, "Code content", true, TEXT);
        assertNextToken(parser, "`", false, CODE);
        assertFalse(parser.hasNextToken());
    }

    @Test
    public void testCode2() {
        parser.parse("`Code content" + NEWLINE + "across multiple` lines");
        assertNextToken(parser, "`", false, CODE);
        assertNextToken(parser, "Code content" + NEWLINE + "across multiple", true, TEXT);
        assertNextToken(parser, "`", false, CODE);
        assertNextToken(parser, " lines", true, TEXT);
        assertFalse(parser.hasNextToken());
    }

    @Test
    public void testEmphasis1() {
        parser.parse("_Italics_");
        assertNextToken(parser, "_", false, EMPHASIS);
        assertNextToken(parser, "Italics", true, TEXT);
        assertNextToken(parser, "_", false, EMPHASIS);
        assertFalse(parser.hasNextToken());
    }

    @Test
    public void testEmphasis2() {
        parser.parse("*Italics*");
        assertNextToken(parser, "*", false, EMPHASIS);
        assertNextToken(parser, "Italics", true, TEXT);
        assertNextToken(parser, "*", false, EMPHASIS);
        assertFalse(parser.hasNextToken());
    }

    @Test
    public void testFencedCodeBlock() {
        parser.parse("```" + NEWLINE
                + "export const pi = 3.14" + NEWLINE + "fenced code block" + NEWLINE
                + "```");
        assertNextToken(parser, "```", false, FENCED_CODE_BLOCK);
        assertNextToken(parser, NEWLINE, false, SOFT_LINE_BREAK);
        assertNextToken(parser, "export const pi = 3.14" + NEWLINE + "fenced code block" + NEWLINE, true, TEXT);
        assertNextToken(parser, "```", false, FENCED_CODE_BLOCK);
        assertNextToken(parser, NEWLINE, false, SOFT_LINE_BREAK);
        assertFalse(parser.hasNextToken());
    }

    @Test
    public void testFencedCodeBlockWithInfo() {
        parser.parse("```python" + NEWLINE
                + "Content in a" + NEWLINE + "fenced code block" + NEWLINE
                + "```");
        assertNextToken(parser, "```", false, FENCED_CODE_BLOCK);
        assertNextToken(parser, "python", false, FENCED_CODE_BLOCK_INFO);
        assertNextToken(parser, NEWLINE, false, SOFT_LINE_BREAK);
        assertNextToken(parser, "Content in a" + NEWLINE + "fenced code block" + NEWLINE, true, TEXT);
        assertNextToken(parser, "```", false, FENCED_CODE_BLOCK);
        assertNextToken(parser, NEWLINE, false, SOFT_LINE_BREAK);
        assertFalse(parser.hasNextToken());
    }

    @Test
    public void testFencedCodeBlockWithInfoWithSpace() {
        parser.parse("``` python" + NEWLINE
                + "Content in a" + NEWLINE + "fenced code block" + NEWLINE
                + "```");
        assertNextToken(parser, "```", false, FENCED_CODE_BLOCK);
        assertNextToken(parser, " python", false, FENCED_CODE_BLOCK_INFO);
        assertNextToken(parser, NEWLINE, false, SOFT_LINE_BREAK);
        assertNextToken(parser, "Content in a" + NEWLINE + "fenced code block" + NEWLINE, true, TEXT);
        assertNextToken(parser, "```", false, FENCED_CODE_BLOCK);
        assertNextToken(parser, NEWLINE, false, SOFT_LINE_BREAK);
        assertFalse(parser.hasNextToken());
    }

    @Test
    public void testHeading1A() {
        parser.parse("# Heading");
        assertNextToken(parser, "# ", false, HEADING_PREFIX);
        assertNextToken(parser, "Heading", true, TEXT);
        assertNextToken(parser, NEWLINE, false, SOFT_LINE_BREAK); // Heading always ends with a newline.
        assertFalse(parser.hasNextToken());
    }

    @Test
    public void testHeading1AX() {
        parser.parse("# Heading" + NEWLINE);
        assertNextToken(parser, "# ", false, HEADING_PREFIX);
        assertNextToken(parser, "Heading", true, TEXT);
        assertNextToken(parser, NEWLINE, false, SOFT_LINE_BREAK);
        assertFalse(parser.hasNextToken());
    }

    @Test
    public void testHeading1B() {
        parser.parse("Heading" + NEWLINE + "=======");
        assertNextToken(parser, "Heading", true, TEXT);
        assertNextToken(parser, NEWLINE, false, SOFT_LINE_BREAK);
        assertNextToken(parser, "=======", false, HEADING_UNDERLINE);
        assertNextToken(parser, NEWLINE, false, SOFT_LINE_BREAK);
        assertFalse(parser.hasNextToken());
    }

    @Test
    public void testHeading2A() {
        parser.parse("## Heading");
        assertNextToken(parser, "## ", false, HEADING_PREFIX);
        assertNextToken(parser, "Heading", true, TEXT);
        assertNextToken(parser, NEWLINE, false, SOFT_LINE_BREAK);
        assertFalse(parser.hasNextToken());
    }

    @Test
    public void testHeading2B() {
        parser.parse("Heading" + NEWLINE + "-------");
        assertNextToken(parser, "Heading", true, TEXT);
        assertNextToken(parser, NEWLINE, false, SOFT_LINE_BREAK);
        assertNextToken(parser, "-------", false, HEADING_UNDERLINE);
        assertNextToken(parser, NEWLINE, false, SOFT_LINE_BREAK);
        assertFalse(parser.hasNextToken());
    }

    @Test
    public void testHtmlBlock1() {
        parser.parse("<table><tr><td>Test</td></tr></table>");
        assertNextToken(parser, "<table><tr><td>Test</td></tr></table>", true, HTML_BLOCK);
        assertFalse(parser.hasNextToken());
    }

    @Test
    public void testHtmlBlock2() {
        parser.parse("<![CDATA[Lorem ipsum" + NEWLINE + " dolor sit amet]]>");
        assertNextToken(parser, "<![CDATA[Lorem ipsum" + NEWLINE + " dolor sit amet]]>", true, HTML_BLOCK);
        assertFalse(parser.hasNextToken());
    }

    @Test
    public void testHtmlBlockWithMarkdown() {
        parser.parse("<table><tr><td>" + NEWLINE + NEWLINE
                + "**Bold**" + NEWLINE + NEWLINE + "*Italic*" + NEWLINE + NEWLINE + "</td></tr></table>");
        
        assertNextToken(parser, "<table><tr><td>", true, HTML_BLOCK);
        assertNextToken(parser, NEWLINE, false, SOFT_LINE_BREAK);
        assertNextToken(parser, NEWLINE, false, BLANK_LINE);

        assertNextToken(parser, "**", false, STRONG_EMPHASIS);
        assertNextToken(parser, "Bold", true, TEXT);
        assertNextToken(parser, "**", false, STRONG_EMPHASIS);
        assertNextToken(parser, NEWLINE, false, SOFT_LINE_BREAK);
        assertNextToken(parser, NEWLINE, false, BLANK_LINE);

        assertNextToken(parser, "*", false, EMPHASIS);
        assertNextToken(parser, "Italic", true, TEXT);
        assertNextToken(parser, "*", false, EMPHASIS);
        assertNextToken(parser, NEWLINE, false, SOFT_LINE_BREAK);
        assertNextToken(parser, NEWLINE, false, BLANK_LINE);
        assertNextToken(parser, "", false, LINE_PREFIX);
        assertNextToken(parser, "</td></tr></table>", true, HTML_BLOCK);
        assertFalse(parser.hasNextToken());
    }

    @Test
    public void testHtmlCommentBlock() {
        parser.parse("<!-- HTML" + NEWLINE + " comment -->");
        assertNextToken(parser, "<!-- HTML" + NEWLINE + " comment -->", false, HTML_COMMENT_BLOCK);
        assertFalse(parser.hasNextToken());
    }

    @Test
    public void testHtmlEntity() { // Currently, HTML entities are pass through, treated as regular text.
        parser.parse("&gt; &amp;" + NEWLINE + "&quot;");
        assertNextToken(parser, "&gt;", false, HTML_ENTITY);
        assertNextToken(parser, " ", false, TEXT);
        assertNextToken(parser, "&amp;", false, HTML_ENTITY);
        assertNextToken(parser, NEWLINE, true, SOFT_LINE_BREAK);
        assertNextToken(parser, "&quot;", false, HTML_ENTITY);
        assertFalse(parser.hasNextToken());
    }

    @Test
    public void testHtmlInline() {
        parser.parse("This contains <span>some inline</span> HTML");
        assertNextToken(parser, "This contains ", true, TEXT);
        assertNextToken(parser, "<span>", false, HTML_INLINE);
        assertNextToken(parser, "some inline", true, TEXT);
        assertNextToken(parser, "</span>", false, HTML_INLINE);
        assertNextToken(parser, " HTML", true, TEXT);
        assertFalse(parser.hasNextToken());
    }

    @Test
    public void testImage() {
        parser.parse("![Image](https://www.google.com)");
        assertNextToken(parser, "![", false, IMAGE);
        assertNextToken(parser, "Image", true, TEXT);
        assertNextToken(parser, "](https://www.google.com)", false, IMAGE);
        assertFalse(parser.hasNextToken());
    }

    @Test
    public void testImageRef() {
        parser.parse("![Image][A]");
        assertNextToken(parser, "![", false, IMAGE_REF);
        assertNextToken(parser, "Image", true, TEXT);
        assertNextToken(parser, "]", false, IMAGE_REF);
        assertNextToken(parser, "[", false, IMAGE_REF);
        assertNextToken(parser, "A", false, IMAGE_REF);
        assertNextToken(parser, "]", false, IMAGE_REF);
        assertFalse(parser.hasNextToken());
    }

    @Test
    public void testImageRefBug() {
        parser.parse("![dummytext](<test_with space.png>)");
        assertNextToken(parser, "![", false, IMAGE);
        assertNextToken(parser, "dummytext", true, TEXT);
        assertNextToken(parser, "](<test_with space.png>)", false, IMAGE);
        assertFalse(parser.hasNextToken());
    }

    @Test
    public void testIndentedCodeBlock() {
        parser.parse("    export const pi = 3.14" + NEWLINE
                + "    in an indented" + NEWLINE
                + "    code block");
        assertNextToken(parser, "    ", false, LINE_PREFIX);
        assertNextToken(parser, "export const pi = 3.14" + NEWLINE
                                        + "in an indented" + NEWLINE
                                        + "code block", true, TEXT);
        assertNextToken(parser, "", false, END_TEXT_UNIT);
        assertNextToken(parser, "", false, LINE_PREFIX);
        assertFalse(parser.hasNextToken());
    }

    @Test
    public void testHardLineBreak() {
        parser.parse("This is  " + NEWLINE + "separated by a hard line break");
        assertNextToken(parser, "This is", true, TEXT);
        assertNextToken(parser, "  ", false, HARD_LINE_BREAK);
        //assertNextToken(parser, NEWLINE, true, SOFT_LINE_BREAK);
        assertNextToken(parser, NEWLINE + "separated by a hard line break", true, TEXT);
        assertFalse(parser.hasNextToken());
    }

    @Test
    public void testLink1() {
        parser.parse("[Link](https://www.google.com 'Title')");
        assertNextToken(parser, "[", false, LINK);
        assertNextToken(parser, "Link", true, TEXT);
        assertNextToken(parser, "](https://www.google.com '", false, LINK);
        assertNextToken(parser, "Title", true, TEXT);
        assertNextToken(parser, "')", false, LINK);
        assertFalse(parser.hasNextToken());
    }

    @Test
    public void testLink2() {
        parser.parse("[Link](https://www.google.com)");
        assertNextToken(parser, "[", false, LINK);
        assertNextToken(parser, "Link", true, TEXT);
        assertNextToken(parser, "](https://www.google.com)", false, LINK);
        assertFalse(parser.hasNextToken());
    }

    @Test
    public void testLink3() {
        parser.parse("[Link]()");
        assertNextToken(parser, "[", false, LINK);
        assertNextToken(parser, "Link", true, TEXT);
        assertNextToken(parser, "]()", false, LINK);
        assertFalse(parser.hasNextToken());
    }

    @Test
    public void testLink4() {
        parser.parse("[Link](<https://www.google.com>)");
        assertNextToken(parser, "[", false, LINK);
        assertNextToken(parser, "Link", true, TEXT);
        assertNextToken(parser, "](<https://www.google.com>)", false, LINK);
        assertFalse(parser.hasNextToken());
    }

    @Test
    public void testLink5() {
        parser.parse("[Link](\\(foo\\))");
        assertNextToken(parser, "[", false, LINK);
        assertNextToken(parser, "Link", true, TEXT);
        assertNextToken(parser, "](\\(foo\\))", false, LINK);
        assertFalse(parser.hasNextToken());
    }

    @Test
    public void testLink6() {
        parser.parse("[Link](\\(foo\\))");
        assertNextToken(parser, "[", false, LINK);
        assertNextToken(parser, "Link", true, TEXT);
        assertNextToken(parser, "](\\(foo\\))", false, LINK);
        assertFalse(parser.hasNextToken());
    }

    @Test
    public void testLinkRef() {
        parser.parse("[Link][B]");
        assertNextToken(parser, "[", false, LINK_REF);
        assertNextToken(parser, "Link", true, TEXT);
        assertNextToken(parser, "]", false, LINK_REF);
        assertNextToken(parser, "[", false, LINK_REF);
        assertNextToken(parser, "B", false, LINK_REF);
        assertNextToken(parser, "]", false, LINK_REF);
        assertFalse(parser.hasNextToken());
    }

    @Test
    public void testParagraph() {
        parser.parse("A paragraph" + NEWLINE + NEWLINE);
        assertNextToken(parser, "A paragraph", true, TEXT);
        assertNextToken(parser, NEWLINE, false, SOFT_LINE_BREAK);
        assertNextToken(parser, NEWLINE, false, BLANK_LINE);
        assertFalse(parser.hasNextToken());
    }

    @Test
    public void testOrderedList() {
        parser.parse("1. First" + NEWLINE + "2. Second" + NEWLINE + "3. Third");
        assertNextToken(parser, "", false, LINE_PREFIX);
        assertNextToken(parser, "1. ", false, ORDERED_LIST_ITEM);
        assertNextToken(parser, "First", true, TEXT);
        assertNextToken(parser, NEWLINE, false, SOFT_LINE_BREAK);
        assertNextToken(parser, "", false, LINE_PREFIX);
        assertNextToken(parser, "", false, LINE_PREFIX);
        assertNextToken(parser, "2. ", false, ORDERED_LIST_ITEM);
        assertNextToken(parser, "Second", true, TEXT);
        assertNextToken(parser, NEWLINE, false, SOFT_LINE_BREAK);
        assertNextToken(parser, "", false, LINE_PREFIX);
        assertNextToken(parser, "", false, LINE_PREFIX);
        assertNextToken(parser, "3. ", false, ORDERED_LIST_ITEM);
        assertNextToken(parser, "Third", true, TEXT);
        assertNextToken(parser, "", false, LINE_PREFIX);
        assertFalse(parser.hasNextToken());
    }

    @Test
    public void testOrderedListWithNestedIndents() {
        parser.parse("1. First" + NEWLINE +
                "  - test1\n" +
                "  - test2\n");
        assertNextToken(parser, "", false, LINE_PREFIX);
        assertNextToken(parser, "1. ", false, ORDERED_LIST_ITEM);
        assertNextToken(parser, "First", true, TEXT);
        assertNextToken(parser, NEWLINE, false, SOFT_LINE_BREAK);
        assertNextToken(parser, "", false, LINE_PREFIX);
        assertNextToken(parser, "  ", false, LINE_PREFIX);
        assertNextToken(parser, "- ", false, BULLET_LIST_ITEM);
        assertNextToken(parser, "test1", true, TEXT);
        assertNextToken(parser, NEWLINE, false, SOFT_LINE_BREAK);
        assertNextToken(parser, "", false, LINE_PREFIX);
        assertNextToken(parser, "  ", false, LINE_PREFIX);
        assertNextToken(parser, "- ", false, BULLET_LIST_ITEM);
        assertNextToken(parser, "test2", true, TEXT);
        assertNextToken(parser, NEWLINE, false, SOFT_LINE_BREAK);
        assertNextToken(parser, "", false, LINE_PREFIX);
        assertFalse(parser.hasNextToken());
    }

    @Test
    public void testReferenceDefinition1() {
        parser.parse("[1]: http://google.com/ 'Google'");
        assertNextToken(parser, "[", false, REFERENCE);
        assertNextToken(parser, "1", false, REFERENCE);
        assertNextToken(parser, "]: ", false, REFERENCE);
        assertNextToken(parser, "http://google.com/", false, REFERENCE);
        assertNextToken(parser, " '", false, REFERENCE);
        assertNextToken(parser, "Google", false, REFERENCE); // Title for the unused reference doesn't get extracted.
        assertNextToken(parser, "'", false, REFERENCE);
        assertNextToken(parser, NEWLINE, false, REFERENCE);
        assertFalse(parser.hasNextToken());
    }

    @Test
    public void testMdxExport() {
        String mdxSnippet = "export const JsonRpcTerminal = (props) => {\n" +
                "  const [value, setValue] = useState(\"\");\n" +
                "  const { method, params, network } = props;\n" +
                "  return (\n" +
                "    <div>\n" +
                "      <div>\n" +
                "        {value != \"\" ? <pre className=\"json_rpc_terminal\">{value}</pre> : null}\n" +
                "      </div>\n" +
                "      <div>\n" +
                "        {value == \"\" ? (\n" +
                "          <button\n" +
                "            className=\"json_rpc_terminal_button\"\n" +
                "            onClick={() => {\n" +
                "              fetch(network, {\n" +
                "                method: \"POST\",\n" +
                "                headers: {\n" +
                "                  Accept: \"application/json\",\n" +
                "                  \"Content-Type\": \"application/json\",\n" +
                "                },\n" +
                "                body: JSON.stringify({\n" +
                "                  jsonrpc: \"2.0\",\n" +
                "                  method: method,\n" +
                "                  params: params,\n" +
                "                  id: 1,\n" +
                "                }),\n" +
                "              })\n" +
                "                .then((res) => res.json())\n" +
                "                .then((response) => {\n" +
                "                  setValue(JSON.stringify(response));\n" +
                "                });\n" +
                "            }}\n" +
                "          >\n" +
                "            Run command\n" +
                "          </button>\n" +
                "        ) : (\n" +
                "          <button\n" +
                "            className=\"json_rpc_terminal_button\"\n" +
                "            onClick={() => {\n" +
                "              setValue(\"\");\n" +
                "            }}\n" +
                "          >\n" +
                "            Clear Terminal\n" +
                "          </button>\n" +
                "        )}\n" +
                "      </div>\n" +
                "    </div>\n" +
                "  );\n" +
                "};\n";

        Parameters params = new Parameters();
        params.setParseMdx(true);
        parser = new MarkdownParser(params, NEWLINE);
        parser.parse("dummy text\n" +
                "\n" +
               mdxSnippet +
                "\n" +
                "dummy text\n" +
                "\nexport const pi = 3.14\n");
        assertNextToken(parser, "dummy text", true, TEXT);
        assertNextToken(parser, NEWLINE, false, SOFT_LINE_BREAK);
        assertNextToken(parser,NEWLINE, false, BLANK_LINE);
        assertNextToken(parser, mdxSnippet, false, MDX_EXPORT);
        assertNextToken(parser, NEWLINE, false, SOFT_LINE_BREAK);
        assertNextToken(parser,NEWLINE, false, BLANK_LINE);
        assertNextToken(parser, "", false, LINE_PREFIX);
        assertNextToken(parser, "dummy text", true, TEXT);
        assertNextToken(parser, NEWLINE, false, SOFT_LINE_BREAK);
        assertNextToken(parser,NEWLINE, false, BLANK_LINE);
        assertNextToken(parser, "export const pi = 3.14\n", false, MDX_EXPORT);
        assertNextToken(parser, NEWLINE, false, SOFT_LINE_BREAK);
        assertFalse(parser.hasNextToken());
    }

    @Test
    public void testReferenceDefinition1plus() { // Used reference.
        parser.parse("[Link][1]\n\n[1]: http://google.com/ 'Google'");
        assertNextToken(parser, "[", false, LINK_REF);
        assertNextToken(parser, "Link", true, TEXT);
        assertNextToken(parser, "]", false, LINK_REF);
        assertNextToken(parser, "[", false, LINK_REF);
        assertNextToken(parser, "1", false, LINK_REF);
        assertNextToken(parser, "]", false, LINK_REF);
        assertNextToken(parser, NEWLINE, false, SOFT_LINE_BREAK);
        assertNextToken(parser, NEWLINE, false, BLANK_LINE);
        assertNextToken(parser, "[", false, REFERENCE);
        assertNextToken(parser, "1", false, REFERENCE);
        assertNextToken(parser, "]: ", false, REFERENCE);
        assertNextToken(parser, "http://google.com/", false, REFERENCE);
        assertNextToken(parser, " '", false, REFERENCE);
        assertNextToken(parser, "Google", true, REFERENCE); 
        assertNextToken(parser, "'", false, REFERENCE);
        assertNextToken(parser, NEWLINE, false, REFERENCE);
        assertFalse(parser.hasNextToken());
    }
    
    @Test
    public void testReferenceDefinition2() {
        parser.parse("[1]: <http://google.com/>");
        assertNextToken(parser, "[", false, REFERENCE);
        assertNextToken(parser, "1", false, REFERENCE);
        assertNextToken(parser, "]: ", false, REFERENCE);
        assertNextToken(parser, "<", false, REFERENCE);
        assertNextToken(parser, "http://google.com/", false, REFERENCE);
        assertNextToken(parser, ">", false, REFERENCE);
        assertNextToken(parser, NEWLINE, false, REFERENCE);
        assertFalse(parser.hasNextToken());
    }

    @Test
    public void testTitleAdmonitions() {
        parser.parse("!!! note \"This is a title\"\n" +
                "    Some admonitions have titles.");
        assertNextToken(parser, "", false, LINE_PREFIX);
        assertNextToken(parser, "!!! ", false, ADMONITION_OPENING);
        assertNextToken(parser, "note ", false, ADMONITION_INFO);
        assertNextToken(parser, "\"", false, TEXT);
        assertNextToken(parser, "This is a title", true, TEXT);
        assertNextToken(parser, "\"", false, TEXT);
        assertNextToken(parser, "\n", false, SOFT_LINE_BREAK);
        assertNextToken(parser, "    ", false, LINE_PREFIX);
        assertNextToken(parser, "Some admonitions have titles.", true, TEXT);
        assertNextToken(parser, "", false, END_TEXT_UNIT);
        assertNextToken(parser, "", false, LINE_PREFIX);
        assertFalse(parser.hasNextToken());
    }

    @Test
    public void testNoTitleAdmonitions() {
        parser.parse("!!! note\n" +
                "    Some admonitions do not have titles. \"note\" should be translated.");
        assertNextToken(parser, "", false, LINE_PREFIX);
        assertNextToken(parser, "!!! ", false, ADMONITION_OPENING);
        assertNextToken(parser, "note", true, TEXT);
        assertNextToken(parser, "\n", false, SOFT_LINE_BREAK);
        assertNextToken(parser, "    ", false, LINE_PREFIX);
        assertNextToken(parser, "Some admonitions do not have titles. \"note\" should be translated.", true, TEXT);
        assertNextToken(parser, "", false, END_TEXT_UNIT);
        assertNextToken(parser, "", false, LINE_PREFIX);
        assertFalse(parser.hasNextToken());
    }

    @Test
    public void testNoHeaderAdmonitions() {
        parser.parse("!!! note \"\"\n" +
                "    Admonition without header");
        assertNextToken(parser, "", false, LINE_PREFIX);
        assertNextToken(parser, "!!! ", false, ADMONITION_OPENING);
        assertNextToken(parser, "note ", false, ADMONITION_INFO);
        assertNextToken(parser, "\"\"", false, TEXT);
        assertNextToken(parser, "\n", false, SOFT_LINE_BREAK);
        assertNextToken(parser, "    ", false, LINE_PREFIX);
        assertNextToken(parser, "Admonition without header", true, TEXT);
        assertNextToken(parser, "", false, END_TEXT_UNIT);
        assertNextToken(parser, "", false, LINE_PREFIX);
        assertFalse(parser.hasNextToken());
    }

    @Test
    public void testAdmonitionsWithNewline() {
        parser.parse("!!! note\n" +
                "    \n" +
                "    There's a blank line between the opening and the content.");
        assertNextToken(parser, "", false, LINE_PREFIX);
        assertNextToken(parser, "!!! ", false, ADMONITION_OPENING);
        assertNextToken(parser, "note", true, TEXT);
        assertNextToken(parser, "\n", false, SOFT_LINE_BREAK);
        assertNextToken(parser, "\n", false, BLANK_LINE);
        assertNextToken(parser, "    ", false, LINE_PREFIX);
        assertNextToken(parser, "There's a blank line between the opening and the content.", true, TEXT);
        assertNextToken(parser, "", false, END_TEXT_UNIT);
        assertNextToken(parser, "", false, LINE_PREFIX);
        assertFalse(parser.hasNextToken());
    }

    @Test
    public void testAdmonitionWithIndent() {
        parser.parse("* Bullet point\n\n" +
                "    !!! note\n" +
                "        Indented admonition");
        assertNextToken(parser, "", false, LINE_PREFIX);
        assertNextToken(parser, "* ", false, BULLET_LIST_ITEM);
        assertNextToken(parser, "Bullet point", true, TEXT);
        assertNextToken(parser, "\n", false, SOFT_LINE_BREAK);
        assertNextToken(parser, "\n", false, BLANK_LINE);
        assertNextToken(parser, "    ", false, LINE_PREFIX);
        assertNextToken(parser, "!!! ", false, ADMONITION_OPENING);
        assertNextToken(parser, "note", true, TEXT);
        assertNextToken(parser, "\n", false, SOFT_LINE_BREAK);
        assertNextToken(parser, "        ", false, LINE_PREFIX);
        assertNextToken(parser, "Indented admonition", true, TEXT);
        assertNextToken(parser, "", false, END_TEXT_UNIT);
        assertNextToken(parser, "", false, LINE_PREFIX);
        assertNextToken(parser, "", false, LINE_PREFIX);
        assertFalse(parser.hasNextToken());
    }


    @Test
    public void testAdmonitionWhitespaceBeforeHeading() {
        parser.parse("!!! important\n" +
                "\n" +
                "    Lorem ipsum dolor sit amet.\n" +
                "\n" +
                "### Lorem ipsum");
        assertNextToken(parser, "", false, LINE_PREFIX);
        assertNextToken(parser, "!!! ", false, ADMONITION_OPENING);
        assertNextToken(parser, "important", true, TEXT);
        assertNextToken(parser, "\n", false, SOFT_LINE_BREAK);
        assertNextToken(parser, "\n", false, BLANK_LINE);
        assertNextToken(parser, "    ", false, LINE_PREFIX);
        assertNextToken(parser, "Lorem ipsum dolor sit amet.", true, TEXT);
        assertNextToken(parser, "\n", false, SOFT_LINE_BREAK);
        assertNextToken(parser, "", false, END_TEXT_UNIT);
        assertNextToken(parser, "", false, LINE_PREFIX);
        assertNextToken(parser, "\n", false, BLANK_LINE);
        assertNextToken(parser, "### ", false, HEADING_PREFIX);
        assertNextToken(parser, "Lorem ipsum", true, TEXT);
        assertNextToken(parser, "\n", false, SOFT_LINE_BREAK);
        assertFalse(parser.hasNextToken());
    }


    @Test
    public void testAdmonitionWithNestedBulletList() {
        parser.parse(
                "!!! important\n" +
                "\n" +
                "    Lorem ipsum dolor sit amet.\n" +
                "\n" +
                "    * Lorem ipsum dolor sit amet.\n" +
                "\n" +
                "    * Lorem ipsum dolor sit amet.\n" +
                "\n" +
                "### Lorem ipsum");
        assertNextToken(parser, "", false, LINE_PREFIX);
        assertNextToken(parser, "!!! ", false, ADMONITION_OPENING);
        assertNextToken(parser, "important", true, TEXT);
        assertNextToken(parser, "\n", false, SOFT_LINE_BREAK);
        assertNextToken(parser, "\n", false, BLANK_LINE);
        assertNextToken(parser, "    ", false, LINE_PREFIX);
        assertNextToken(parser, "Lorem ipsum dolor sit amet.", true, TEXT);
        assertNextToken(parser, "\n", false, SOFT_LINE_BREAK);
        assertNextToken(parser, "\n", false, SOFT_LINE_BREAK);
        assertNextToken(parser, "    ", false, LINE_PREFIX);
        assertNextToken(parser, "* ", false, BULLET_LIST_ITEM);
        assertNextToken(parser, "Lorem ipsum dolor sit amet.", true, TEXT);
        assertNextToken(parser, "\n", false, SOFT_LINE_BREAK);
        assertNextToken(parser, "    ", false, LINE_PREFIX);
        assertNextToken(parser, "\n", false, BLANK_LINE);
        assertNextToken(parser, "    ", false, LINE_PREFIX);
        assertNextToken(parser, "* ", false, BULLET_LIST_ITEM);
        assertNextToken(parser, "Lorem ipsum dolor sit amet.", true, TEXT);
        assertNextToken(parser, "    ", false, LINE_PREFIX);
        assertNextToken(parser, "", false, END_TEXT_UNIT);
        assertNextToken(parser, "", false, LINE_PREFIX);
        assertNextToken(parser, "\n", false, BLANK_LINE);
        assertNextToken(parser, "\n", false, BLANK_LINE);
        assertNextToken(parser, "### ", false, HEADING_PREFIX);
        assertNextToken(parser, "Lorem ipsum", true, TEXT);
        assertNextToken(parser, "\n", false, SOFT_LINE_BREAK);
        assertFalse(parser.hasNextToken());
    }

    @Test
    public void testDocusaurusAdmonition() {
        parser.parse(":::note\n" +
                "\n" +
                "Docusaurus has a very different format for admonitions.\n" +
                "\n" +
                ":::");
        assertNextToken(parser, ":::", false, ADMONITION_OPENING);
        assertNextToken(parser, "note", false, ADMONITION_INFO);
        assertNextToken(parser, "\n", false, SOFT_LINE_BREAK);
        assertNextToken(parser, "\n", false, BLANK_LINE);
        assertNextToken(parser, "", false, LINE_PREFIX);
        assertNextToken(parser, "Docusaurus has a very different format for admonitions.", true, TEXT);
        assertNextToken(parser, "\n", false, SOFT_LINE_BREAK);
        assertNextToken(parser, "\n", false, BLANK_LINE);
        assertNextToken(parser, ":::", false, ADMONITION_CLOSING);
        assertFalse(parser.hasNextToken());
    }

    @Test
    public void testDocusaurusAdmonitionWithTitle() {
        parser.parse(":::caution This one has a title\n" +
                "\n" +
                "Docusaurus has a very different format for admonitions.\n" +
                "\n" +
                ":::");
        assertNextToken(parser, ":::", false, ADMONITION_OPENING);
        assertNextToken(parser, "caution", false, ADMONITION_INFO);
        assertNextToken(parser, " ", false, WHITE_SPACE);
        assertNextToken(parser, "This one has a title", true, TEXT);
        assertNextToken(parser, "\n", false, SOFT_LINE_BREAK);
        assertNextToken(parser, "\n", false, BLANK_LINE);
        assertNextToken(parser, "", false, LINE_PREFIX);
        assertNextToken(parser, "Docusaurus has a very different format for admonitions.", true, TEXT);
        assertNextToken(parser, "\n", false, SOFT_LINE_BREAK);
        assertNextToken(parser, "\n", false, BLANK_LINE);
        assertNextToken(parser, ":::", false, ADMONITION_CLOSING);
        assertFalse(parser.hasNextToken());
    }

    @Test
    public void testDocusaurusAdmonitionWithHtmlBlock() {
        parser.parse(":::tip Lorem Ipsum\n" +
                "Dolor sit amet.\n" +
                "\n" +
                "<video loop autoplay width=\"70%\" height=\"70%\" controls=\"true\" >\n" +
                "  <source type=\"video/mp4\" src=\"/lorem/ipsum/dolor_sit_amet.mp4\"></source>\n" +
                "  <p>Your browser does not support the video element.</p>\n" +
                "</video>\n" +
                ":::\n");
        assertNextToken(parser, ":::", false, ADMONITION_OPENING);
        assertNextToken(parser, "tip", false, ADMONITION_INFO);
        assertNextToken(parser, " ", false, WHITE_SPACE);
        assertNextToken(parser, "Lorem Ipsum", true, TEXT);
        assertNextToken(parser, "\n", false, SOFT_LINE_BREAK);
        assertNextToken(parser, "\nDolor sit amet.", true, TEXT);
        assertNextToken(parser, "\n", false, SOFT_LINE_BREAK);
        assertNextToken(parser, "\n", false, BLANK_LINE);
        assertNextToken(parser, "<video loop autoplay width=\"70%\" height=\"70%\" controls=\"true\" >\n" +
                "  <source type=\"video/mp4\" src=\"/lorem/ipsum/dolor_sit_amet.mp4\"></source>\n" +
                "  <p>Your browser does not support the video element.</p>\n" +
                "</video>\n", true, HTML_BLOCK);
        assertNextToken(parser, "\n", false, SOFT_LINE_BREAK);
        assertNextToken(parser, ":::", false, ADMONITION_CLOSING);
        assertNextToken(parser, "\n", false, SOFT_LINE_BREAK);
        assertFalse(parser.hasNextToken());
    }

    @Test
    public void testNestedCollapsibleAdmonitions() {
        parser.parse("??? note \"Open styled details\"\n" +
                "    ??? danger \"Nested details!\"\n" +
                "        And more content again.");
        assertNextToken(parser, "", false, LINE_PREFIX);
        assertNextToken(parser, "??? ", false, ADMONITION_OPENING);
        assertNextToken(parser, "note ", false, ADMONITION_INFO);
        assertNextToken(parser, "\"", false, TEXT);
        assertNextToken(parser, "Open styled details", true, TEXT);
        assertNextToken(parser, "\"", false, TEXT);
        assertNextToken(parser, "\n", false, SOFT_LINE_BREAK);
        assertNextToken(parser, "    ", false, LINE_PREFIX);
        assertNextToken(parser, "??? ", false, ADMONITION_OPENING);
        assertNextToken(parser, "danger ", false, ADMONITION_INFO);
        assertNextToken(parser, "\"", false, TEXT);
        assertNextToken(parser, "Nested details!", true, TEXT);
        assertNextToken(parser, "\"", false, TEXT);
        assertNextToken(parser, "\n", false, SOFT_LINE_BREAK);
        assertNextToken(parser, "        ", false, LINE_PREFIX);
        assertNextToken(parser, "And more content again.", true, TEXT);
        assertNextToken(parser, "", false, END_TEXT_UNIT);
        assertNextToken(parser, "    ", false, LINE_PREFIX);
        assertNextToken(parser, "", false, END_TEXT_UNIT);
        assertNextToken(parser, "", false, LINE_PREFIX);
        assertFalse(parser.hasNextToken());
    }

    @Test
    public void testNestedFencedCodeBlock() {
        parser.parse("??? type \"Python generator example\"\n" +
                "    A generator function that yields ints is secretly just a function that\n" +
                "    returns an iterator of ints, so that's how we annotate it\n" +
                "    ``` python\n" +
                "    def g(n: int) -> Iterator[int]:\n" +
                "        i = 0\n" +
                "        while i < n:\n" +
                "            yield i\n" +
                "            i += 1\n" +
                "    ```\n");
        assertNextToken(parser, "", false, LINE_PREFIX);
        assertNextToken(parser, "??? ", false, ADMONITION_OPENING);
        assertNextToken(parser, "type ", false, ADMONITION_INFO);
        assertNextToken(parser, "\"", false, TEXT);
        assertNextToken(parser, "Python generator example", true, TEXT);
        assertNextToken(parser, "\"", false, TEXT);
        assertNextToken(parser, "\n", false, SOFT_LINE_BREAK);
        assertNextToken(parser, "    ", false, LINE_PREFIX);
        assertNextToken(parser, "A generator function that yields ints is secretly just a function that\n" +
                "    returns an iterator of ints, so that's how we annotate it", true, TEXT);
        assertNextToken(parser, "\n", false, SOFT_LINE_BREAK);
        assertNextToken(parser, "    ", false, LINE_PREFIX);
        assertNextToken(parser, "```", false, FENCED_CODE_BLOCK);
        assertNextToken(parser, " python", false, FENCED_CODE_BLOCK_INFO);
        assertNextToken(parser, "\n", false, SOFT_LINE_BREAK);
        assertNextToken(parser, "def g(n: int) -> Iterator[int]:\n", true, TEXT);
        assertNextToken(parser, "        ", false, LINE_PREFIX);
        assertNextToken(parser,
                "i = 0\n" +
                "while i < n:\n", true, TEXT);
        assertNextToken(parser, "            ", false, LINE_PREFIX);
        assertNextToken(parser,
                "yield i\n" +
                "i += 1\n", true, TEXT);
        assertNextToken(parser, "    ", false, LINE_PREFIX);
        assertNextToken(parser, "```", false, FENCED_CODE_BLOCK);
        assertNextToken(parser, "\n", false, SOFT_LINE_BREAK);
        assertNextToken(parser, "", false, END_TEXT_UNIT);
        assertNextToken(parser, "", false, LINE_PREFIX);
        assertFalse(parser.hasNextToken());
    }


    @Test
    public void testIndentedTextInBulletList() {
        parser.parse("* Opening bullet point:\n" +
                "\n" +
                "    * `inline code 1`\n" +
                "\n" +
                "    Here is an example of some indented text.\n" +
                "\n" +
                "    ```\n" +
                "    a fenced\n" +
                "    code block\n" +
                "    ```\n" +
                "\n" +
                "    More indented text.\n" +
                "\n");
        assertNextToken(parser, "", false, LINE_PREFIX);
        assertNextToken(parser, "* ", false, BULLET_LIST_ITEM);
        assertNextToken(parser, "Opening bullet point:", true, TEXT);
        assertNextToken(parser, "\n", false, SOFT_LINE_BREAK);
        assertNextToken(parser, "\n", false, BLANK_LINE);
        assertNextToken(parser, "    ", false, LINE_PREFIX);
        assertNextToken(parser, "* ", false, BULLET_LIST_ITEM);
        assertNextToken(parser, "`", false, CODE);
        assertNextToken(parser, "inline code 1", true, TEXT);
        assertNextToken(parser, "`", false, CODE);
        assertNextToken(parser, "\n", false, SOFT_LINE_BREAK);
        assertNextToken(parser, "", false, LINE_PREFIX);
        assertNextToken(parser, "\n", false, BLANK_LINE);
        assertNextToken(parser, "    ", false, LINE_PREFIX);
        assertNextToken(parser, "Here is an example of some indented text.", true, TEXT);
        assertNextToken(parser, "\n", false, SOFT_LINE_BREAK);
        assertNextToken(parser, "\n", false, BLANK_LINE);
        assertNextToken(parser, "    ", false, LINE_PREFIX);
        assertNextToken(parser, "```", false, FENCED_CODE_BLOCK);
        assertNextToken(parser, "\n", false, SOFT_LINE_BREAK);
        assertNextToken(parser, "a fenced\ncode block\n", true, TEXT);
        assertNextToken(parser, "    ", false, LINE_PREFIX);
        assertNextToken(parser, "```", false, FENCED_CODE_BLOCK);
        assertNextToken(parser, "\n", false, SOFT_LINE_BREAK);
        assertNextToken(parser, "\n", false, BLANK_LINE);
        assertNextToken(parser, "    ", false, LINE_PREFIX);
        assertNextToken(parser, "More indented text.", true, TEXT);
        assertNextToken(parser, "\n", false, SOFT_LINE_BREAK);
        assertNextToken(parser, "", false, LINE_PREFIX);
        assertNextToken(parser, "\n", false, BLANK_LINE);
        assertFalse(parser.hasNextToken());
    }

    @Test
    public void testSoftLineBreak() {
        parser.parse("This is" + NEWLINE + "a test");
        assertNextToken(parser, "This is" + NEWLINE + "a test", true, TEXT);
        assertFalse(parser.hasNextToken());
    }

    @Test
    public void testStrongEmphasis1() {
        parser.parse("__Bold__");
        assertNextToken(parser, "__", false, STRONG_EMPHASIS);
        assertNextToken(parser, "Bold", true, TEXT);
        assertNextToken(parser, "__", false, STRONG_EMPHASIS);
        assertFalse(parser.hasNextToken());
    }

    @Test
    public void testStrongEmphasis2() {
        parser.parse("**Bold**");
        assertNextToken(parser, "**", false, STRONG_EMPHASIS);
        assertNextToken(parser, "Bold", true, TEXT);
        assertNextToken(parser, "**", false, STRONG_EMPHASIS);
        assertFalse(parser.hasNextToken());
    }

    @Test
    public void testThematicBreak() {
        parser.parse("Hello world\n\n---\n\nHello again world");
        assertNextToken(parser, "Hello world", true, TEXT);
        assertNextToken(parser, NEWLINE, false, SOFT_LINE_BREAK);
        assertNextToken(parser, NEWLINE, false, BLANK_LINE);
        assertNextToken(parser, "---", false, THEMATIC_BREAK);
        assertNextToken(parser, NEWLINE, false, SOFT_LINE_BREAK);
        assertNextToken(parser, NEWLINE, false, BLANK_LINE);
        assertNextToken(parser, "", false, LINE_PREFIX);
        assertNextToken(parser, "Hello again world", true, TEXT);
        assertFalse(parser.hasNextToken());
    }

    @Test
    public void testCommonMarkTokens() throws Exception {
        parser.parse(getFileContents("commonmark.md"));

        assertNextToken(parser, "## ", false, HEADING_PREFIX);
        assertNextToken(parser, "Try CommonMark", true, TEXT);
        assertNextToken(parser, NEWLINE, false, SOFT_LINE_BREAK);
        assertNextToken(parser, NEWLINE, false, BLANK_LINE);
        assertNextToken(parser, "", false, LINE_PREFIX);
        assertNextToken(parser, "You can try CommonMark here.  This dingus is powered by" + NEWLINE,
                true, TEXT);
        assertNextToken(parser, "[", false, LINK);
        assertNextToken(parser, "commonmark.js", true, TEXT);
        assertNextToken(parser, "](https://github.com/jgm/commonmark.js)", false, LINK);
        assertNextToken(parser, ", the" + NEWLINE + "JavaScript reference implementation.", true, TEXT);
        assertNextToken(parser, NEWLINE, false, SOFT_LINE_BREAK);
        assertNextToken(parser, NEWLINE, false, BLANK_LINE);

        assertNextToken(parser, "", false, LINE_PREFIX);
        assertNextToken(parser, "1. ", false, ORDERED_LIST_ITEM);
        assertNextToken(parser, "item one", true, TEXT);
        assertNextToken(parser, NEWLINE, false, SOFT_LINE_BREAK);
        assertNextToken(parser, "", false, LINE_PREFIX);

        assertNextToken(parser, "", false, LINE_PREFIX);
        assertNextToken(parser, "2. ", false, ORDERED_LIST_ITEM);
        assertNextToken(parser, "item two", true, TEXT);
        assertNextToken(parser, NEWLINE, false, SOFT_LINE_BREAK);

        assertNextToken(parser, "   ", false, LINE_PREFIX);
        assertNextToken(parser, "- ", false, BULLET_LIST_ITEM);

        assertNextToken(parser, "sublist", true, TEXT);
        assertNextToken(parser, NEWLINE, false, SOFT_LINE_BREAK);
        assertNextToken(parser, "", false, LINE_PREFIX);
        assertNextToken(parser, "   ", false, LINE_PREFIX);
        assertNextToken(parser, "- ", false, BULLET_LIST_ITEM);

        assertNextToken(parser, "sublist", true, TEXT);
        assertNextToken(parser, NEWLINE, false, SOFT_LINE_BREAK);
        assertNextToken(parser, "", false, LINE_PREFIX);
        assertNextToken(parser, "", false, LINE_PREFIX);

        assertNextToken(parser, NEWLINE, false, BLANK_LINE);

        assertFalse(parser.hasNextToken());
    }

    @Test
    public void testTable1Tokens() throws Exception {
        parser.parse(getFileContents("table1.md"));

        // Table header row
        assertNextToken(parser, "| ", false, TABLE_PIPE);
        assertNextToken(parser, "Command", true, TEXT);
        assertNextToken(parser, " ", false, WHITE_SPACE);
        assertNextToken(parser, "| ", false, TABLE_PIPE);
        assertNextToken(parser, "Description", true, TEXT);
        assertNextToken(parser, " ", false, WHITE_SPACE);
        assertNextToken(parser, "|", false, TABLE_PIPE);
        assertNextToken(parser, NEWLINE, false, SOFT_LINE_BREAK);

        // Table separator row
        assertNextToken(parser, "| --- | ---: |", false, TABLE_SEPARATOR);
        assertNextToken(parser, NEWLINE, false, SOFT_LINE_BREAK);

        // Table body row
        assertNextToken(parser, "| ", false, TABLE_PIPE);
        assertNextToken(parser, "`", false, CODE);
        assertNextToken(parser, "git status", true, TEXT);
        assertNextToken(parser, "`", false, CODE);
        assertNextToken(parser, " ", false, WHITE_SPACE);
        assertNextToken(parser, "| ", false, TABLE_PIPE);
        assertNextToken(parser, "List all ", true, TEXT);
        assertNextToken(parser, "**", false, STRONG_EMPHASIS);
        assertNextToken(parser, "new", true, TEXT);
        assertNextToken(parser, "**", false, STRONG_EMPHASIS);
        assertNextToken(parser, " or ", true, TEXT);
        assertNextToken(parser, "_", false, EMPHASIS);
        assertNextToken(parser, "modified", true, TEXT);
        assertNextToken(parser, "_", false, EMPHASIS);
        assertNextToken(parser, " files", true, TEXT);
        assertNextToken(parser, " ", false, WHITE_SPACE);
        assertNextToken(parser, "|", false, TABLE_PIPE);
        assertNextToken(parser, NEWLINE, false, SOFT_LINE_BREAK);
        assertNextToken(parser, NEWLINE, false, BLANK_LINE);

        assertFalse(parser.hasNextToken());
    }

    @Test
    public void testLinkWithText() throws Exception {
        parser.parse("Welcome to the [Okapi Framework](http://okapiframework.org/)");
        assertNextToken(parser, "Welcome to the ", true, TEXT);
        assertNextToken(parser, "[", false, LINK);
        assertNextToken(parser, "Okapi Framework", true, TEXT);
        assertNextToken(parser, "](http://okapiframework.org/)", false, LINK);
    }

    @Test
    public void testMathlm() throws Exception {
        String mathBlock =
                "<math xmlns=\"http://www.w3.org/1998/Math/MathML\">\n" +
                        "  <mstyle displaystyle=\"true\">\n" +
                        "    <msup>\n" +
                        "      <mrow>\n" +
                        "        <mi> &#x03C1;<!--greek small letter rho--> </mi>\n" +
                        "      </mrow>\n" +
                        "      <mrow>\n" +
                        "        <mi> charge </mi>\n" +
                        "      </mrow>\n" +
                        "    </msup>\n" +
                        "  </mstyle></math>";
        String snippet = "This contains a math block\n\n" +
                mathBlock +
                "\n\n" +
                "End of the math block";
        parser.parse(snippet);
        assertNextToken(parser, "This contains a math block", true, TEXT);
        assertNextToken(parser, NEWLINE, false, SOFT_LINE_BREAK);
        assertNextToken(parser, NEWLINE, false, BLANK_LINE);
        assertNextToken(parser, "", false, LINE_PREFIX);
        assertNextToken(parser, mathBlock, true, MarkdownTokenType.HTML_BLOCK);
        assertNextToken(parser, NEWLINE, false, SOFT_LINE_BREAK);
        assertNextToken(parser, NEWLINE, false, BLANK_LINE);
        assertNextToken(parser, "", false, LINE_PREFIX);
        assertNextToken(parser, "End of the math block", true, TEXT);
    }

    @Test
    public void testMathlmSingleLine() throws Exception {
        String snippet =
                "<math xmlns=\"http://www.w3.org/1998/Math/MathML\"> <mstyle displaystyle=\"true\"> " +
                  "<mo> &#x2207;<!--nabla--> </mo><mo> &#x00D7;<!--multiplication sign--> </mo></mo> " +
                  "<mfenced> <mrow> " +
                    "<mover> <mrow> <mi> H </mi> </mrow> <mrow> <mo> &#x2192;<!--rightwards arrow--> </mo> </mrow> </mover> " +
                  "</mrow> </mfenced> " +
                  "<mo> = </mo> " +
                  "<mover> <mrow> <mi> J </mi> </mrow> <mrow> <mo> &#x2192;<!--rightwards arrow--> </mo> </mrow> </mover> " +
                  "<mo> + </mo> " +
                  "<mfrac> " +
                   "<mrow> " +
                    "<mi> d </mi> " +
                    "<mover> " +
                     "<mrow> <mi> D </mi> </mrow> " +
                     "<mrow> <mo> &#x2192;<!--rightwards arrow--> </mo> </mrow> " +
                   "</mover> </mrow> " +
                 "<mrow> <mo> &#x2202;<!--partial differential--> </mo> <mi> t </mi> </mrow> " +
                "</mfrac> " +
                "</mstyle></math>(from Ampere's law)";
        parser.parse(snippet);
        assertNextToken(parser, snippet, true, HTML_BLOCK);
    }

    @Test
    public void testMathlmInListItem() throws Exception {
        String ulMathBlock =
                "<ul>\n" +
                "<li><math xmlns=\"http://www.w3.org/1998/Math/MathML\">\n" +
                "  <mstyle displaystyle=\"true\">\n" +
                "    <mover>\n" +
                "      <mrow>\n" +
                "        <mi> H </mi>\n" +
                "      </mrow>\n" +
                "      <mrow>\n" +
                "        <mo> &#x2192;<!--rightwards arrow--> </mo>\n" +
                "      </mrow>\n" +
                "    </mover>\n" +
                "  </mstyle></math> is the magnetic field intensity</li>\n" +
                "</ul>";
        String snippet = "This list contains a math block\n\n" +
                ulMathBlock;
        parser.parse(snippet);
        assertNextToken(parser, "This list contains a math block", true, TEXT);
        assertNextToken(parser, NEWLINE, false, SOFT_LINE_BREAK);
        assertNextToken(parser, NEWLINE, false, BLANK_LINE);
        assertNextToken(parser, "", false, LINE_PREFIX);
        assertNextToken(parser, ulMathBlock, true, HTML_BLOCK); // It's translatable because of the text after </math>.
    }

    @Test
    public void testIndentedText() {
        parser.parse("* bullet point" + NEWLINE + NEWLINE + "    line");
        assertNextToken(parser, "", false, LINE_PREFIX);
        assertNextToken(parser, "* ", false, BULLET_LIST_ITEM);
        assertNextToken(parser, "bullet point", true, TEXT);
        assertNextToken(parser, NEWLINE, false, SOFT_LINE_BREAK);
        assertNextToken(parser, NEWLINE, false, BLANK_LINE);
        assertNextToken(parser, "    ", false, LINE_PREFIX);
        assertNextToken(parser, "line", true, TEXT);
    }

    @Test
    public void testIndentedHtml() {
        parser.parse("<table>" + NEWLINE
                + "  <tbody><tr>" + NEWLINE
                + "   <td><ul>" + NEWLINE
                + "  <li>element1</li>" + NEWLINE
                + "  <li>element2</li>" + NEWLINE
                + "</ul>" + NEWLINE
                + "</td>" + NEWLINE
                + "    <td>" + NEWLINE
                + "      <ul>" + NEWLINE + NEWLINE
                + "        <li>element3</li>" + NEWLINE + NEWLINE
                + "        <li>element4</li>" + NEWLINE + NEWLINE
                + "      </ul>" + NEWLINE
                + "    </td>" + NEWLINE
                + "  </tr>" + NEWLINE
                + "</tbody></table>");
        assertNextToken(parser, "<table>" + NEWLINE
                + "  <tbody><tr>" + NEWLINE
                + "   <td><ul>" + NEWLINE
                + "  <li>element1</li>" + NEWLINE
                + "  <li>element2</li>" + NEWLINE
                + "</ul>" + NEWLINE
                + "</td>" + NEWLINE
                + "    <td>" + NEWLINE
                + "      <ul>", true, HTML_BLOCK);
        assertNextToken(parser, NEWLINE, false, SOFT_LINE_BREAK);
        assertNextToken(parser, NEWLINE, false, BLANK_LINE);
        assertNextToken(parser, "    ", false, LINE_PREFIX);
        assertNextToken(parser, "    <li>element3</li>" + NEWLINE + NEWLINE
                + "    <li>element4</li>" + NEWLINE + NEWLINE
                + "  </ul>" + NEWLINE
                + "</td>" + NEWLINE, true, TEXT);
        assertNextToken(parser, "", false, END_TEXT_UNIT);
        assertNextToken(parser, "", false, LINE_PREFIX);
        assertNextToken(parser, "  </tr>" + NEWLINE
                + "</tbody></table>", true, HTML_BLOCK);
    }

    @Test
    public void testIndentedAutoLink() {
        parser.parse("* bullet point" + NEWLINE + NEWLINE +"    <https://www.google.com>");
        assertNextToken(parser, "", false, LINE_PREFIX);
        assertNextToken(parser, "* ", false, BULLET_LIST_ITEM);
        assertNextToken(parser, "bullet point", true, TEXT);
        assertNextToken(parser, NEWLINE, false, SOFT_LINE_BREAK);
        assertNextToken(parser, NEWLINE, false, BLANK_LINE);
        assertNextToken(parser, "    <https://www.google.com>", false, AUTO_LINK);
    }

    @Test
    public void testIndentedInlineLink() {
        parser.parse("* bullet point" + NEWLINE + NEWLINE +"    [link](https://google.com)");
        assertNextToken(parser, "", false, LINE_PREFIX);
        assertNextToken(parser, "* ", false, BULLET_LIST_ITEM);
        assertNextToken(parser, "bullet point", true, TEXT);
        assertNextToken(parser, NEWLINE, false, SOFT_LINE_BREAK);
        assertNextToken(parser, NEWLINE, false, BLANK_LINE);
        assertNextToken(parser, "    [", false, LINK);
        assertNextToken(parser, "link", true, TEXT);
        assertNextToken(parser, "](https://google.com)", false, LINK);
    }

    @Test
    public void testIndentedHtmlBlock() {
        parser.parse("* bullet point" + NEWLINE + NEWLINE +"    <img src=\"img.png\">");
        assertNextToken(parser, "", false, LINE_PREFIX);
        assertNextToken(parser, "* ", false, BULLET_LIST_ITEM);
        assertNextToken(parser, "bullet point", true, TEXT);
        assertNextToken(parser, NEWLINE, false, SOFT_LINE_BREAK);
        assertNextToken(parser, NEWLINE, false, BLANK_LINE);
        assertNextToken(parser, "  ", false, LINE_PREFIX);
        assertNextToken(parser, "  <img src=\"img.png\">", true, HTML_BLOCK);
    }

    @Test
    public void testIndentedOrderedListAfterDocusaurusAdmonition() throws Exception {
        try (MarkdownFilter filter = new MarkdownFilter()) {
            FilterConfigurationMapper mapper = new FilterConfigurationMapper();

            IParameters markdownParameters = new Parameters();
            String markdownParamsAsString = Util.normalizeNewlines(getFileContents("../custom-configs/okf_markdown@custom15437.fprm"));
            markdownParameters.fromString(markdownParamsAsString);

            IParameters yamlParameters = new net.sf.okapi.filters.yaml.Parameters();
            String yamlParamsAsString = Util.normalizeNewlines(getFileContents("../custom-configs/okf_yaml@custom15437.fprm"));
            yamlParameters.fromString(yamlParamsAsString);

            IParameters htmlParameters = new net.sf.okapi.filters.html.Parameters();
            String htmlParamsAsString = Util.normalizeNewlines(getFileContents("../custom-configs/okf_html@custom15437.fprm"));
            htmlParameters.fromString(htmlParamsAsString);

            mapper.addConfiguration(
                    new FilterConfiguration("okf_markdown@custom15437",
                            MimeTypeMapper.MARKDOWN_MIME_TYPE,
                            MarkdownFilter.class.getName(),
                            "Markdown", "", null, markdownParameters, ".md;.mdx"));

            mapper.addConfiguration(
                    new FilterConfiguration("okf_yaml@custom15437",
                            MimeTypeMapper.YAML_MIME_TYPE,
                            YamlFilter.class.getName(),
                            "Yaml", "", null, yamlParameters, ".yml"));

            mapper.addConfiguration(
                    new FilterConfiguration("okf_html@custom15437",
                            MimeTypeMapper.HTML_MIME_TYPE,
                            HtmlFilter.class.getName(),
                            "Html", "", null, htmlParameters, ".html"));

            filter.setFilterConfigurationMapper(mapper);

            Parameters params = filter.getParameters();
            params.setYamlSubfilter("okf_yaml@custom15437");
            params.setHtmlSubfilter("okf_html@custom15437");

            MarkdownParser customParser = new MarkdownParser(params, NEWLINE);

            customParser.parse("#### lorem ipsum\n" +
                    "\n" +
                    "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lorem ipsum dolor sit amet, consectetur adipiscing elit.\n" +
                    "\n" +
                    "Lorem ipsum dolor sit amet, consectetur adipiscing elit.\n" +
                    "\n" +
                    "::: lorem-ipsum\n" +
                    "  ::: panel Ipsum\n" +
                    "\n" +
                    "  ```JSON\n" +
                    "  {\n" +
                    "    \"timestamp\": “%{strftime({\"%Y-%m-%dT%H:%M:%S\"}, time.start)}V”,\n" +
                    "    \"policy_name\": \"%{json.escape(“<lorem ipsum dolor>”)}V”,\n" +
                    "    \"url\": “%{json.escape(req.url)}V”,\n" +
                    "    \"lorem\": <lorem>,\n" +
                    "    \"ipsum\": <ipsum>,\n" +
                    "    \"dolor\": \"<dolor>\",\n" +
                    "    \"sit\":  “<sit>\"\n" +
                    "  }\n" +
                    "  ```\n" +
                    "\n" +
                    "  :::\n" +
                    "  ::: panel Lorem\n" +
                    "\n" +
                    "  ```JSON\n" +
                    "  {\n" +
                    "    \"time_start\": “%{strftime({\"%Y-%m-%dT%H:%M:%S%Z\"}, time.start)}V”,\n" +
                    "    \"ddsource\": \"lorem\",\n" +
                    "    \"service\": “%{req.service_id}V\",\n" +
                    "    \"policy_name\": \"%{json.escape(“<lorem ipsum dolor>”)}V”,\n" +
                    "    \"url\": “%{json.escape(req.url)}V”,\n" +
                    "    \"lorem\": <lorem>,\n" +
                    "    \"ipsum\": <ipsum>,\n" +
                    "    \"dolor\": \"<dolor>\",\n" +
                    "    \"sit\":  “<sit>\"\n" +
                    "  }\n" +
                    "  ```\n" +
                    "\n" +
                    "  :::\n" +
                    "  ::: panel Lorem\n" +
                    "\n" +
                    "  ```JSON\n" +
                    "  {\n" +
                    "    \"time\": “%{strftime({\"%Y-%m-%dT%H:%M:%SZ\"}, time.start)}V”,\n" +
                    "    \"data\":{\n" +
                    "      \"service_id\": “%{req.service_id}V\",\n" +
                    "      \"policy_name\": \"%{json.escape(“<lorem ipsum dolor>”)}V”,\n" +
                    "      \"url\": “%{json.escape(req.url)}V”,\n" +
                    "      \"lorem\": <lorem>,\n" +
                    "      \"ipsum\": <ipsum>,\n" +
                    "      \"dolor\": \"<dolor>\",\n" +
                    "      \"sit\":  “<sit>\"\n" +
                    "    }\n" +
                    "  }\n" +
                    "  ```\n" +
                    "\n" +
                    "  :::\n" +
                    ":::\n" +
                    "\n" +
                    "## Lorem Ipsum Dolor Sit Amet\n" +
                    "\n" +
                    "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.\n" +
                    "\n" +
                    "Lorem ipsum dolor sit amet, consectetur adipiscing elit.\n" +
                    "\n" +
                    "1. <T>lorem-ipsum</T>\n" +
                    "1. Lorem **ipsum dolor sit amet**.\n" +
                    "1. Lorem ipsum dolor sit amet, consectetur adipiscing elit.\n" +
                    "1. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed do eiusmod tempor incididunt ut labore **Et dolore magna aliqua**.\n" +
                    "1. Lorem ipsum dolor sit amet, consectetur adipiscing elit\n" +
                    "1. Lorem **ipsum dolor sit amet**.");
        } catch (IndexOutOfBoundsException e) {
            LOGGER.warn("Indent parse failure", e);
            Assert.fail("Ran into an index out of bounds exception during indent parsing.");
        }
    }


    @Test
    public void testHtmlBlockMisparsedDocusaurusAdmonition() throws Exception {
        try (MarkdownFilter filter = new MarkdownFilter()) {
            FilterConfigurationMapper mapper = new FilterConfigurationMapper();

            IParameters markdownParameters = new Parameters();
            String markdownParamsAsString = Util.normalizeNewlines(getFileContents("../custom-configs/okf_markdown@custom15437.fprm"));
            markdownParameters.fromString(markdownParamsAsString);

            IParameters yamlParameters = new net.sf.okapi.filters.yaml.Parameters();
            String yamlParamsAsString = Util.normalizeNewlines(getFileContents("../custom-configs/okf_yaml@custom15437.fprm"));
            yamlParameters.fromString(yamlParamsAsString);

            IParameters htmlParameters = new net.sf.okapi.filters.html.Parameters();
            String htmlParamsAsString = Util.normalizeNewlines(getFileContents("../custom-configs/okf_html@custom15437.fprm"));
            htmlParameters.fromString(htmlParamsAsString);

            mapper.addConfiguration(
                    new FilterConfiguration("okf_markdown@custom15437",
                            MimeTypeMapper.MARKDOWN_MIME_TYPE,
                            MarkdownFilter.class.getName(),
                            "Markdown", "", null, markdownParameters, ".md;.mdx"));

            mapper.addConfiguration(
                    new FilterConfiguration("okf_yaml@custom15437",
                            MimeTypeMapper.YAML_MIME_TYPE,
                            YamlFilter.class.getName(),
                            "Yaml", "", null, yamlParameters, ".yml"));

            mapper.addConfiguration(
                    new FilterConfiguration("okf_html@custom15437",
                            MimeTypeMapper.HTML_MIME_TYPE,
                            HtmlFilter.class.getName(),
                            "Html", "", null, htmlParameters, ".html"));

            filter.setFilterConfigurationMapper(mapper);

            Parameters params = filter.getParameters();
            params.setYamlSubfilter("okf_yaml@custom15437");
            params.setHtmlSubfilter("okf_html@custom15437");

            MarkdownParser customParser = new MarkdownParser(params, NEWLINE);

            customParser.parse("### Lorem ipsum\n" +
                    "\n" +
                    "  Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet [test test](/en/guides/test) Lorem ipsum dolor sit amet.\n" +
                    "\n" +
                    "  <LoremIpsum />\n" +
                    "  :::\n" +
                    "  ::: panel test test\n" +
                    "  1. <T>test-test-test</T>\n" +
                    "  2. Lorem ipsum dolor sit amet, test **test test**.\n" +
                    "  3. Lorem ipsum dolor sit amet **Lorem ipsum dolor sit amet** test test test:\n" +
                    "     * <T>compute-logging-name-field</T>\n" +
                    "     * Test test **Lorem ipsum** test, Lorem ipsum dolor sit amet **lorem ipsum** or **dolor sit**.\n" +
                    "     * Lorem ipsum dolor sit amet **lorem ipsum**, Lorem ipsum dolor sit amet. Lorem ipsum [lorem ipsum](https://dummy.url.com/) lorem ipsum.\n" +
                    "\n" +
                    "     <Alert note>Lorem ipsum dolor sit amet **lorem ipsum** Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet **lorem ipsum** Lorem ipsum dolor sit amet.</Alert>\n" +
                    "\n" +
                    "     * Lorem ipsum dolor sit amet **test test**, Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet [Lorem ipsum dolor sit amet](/test/url/slug).\n" +
                    "     * Test test **lorem ipsum** test, Lorem ipsum dolor sit amet.\n" +
                    "     * Test test **Lorem** ipsum, Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet.\n" +
                    "  4. <T>test-test</T>\n" +
                    "  5. <T>test-test-test</T>\n" +
                    "  :::\n" +
                    ":::");

            String tokenDump = customParser.dumpTokens();
            // ensure correct line prefixing
            assertTrue(tokenDump.contains("[, false, LINE_PREFIX]\n" +
                    "[  <LoremIpsum />"));
            assertTrue(tokenDump.contains("\n:::"));
        } catch (IndexOutOfBoundsException e) {
            LOGGER.warn("Indent parse failure", e);
            Assert.fail("Ran into an index out of bounds exception during indent parsing.");
        }
    }
    
    private String getFileContents(String filename) throws Exception {
        try (InputStream is = FileLocation.fromClass(getClass()).in(filename).asInputStream();
                Scanner scanner = new Scanner(is)) {
            return scanner.useDelimiter("\\A").next().replaceAll(System.lineSeparator(), NEWLINE);
        }
    }

    private void assertNextToken(MarkdownParser parser, String content, boolean isTranslatable,
            MarkdownTokenType type) {
        assertTrue(parser.hasNextToken());
        MarkdownToken token = parser.getNextToken();
        assertEquals(content, token.getContent());
        assertEquals(isTranslatable, token.isTranslatable());
        assertEquals(type, token.getType());
    }

}
