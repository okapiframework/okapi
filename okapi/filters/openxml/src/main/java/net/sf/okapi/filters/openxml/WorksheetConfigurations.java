/*
 * =============================================================================
 * Copyright (C) 2010-2021 by the Okapi Framework contributors
 * -----------------------------------------------------------------------------
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =============================================================================
 */
package net.sf.okapi.filters.openxml;

import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public interface WorksheetConfigurations {

    /**
     * Obtains source and target columns for a specified worksheet name.
     * @param worksheetName The worksheet name
     * @return The source and target columns
     */
    SourceAndTargetColumns sourceAndTargetColumnsFor(final String worksheetName);

    /**
     * Obtains the target columns maximum number of characters for a specified worksheet name.
     * @param worksheetName The worksheet name
     * @return The maximum number of characters
     */
    TargetColumnsMaxCharacters targetColumnsMaxCharactersFor(final String worksheetName);

    /**
     * Obtains excluded rows for a specified worksheet name.
     * @param worksheetName The worksheet name
     * @return The excluded rows
     */
    Set<Integer> excludedRowsFor(final String worksheetName);

    /**
     * Obtains excluded columns for a specified worksheet name.
     * @param worksheetName The worksheet name
     * @return The excluded rows
     */
    Set<String> excludedColumnsFor(final String worksheetName);

    /**
     * Obtains metadata rows for a specified worksheet name.
     * @param worksheetName The worksheet name
     * @return The excluded rows
     */
    Set<Integer> metadataRowsFor(final String worksheetName);

    /**
     * Obtains metadata columns for a specified worksheet name.
     * @param worksheetName The worksheet name
     * @return The metadata rows
     */
    Set<String> metadataColumnsFor(final String worksheetName);

    /**
     * Adds worksheet configurations from input.
     * @param input The worksheet configurations input to add from
     */
    void addFrom(final WorksheetConfigurations.Input input);

    /**
     * Obtains the worksheet configurations output with the written worksheet configurations to it.
     * @param output The output
     * @return The output with the written worksheet configurations
     */
    <T> T writtenTo(final WorksheetConfigurations.Output<T> output);

    final class Default implements WorksheetConfigurations {
        private static final String EMPTY = "";
        private final List<WorksheetConfiguration> configurations;

        public Default(final WorksheetConfiguration... configurations) {
            this(new LinkedList<>(Arrays.asList(configurations)));
        }

        public Default(final List<WorksheetConfiguration> configurations) {
            this.configurations = configurations;
        }

        @Override
        public SourceAndTargetColumns sourceAndTargetColumnsFor(final String worksheetName) {
            return new SourceAndTargetColumns.Default(
                    this.configurations.stream()
                    .filter(c -> c.matches(worksheetName))
                    .flatMap(c -> c.sourceColumns().stream())
                    .collect(Collectors.toList()),
                this.configurations.stream()
                    .filter(c -> c.matches(worksheetName))
                    .flatMap(c -> c.targetColumns().stream())
                    .collect(Collectors.toList())
            );
        }

        @Override
        public TargetColumnsMaxCharacters targetColumnsMaxCharactersFor(final String worksheetName) {
            return new TargetColumnsMaxCharacters.Default(
                this.configurations.stream()
                    .filter(c -> c.matches(worksheetName))
                    .flatMap(c -> c.targetColumns().stream())
                    .collect(Collectors.toList()),
                this.configurations.stream()
                    .filter(c -> c.matches(worksheetName))
                    .flatMap(c -> c.targetColumnsMaxCharacters().stream())
                    .collect(Collectors.toList())
            );
        }

        @Override
        public Set<Integer> excludedRowsFor(final String worksheetName) {
            return this.configurations.stream()
                .filter(c -> c.matches(worksheetName))
                .flatMap(c -> c.excludedRows().stream())
                .collect(Collectors.toSet());
        }

        @Override
        public Set<String> excludedColumnsFor(final String worksheetName) {
            return this.configurations.stream()
                .filter(c -> c.matches(worksheetName))
                .flatMap(c -> c.excludedColumns().stream())
                .collect(Collectors.toSet());
        }

        @Override
        public Set<Integer> metadataRowsFor(final String worksheetName) {
            return this.configurations.stream()
                .filter(c -> c.matches(worksheetName))
                .flatMap(c -> c.metadataRows().stream())
                .collect(Collectors.toSet());
        }

        @Override
        public Set<String> metadataColumnsFor(final String worksheetName) {
            return this.configurations.stream()
                .filter(c -> c.matches(worksheetName))
                .flatMap(c -> c.metadataColumns().stream())
                .collect(Collectors.toSet());
        }

        @Override
        public void addFrom(final WorksheetConfigurations.Input input) {
            final Iterator<WorksheetConfiguration> iterator = input.read();
            while (iterator.hasNext()) {
                this.configurations.add(iterator.next());
            }
        }

        @Override
        public <T> T writtenTo(final Output<T> output) {
            return output.writtenWith(this.configurations.iterator());
        }
    }

    /**
     * The worksheet configurations input.
     */
    interface Input {
        /**
         * Obtains an iterator of the worksheet configurations, which have been read
         * from the input.
         * @return The worksheet configurations iterator
         */
        Iterator<WorksheetConfiguration> read();
    }

    /**
     * The worksheet configurations output.
     * @param <T> The type of the output
     */
    interface Output<T> {
        /**
         * Obtains a written output with the help of a provided worksheet configurations iterator.
         * @param worksheetConfigurationsIterator The worksheet configurations iterator
         * @return The written output
         */
        T writtenWith(final Iterator<WorksheetConfiguration> worksheetConfigurationsIterator);
    }
}
