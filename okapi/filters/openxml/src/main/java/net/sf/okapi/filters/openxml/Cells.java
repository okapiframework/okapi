/*
 * =============================================================================
 * Copyright (C) 2010-2021 by the Okapi Framework contributors
 * -----------------------------------------------------------------------------
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =============================================================================
 */
package net.sf.okapi.filters.openxml;

import javax.xml.stream.XMLEventFactory;
import javax.xml.stream.events.Attribute;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.ListIterator;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.stream.Collectors;

interface Cells {
    void add(final Cell cell);
    void add(final Cells cells);
    Cells of(final CellType type);
    ListIterator<Cell> iterator();
    boolean worksheetStartsAt(final ListIterator<Cell> iterator);
    boolean rowStartsAt(final ListIterator<Cell> iterator);
    Markup asMarkup();
    int size();
    void clear();

    class Default implements Cells {
        private final boolean extractCopied;
        private final XMLEventFactory eventFactory;
        private final SharedStringsFragments sharedStringsFragments;
        private final ArrayList<Cell> items;
        private final Map<String, Integer> formerValues;
        private int sharedStringIndex;

        Default(
            final boolean extractCopied,
            final XMLEventFactory eventFactory,
            final SharedStringsFragments sharedStringsFragments
        ) {
            this(extractCopied, eventFactory, sharedStringsFragments, new ArrayList<>(), new HashMap<>());
        }

        Default(
            final boolean extractCopied,
            final XMLEventFactory eventFactory,
            final SharedStringsFragments sharedStringsFragments,
            final ArrayList<Cell> items,
            final Map<String, Integer> formerValues
        ) {
            this.extractCopied = extractCopied;
            this.eventFactory = eventFactory;
            this.sharedStringsFragments = sharedStringsFragments;
            this.items = items;
            this.formerValues = formerValues;
        }

        @Override
        public void add(final Cell cell) {
            if (this.extractCopied || CellType.SHARED_STRING != cell.type() || cell.excluded() || !cell.valuePresent()) {
                addAndUpdate(cell);
            } else {
                final String fs = cell.value().asFormattedString();
                final int sharedIndex = this.formerValues.getOrDefault(fs, -1);
                if (sharedIndex != -1) {
                    cell.value().update(this.eventFactory.createCharacters(String.valueOf(sharedIndex)));
                } else {
                    this.formerValues.put(fs, sharedStringIndex);
                    addAndUpdate(cell);
                }
            }
        }

        private void addAndUpdate(final Cell cell) {
            this.items.add(cell);
            if (CellType.SHARED_STRING == cell.type() || CellType.INLINE_STRING == cell.type()) {
                cell.value().update(this.eventFactory.createCharacters(String.valueOf(this.sharedStringIndex)));
                if (CellType.INLINE_STRING == cell.type()) {
                    this.sharedStringsFragments.addStringItemFormattedInlineAt(this.sharedStringIndex);
                    cell.refineExcluded();
                }
                this.sharedStringIndex++;
            }
        }

        @Override
        public void add(final Cells cells) {
            this.items.ensureCapacity(this.items.size() + cells.size());

            final Iterator<Cell> iterator = cells.iterator();
            while (iterator.hasNext()) {
                add(iterator.next());
            }
        }

        @Override
        public Cells of(final CellType type) {
            return new Default(
                this.extractCopied,
                this.eventFactory,
                this.sharedStringsFragments,
                this.items.stream()
                    .filter(c -> c.type() == type)
                    .collect(Collectors.toCollection(ArrayList::new)),
                new HashMap<>(this.formerValues)
            );
        }

        @Override
        public ListIterator<Cell> iterator() {
            return this.items.listIterator();
        }

        @Override
        public boolean worksheetStartsAt(final ListIterator<Cell> iterator) {
            if (!iterator.hasPrevious()) {
                // at the start of the list
                return true;
            }
            final Cell current = iterator.previous();
            if (!iterator.hasPrevious()) {
                // at the start of the list
                iterator.next(); // restore the position
                return true;
            }
            final Cell previous = iterator.previous();
            iterator.next(); // restore the position
            iterator.next();
            return !previous.worksheetName().equals(current.worksheetName());
        }

        @Override
        public boolean rowStartsAt(final ListIterator<Cell> iterator) {
            if (!iterator.hasPrevious()) {
                // at the start of the list
                return true;
            }
            final Cell current = iterator.previous();
            if (!iterator.hasPrevious()) {
                // at the start of the list
                iterator.next(); // restore the position
                return true;
            }
            final Cell previous = iterator.previous();
            iterator.next(); // restore the position
            iterator.next();
            return previous.cellReferencesRange().first().row() != current.cellReferencesRange().first().row();
        }

        @Override
        public Markup asMarkup() {
            final Markup m = new Markup.General(new ArrayList<>(this.items.size()));
            this.items.forEach(cell -> m.add(cell.asMarkup()));
            return m;
        }

        @Override
        public int size() {
            return this.items.size();
        }

        @Override
        public void clear() {
            this.items.clear();
            this.formerValues.clear();
        }
    }

    class Sorted implements Cells {
        private final ConditionalParameters conditionalParameters;
        private final SourceAndTargetColumns sourceAndTargetColumns;
        private final Set<Integer> excludedRows;
        private final SortedMap<String, Cell> items;

        Sorted(
            final ConditionalParameters conditionalParameters,
            final SourceAndTargetColumns sourceAndTargetColumns,
            final Set<Integer> excludedRows
        ) {
            this(
                conditionalParameters,
                sourceAndTargetColumns,
                excludedRows,
                new TreeMap<>(new StringsComparator())
            );
        }

        Sorted(
            final ConditionalParameters conditionalParameters,
            final SourceAndTargetColumns sourceAndTargetColumns,
            final Set<Integer> excludedRows,
            final SortedMap<String, Cell> items
        ) {
            this.conditionalParameters = conditionalParameters;
            this.sourceAndTargetColumns = sourceAndTargetColumns;
            this.excludedRows = excludedRows;
            this.items = items;
        }

        @Override
        public void add(final Cell cell) {
            final String column = cell.cellReferencesRange().first().column();
            this.items.put(column, cell);
        }

        @Override
        public void add(final Cells cells) {
            final Iterator<Cell> iterator = cells.iterator();
            while (iterator.hasNext()) {
                add(iterator.next());
            }
        }

        @Override
        public Cells of(final CellType type) {
            throw new UnsupportedOperationException();
        }

        @Override
        public ListIterator<Cell> iterator() {
            alignWithSourceAndTargetColumns();
            return new ArrayList<>(this.items.values()).listIterator();
        }

        private void alignWithSourceAndTargetColumns() {
            this.sourceAndTargetColumns.source().forEach(sc -> {
                final String tc = this.sourceAndTargetColumns.targetFor(sc);
                if (!cellExcludedIn(tc)) {
                    if (this.items.containsKey(sc)) {
                        this.items.put(
                            tc,
                            this.items.get(sc).copiedWithAdjusted(tc, cellStyleAttributeIn(tc))
                        );
                    } else {
                        // source does not exist
                        if (!this.conditionalParameters.getPreserveExcelStylesInTargetColumns()) {
                            this.items.remove(tc);
                        }
                    }
                }
            });
        }

        private Attribute cellStyleAttributeIn(final String column) {
            final Attribute sa;
            if (this.conditionalParameters.getPreserveExcelStylesInTargetColumns()) {
                if (this.items.containsKey(column)) {
                    sa = this.items.get(column).styleAttribute();
                } else {
                    sa = null;
                }
            } else {
                sa = null;
            }
            return sa;
        }

        private boolean cellExcludedIn(final String column) {
            final boolean excluded;
            if (this.items.containsKey(column)) {
                excluded = this.items.get(column).excluded();
            } else {
                excluded = this.items.values().stream()
                    .findFirst()
                    .map(c -> c.cellReferencesRange().anyMatch(this.excludedRows, Collections.emptySet()))
                    .orElse(false); // there are no cells, should never reach here
            }
            return excluded;
        }

        @Override
        public boolean worksheetStartsAt(final ListIterator<Cell> iterator) {
            throw new UnsupportedOperationException();
        }

        @Override
        public boolean rowStartsAt(final ListIterator<Cell> iterator) {
            throw new UnsupportedOperationException();
        }

        @Override
        public Markup asMarkup() {
            final Markup m = new Markup.General(new ArrayList<>(this.items.size()));
            this.items.values().forEach(cell -> m.add(cell.asMarkup()));
            return m;
        }

        @Override
        public int size() {
            return this.items.size();
        }

        @Override
        public void clear() {
            this.items.clear();
        }
    }
}
