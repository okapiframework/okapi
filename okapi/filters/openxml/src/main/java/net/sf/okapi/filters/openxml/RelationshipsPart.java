/*===========================================================================
  Copyright (C) 2016-2017 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
===========================================================================*/

package net.sf.okapi.filters.openxml;

import net.sf.okapi.common.Event;
import net.sf.okapi.common.EventType;
import net.sf.okapi.common.IdGenerator;
import net.sf.okapi.common.resource.DocumentPart;
import net.sf.okapi.common.resource.Ending;
import net.sf.okapi.common.resource.TextFragment;
import net.sf.okapi.common.resource.TextUnit;
import net.sf.okapi.common.skeleton.GenericSkeleton;

import javax.xml.stream.XMLStreamException;
import java.io.IOException;
import java.util.Iterator;
import java.util.zip.ZipEntry;

class RelationshipsPart extends TranslatablePart {
    static final String SUB_PATH = "_rels/";
    static final String EXTENSION = ".rels";
    static final String ROOT_RELS = SUB_PATH.concat(EXTENSION);
    private static final String HYPERLINK = "/hyperlink";

    private final Relationships relationships;
    private final IdGenerator textUnitIdGeneration;
    private final String hyperlinkType;
    private String subDocumentId;
    private Iterator<Event> filterEventIterator;

    RelationshipsPart(Document.General generalDocument, ZipEntry entry, final Relationships relationships) {
        super(generalDocument, entry);
        this.relationships = relationships;
        this.textUnitIdGeneration = new IdGenerator(entry.getName(), IdGenerator.TEXT_UNIT);
        this.hyperlinkType = generalDocument.mainPartRelationshipsNamespace().uri().concat(HYPERLINK);
    }

    @Override
    public Event open() throws IOException, XMLStreamException {
        this.subDocumentId = this.generalDocument.nextSubDocumentId();
        formEvents();
        return createStartSubDocumentEvent(
            this.generalDocument.documentId(),
            this.subDocumentId
        );
    }

    private void formEvents() {
        addEventToDocumentPart(this.relationships.startDocument());
        addEventToDocumentPart(this.relationships.startElement());
        flushDocumentPart();
        final Iterator<Relationship> iterator = this.relationships.iterator();
        while (iterator.hasNext()) {
            final Relationship r = iterator.next();
            final String type = r.type();
            if (this.hyperlinkType.equals(r.type()) && Relationship.EXTERNAL_TARGET_MODE.equals(r.targetMode()) && !r.target().isEmpty()) {
                filterEvents.add(new Event(EventType.TEXT_UNIT, textUnitFor(r)));
            } else {
                filterEvents.add(new Event(EventType.DOCUMENT_PART, documentPartFor(r)));
            }
        }
        addEventToDocumentPart(this.relationships.endElement());
        addEventToDocumentPart(this.relationships.endDocument());
        flushDocumentPart();
        this.filterEvents.add(new Event(EventType.END_SUBDOCUMENT, new Ending(this.subDocumentId)));
        this.filterEventIterator = this.filterEvents.iterator();
    }

    private DocumentPart documentPartFor(final Relationship relationship) {
        return new DocumentPart(
            this.documentPartIds.createId(),
            false,
            new GenericSkeleton(XMLEventSerializer.serialize(relationship.asMarkup()))
        );
    }

    private TextUnit textUnitFor(final Relationship relationship) {
        final String[] parts = skeletonParts(XMLEventSerializer.serialize(relationship.asMarkup()));
        final GenericSkeleton skel = new GenericSkeleton();
        skel.append(parts[0]);
        final TextUnit textUnit = new TextUnit(this.textUnitIdGeneration.createId());
        textUnit.setSourceContent(new TextFragment(parts[1]));
        skel.addContentPlaceholder(textUnit);
        skel.append(parts[2]);
        textUnit.setSkeleton(skel);
        return textUnit;
    }

    /**
     * Obtains skeleton parts from a relationship string.
     *
     * The order of the parts is the following:
     *   the first skeleton part,
     *   a value for a text unit,
     *   the last skeleton part.
     * @param string A string to process
     * @return An array of parts
     */
    private static String[] skeletonParts(final String string) {
        final String startValueMarker = Relationship.TARGET.getPrefix().isEmpty()
            ? Relationship.TARGET.getLocalPart() + "=\""
            : Relationship.TARGET.getPrefix() + ":" + Relationship.TARGET.getLocalPart() + "=\"";

        final int valueIndex = string.indexOf(startValueMarker) + startValueMarker.length();
        final int lastPartIndex = string.indexOf("\"", valueIndex);

        return new String[] {
            string.substring(0, valueIndex),
            string.substring(valueIndex, lastPartIndex),
            string.substring(lastPartIndex)
        };
    }

    @Override
    public boolean hasNextEvent() {
        return filterEventIterator.hasNext();
    }

    @Override
    public Event nextEvent() {
        return filterEventIterator.next();
    }

    @Override
    public void close() {

    }

    @Override
    public void logEvent(Event e) {

    }
}
