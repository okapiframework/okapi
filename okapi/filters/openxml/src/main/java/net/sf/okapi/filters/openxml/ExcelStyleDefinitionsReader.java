/*
 * =============================================================================
 * Copyright (C) 2010-2021 by the Okapi Framework contributors
 * -----------------------------------------------------------------------------
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =============================================================================
 */
package net.sf.okapi.filters.openxml;

import javax.xml.stream.XMLEventFactory;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.EndDocument;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartDocument;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

final class ExcelStyleDefinitionsReader implements StyleDefinitionsReader {
    private static final String UNEXPECTED_STRUCTURE = "Unexpected styles structure: ";
    private final ConditionalParameters conditionalParameters;
    private final XMLEventFactory eventFactory;
    private final PresetColorValues presetColorValues;
    private final PresetColorValues highlightColorValues;
    private final SystemColorValues systemColorValues;
    private final IndexedColors.Default defaultIndexedColors;
    private final Theme theme;
    private final XMLEventReader eventReader;
    private final ExcelStyleDefinitionsReader.Cache cache;

    ExcelStyleDefinitionsReader(
        final ConditionalParameters conditionalParameters,
        final XMLEventFactory eventFactory,
        final PresetColorValues presetColorValues,
        final PresetColorValues highlightColorValues,
        final SystemColorValues systemColorValues,
        final IndexedColors.Default defaultIndexedColors,
        final Theme theme,
        final XMLEventReader eventReader
    ) {
        this.conditionalParameters = conditionalParameters;
        this.eventFactory = eventFactory;
        this.presetColorValues = presetColorValues;
        this.highlightColorValues = highlightColorValues;
        this.systemColorValues = systemColorValues;
        this.defaultIndexedColors = defaultIndexedColors;
        this.theme = theme;
        this.eventReader = eventReader;
        this.cache = new ExcelStyleDefinitionsReader.Cache();
    }

    StartDocument startDocument() throws XMLStreamException {
        if (this.cache.startDocumentAvailable()) {
            return this.cache.startDocument;
        }
        while (this.eventReader.hasNext()) {
            final XMLEvent event = this.eventReader.nextEvent();
            if (event.isStartDocument()) {
                this.cache.startDocument = (StartDocument) event;
                return this.cache.startDocument;
            }
        }
        throw new IllegalStateException(UNEXPECTED_STRUCTURE.concat("the start document event is absent"));
    }

    @Override
    public StartElement startElement() throws XMLStreamException {
        if (!this.cache.startDocumentAvailable()) {
            startDocument();
        }
        if (this.cache.startElementAvailable()) {
            return this.cache.startElement;
        }
        while (this.eventReader.hasNext()) {
            final XMLEvent event = this.eventReader.nextEvent();
            if (event.isStartElement()) {
                final StartElement se = event.asStartElement();
                if (ExcelStyleDefinitions.STYLESHEET.equals(event.asStartElement().getName().getLocalPart())) {
                    this.cache.startElement = se;
                    return this.cache.startElement;
                }
            }
        }
        throw new IllegalStateException(UNEXPECTED_STRUCTURE.concat("the start element is absent"));
    }

    ExcelStyleDefinition.NumberFormats numberFormats() throws XMLStreamException {
        if (!this.cache.startElementAvailable()) {
            startElement();
        }
        if (this.cache.numberFormatsAvailable()) {
            return this.cache.numberFormats;
        }
        cacheStyleDefinitionOrEndElement();
        if (!this.cache.numberFormatsAvailable()) {
            this.cache.numberFormats = new ExcelStyleDefinition.NumberFormats(
                this.eventFactory,
                this.eventFactory.createStartElement(
                    startElement().getName().getPrefix(),
                    startElement().getName().getNamespaceURI(),
                    ExcelStyleDefinition.Fonts.NAME
                )
            );
        }
        return this.cache.numberFormats;
    }

    ExcelStyleDefinition.Fonts fonts() throws XMLStreamException {
        if (!this.cache.startDocumentAvailable()) {
            startDocument();
        }
        if (this.cache.fontsAvailable()) {
            return this.cache.fonts;
        }
        cacheStyleDefinitionOrEndElement();
        if (!this.cache.fontsAvailable()) {
            this.cache.fonts = new ExcelStyleDefinition.Fonts(
                this.conditionalParameters,
                this.eventFactory,
                this.presetColorValues,
                this.highlightColorValues,
                this.systemColorValues,
                this.defaultIndexedColors,
                this.theme,
                this.eventFactory.createStartElement(
                    startElement().getName().getPrefix(),
                    startElement().getName().getNamespaceURI(),
                    ExcelStyleDefinition.Fonts.NAME
                )
            );
        }
        return this.cache.fonts;
    }

    ExcelStyleDefinition.Fills fills() throws XMLStreamException {
        if (!this.cache.fillsAvailable()) {
            fonts();
        }
        if (this.cache.fillsAvailable()) {
            return this.cache.fills;
        }
        cacheStyleDefinitionOrEndElement();
        if (!this.cache.fillsAvailable()) {
            this.cache.fills = new ExcelStyleDefinition.Fills(
                this.eventFactory,
                this.presetColorValues,
                this.systemColorValues,
                this.defaultIndexedColors,
                this.theme,
                this.eventFactory.createStartElement(
                    startElement().getName().getPrefix(),
                    startElement().getName().getNamespaceURI(),
                    ExcelStyleDefinition.Fills.NAME
                )
            );
        }
        return this.cache.fills;
    }

    ExcelStyleDefinition.Borders borders() throws XMLStreamException {
        if (!this.cache.fontsAvailable()) {
            fonts();
        }
        if (this.cache.bordersAvailable()) {
            return this.cache.borders;
        }
        cacheStyleDefinitionOrEndElement();
        if (!this.cache.bordersAvailable()) {
            this.cache.borders = new ExcelStyleDefinition.Borders(
                new ExcelStyleDefinition.Default(
                    this.eventFactory,
                    this.eventFactory.createStartElement(
                        startElement().getName().getPrefix(),
                        startElement().getName().getNamespaceURI(),
                        ExcelStyleDefinition.Borders.NAME
                    )
                )
            );
        }
        return this.cache.borders;
    }

    ExcelStyleDefinition.CellStyleFormats cellStyleFormats() throws XMLStreamException {
        if (!this.cache.bordersAvailable()) {
            borders();
        }
        if (this.cache.cellStyleFormatsAvailable()) {
            return this.cache.cellStyleFormats;
        }
        cacheStyleDefinitionOrEndElement();
        if (!this.cache.cellStyleFormatsAvailable()) {
            this.cache.cellStyleFormats = new ExcelStyleDefinition.CellStyleFormats(
                new ExcelStyleDefinition.CellFormats(
                    this.eventFactory,
                    this.eventFactory.createStartElement(
                        startElement().getName().getPrefix(),
                        startElement().getName().getNamespaceURI(),
                        ExcelStyleDefinition.CellStyleFormats.NAME
                    )
                )
            );
        }
        return this.cache.cellStyleFormats;
    }

    ExcelStyleDefinition.CellFormats cellFormats() throws XMLStreamException {
        if (!this.cache.cellStyleFormatsAvailable()) {
            cellStyleFormats();
        }
        if (this.cache.cellFormatsAvailable()) {
            return this.cache.cellFormats;
        }
        cacheStyleDefinitionOrEndElement();
        if (!this.cache.cellFormatsAvailable()) {
            this.cache.cellFormats = new ExcelStyleDefinition.CellFormats(
                this.eventFactory,
                this.eventFactory.createStartElement(
                    startElement().getName().getPrefix(),
                    startElement().getName().getNamespaceURI(),
                    ExcelStyleDefinition.CellFormats.NAME
                )
            );
        }
        return this.cache.cellFormats;
    }

    ExcelStyleDefinition.CellStyles cellStyles() throws XMLStreamException {
        if (!this.cache.cellFormatsAvailable()) {
            cellFormats();
        }
        if (this.cache.cellStylesAvailable()) {
            return this.cache.cellStyles;
        }
        cacheStyleDefinitionOrEndElement();
        if (!this.cache.cellStylesAvailable()) {
            this.cache.cellStyles = new ExcelStyleDefinition.CellStyles(
                this.eventFactory,
                this.eventFactory.createStartElement(
                    startElement().getName().getPrefix(),
                    startElement().getName().getNamespaceURI(),
                    ExcelStyleDefinition.CellStyles.NAME
                )
            );
        }
        return this.cache.cellStyles;
    }

    ExcelStyleDefinition.DifferentialFormats differentialFormats() throws XMLStreamException {
        if (!this.cache.cellStylesAvailable()) {
            cellStyles();
        }
        if (this.cache.differentialFormatsAvailable()) {
            return this.cache.differentialFormats;
        }
        cacheStyleDefinitionOrEndElement();
        if (!this.cache.differentialFormatsAvailable()) {
            this.cache.differentialFormats = new ExcelStyleDefinition.DifferentialFormats(
                this.conditionalParameters,
                this.eventFactory,
                this.presetColorValues,
                this.highlightColorValues,
                this.systemColorValues,
                this.defaultIndexedColors,
                this.theme,
                this.eventFactory.createStartElement(
                    startElement().getName().getPrefix(),
                    startElement().getName().getNamespaceURI(),
                    ExcelStyleDefinition.DifferentialFormats.NAME
                )
            );
        }
        return this.cache.differentialFormats;
    }

    ExcelStyleDefinition.TableStyles tableStyles() throws XMLStreamException {
        if (!this.cache.differentialFormatsAvailable()) {
            differentialFormats();
        }
        if (this.cache.tableStylesAvailable()) {
            return this.cache.tableStyles;
        }
        cacheStyleDefinitionOrEndElement();
        if (!this.cache.tableStylesAvailable()) {
            this.cache.tableStyles = new ExcelStyleDefinition.TableStyles(
                new ExcelStyleDefinition.Default(
                    this.eventFactory,
                    this.eventFactory.createStartElement(
                        startElement().getName().getPrefix(),
                        startElement().getName().getNamespaceURI(),
                        ExcelStyleDefinition.TableStyles.NAME
                    )
                )
            );
        }
        return this.cache.tableStyles;
    }

    ExcelStyleDefinition.Colors colors() throws XMLStreamException {
        if (!this.cache.tableStylesAvailable()) {
            tableStyles();
        }
        if (this.cache.colorsAvailable()) {
            return this.cache.colors;
        }
        cacheStyleDefinitionOrEndElement();
        if (!this.cache.colorsAvailable()) {
            this.cache.colors = new ExcelStyleDefinition.Colors(
                this.eventFactory,
                this.presetColorValues,
                this.systemColorValues,
                this.defaultIndexedColors,
                this.theme,
                this.eventFactory.createStartElement(
                    startElement().getName().getPrefix(),
                    startElement().getName().getNamespaceURI(),
                    ExcelStyleDefinition.Colors.NAME
                )
            );
        }
        return this.cache.colors;
    }

    ExcelStyleDefinition.Extensions extensions() throws XMLStreamException {
        if (!this.cache.colorsAvailable()) {
            colors();
        }
        if (this.cache.extensionsAvailable()) {
            return this.cache.extensions;
        }
        cacheStyleDefinitionOrEndElement();
        if (!this.cache.extensionsAvailable()) {
            this.cache.extensions = new ExcelStyleDefinition.Extensions(
                new ExcelStyleDefinition.Default(
                    this.eventFactory,
                    this.eventFactory.createStartElement(
                        startElement().getName().getPrefix(),
                        startElement().getName().getNamespaceURI(),
                        ExcelStyleDefinition.Extensions.NAME
                    )
                )
            );
        }
        return this.cache.extensions;
    }

    private void cacheStyleDefinitionOrEndElement() throws XMLStreamException {
        while (this.eventReader.hasNext()) {
            final XMLEvent event = this.eventReader.nextEvent();
            if (event.isStartElement()) {
                cacheStyleDefinitionFrom(event.asStartElement());
                break;
            }
            if (event.isEndElement() && ExcelStyleDefinitions.STYLESHEET.equals(event.asEndElement().getName().getLocalPart())) {
                this.cache.endElement = event.asEndElement();
                break;
            }
        }
    }

    void cacheStyleDefinitionFrom(final StartElement startElement) throws XMLStreamException {
        switch (startElement.getName().getLocalPart()) {
            case ExcelStyleDefinition.NumberFormats.NAME:
                cacheNumberFormatsFrom(startElement);
                break;
            case ExcelStyleDefinition.Fonts.NAME:
                cacheFontsFrom(startElement);
                break;
            case ExcelStyleDefinition.Fills.NAME:
                cacheFillsFrom(startElement);
                break;
            case ExcelStyleDefinition.Borders.NAME:
                cacheBordersFrom(startElement);
                break;
            case ExcelStyleDefinition.CellStyleFormats.NAME:
                cacheCellStyleFormatsFrom(startElement);
                break;
            case ExcelStyleDefinition.CellFormats.NAME:
                cacheCellFormatsFrom(startElement);
                break;
            case ExcelStyleDefinition.CellStyles.NAME:
                cacheCellStylesFrom(startElement);
                break;
            case ExcelStyleDefinition.DifferentialFormats.NAME:
                cacheDifferentialFormatsFrom(startElement);
                break;
            case ExcelStyleDefinition.TableStyles.NAME:
                cacheTableStylesFrom(startElement);
                break;
            case ExcelStyleDefinition.Colors.NAME:
                cacheColorsFrom(startElement);
                break;
            case ExcelStyleDefinition.Extensions.NAME:
                cacheExtensionsFrom(startElement);
                break;
            default:
                throw new IllegalStateException(
                    UNEXPECTED_STRUCTURE
                        .concat("unsupported element: ")
                        .concat(startElement.getName().getLocalPart())
                );
        }
    }

    private void cacheNumberFormatsFrom(final StartElement startElement) throws XMLStreamException {
        this.cache.numberFormats = new ExcelStyleDefinition.NumberFormats(
            this.eventFactory,
            startElement
        );
        this.cache.numberFormats.readWith(this.eventReader);
    }

    private void cacheFontsFrom(final StartElement startElement) throws XMLStreamException {
        this.cache.fonts = new ExcelStyleDefinition.Fonts(
            this.conditionalParameters,
            this.eventFactory,
            this.presetColorValues,
            this.highlightColorValues,
            this.systemColorValues,
            this.defaultIndexedColors,
            this.theme,
            startElement
        );
        this.cache.fonts.readWith(this.eventReader);
    }

    private void cacheFillsFrom(final StartElement startElement) throws XMLStreamException {
        this.cache.fills = new ExcelStyleDefinition.Fills(
            this.eventFactory,
            this.presetColorValues,
            this.systemColorValues,
            this.defaultIndexedColors,
            this.theme,
            startElement
        );
        this.cache.fills.readWith(this.eventReader);
    }

    private void cacheBordersFrom(final StartElement startElement) throws XMLStreamException {
        this.cache.borders = new ExcelStyleDefinition.Borders(
            new ExcelStyleDefinition.Default(
                this.eventFactory,
                startElement
            )
        );
        this.cache.borders.readWith(this.eventReader);
    }

    private void cacheCellStyleFormatsFrom(final StartElement startElement) throws XMLStreamException {
        this.cache.cellStyleFormats = new ExcelStyleDefinition.CellStyleFormats(
            new ExcelStyleDefinition.CellFormats(
                this.eventFactory,
                startElement
            )
        );
        this.cache.cellStyleFormats.readWith(this.eventReader);
    }

    private void cacheCellFormatsFrom(final StartElement startElement) throws XMLStreamException {
        this.cache.cellFormats = new ExcelStyleDefinition.CellFormats(
            this.eventFactory,
            startElement
        );
        this.cache.cellFormats.readWith(this.eventReader);
    }

    private void cacheCellStylesFrom(final StartElement startElement) throws XMLStreamException {
        this.cache.cellStyles = new ExcelStyleDefinition.CellStyles(
            this.eventFactory,
            startElement
        );
        this.cache.cellStyles.readWith(this.eventReader);
    }

    private void cacheDifferentialFormatsFrom(final StartElement startElement) throws XMLStreamException {
        this.cache.differentialFormats = new ExcelStyleDefinition.DifferentialFormats(
            this.conditionalParameters,
            this.eventFactory,
            this.presetColorValues,
            this.highlightColorValues,
            this.systemColorValues,
            this.defaultIndexedColors,
            this.theme,
            startElement
        );
        this.cache.differentialFormats.readWith(this.eventReader);
    }

    private void cacheTableStylesFrom(final StartElement startElement) throws XMLStreamException {
        this.cache.tableStyles = new ExcelStyleDefinition.TableStyles(
            new ExcelStyleDefinition.Default(
                this.eventFactory,
                startElement
            )
        );
        this.cache.tableStyles.readWith(this.eventReader);
    }

    private void cacheColorsFrom(final StartElement startElement) throws XMLStreamException {
        this.cache.colors = new ExcelStyleDefinition.Colors(
            this.eventFactory,
            this.presetColorValues,
            this.systemColorValues,
            this.defaultIndexedColors,
            this.theme,
            startElement
        );
        this.cache.colors.readWith(this.eventReader);
    }

    private void cacheExtensionsFrom(final StartElement startElement) throws XMLStreamException {
        this.cache.extensions = new ExcelStyleDefinition.Extensions(
            new ExcelStyleDefinition.Default(
                this.eventFactory,
                startElement
            )
        );
        this.cache.extensions.readWith(this.eventReader);
    }

    @Override
    public EndElement endElement() throws XMLStreamException {
        if (!this.cache.extensionsAvailable()) {
            extensions();
        }
        if (this.cache.endElementAvailable()) {
            return this.cache.endElement;
        }
        while (this.eventReader.hasNext()) {
            final XMLEvent event = this.eventReader.nextEvent();
            if (event.isEndElement() && ExcelStyleDefinitions.STYLESHEET.equals(event.asEndElement().getName().getLocalPart())) {
                this.cache.endElement = event.asEndElement();
                return this.cache.endElement;
            }
        }
        throw new IllegalStateException(UNEXPECTED_STRUCTURE.concat("the end element is absent"));
    }

    EndDocument endDocument() throws XMLStreamException {
        if (!this.cache.endElementAvailable()) {
            endElement();
        }
        if (this.cache.endDocumentAvailable()) {
            return this.cache.endDocument;
        }
        while (this.eventReader.hasNext()) {
            final XMLEvent event = this.eventReader.nextEvent();
            if (event.isEndDocument()) {
                this.cache.endDocument = (EndDocument) event;
                return this.cache.endDocument;
            }
        }
        this.cache.endDocument = this.eventFactory.createEndDocument();
        return this.cache.endDocument;
    }

    private static final class Cache {
        private StartDocument startDocument;
        private StartElement startElement;
        private ExcelStyleDefinition.NumberFormats numberFormats;
        private ExcelStyleDefinition.Fonts fonts;
        private ExcelStyleDefinition.Fills fills;
        private ExcelStyleDefinition.Borders borders;
        private ExcelStyleDefinition.CellStyleFormats cellStyleFormats;
        private ExcelStyleDefinition.CellFormats cellFormats;
        private ExcelStyleDefinition.CellStyles cellStyles;
        private ExcelStyleDefinition.DifferentialFormats differentialFormats;
        private ExcelStyleDefinition.TableStyles tableStyles;
        private ExcelStyleDefinition.Colors colors;
        private ExcelStyleDefinition.Extensions extensions;
        private EndElement endElement;
        private EndDocument endDocument;

        Cache() {
        }

        boolean startDocumentAvailable() {
            return null != this.startDocument;
        }

        boolean startElementAvailable() {
            return null != this.startElement;
        }

        boolean numberFormatsAvailable() {
            return null != this.numberFormats;
        }

        boolean fontsAvailable() {
            return null != this.fonts;
        }

        boolean fillsAvailable() {
            return null != this.fills;
        }

        boolean bordersAvailable() {
            return null != this.borders;
        }

        boolean cellStyleFormatsAvailable() {
            return null != this.cellStyleFormats;
        }

        boolean cellFormatsAvailable() {
            return null != this.cellFormats;
        }

        boolean cellStylesAvailable() {
            return null != this.cellStyles;
        }

        boolean differentialFormatsAvailable() {
            return null != this.differentialFormats;
        }

        boolean tableStylesAvailable() {
            return null != this.tableStyles;
        }

        boolean colorsAvailable() {
            return null != this.colors;
        }

        boolean extensionsAvailable() {
            return null != this.extensions;
        }

        boolean endElementAvailable() {
            return null != this.endElement;
        }

        boolean endDocumentAvailable() {
            return null != this.endDocument;
        }
    }
}
