/*===========================================================================
  Copyright (C) 2016-2017 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
===========================================================================*/

package net.sf.okapi.filters.openxml;

import net.sf.okapi.common.exceptions.OkapiBadFilterInputException;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

interface WorkbookFragments {
	String SHEET = "sheet";
	QName SHEET_NAME = new QName("name");
	List<XMLEvent> events();
	boolean date1904();
	List<String> worksheetPartNames();
	String localisedWorksheetNameFor(final String worksheetPartName);
	String localisedWorksheetNameFor(final int worksheetNumber);
	boolean worksheetPartNameHiddenFor(final String worksheetPartName);
	boolean localisedWorksheetNameHiddenFor(final String localisedWorksheetName);
	void readWith(final XMLEventReader eventReader) throws XMLStreamException;

	final class Default implements WorkbookFragments {
		private static final String EMPTY = "";
		private static final String WORKSHEET_NAME_HAS_NOT_BEEN_FOUND = "The provided worksheet name has not been found: ";
		private static final String WORKBOOK = "workbook";
		private static final String WORKBOOK_PR = "workbookPr";
		private static final QName DATE_1904 = new QName("date1904");
		private static final QName SHEET_STATE = new QName("state");
		private static final String ID = "id";
		private static final String HIDDEN = "hidden";

		private final ConditionalParameters conditionalParameters;
		private final Relationships relationships;
		private final List<XMLEvent> events;
		private final List<String> worksheetNames;
		private final Map<String, String> localisedWorksheetNames;
		private final Set<String > hiddenWorksheets;

		private boolean date1904;
		private QName sheet;
		private QName id;

		Default(
			final ConditionalParameters conditionalParameters,
			final Relationships relationships
		) {
			this(
				conditionalParameters,
				relationships,
				new ArrayList<>(),
				new ArrayList<>(),
				new HashMap<>(),
				new HashSet<>()
			);
		}

		Default(
			final ConditionalParameters conditionalParameters,
			final Relationships relationships,
			final List<XMLEvent> events,
			final List<String> worksheetNames,
			final Map<String, String> localisedWorksheetNames,
			final Set<String> hiddenWorksheets
		) {
			this.conditionalParameters = conditionalParameters;
			this.relationships = relationships;
			this.events = events;
			this.worksheetNames = worksheetNames;
			this.localisedWorksheetNames = localisedWorksheetNames;
			this.hiddenWorksheets = hiddenWorksheets;
		}

		@Override
		public List<XMLEvent> events() {
			return this.events;
		}

		@Override
		public boolean date1904() {
			return this.date1904;
		}

		@Override
		public List<String> worksheetPartNames() {
			return this.worksheetNames;
		}

		@Override
		public String localisedWorksheetNameFor(final String worksheetPartName) {
			if (!this.localisedWorksheetNames.containsKey(worksheetPartName)) {
				throw new IllegalStateException(Default.WORKSHEET_NAME_HAS_NOT_BEEN_FOUND.concat(worksheetPartName));
			}
			return this.localisedWorksheetNames.get(worksheetPartName);
		}

		@Override
		public String localisedWorksheetNameFor(final int worksheetNumber) {
			if (this.worksheetNames.size() < worksheetNumber) {
				return EMPTY;
			}
			final String name = this.worksheetNames.get(worksheetNumber - 1);
			return null == name
				? EMPTY
				: this.localisedWorksheetNames.get(name);
		}

		@Override
		public boolean worksheetPartNameHiddenFor(final String worksheetPartName) {
			if (!this.worksheetNames.contains(worksheetPartName)) {
				return false;
			}
			return this.hiddenWorksheets.contains(worksheetPartName);
		}

		@Override
		public boolean localisedWorksheetNameHiddenFor(final String localisedWorksheetName) {
			if (this.localisedWorksheetNames.entrySet().stream().noneMatch(e -> e.getValue().equals(localisedWorksheetName))) {
				return false;
			}
			final List<String> worksheetNames = this.localisedWorksheetNames.entrySet().stream()
				.filter(e -> e.getValue().equals(localisedWorksheetName))
				.map(e -> e.getKey())
				.collect(Collectors.toList());
			return this.hiddenWorksheets.contains(worksheetNames.get(0));
		}

		@Override
		public void readWith(final XMLEventReader reader) throws XMLStreamException {
			while (reader.hasNext()) {
				final XMLEvent e = reader.nextEvent();
				this.events.add(e);
				if (!e.isStartElement()) {
					continue;
				}
				final StartElement el = e.asStartElement();
				if (Default.WORKBOOK.equals(el.getName().getLocalPart())) {
					qualifyNames(el);
				} else if (Default.WORKBOOK_PR.equals(el.getName().getLocalPart())) {
					this.date1904 = XMLEventHelpers.getBooleanAttributeValue(el, Default.DATE_1904, false);
				} else if (el.getName().equals(this.sheet)) {
					if (null == this.id) {
						qualifyIdName(el);
					}
					final String worksheetName = relationshipTargetFor(
						el.getAttributeByName(this.id).getValue()
					);
					this.worksheetNames.add(worksheetName);
					this.localisedWorksheetNames.put(
						worksheetName,
						el.getAttributeByName(Default.SHEET_NAME).getValue()
					);
					final String state = XMLEventHelpers.getAttributeValue(el, Default.SHEET_STATE);
					if (!this.conditionalParameters.getTranslateExcelHidden()
						&& Default.HIDDEN.equals(state)) {
						this.hiddenWorksheets.add(worksheetName);
					}
				}
			}
		}

		private void qualifyNames(final StartElement startElement) {
			this.sheet = new QName(
				startElement.getName().getNamespaceURI(),
				SHEET,
				startElement.getName().getPrefix()
			);
			qualifyIdName(startElement);
		}

		private void qualifyIdName(final StartElement startElement) {
			final String namespaceUri = startElement.getNamespaceURI(Namespace.PREFIX_R);
			if (null == namespaceUri) {
				return;
			}
			this.id = new QName(
				namespaceUri, ID, Namespace.PREFIX_R
			);
		}

		private String relationshipTargetFor(final String id) {
			final Iterator<Relationship> ri = this.relationships.with(id).iterator();
			if (!ri.hasNext()) {
				throw new OkapiBadFilterInputException(
					String.format("%s: %s", Relationships.UNEXPECTED_NUMBER_OF_RELATIONSHIPS, id)
				);
			}
			return ri.next().target();
		}
	}
}
