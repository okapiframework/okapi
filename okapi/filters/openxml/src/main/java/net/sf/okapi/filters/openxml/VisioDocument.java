/*===========================================================================
  Copyright (C) 2016-2017 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
===========================================================================*/

package net.sf.okapi.filters.openxml;

import net.sf.okapi.common.Event;
import net.sf.okapi.common.exceptions.OkapiBadFilterInputException;

import javax.xml.stream.XMLStreamException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.zip.ZipEntry;

import static java.util.Collections.enumeration;
import static java.util.Collections.list;

class VisioDocument implements Document {

    private static final String MASTERS = Namespaces.VisioDocumentRelationships.getDerivedURI("/masters");
    private static final String PAGES = Namespaces.VisioDocumentRelationships.getDerivedURI("/pages");

    private static final String MASTER = Namespaces.VisioDocumentRelationships.getDerivedURI("/master");
    private static final String PAGE = Namespaces.VisioDocumentRelationships.getDerivedURI("/page");

    private final Document.General generalDocument;
    private PresetColorValues highlightColorValues;
    private SystemColorValues systemColorValues;
    private Enumeration<? extends ZipEntry> entries;
    private List<String> mastersAndPages;

    VisioDocument(final Document.General generalDocument) {
        this.generalDocument = generalDocument;
    }

    @Override
    public Event open() throws IOException, XMLStreamException {
        this.highlightColorValues = new PresetColorValues.Default(Collections.emptyList());
        this.systemColorValues = new SystemColorValues.Default(Collections.emptyList());
        this.mastersAndPages = mastersAndPages();
        this.entries = entries();

        return this.generalDocument.startDocumentEvent();
    }

    private List<String> mastersAndPages() throws IOException, XMLStreamException {
        final Iterator<Relationship> mastersIterator =
            this.generalDocument.mainPartRelationships().of(MASTERS).iterator();
        if (!mastersIterator.hasNext()) {
            throw new OkapiBadFilterInputException(Relationships.UNEXPECTED_NUMBER_OF_RELATIONSHIPS);
        }
        final Iterator<Relationship> pagesIterator =
            this.generalDocument.mainPartRelationships().of(PAGES).iterator();
        if (!pagesIterator.hasNext()) {
            throw new OkapiBadFilterInputException(Relationships.UNEXPECTED_NUMBER_OF_RELATIONSHIPS);
        }
        final Relationships masterRelationships =
            this.generalDocument.relationshipsFor(mastersIterator.next().target());
        final Relationships pageRelationships =
            this.generalDocument.relationshipsFor(pagesIterator.next().target());
        final List<String> targets = new ArrayList<>(this.generalDocument.targetsOf(masterRelationships.of(MASTER)));
        targets.addAll(this.generalDocument.targetsOf(pageRelationships.of(PAGE)));
        return targets;
    }

    private Enumeration<? extends ZipEntry> entries() {
        final List<? extends ZipEntry> entries = list(this.generalDocument.entries());
        entries.sort(new ZipEntryComparator(mastersAndPages));

        return enumeration(entries);
    }

    @Override
    public PresetColorValues highlightColorValues() {
        return this.highlightColorValues;
    }

    @Override
    public SystemColorValues systemColorValues() {
        return this.systemColorValues;
    }

    @Override
    public IndexedColors indexedColors() {
        return StyleDefinitions.Empty.INDEXED_COLORS;
    }

    @Override
    public boolean isStyledTextPart(final ZipEntry entry) {
        return false;
    }

    @Override
    public boolean hasNextPart() {
        return this.entries.hasMoreElements();
    }

    @Override
    public Part nextPart() {
        final ZipEntry entry = this.entries.nextElement();
        if (!isTranslatablePart(entry)) {
            return new NonModifiablePart(this.generalDocument, entry);
        }

        return new MasterAndPagePart(this.generalDocument, entry);
    }

    @Override
    public StyleDefinitions styleDefinitionsFor(final ZipEntry entry) {
        return new StyleDefinitions.Empty();
    }

    private boolean isTranslatablePart(final ZipEntry entry) {
        final String contentType = this.generalDocument.contentTypeFor(entry);
        return entry.getName().endsWith(".xml") && (isMasterPart(contentType) || isPagePart(contentType));
    }

    private static boolean isMasterPart(String type) {
        return ContentTypes.Values.Visio.MASTER_TYPE.equals(type);
    }

    private static boolean isPagePart(String type) {
        return ContentTypes.Values.Visio.PAGE_TYPE.equals(type);
    }

    @Override
    public void close() throws IOException {
    }
}
