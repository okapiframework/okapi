/*
 * =============================================================================
 * Copyright (C) 2010-2023 by the Okapi Framework contributors
 * -----------------------------------------------------------------------------
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =============================================================================
 */
package net.sf.okapi.filters.openxml;

import javax.xml.namespace.QName;
import javax.xml.stream.events.XMLEvent;
import java.util.List;

interface NumberingProperties {
    String NAME = "numPr";

    String numberingId();
    String numberingLevelId();

    final class Empty implements NumberingProperties {
        private final static String EMPTY = "";
        private final static String ZERO = "0";
        @Override
        public String numberingId() {
            return EMPTY;
        }

        @Override
        public String numberingLevelId() {
            return ZERO;
        }
    }

    final class Default implements  NumberingProperties {
        private final static String NUM_ID = "numId";
        private final static String ILVL = "ilvl";
        private final static String VAL = "val";
        private final List<XMLEvent> events;
        private String numberingId;
        private String numberingLevelId;

        Default(final List<XMLEvent> events) {
            this.events = events;
        }

        @Override
        public String numberingId() {
            if (null == numberingId) {
                if (2 < this.events.size()) {
                    this.numberingId = valueFrom(NUM_ID, Empty.EMPTY);
                } else {
                    this.numberingId = Empty.EMPTY;
                }
            }
            return this.numberingId;
        }

        @Override
        public String numberingLevelId() {
            if (null == numberingLevelId) {
                if (2 < this.events.size()) {
                    this.numberingLevelId = valueFrom(ILVL, Empty.ZERO);
                } else {
                    this.numberingLevelId = Empty.ZERO;
                }
            }
            return this.numberingLevelId;
        }

        private String valueFrom(final String name, final String defaultValue) {
            return this.events.subList(1, this.events.size() - 1).stream()
                .filter(e -> e.isStartElement() && name.equals(e.asStartElement().getName().getLocalPart()))
                .map(e -> e.asStartElement())
                .findFirst()
                .map(se -> se.getAttributeByName(
                        new QName(
                            se.getName().getNamespaceURI(),
                            VAL,
                            se.getName().getPrefix()
                        )
                    ).getValue()
                )
                .orElse(defaultValue);
        }
    }
}
