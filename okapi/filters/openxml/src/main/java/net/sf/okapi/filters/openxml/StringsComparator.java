/*
 * =============================================================================
 * Copyright (C) 2010-2023 by the Okapi Framework contributors
 * -----------------------------------------------------------------------------
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =============================================================================
 */
package net.sf.okapi.filters.openxml;

import java.util.Comparator;

public class StringsComparator implements Comparator<String> {
    @Override
    public int compare(final String s1, final String s2) {
        final int result;
        if (s1.length() > s2.length()) {
            result = 1;
        } else if (s1.length() < s2.length()) {
            result = -1;
        } else {
            result = s1.compareTo(s2);
        }
        return result;
    }
}
