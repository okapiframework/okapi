/*
 * =============================================================================
 * Copyright (C) 2010-2023 by the Okapi Framework contributors
 * -----------------------------------------------------------------------------
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =============================================================================
 */
package net.sf.okapi.filters.openxml;

final class ConsequentialDetectedRunFonts {
    private final RunFonts first;
    private final RunFonts last;
    private Boolean mergeable;

    ConsequentialDetectedRunFonts(final RunFonts first, final RunFonts last) {
        this.first = first;
        this.last = last;
    }

    boolean mergeable() {
        if (null == this.mergeable) {
            this.mergeable = this.first.canBeMerged(this.last)
                && (
                    contentCategoriesComplemented()
                    || firstContentCategoriesIntersectedAtNonComplexScriptAndComplementedAtComplexScript()
                    || firstContentCategoriesIntersectedAtComplexScriptAndComplementedAtNonComplexScript()
                    || lastContentCategoriesIntersectedAtNonComplexScriptAndComplementedAtComplexScript()
                    || lastContentCategoriesIntersectedAtComplexScriptAndComplementedAtNonComplexScript()
                );
        }
        return this.mergeable;
    }

    boolean contentCategoriesComplemented() {
        return !this.first.containsDetectedNonComplexScriptContentCategories()
            && this.first.containsDetectedComplexScriptContentCategories()
            && this.last.containsDetectedNonComplexScriptContentCategories()
            && !this.last.containsDetectedComplexScriptContentCategories()

            || this.first.containsDetectedNonComplexScriptContentCategories()
            && !this.first.containsDetectedComplexScriptContentCategories()
            && !this.last.containsDetectedNonComplexScriptContentCategories()
            && this.last.containsDetectedComplexScriptContentCategories();
    }

    boolean firstContentCategoriesIntersectedAtNonComplexScriptAndComplementedAtComplexScript() {
        return this.first.containsDetectedNonComplexScriptContentCategories()
            && !this.first.containsDetectedComplexScriptContentCategories()
            && this.last.containsDetectedNonComplexScriptContentCategories()
            && this.last.containsDetectedComplexScriptContentCategories();
    }

    boolean lastContentCategoriesIntersectedAtNonComplexScriptAndComplementedAtComplexScript() {
        return this.last.containsDetectedNonComplexScriptContentCategories()
            && !this.last.containsDetectedComplexScriptContentCategories()
            && this.first.containsDetectedNonComplexScriptContentCategories()
            && this.first.containsDetectedComplexScriptContentCategories();
    }

    boolean firstContentCategoriesIntersectedAtComplexScriptAndComplementedAtNonComplexScript() {
        return !this.first.containsDetectedNonComplexScriptContentCategories()
            && this.first.containsDetectedComplexScriptContentCategories()
            && this.last.containsDetectedNonComplexScriptContentCategories()
            && this.last.containsDetectedComplexScriptContentCategories();
    }

    boolean lastContentCategoriesIntersectedAtComplexScriptAndComplementedAtNonComplexScript() {
        return !this.last.containsDetectedNonComplexScriptContentCategories()
            && this.last.containsDetectedComplexScriptContentCategories()
            && this.first.containsDetectedNonComplexScriptContentCategories()
            && this.first.containsDetectedComplexScriptContentCategories();
    }
}
