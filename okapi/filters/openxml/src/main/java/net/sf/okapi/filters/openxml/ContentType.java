/*
 * =============================================================================
 * Copyright (C) 2010-2022 by the Okapi Framework contributors
 * -----------------------------------------------------------------------------
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =============================================================================
 */
package net.sf.okapi.filters.openxml;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventFactory;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.util.ArrayList;

interface ContentType {
    String id();
    String value();
    void readWith(final XMLEventReader eventReader) throws XMLStreamException;
    Markup asMarkup();

    class Default implements ContentType {
        static final String NAME = "Default";
        static final QName EXTENSION = new QName("Extension");
        static final QName CONTENT_TYPE = new QName("ContentType");

        private final XMLEventFactory eventFactory;
        private final StartElement startElement;
        private EndElement endElement;

        Default(final XMLEventFactory eventFactory, final StartElement startElement) {
            this.eventFactory = eventFactory;
            this.startElement = startElement;
        }

        @java.lang.Override
        public String id() {
            return this.startElement.getAttributeByName(EXTENSION).getValue();
        }

        @java.lang.Override
        public String value() {
            return this.startElement.getAttributeByName(CONTENT_TYPE).getValue();
        }

        @java.lang.Override
        public void readWith(final XMLEventReader eventReader) throws XMLStreamException {
            while (eventReader.hasNext()) {
                final XMLEvent e = eventReader.nextEvent();
                if (e.isEndElement() && e.asEndElement().getName().equals(this.startElement.getName())) {
                    this.endElement = e.asEndElement();
                    break;
                }
            }
        }

        @java.lang.Override
        public Markup asMarkup() {
            final MarkupBuilder mb = new MarkupBuilder(
                new Markup.General(
                    new ArrayList<>(1)
                ),
                new ArrayList<>(2)
            );
            mb.add(this.startElement);
            mb.add(endElement());
            return mb.build();
        }

        private EndElement endElement() {
            if (null == this.endElement) {
                this.endElement = this.eventFactory.createEndElement(
                    this.startElement.getName(),
                    this.startElement.getNamespaces()
                );
            }
            return this.endElement;
        }
    }

    class Override implements ContentType {
        static String NAME = "Override";
        static final QName PART_NAME = new QName("PartName");

        private final ContentType.Default defaultType;

        Override(final ContentType.Default defaultType) {
            this.defaultType = defaultType;
        }

        @java.lang.Override
        public String id() {
            return this.defaultType.startElement.getAttributeByName(PART_NAME).getValue();
        }

        @java.lang.Override
        public String value() {
            return this.defaultType.value();
        }

        @java.lang.Override
        public void readWith(final XMLEventReader eventReader) throws XMLStreamException {
            this.defaultType.readWith(eventReader);
        }

        @java.lang.Override
        public Markup asMarkup() {
            return this.defaultType.asMarkup();
        }
    }
}
