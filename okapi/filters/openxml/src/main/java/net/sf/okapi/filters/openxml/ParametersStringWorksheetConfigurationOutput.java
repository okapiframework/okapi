/*
 * =============================================================================
 * Copyright (C) 2010-2021 by the Okapi Framework contributors
 * -----------------------------------------------------------------------------
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =============================================================================
 */
package net.sf.okapi.filters.openxml;

import net.sf.okapi.common.ParametersString;

import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

final class ParametersStringWorksheetConfigurationOutput implements WorksheetConfiguration.Output<ParametersString> {
    @Override
    public ParametersString writtenWith(
        final Pattern namePattern,
        final Set<String> sourceColumns,
        final Set<String> targetColumns,
        final List<String> targetColumnsMaxCharacters,
        final Set<Integer> excludedRows,
        final Set<String> excludedColumns,
        final Set<Integer> metadataRows,
        final Set<String> metadataColumns
    ) {
        final ParametersString ps = new ParametersString();
        ps.setString(ParametersStringWorksheetConfiguration.NAME_PATTERN, namePattern.toString());
        if (!sourceColumns.isEmpty()) {
            ps.setString(
                ParametersStringWorksheetConfiguration.SOURCE_COLUMNS,
                stringsToString(sourceColumns)
            );
        }
        if (!targetColumns.isEmpty()) {
            ps.setString(
                ParametersStringWorksheetConfiguration.TARGET_COLUMNS,
                stringsToString(targetColumns)
            );
        }
        if (!targetColumnsMaxCharacters.isEmpty()) {
            ps.setString(
                ParametersStringWorksheetConfiguration.TARGET_COLUMNS_MAX_CHARACTERS,
                stringsToString(targetColumnsMaxCharacters)
            );
        }
        if (!excludedRows.isEmpty()) {
            ps.setString(
                ParametersStringWorksheetConfiguration.EXCLUDED_ROWS,
                integersToString(excludedRows)
            );
        }
        if (!excludedColumns.isEmpty()) {
            ps.setString(
                ParametersStringWorksheetConfiguration.EXCLUDED_COLUMNS,
                stringsToString(excludedColumns)
            );
        }
        if (!metadataRows.isEmpty()) {
            ps.setString(
                ParametersStringWorksheetConfiguration.METADATA_ROWS,
                integersToString(metadataRows)
            );
        }
        if (!metadataColumns.isEmpty()) {
            ps.setString(
                ParametersStringWorksheetConfiguration.METADATA_COLUMNS,
                stringsToString(metadataColumns)
            );
        }
        return ps;
    }

    private static String integersToString(final Set<Integer> rows) {
        return rows.stream()
                .map(Integer::toUnsignedString)
                .collect(Collectors.joining(ParametersStringWorksheetConfiguration.DELIMITER));
    }

    private static String stringsToString(final Collection<String> columns) {
        return String.join(ParametersStringWorksheetConfiguration.DELIMITER, columns);
    }
}