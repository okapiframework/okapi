/*===========================================================================
  Copyright (C) 2016-2017 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
===========================================================================*/

package net.sf.okapi.filters.openxml;

import net.sf.okapi.common.Event;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLStreamException;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.zip.ZipEntry;

import static net.sf.okapi.filters.openxml.ContentTypes.Values.Common.CORE_PROPERTIES_TYPE;
import static net.sf.okapi.filters.openxml.ContentTypes.Values.Drawing;
import static net.sf.okapi.filters.openxml.ContentTypes.Values.Word;
import static net.sf.okapi.filters.openxml.ParseType.MSWORDDOCPROPERTIES;

class WordDocument implements Document {
	private static final String GLOSSARY_DOCUMENT = "/glossaryDocument";
	private static final String STYLES = "/styles";
	private static final String NUMBERING = "/" + NumberingDefinitions.NAME;
	private static final String EMPTY = "";

	private final Document.General generalDocument;
	private Enumeration<? extends ZipEntry> entries;
	private PresetColorValues highlightColorValues;
	private SystemColorValues systemColorValues;
	private String glossaryPartPath;
	private String glossaryStyleDefinitionsPartPath;
	private StyleDefinitions mainStyleDefinitions;
	private StyleDefinitions glossaryStyleDefinitions;
	private NumberingDefinitions numberingDefinitions;
	private LinkedHashMap<ZipEntry, Markup> postponedParts;

	WordDocument(final Document.General generalDocument) {
		this.generalDocument = generalDocument;
	}

	@Override
	public Event open() throws IOException, XMLStreamException {
		this.postponedParts = new LinkedHashMap<>();
		this.entries = entries();
		this.highlightColorValues = new HighlightColorValues();
		this.systemColorValues = new SystemColorValues.Default(Collections.emptyList());
		initialiseGlossaryPartName();
		initialiseStyleDefinitions();
		initialiseNumberingDefinitions();
		return this.generalDocument.startDocumentEvent();
	}

	private Enumeration<? extends ZipEntry> entries() {
		final List<ZipEntry> entryList = new ArrayList<>();
		final String numberingPartPath = this.generalDocument.targetFor(NUMBERING);
		entryList.addAll(Collections.list(this.generalDocument.entries()));
		final ListIterator<? extends  ZipEntry> iterator = entryList.listIterator();
		while (iterator.hasNext()) {
			final String name = iterator.next().getName();
			if (name.equals(numberingPartPath)) {
				iterator.remove();
			}
		}
		if (!numberingPartPath.isEmpty()) {
			entryList.add(new ZipEntry(numberingPartPath));
		}
		entryList.sort(new ZipEntryComparator(reorderedPartPaths()));
		return Collections.enumeration(entryList);
	}

	private List<String> reorderedPartPaths() {
		final List<String> names = new LinkedList<>();
		names.add(this.generalDocument.relationshipsPartPathFor(this.generalDocument.mainPartPath()).asString());
		names.add(this.generalDocument.mainPartPath());
		return names;
	}

	private void initialiseGlossaryPartName() {
		final String glossaryDocumentSourceType =  this.generalDocument.mainPartRelationshipsNamespace()
				.uri().concat(GLOSSARY_DOCUMENT);
		final Iterator<Relationship> ri =
			this.generalDocument.mainPartRelationships().of(glossaryDocumentSourceType).iterator();
		if (ri.hasNext()) {
			this.glossaryPartPath = ri.next().target();
		} else {
			this.glossaryPartPath = EMPTY;
		}
	}

	private void initialiseStyleDefinitions() throws IOException, XMLStreamException {
		final String stylesType = this.generalDocument.mainPartRelationshipsNamespace().uri().concat(STYLES);
		final Iterator<Relationship> ri =
			this.generalDocument.mainPartRelationships().of(stylesType).iterator();
		if (ri.hasNext()) {
			this.mainStyleDefinitions = styleDefinitions(ri.next().target());
		} else {
			this.mainStyleDefinitions = styleDefinitions(EMPTY);
		}
		if (this.glossaryPartPath.isEmpty()) {
			this.glossaryStyleDefinitions = styleDefinitions(EMPTY);
		} else {
			final Iterator<Relationship> gri =
				this.generalDocument.relationshipsFor(this.glossaryPartPath).of(stylesType).iterator();
			if (gri.hasNext()) {
				this.glossaryStyleDefinitionsPartPath = gri.next().target();
				this.glossaryStyleDefinitions = styleDefinitions(this.glossaryStyleDefinitionsPartPath);
			} else {
				this.glossaryStyleDefinitionsPartPath = EMPTY;
				this.glossaryStyleDefinitions = styleDefinitions(EMPTY);
			}
		}
	}

	private StyleDefinitions styleDefinitions(final String partPath) throws IOException, XMLStreamException {
		if (partPath.isEmpty()) {
			return new StyleDefinitions.Empty();
		}
		try (final Reader reader = this.generalDocument.getPartReader(partPath)) {
			final StyleDefinitions styleDefinitions = new WordStyleDefinitions(
				this.generalDocument.conditionalParameters(),
				this.generalDocument.eventFactory(),
				this.generalDocument.presetColorValues(),
				this.highlightColorValues,
				this.systemColorValues,
				this.generalDocument.mainPartTheme()
			);
			styleDefinitions.readWith(
				new WordStyleDefinitionsReader(
					this.generalDocument.conditionalParameters(),
					this.generalDocument.eventFactory(),
                    this.generalDocument.presetColorValues(),
					this.highlightColorValues,
					this.systemColorValues,
					StyleDefinitions.Empty.INDEXED_COLORS,
					this.generalDocument.mainPartTheme(),
					this.generalDocument.inputFactory().createXMLEventReader(reader)
				)
			);
			return styleDefinitions;
		}
	}

	private void initialiseNumberingDefinitions() throws IOException, XMLStreamException {
		final String partPath = this.generalDocument.targetFor(NUMBERING);
		if (partPath.isEmpty()) {
			this.numberingDefinitions = new NumberingDefinitions.Empty();
		} else {
			try (final Reader reader = this.generalDocument.getPartReader(partPath)) {
				this.numberingDefinitions = new NumberingDefinitions.Default();
				this.numberingDefinitions.readWith(this.generalDocument.inputFactory().createXMLEventReader(reader));
			}
		}
	}

	@Override
	public PresetColorValues highlightColorValues() {
		return this.highlightColorValues;
	}

	@Override
	public SystemColorValues systemColorValues() {
		return this.systemColorValues;
	}

	@Override
	public IndexedColors indexedColors() {
		return this.mainStyleDefinitions.indexedColors();
	}

	@Override
	public boolean isStyledTextPart(final ZipEntry entry) {
		final String type = this.generalDocument.contentTypeFor(entry);
		return (
			type.equals(Word.MAIN_DOCUMENT_TYPE) ||
			type.equals(Word.MACRO_ENABLED_MAIN_DOCUMENT_TYPE) ||
			type.equals(Word.HEADER_TYPE) ||
			type.equals(Word.FOOTER_TYPE) ||
			type.equals(Word.ENDNOTES_TYPE) ||
			type.equals(Word.FOOTNOTES_TYPE) ||
			type.equals(Word.COMMENTS_TYPE) ||
			type.equals(Drawing.DIAGRAM_DATA_TYPE) ||
			type.equals(Drawing.CHART_TYPE) ||
			isGlossaryStyledTextPart(entry)
		);
	}

	private boolean isGlossaryStyledTextPart(final ZipEntry entry) {
		final String type = this.generalDocument.contentTypeFor(entry);
		return type.equals(Word.GLOSSARY_DOCUMENT_TYPE);
	}

	@Override
	public boolean hasNextPart() {
		return this.entries.hasMoreElements() || !this.postponedParts.isEmpty();
	}

	@Override
	public Part nextPart() throws IOException, XMLStreamException {
		if (!this.entries.hasMoreElements()) {
			return nextPostponedPart();
		}
		final ZipEntry entry = this.entries.nextElement();
		final String contentType = this.generalDocument.contentTypeFor(entry);

		if (!isTranslatablePart(entry)) {
			if (isModifiablePart(entry.getName(), contentType)) {
				switch (contentType) {
					case Word.STYLES_TYPE:
						this.postponedParts.put(entry, new Markup.Empty());
						return nextPart();
					case Drawing.THEME_TYPE:
						return new ModifiablePart(this.generalDocument, entry, this.generalDocument.inputStreamFor(entry));
					default:
						throw new IllegalStateException("Unsupported modifiable content type: ".concat(contentType));
				}
			}
			return new NonModifiablePart(this.generalDocument, entry);
		}

		if (isStyledTextPart(entry)) {
			final StyleDefinitions styleDefinitions = styleDefinitionsFor(entry);
			final StyleOptimisation styleOptimisation = styleOptimisationsFor(entry, styleDefinitions);
			return new StyledTextPart(
				this.generalDocument,
				entry,
				styleDefinitions,
				styleOptimisation,
				this.numberingDefinitions,
				new ContentCategoriesDetection.Default(this.generalDocument.sourceLocale())
			);
		}
		if (ContentTypes.Values.Common.PACKAGE_RELATIONSHIPS.equals(contentType)) {
			final Relationships relationships;
			if (entry.getName().equals(this.generalDocument.relationshipsPartPathFor(this.generalDocument.mainPartPath()).asString())) {
				relationships = this.generalDocument.mainPartRelationships();
			} else {
				relationships = this.generalDocument.relationshipsFrom(entry.getName());
			}
			return new RelationshipsPart(this.generalDocument, entry, relationships);
		}
		if (ContentTypes.Values.Word.NUMBERING_TYPE.equals(contentType)) {
			return new NumberingPart(this.generalDocument, entry, this.numberingDefinitions);
		}
		final ContentFilter contentFilter = new ContentFilter(
			this.generalDocument.conditionalParameters(),
			entry.getName()
		);
		ParseType parseType = ParseType.MSWORD;
		if (Word.SETTINGS_TYPE.equals(contentType)) {
			contentFilter.setBInSettingsFile(true);
		}
		else if (CORE_PROPERTIES_TYPE.equals(contentType)) {
			parseType = MSWORDDOCPROPERTIES;
		}
		contentFilter.setUpConfig(parseType);

		return new DefaultPart(this.generalDocument, entry, contentFilter);
	}

	private Part nextPostponedPart() {
		final Iterator<Map.Entry<ZipEntry, Markup>> iterator = postponedParts.entrySet().iterator();
		final Map.Entry<ZipEntry, Markup> mapEntry = iterator.next();
		iterator.remove();

		final String contentType = this.generalDocument.contentTypeFor(mapEntry.getKey());
		if (Word.STYLES_TYPE.equals(contentType)) {
			return new MarkupModifiablePart(
				this.generalDocument,
				mapEntry.getKey(),
				styleDefinitions(mapEntry.getKey()).asMarkup()
			);
		}
		return new MarkupModifiablePart(
			this.generalDocument,
			mapEntry.getKey(),
			mapEntry.getValue()
		);
	}

	private StyleDefinitions styleDefinitions(final ZipEntry entry) {
		if (entry.getName().equals(this.glossaryStyleDefinitionsPartPath)) {
			return this.glossaryStyleDefinitions;
		}
		return this.mainStyleDefinitions;
	}

	@Override
	public StyleDefinitions styleDefinitionsFor(final ZipEntry entry) {
		if (isGlossaryStyledTextPart(entry)) {
			return this.glossaryStyleDefinitions;
		}
		return this.mainStyleDefinitions;
	}

	private StyleOptimisation styleOptimisationsFor(final ZipEntry entry, final StyleDefinitions styleDefinitions) throws IOException, XMLStreamException {
		final StyleOptimisation styleOptimisation;
		if (this.generalDocument.conditionalParameters().getAllowWordStyleOptimisation()) {
			final Iterator<Namespace> iterator = this.generalDocument.namespacesOf(entry).with(Namespace.PREFIX_W).iterator();
			if (iterator.hasNext()) {
				final Namespace namespace = iterator.next();
				styleOptimisation = new StyleOptimisation.Default(
					new StyleOptimisation.Bypass(),
					this.generalDocument.conditionalParameters(),
					this.generalDocument.eventFactory(),
					this.generalDocument.presetColorValues(),
					this.generalDocument.highlightColorValues(),
					this.generalDocument.systemColorValues(),
					this.generalDocument.indexedColors(),
					this.generalDocument.mainPartTheme(),
					new QName(namespace.uri(),
						ParagraphBlockProperties.PPR, namespace.prefix()),
					new QName(namespace.uri(), RunProperties.RPR, namespace.prefix()),
					Collections.singletonList(
						new QName(namespace.uri(), RunProperty.StyleRunProperty.NAME, namespace.prefix())
					),
					styleDefinitions
				);
			} else {
				styleOptimisation = new StyleOptimisation.Bypass();
			}
		} else {
			styleOptimisation = new StyleOptimisation.Bypass();
		}
		return styleOptimisation;
	}

	private boolean isTranslatablePart(final ZipEntry entry) throws XMLStreamException, IOException {
		final String type = this.generalDocument.contentTypeFor(entry);
		if (!entry.getName().endsWith(".xml") && !entry.getName().endsWith(RelationshipsPart.EXTENSION)) return false;
		if (type.equals(Word.MAIN_DOCUMENT_TYPE)) return true;
		if (type.equals(Word.MACRO_ENABLED_MAIN_DOCUMENT_TYPE)) return true;
		if (this.generalDocument.conditionalParameters().getTranslateDocProperties() && type.equals(CORE_PROPERTIES_TYPE)) return true;
		if (ContentTypes.Values.Common.PACKAGE_RELATIONSHIPS.equals(type)) {
			return this.generalDocument.conditionalParameters().getExtractExternalHyperlinks()
				&& this.generalDocument.relationshipsFrom(entry.getName()).availableWith(Relationship.EXTERNAL_TARGET_MODE);
		}
		if (type.equals(Word.HEADER_TYPE) || type.equals(Word.FOOTER_TYPE)) {
			return this.generalDocument.conditionalParameters().getTranslateWordHeadersFooters();
		}
		if (type.equals(Word.NUMBERING_TYPE)) {
			return this.generalDocument.conditionalParameters().getTranslateWordNumberingLevelText();
		}
		if (type.equals(Word.COMMENTS_TYPE)) {
			return this.generalDocument.conditionalParameters().getTranslateComments();
		}
		if (type.equals(Word.SETTINGS_TYPE)) return true;
		if (type.equals(Drawing.CHART_TYPE)) return true;
		if (isStyledTextPart(entry)) return true;
		return false;
	}

	private boolean isModifiablePart(final String entryName, final String contentType) {
		return Word.STYLES_TYPE.equals(contentType);
	}

	@Override
	public void close() throws IOException {
	}
}
