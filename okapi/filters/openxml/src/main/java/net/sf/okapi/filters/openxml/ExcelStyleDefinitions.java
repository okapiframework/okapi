/*
 * =============================================================================
 * Copyright (C) 2010-2021 by the Okapi Framework contributors
 * -----------------------------------------------------------------------------
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =============================================================================
 */
package net.sf.okapi.filters.openxml;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.EndDocument;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartDocument;
import javax.xml.stream.events.StartElement;
import java.util.Arrays;
import java.util.LinkedList;

final class ExcelStyleDefinitions implements StyleDefinitions {
    static final String STYLESHEET = "styleSheet";
    private StartDocument startDocument;
    private StartElement startElement;
    private ExcelStyleDefinition.NumberFormats numberFormats;
    private ExcelStyleDefinition.Fonts fonts;
    private ExcelStyleDefinition.Fills fills;
    private ExcelStyleDefinition.Borders borders;
    private ExcelStyleDefinition.CellStyleFormats cellStyleFormats;
    private ExcelStyleDefinition.CellFormats cellFormats;
    private ExcelStyleDefinition.CellStyles cellStyles;
    private ExcelStyleDefinition.DifferentialFormats differentialFormats;
    private ExcelStyleDefinition.TableStyles tableStyles;
    private ExcelStyleDefinition.Colors colors;
    private ExcelStyleDefinition.Extensions extensions;
    private EndElement endElement;
    private EndDocument endDocument;

    @Override
    public void readWith(final StyleDefinitionsReader reader) throws XMLStreamException {
        final ExcelStyleDefinitionsReader styleDefinitionsReader =
            (ExcelStyleDefinitionsReader) reader;
        this.startDocument = styleDefinitionsReader.startDocument();
        this.startElement = styleDefinitionsReader.startElement();
        this.numberFormats = styleDefinitionsReader.numberFormats();
        this.fonts = styleDefinitionsReader.fonts();
        this.fills = styleDefinitionsReader.fills();
        this.borders = styleDefinitionsReader.borders();
        this.cellStyleFormats = styleDefinitionsReader.cellStyleFormats();
        this.cellFormats = styleDefinitionsReader.cellFormats();
        this.cellStyles = styleDefinitionsReader.cellStyles();
        this.differentialFormats = styleDefinitionsReader.differentialFormats();
        this.tableStyles = styleDefinitionsReader.tableStyles();
        this.colors = styleDefinitionsReader.colors();
        this.extensions = styleDefinitionsReader.extensions();
        this.endElement = styleDefinitionsReader.endElement();
        this.endDocument = styleDefinitionsReader.endDocument();
    }

    @Override
    public IndexedColors indexedColors() {
        return this.colors.indexedColors();
    }

    @Override
    public void place(final String parentId, final ParagraphBlockProperties paragraphBlockProperties, final RunProperties runProperties) {
    }

    @Override
    public String placedId() {
        throw new UnsupportedOperationException();
    }

    @Override
    public ParagraphBlockProperties combinedParagraphBlockProperties(final ParagraphBlockProperties paragraphBlockProperties) {
        return paragraphBlockProperties;
    }

    @Override
    public RunProperties combinedRunProperties(final String paragraphStyle, final String runStyle, final RunProperties runProperties) {
        return runProperties;
    }

    /**
     * Obtains a combined differential format.
     * As MS Excel application applies direct cell formatting with disregard of cell style
     * formatting and cell styles, the implementation is aligned with this behaviour.
     * @param cellFormatIndex A cell format index
     * @return The combined differential format
     */
    @Override
    public DifferentialFormat.Combined combinedDifferentialFormatFor(final int cellFormatIndex) {
        final CellFormat format = this.cellFormats.referencedBy(cellFormatIndex);
        return new DifferentialFormat.Combined(
            this.numberFormats.referencedBy(format.numberFormatId()),
            this.fonts.referencedBy(format.fontId()),
            this.fills.referencedBy(format.fillId()),
            format.alignment(),
            format.protection()
        );
    }

    @Override
    public StyleDefinitions mergedWith(final StyleDefinitions other) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Markup asMarkup() {
        final Markup markup = new Markup.General(new LinkedList<>());
        markup.addComponent(new MarkupComponent.General(Arrays.asList(this.startDocument, this.startElement)));
        markup.add(this.numberFormats.asMarkup());
        markup.add(this.fonts.asMarkup());
        markup.add(this.fills.asMarkup());
        markup.add(this.borders.asMarkup());
        markup.add(this.cellStyleFormats.asMarkup());
        markup.add(this.cellFormats.asMarkup());
        markup.add(this.cellStyles.asMarkup());
        markup.add(this.differentialFormats.asMarkup());
        markup.add(this.tableStyles.asMarkup());
        markup.add(this.colors.asMarkup());
        markup.add(this.extensions.asMarkup());
        markup.addComponent(new MarkupComponent.General(Arrays.asList(this.endElement, this.endDocument)));
        return markup;
    }
}
