/*
 * =============================================================================
 * Copyright (C) 2010-2021 by the Okapi Framework contributors
 * -----------------------------------------------------------------------------
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =============================================================================
 */
package net.sf.okapi.filters.openxml;

import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;

public interface WorksheetConfiguration {
    boolean matches(final String worksheetName);
    Set<String> sourceColumns();
    Set<String> targetColumns();
    List<String> targetColumnsMaxCharacters();
    Set<Integer> excludedRows();
    Set<String> excludedColumns();
    Set<Integer> metadataRows();
    Set<String> metadataColumns();

    /**
     * Obtains the worksheet configuration output with the written worksheet configuration to it.
     * @param output The output
     * @return The output with the written worksheet configuration
     */
    <T> T writtenTo(final WorksheetConfiguration.Output<T> output);

    final class Default implements WorksheetConfiguration {
        private final Pattern namePattern;
        private final Set<String> sourceColumns;
        private final Set<String> targetColumns;
        private final List<String> targetColumnsMaxCharacters;
        private final Set<Integer> excludedRows;
        private final Set<String> excludedColumns;
        private final Set<Integer> metadataRows;
        private final Set<String> metadataColumns;

        public Default(
            final String nameString,
            final List<String> sourceColumns,
            final List<String> targetColumns,
            final List<String> targetColumnsMaxCharacters,
            final List<Integer> excludedRows,
            final List<String> excludedColumns,
            final List<Integer> metadataRows,
            final List<String> metadataColumns
        ) {
            this(
                nameString,
                new LinkedHashSet<>(sourceColumns),
                new LinkedHashSet<>(targetColumns),
                new LinkedList<>(targetColumnsMaxCharacters),
                new LinkedHashSet<>(excludedRows),
                new LinkedHashSet<>(excludedColumns),
                new LinkedHashSet<>(metadataRows),
                new LinkedHashSet<>(metadataColumns)
            );
        }

        Default(
            final String nameString,
            final Set<String> sourceColumns,
            final Set<String> targetColumns,
            final List<String> targetColumnsMaxCharacters,
            final Set<Integer> excludedRows,
            final Set<String> excludedColumns,
            final Set<Integer> metadataRows,
            final Set<String> metadataColumns
        ) {
            this(
                Pattern.compile(nameString),
                sourceColumns,
                targetColumns,
                targetColumnsMaxCharacters,
                excludedRows,
                excludedColumns,
                metadataRows,
                metadataColumns
            );
        }

        Default(
            final Pattern namePattern,
            final Set<String> sourceColumns,
            final Set<String> targetColumns,
            final List<String> targetColumnsMaxCharacters,
            final Set<Integer> excludedRows,
            final Set<String> excludedColumns,
            final Set<Integer> metadataRows,
            final Set<String> metadataColumns
        ) {
            this.namePattern = namePattern;
            this.sourceColumns = sourceColumns;
            this.targetColumns = targetColumns;
            this.targetColumnsMaxCharacters = targetColumnsMaxCharacters;
            this.excludedRows = excludedRows;
            this.excludedColumns = excludedColumns;
            this.metadataRows = metadataRows;
            this.metadataColumns = metadataColumns;
        }

        @Override
        public boolean matches(final String worksheetName) {
            return this.namePattern.matcher(worksheetName).matches();
        }

        @Override
        public Set<String> sourceColumns() {
            return this.sourceColumns;
        }

        @Override
        public Set<String> targetColumns() {
            return this.targetColumns;
        }

        @Override
        public List<String> targetColumnsMaxCharacters() {
            return this.targetColumnsMaxCharacters;
        }

        @Override
        public Set<Integer> excludedRows() {
            return this.excludedRows;
        }

        @Override
        public Set<String> excludedColumns() {
            return this.excludedColumns;
        }

        @Override
        public Set<Integer> metadataRows() {
            return this.metadataRows;
        }

        @Override
        public Set<String> metadataColumns() {
            return this.metadataColumns;
        }

        @Override
        public <T> T writtenTo(final Output<T> output) {
            return output.writtenWith(
                this.namePattern,
                this.sourceColumns,
                this.targetColumns,
                this.targetColumnsMaxCharacters,
                this.excludedRows,
                this.excludedColumns,
                this.metadataRows,
                this.metadataColumns
            );
        }
    }

    /**
     * The worksheet configuration output.
     * @param <T> The type of the output
     */
    interface Output<T> {
        /**
         * Obtains a written output with the help of provided name pattern
         * and excluded rows.
         * @param namePattern The name pattern
         * @param sourceColumns The source columns
         * @param targetColumns The target columns
         * @param targetColumnsMaxCharacters The maximum characters for the target columns
         * @param excludedRows The excluded rows
         * @param excludedColumns The excluded columns
         * @param metadataRows The metadata rows
         * @param metadataColumns The metadata columns
         * @return The written output
         */
        T writtenWith(
            final Pattern namePattern,
            final Set<String> sourceColumns,
            final Set<String> targetColumns,
            final List<String> targetColumnsMaxCharacters,
            final Set<Integer> excludedRows,
            final Set<String> excludedColumns,
            final Set<Integer> metadataRows,
            final Set<String> metadataColumns
        );
    }
}
