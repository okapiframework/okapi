/*
 * =============================================================================
 * Copyright (C) 2010-2025 by the Okapi Framework contributors
 * -----------------------------------------------------------------------------
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =============================================================================
 */
package net.sf.okapi.filters.openxml;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public interface TargetColumnsMaxCharacters {
    String maxCharactersFor(final String target);

    class Default implements TargetColumnsMaxCharacters {
        private static final String EMPTY = "";
        private final Map<String, String> maxCharactersByTargetColumns;

        Default(final List<String> targetColumns, final List<String> maxCharacters) {
            this.maxCharactersByTargetColumns = IntStream.range(0, Math.min(targetColumns.size(), maxCharacters.size()))
                .boxed()
                .collect(Collectors.toMap(targetColumns::get, maxCharacters::get));
        }

        @Override
        public String maxCharactersFor(final String target) {
            return this.maxCharactersByTargetColumns.getOrDefault(target, EMPTY);
        }
    }
}
