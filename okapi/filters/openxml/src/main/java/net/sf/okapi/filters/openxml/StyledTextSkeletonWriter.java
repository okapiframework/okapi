/*===========================================================================
  Copyright (C) 2016-2017 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
===========================================================================*/

package net.sf.okapi.filters.openxml;

import net.sf.okapi.common.IdGenerator;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.encoder.EncoderContext;
import net.sf.okapi.common.encoder.EncoderManager;
import net.sf.okapi.common.filters.fontmappings.FontMappings;
import net.sf.okapi.common.resource.DocumentPart;
import net.sf.okapi.common.resource.EndSubfilter;
import net.sf.okapi.common.resource.Ending;
import net.sf.okapi.common.resource.ITextUnit;
import net.sf.okapi.common.resource.StartDocument;
import net.sf.okapi.common.resource.StartGroup;
import net.sf.okapi.common.resource.StartSubDocument;
import net.sf.okapi.common.resource.StartSubfilter;
import net.sf.okapi.common.resource.TextContainer;
import net.sf.okapi.common.resource.TextFragment;
import net.sf.okapi.common.skeleton.GenericSkeleton;
import net.sf.okapi.common.skeleton.GenericSkeletonWriter;
import net.sf.okapi.common.skeleton.ISkeletonWriter;

import javax.xml.stream.XMLEventFactory;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

class StyledTextSkeletonWriter implements ISkeletonWriter {
	static final String XML_VERSION = "1.0";
	static final String XML_ENCODING = OpenXMLFilter.ENCODING.name();
	private static final String XML_HEADER =
		"<?xml version=\"" + XML_VERSION + "\" encoding=\"" + XML_ENCODING + "\"?>";
	private static final String EMPTY_STRING = "";
	private final ConditionalParameters parameters;
	private final XMLEventFactory eventFactory;
	private final PresetColorValues presetColorValues;
	private final PresetColorValues highlightColorValues;
	private final SystemColorValues systemColorValues;
	private final IndexedColors indexedColors;
	private final Theme theme;
	private final LocaleId sourceLocale;
	private LocaleId targetLocale;
	private final DispersedTranslations dispersedTranslations;
	private final FontMappings applicableFontMappings;
	private final StyleDefinitions styleDefinitions;
	private final GenericSkeletonWriter genericSkeletonWriter;

	private IdGenerator nestedBlockIds = new IdGenerator(null);
	private Map<String, String> processedReferents = new HashMap<>();
	private Deque<Referring> referrings = new ArrayDeque<>();

	StyledTextSkeletonWriter(
        final ConditionalParameters parameters,
        final XMLEventFactory eventFactory,
        final PresetColorValues presetColorValues,
        final PresetColorValues highlightColorValues,
        final SystemColorValues systemColorValues,
		final IndexedColors indexedColors,
		final Theme theme,
		final LocaleId sourceLocale,
        final LocaleId targetLocale,
        final DispersedTranslations dispersedTranslations,
        final FontMappings applicableFontMappings,
        final StyleDefinitions styleDefinitions,
        final GenericSkeletonWriter genericSkeletonWriter
    ) {
		this.parameters = parameters;
		this.eventFactory = eventFactory;
		this.presetColorValues = presetColorValues;
		this.highlightColorValues = highlightColorValues;
		this.systemColorValues = systemColorValues;
		this.indexedColors = indexedColors;
		this.theme = theme;
		this.sourceLocale = sourceLocale;
		this.targetLocale = targetLocale;
		this.dispersedTranslations = dispersedTranslations;
		this.applicableFontMappings = applicableFontMappings;
		this.styleDefinitions = styleDefinitions;
		this.genericSkeletonWriter = genericSkeletonWriter;
	}

	@Override
	public void close() {
	}

	@Override
	public String processStartDocument(LocaleId outputLocale, String outputEncoding,
			EncoderManager encoderManager, StartDocument resource) {
		this.targetLocale = outputLocale;
		this.genericSkeletonWriter.setOutputLoc(outputLocale);
		return XML_HEADER;
	}

	@Override
	public String processEndDocument(Ending resource) {
		return EMPTY_STRING;
	}

	@Override
	public String processStartSubDocument(StartSubDocument resource) {
		return XML_HEADER;
	}

	@Override
	public String processEndSubDocument(Ending resource) {
		return EMPTY_STRING;
	}

	@Override
	public String processStartGroup(StartGroup resource) {
		return EMPTY_STRING;
	}

	@Override
	public String processEndGroup(Ending resource) {
		return EMPTY_STRING;
	}

	@Override
	public String processTextUnit(ITextUnit tu) {
		if ((!ConditionalParameters.EMPTY_SUBFILTER_CONFIGURATION.equals(parameters.getSubfilter())
                	&& null == tu.getSkeleton())
				|| tu.getSkeleton() instanceof GenericSkeleton) {
			// handle subfiltered text units or text units of StringItems
			return genericSkeletonWriter.processTextUnit(tu);
		}
		TextContainer target = getTargetForOutput(tu);

		//In case the AllowEmptyTargets parameter is set to true, allow empty targets by returning an empty string
		if (parameters.getAllowEmptyTargets()) {
			if(target == null) {
				return "";
			}
		}

		// Now I need to transform the block by replacing the run text with
		// the content in the target.
		// I will need to track codes and map them against the run code stack
		// produced in the original mapper.
		// The block contains chunks that look like
		// - [start block]
		// - [good stuff]: 0 or more
		// - [end block]
		// The [good stuff] is all represented somehow in the codes and the text.
		// For every character of text in the target TextContainer, we need to use
		// the most recent open code to indicate our run styling.  If there is none,
		// a default style is used.
		// I'll need to know what run element to use, maybe, which is probably info that should
		// be stored in the block.

		final String serialized;
		// Translatable attribute text TUs has no skeleton, as it's always a referent.
		final XMLEventSerializer writing = new XMLEventSerializer();
		if (tu.getSkeleton() != null) {
			if (tu.getSkeleton() instanceof BlockSkeleton) {
				BlockSkeleton skel = ((BlockSkeleton)tu.getSkeleton());
				Block block = skel.block();
				// This should always have > 2 entries, as otherwise this would have been serialized
				// as a document part.
				final List<Chunk> chunks = block.getChunks();
				chunks.forEach(c -> c.apply(this.applicableFontMappings));
				Nameable nameableMarkupComponent = ((Markup) chunks.get(0)).nameableComponent();
				final MarkupClarificationConfiguration mcc = new MarkupClarificationConfiguration(
					this.parameters,
					this.eventFactory,
                    this.presetColorValues,
					this.highlightColorValues,
                    this.systemColorValues,
					this.indexedColors,
					this.theme,
					this.sourceLocale,
					this.targetLocale,
					this.dispersedTranslations
				);
				mcc.prepareFor(nameableMarkupComponent);
				new MarkupClarification(mcc).performFor((Markup) chunks.get(0));
				writing.addAll(chunks.get(0).getEvents());
				new BlockTextUnitWriter(
					this.parameters,
					this.eventFactory,
					this.targetLocale,
					skel.block().getRunName(),
					skel.block().getTextName(),
					skel.baseRunPropertiesPairWithDetectedRunFonts(),
					skel.hiddenCodes(),
					skel.visibleCodes(),
					writing,
					mcc.runPropertiesClarification()
				).write(target);
				writing.addAll(chunks.get(chunks.size() - 1).getEvents());
				serialized = writing.toString();
			} else {
				throw new IllegalArgumentException("TextUnit " + tu.getId() +
						" has no associated block content");
			}
		}
		else {
			serialized = writing.getAttributeEncoder().encode(target.toString(), EncoderContext.INLINE);
		}

		return processReferences(tu, serialized);
	}

	private String processReferences(final ITextUnit tu, final String serialized) {
		final String output;

		// If this TU is a referent of something, it means it was part of a nested block.
		// We need to save it up for reinsertion into some other TU later on, when we find
		// the correct reference.
		if (tu.isReferent()) {
			if (TextUnitProperties.integer(tu, TextUnitMapper.REFERENCES) != 0) {
				this.referrings.push(
					new Referring(
						serialized,
						TextUnitProperties.integer(tu, TextUnitMapper.REFERENCES)
					)
				);
				output = EMPTY_STRING;
			} else {
				this.processedReferents.put(nestedBlockIds.createId(), serialized);
				this.referrings.peek().foundReferents++;

				if (this.referrings.peek().isLastFoundReferent()) {
					final String resolved = resolveReferences(this.referrings.pop().serialized);
					if (!this.referrings.isEmpty()) {
						// the current text unit is a referent without its own
						// references, so it is safe to invoke the method
						// to finalise the processing
						output = processReferences(tu, resolved);
					} else {
						output = resolved;
					}
				} else {
					output = EMPTY_STRING;
				}
			}
		}
		else {
			if (TextUnitProperties.integer(tu, TextUnitMapper.REFERENCES) == 0) {
				output = serialized;
			} else {
			    this.referrings.push(
					new Referring(
						serialized,
						TextUnitProperties.integer(tu, TextUnitMapper.REFERENCES)
					)
				);
				output = EMPTY_STRING;
			}
		}

		return output;
	}

	private String resolveReferences(String original) {
		// TODO get the StringBuilder directly from the XMLEvent Serializer
		StringBuilder sb = new StringBuilder(original);
		for (Object[] markerInfo = TextFragment.getRefMarker(sb); markerInfo != null;
					  markerInfo = TextFragment.getRefMarker(sb)) {
			String processedReferent = processedReferents.get(markerInfo[0]);
			sb.replace((int)markerInfo[1], (int)markerInfo[2], processedReferent);
		}
		return sb.toString();
	}

	private TextContainer getTargetForOutput(ITextUnit tu) {
		// Disallow empty targets and return the source text, if AllowEmptyTargets parameter is set to false
		if (!parameters.getAllowEmptyTargets()) {
			if (targetLocale == null) {
				return tu.getSource();
			}
		}

		TextContainer trgCont = tu.getTarget(targetLocale);

		if (!parameters.getAllowEmptyTargets()) {
			if (trgCont == null || trgCont.isEmpty()) {
				return tu.getSource();
			}
		}

		return trgCont;
	}

	@Override
	public String processDocumentPart(DocumentPart documentPart) {
		if (documentPart.getSkeleton() instanceof GenericSkeleton) {
			return genericSkeletonWriter.processDocumentPart(documentPart);
		}

		MarkupSkeleton markupSkeleton = (MarkupSkeleton) documentPart.getSkeleton();
		Markup markup = markupSkeleton.getMarkup();

		markup.apply(this.applicableFontMappings);
		Nameable nameableMarkupComponent = markup.nameableComponent();

		if (null != nameableMarkupComponent) {
			// do care about a markup with the start markup component only,
			// as otherwise there is nothing to clarify at all
			final MarkupClarificationConfiguration mcc = new MarkupClarificationConfiguration(
				this.parameters,
				this.eventFactory,
				this.presetColorValues,
				this.highlightColorValues,
				this.systemColorValues,
				this.indexedColors,
				this.theme,
				this.sourceLocale,
				this.targetLocale,
				this.dispersedTranslations
			);
			mcc.prepareFor(nameableMarkupComponent);
			new MarkupClarification(mcc).performFor(markup);
		}

		final String output;
		final String serialized = XMLEventSerializer.serialize(markup);
		if (documentPart.isReferent()) {
			this.referrings.push(
				new Referring(serialized, documentPart.getReferenceCount())
			);
			output = EMPTY_STRING;
		} else {
			output = serialized;
		}
		return output;
	}

	@Override
	public String processStartSubfilter(StartSubfilter resource) {
		return genericSkeletonWriter.processStartSubfilter(resource);
	}

	@Override
	public String processEndSubfilter(EndSubfilter resource) {
		return genericSkeletonWriter.processEndSubfilter(resource);
	}

	/**
	 * Provides the referring connotation.
	 * I.e. a serialized text unit may have references to other
	 * text units (referents).
	 */
	private static final class Referring {
		/**
		 * The serialized text unit.
		 */
		private final String serialized;

		/**
		 * The number of references to other text units.
		 */
		private final int references;

		/**
		 * Number of referents found during processing.
		 */
		private int foundReferents;

		/**
		 * Constructs a Referring instance.
		 *
		 * @param serialized The serialized text unit
		 * @param references The number of references to other text units
		 */
		Referring(String serialized, int references) {
			this.serialized = serialized;
			this.references = references;
		}

		/**
		 * Checks whether the found referent is the last one.
		 * @return {@code true} if this is the last found referent
		 *         {@code false} otherwise
		 */
		boolean isLastFoundReferent() {
			return references == foundReferents;
		}
	}
}
