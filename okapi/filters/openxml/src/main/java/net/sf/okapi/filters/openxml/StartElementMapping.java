/*
 * =============================================================================
 * Copyright (C) 2010-2024 by the Okapi Framework contributors
 * -----------------------------------------------------------------------------
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =============================================================================
 */
package net.sf.okapi.filters.openxml;

import net.sf.okapi.common.Event;
import net.sf.okapi.common.EventType;
import net.sf.okapi.common.IdGenerator;
import net.sf.okapi.common.resource.DocumentPart;
import net.sf.okapi.common.resource.ITextUnit;
import net.sf.okapi.common.resource.TextFragment;
import net.sf.okapi.common.resource.TextUnit;

import javax.xml.stream.XMLEventFactory;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.StartElement;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

final class StartElementMapping {
    private final ConditionalParameters parameters;
    private final XMLEventFactory eventFactory;
    private final IdGenerator documentPartIds;
    private final IdGenerator textUnitIds;
    private final String hiddenAttributeName;
    private final Set<String> hiddenAttributeTrueValues;

    StartElementMapping(
        final ConditionalParameters parameters,
        final XMLEventFactory eventFactory,
        final IdGenerator documentPartIds,
        final IdGenerator textUnitIds,
        final String hiddenAttributeName,
        final Set<String> hiddenAttributeTrueValues
    ) {
        this.parameters = parameters;
        this.eventFactory = eventFactory;
        this.documentPartIds = documentPartIds;
        this.textUnitIds = textUnitIds;
        this.hiddenAttributeName = hiddenAttributeName;
        this.hiddenAttributeTrueValues = hiddenAttributeTrueValues;
    }

    List<Event> eventsFor(final StartElement startElement, final List<String> attributeNames) {
        final List<ITextUnit> textUnits = new ArrayList<>();
        final List<Attribute> attributes = new ArrayList<>();
        final Iterator<Attribute> it = startElement.getAttributes();
        int updatedAttributes = 0;
        while (it.hasNext()) {
            final Attribute a = it.next();
            if (this.hiddenAttributeName.equals(a.getName().getLocalPart())
                && hiddenFor(a.getValue(), startElement.getName().getPrefix())) {
                updatedAttributes = 0;
                break;
            }
            if (!a.getValue().isBlank() && attributeNames.contains(a.getName().getLocalPart())) {
                final String id = this.textUnitIds.createId();
                final TextUnit tu = new TextUnit(id, a.getValue());
                tu.setPreserveWhitespaces(true);
                tu.setIsReferent(true); // logically this is a reference but not a referent
                textUnits.add(tu);
                attributes.add(
                    this.eventFactory.createAttribute(a.getName(), TextFragment.makeRefMarker(id))
                );
                updatedAttributes++;
            } else {
                attributes.add(a);
            }
        }
        final Markup markup;
        if (0 < updatedAttributes) {
            markup = new Markup.General(List.of(new MarkupComponent.Start(
                this.eventFactory,
                this.eventFactory.createStartElement(startElement.getName(), attributes.iterator(), startElement.getNamespaces())
            )));
        } else {
            markup = new Markup.General(List.of(new MarkupComponent.Start(this.eventFactory, startElement)));
        }
        final DocumentPart documentPart = new DocumentPart(this.documentPartIds.createId(), 0 < updatedAttributes, new MarkupSkeleton(markup));
        documentPart.setReferenceCount(updatedAttributes);
        final List<Event> events = new ArrayList<>(1 + textUnits.size());
        events.add(new Event(EventType.DOCUMENT_PART, documentPart));
        textUnits.forEach(tu -> events.add(new Event(EventType.TEXT_UNIT, tu)));
        return events;
    }

    private boolean hiddenFor(final String value, final String prefix) {
        return this.hiddenAttributeTrueValues.contains(value)
            && Namespace.PREFIX_P.equals(prefix)
            && !this.parameters.getTranslatePowerpointHidden();
    }
}
