/*
 * =============================================================================
 * Copyright (C) 2010-2021 by the Okapi Framework contributors
 * -----------------------------------------------------------------------------
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =============================================================================
 */
package net.sf.okapi.filters.openxml;

import javax.xml.stream.events.XMLEvent;
import java.util.List;
import java.util.ListIterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

interface CharactersClarification {
    void adjustClarificationContextWith(final MarkupComponent.Context context);
    void performFor(final List<XMLEvent> events);

    final class Bypass implements CharactersClarification {
        @Override
        public void adjustClarificationContextWith(final MarkupComponent.Context context) {
        }

        @Override
        public void performFor(final List<XMLEvent> events) {
        }
    }

    final class ReferencesTranslation implements CharactersClarification {
        private static final String UNEXPECTED_NUMBER_OF_MATCHING_GROUPS =
            "Unexpected number of matched groups: ";
        private static final String INVALID_REFERENCE = "Invalid reference: ";
        private static final String EMPTY = "";
        private static final String DOUBLE_QUOTES = "\"";
        private static final String COMMA = ",";

        /**
         * A pivot data pattern.
         * E.g.:
         * - GETPIVOTDATA("Sum of Col B2",Sheet1!$B$9)
         * - GETPIVOTDATA("Sales",$B$4,"Region","East","Product","Almond")
         */
        private static final Pattern pivotDataFormula =
            Pattern.compile("(.*?GETPIVOTDATA\\s?\\()(.+?)(\\).*)", Pattern.DOTALL);

        /**
         * A cross reference pattern in formula.
         * E.g.: SUM('Sheet 2'!A2,Sheet1!A3)
         */
        private static final Pattern crossReferenceInFormula =
            Pattern.compile("(.*?[,(']{1,2})([^/?*\\[\\]]+?)([,!)']{1}.*)", Pattern.DOTALL);

        /**
         * Invalid table name characters.
         */
        private static final String INVALID_TABLE_NAME_CHARS = "[^/\\\\*'?\\[\\]:\\(\\), ]";

        /**
         * A structured reference pattern in formula.
         * E.g.:
         * - TableName1[[#All], [Column name 1] : [Column name 2] [Column name 3]]
         * - COLUMN(Table1Custom[COLUMN])
         */
        private static final Pattern structuredReferenceInFormula =
            Pattern.compile("(.*?)((?:" + INVALID_TABLE_NAME_CHARS + "*?[\\[]{2}.+?[\\]]{2})|(?:" + INVALID_TABLE_NAME_CHARS + "*?\\[.+\\]))(.*)", Pattern.DOTALL);

        /**
         * An item and column specifier pattern.
         * E.g.:
         * - [#All]
         * - [#Data]
         * - [Column name 1]
         */
        private static final Pattern itemAndColumnSpecifier =
            Pattern.compile("(.*?[\\[]{1,2})(.+?)([\\]]{1}.*)", Pattern.DOTALL);

        private final ClarificationContext clarificationContext;
        private final DispersedTranslations dispersedTranslations;

        ReferencesTranslation(
            final ClarificationContext clarificationContext,
            final DispersedTranslations dispersedTranslations
        ) {
            this.clarificationContext = clarificationContext;
            this.dispersedTranslations = dispersedTranslations;
        }

        @Override
        public void adjustClarificationContextWith(final MarkupComponent.Context context) {
            this.clarificationContext.adjust(context);
        }

        @Override
        public void performFor(final List<XMLEvent> events) {
            final ListIterator<XMLEvent> iterator = events.listIterator();
            while (iterator.hasNext()) {
                final XMLEvent e = iterator.next();
                if (!e.isCharacters()) {
                    continue;
                }
                final String original = e.asCharacters().getData();
                String translated = translatedWithPivotDataFunctionIn(original);
                translated = translatedWithCrossReferencesIn(translated);
                translated = translatedWithStructuredReferencesIn(translated);
                if (original.equals(translated)) {
                    continue;
                }
                iterator.set(
                    this.clarificationContext.creationalParameters().getEventFactory().createCharacters(translated)
                );
            }
        }

        private String translatedWithPivotDataFunctionIn(final String original) {
            final Matcher matcher = ReferencesTranslation.pivotDataFormula.matcher(original);
            if (!matcher.find()) {
                return original;
            }
            final StringBuilder result = new StringBuilder();
            if (matcher.groupCount() != MatchingGroup.values().length) {
                throw new IllegalStateException(UNEXPECTED_NUMBER_OF_MATCHING_GROUPS.concat(String.valueOf(matcher.groupCount())));
            }
            result.append(matcher.group(MatchingGroup.NON_TRANSLATABLE.value()));
            result.append(
                translatedWithPivotDataIn(matcher.group(MatchingGroup.TRANSLATABLE.value()))
            );
            result.append(matcher.group(MatchingGroup.UNPROCESSED.value()));
            return result.toString();
        }

        private String translatedWithPivotDataIn(final String string) {
            final String[] parts = string.split("\\s?" + COMMA + "\\s?");
            final CrossSheetCellReference pivotTablePartReference = relativeCrossSheetCellReferenceFrom(parts[1]);
            if (!this.dispersedTranslations.presentFor(pivotTablePartReference)) {
                return string;
            }
            final String namespace = this.dispersedTranslations.namespaceFor(pivotTablePartReference);
            final StringBuilder result = new StringBuilder();
            translateAndAppend(namespace, parts[0], result);
            result.append(COMMA);
            result.append(parts[1]);
            if (2 < parts.length) {
                result.append(COMMA);
                for (int i = 2; i < parts.length; i++) {
                    translateAndAppend(namespace, parts[i], result);
                    if (i < parts.length - 1) {
                        result.append(COMMA);
                    }
                }
            }
            return result.toString();
        }

        private void translateAndAppend(final String namespace, final String data, final StringBuilder result) {
            final String unwrappedData = unwrap(data);
            if (this.dispersedTranslations.namespaceAndRawSourceMatch(namespace, unwrappedData)) {
                final String v = this.dispersedTranslations.encodedTargetOrRawSourceFor(
                    namespace,
                    unwrappedData,
                    this.clarificationContext.targetLocale()
                );
                result.append(DOUBLE_QUOTES).append(v).append(DOUBLE_QUOTES);
            } else {
                result.append(data);
            }
        }

        private String unwrap(final String stringInDoubleQuotes) {
            final String result;
            if (2 == stringInDoubleQuotes.length()) {
                result = EMPTY;
            } else {
                result = stringInDoubleQuotes.substring(1, stringInDoubleQuotes.length() - 1);
            }
            return result;
        }

        private CrossSheetCellReference relativeCrossSheetCellReferenceFrom(final String string) {
            final String[] parts = string.split(CrossSheetCellReference.DELIMITER);
            final String worksheetName;
            final CellReference cellReference;
            if (1 == parts.length) {
                worksheetName = this.clarificationContext.markupComponentContext().name();
                cellReference = relativeCellReferenceFrom(parts[0]);
            } else if (2 == parts.length) {
                worksheetName = parts[0];
                cellReference = relativeCellReferenceFrom(parts[1]);
            } else {
                throw new IllegalStateException(INVALID_REFERENCE.concat(string));
            }
            return new CrossSheetCellReference(worksheetName, cellReference);
        }

        private CellReference relativeCellReferenceFrom(final String string) {
            return new CellReference(string.replaceAll("\\$", EMPTY));
        }

        private String translatedWithCrossReferencesIn(final String original) {
            final Matcher matcher = ReferencesTranslation.crossReferenceInFormula.matcher(original);
            if (!matcher.find()) {
                return original;
            }
            return translatedWith(EMPTY, matcher);
        }

        private String translatedWithStructuredReferencesIn(final String original) {
            final Matcher matcher = ReferencesTranslation.structuredReferenceInFormula.matcher(original);
            if (!matcher.find()) {
                return original;
            }
            final StringBuilder result = new StringBuilder();
            String part;
            do {
                if (matcher.groupCount() != MatchingGroup.values().length) {
                    throw new IllegalStateException(UNEXPECTED_NUMBER_OF_MATCHING_GROUPS.concat(String.valueOf(matcher.groupCount())));
                }
                result.append(matcher.group(MatchingGroup.NON_TRANSLATABLE.value()));
                final String reference = matcher.group(
                    MatchingGroup.TRANSLATABLE.value()
                );
                result.append(translatedWithItemAndColumnSpecifiersIn(reference));
                part = matcher.group(MatchingGroup.UNPROCESSED.value());
                matcher.reset(part);
            } while (matcher.find());
            if (part != null) {
                result.append(part);
            }
            return result.toString();
        }

        private String translatedWithItemAndColumnSpecifiersIn(final String original) {
            final Matcher matcher = ReferencesTranslation.itemAndColumnSpecifier.matcher(original);
            if (!matcher.find()) {
                return original;
            }
            if (matcher.groupCount() != MatchingGroup.values().length) {
                throw new IllegalStateException(UNEXPECTED_NUMBER_OF_MATCHING_GROUPS.concat(String.valueOf(matcher.groupCount())));
            }
            final String firstMatchedGroup = matcher.group(MatchingGroup.NON_TRANSLATABLE.value());
            final int startOfTableSpecifier = firstMatchedGroup.indexOf("[");
            final String tableName = -1 == startOfTableSpecifier
                ? EMPTY // todo: if unqualified, the table name has to be obtained from the current context
                : firstMatchedGroup.substring(0, startOfTableSpecifier);
            return translatedWith(tableName, matcher);
        }

        private String translatedWith(final String namespace, final Matcher matcher) {
            final StringBuilder result = new StringBuilder();
            String part;
            do {
                if (matcher.groupCount() != MatchingGroup.values().length) {
                    throw new IllegalStateException(UNEXPECTED_NUMBER_OF_MATCHING_GROUPS.concat(String.valueOf(matcher.groupCount())));
                }
                result.append(matcher.group(MatchingGroup.NON_TRANSLATABLE.value()));
                final String specifier = matcher.group(
                    MatchingGroup.TRANSLATABLE.value()
                );
                if (this.dispersedTranslations.namespaceAndRawSourceMatch(namespace, specifier)) {
                    final String v = this.dispersedTranslations.encodedTargetOrRawSourceFor(
                        namespace,
                        specifier,
                        this.clarificationContext.targetLocale()
                    );
                    result.append(v);
                } else {
                    result.append(specifier);
                }
                part = matcher.group(MatchingGroup.UNPROCESSED.value());
                matcher.reset(part);
            } while (matcher.find());
            if (part != null) {
                result.append(part);
            }
            return result.toString();
        }

        private enum MatchingGroup {
            NON_TRANSLATABLE(1),
            TRANSLATABLE(2),
            UNPROCESSED(3);

            private final int value;

            MatchingGroup(int value) {
                this.value = value;
            }

            int value() {
                return value;
            }
        }
    }
}
