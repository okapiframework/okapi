/*
 * =============================================================================
 * Copyright (C) 2010-2021 by the Okapi Framework contributors
 * -----------------------------------------------------------------------------
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =============================================================================
 */
package net.sf.okapi.filters.openxml;

import net.sf.okapi.common.IdGenerator;
import net.sf.okapi.common.XMLEventsReader;

import javax.xml.stream.XMLEventFactory;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.EndDocument;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartDocument;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

interface SharedStrings {
    String SST = "sst";
    String SI = "si";

    StartDocument startDocument();
    StartElement startElement();
    StringItem stringItemFor(final int index);
    EndElement endElement();
    EndDocument endDocument();
    void readWith(final XMLEventReader eventReader) throws XMLStreamException;
    void addFrom(final Cells cells) throws XMLStreamException;

    final class Default implements SharedStrings {
        private final ConditionalParameters conditionalParameters;
        private final XMLEventFactory eventFactory;
        private final PresetColorValues presetColorValues;
        private final PresetColorValues highlightColorValues;
        private final SystemColorValues systemColorValues;
        private final IndexedColors indexedColors;
        private final Theme theme;
        private final IdGenerator nestedBlockId;
        private final StyleDefinitions styleDefinitions;
        private final StyleOptimisation styleOptimisation;
        private final NumberingDefinitions numberingDefinitions;
        private final ContentCategoriesDetection contentCategoriesDetection;
        private final List<StringItem> stringItems;
        private StartDocument startDocument;
        private StartElement startElement;
        private EndElement endElement;
        private EndDocument endDocument;

        Default(
            final ConditionalParameters conditionalParameters,
            final XMLEventFactory eventFactory,
            final PresetColorValues presetColorValues,
            final PresetColorValues highlightColorValues,
            final SystemColorValues systemColorValues,
            final IndexedColors indexedColors,
            final Theme theme,
            final IdGenerator nestedBlockId,
            final StyleDefinitions styleDefinitions,
            final StyleOptimisation styleOptimisation,
            final NumberingDefinitions numberingDefinitions,
            final ContentCategoriesDetection contentCategoriesDetection
        ) {
            this(
                conditionalParameters,
                eventFactory,
                presetColorValues,
                highlightColorValues,
                systemColorValues,
                indexedColors,
                theme,
                nestedBlockId,
                styleDefinitions,
                styleOptimisation,
                numberingDefinitions,
                contentCategoriesDetection,
                new ArrayList<>()
            );
        }

        Default(
            final ConditionalParameters conditionalParameters,
            final XMLEventFactory eventFactory,
            final PresetColorValues presetColorValues,
            final PresetColorValues highlightColorValues,
            final SystemColorValues systemColorValues,
            final IndexedColors indexedColors,
            final Theme theme,
            final IdGenerator nestedBlockId,
            final StyleDefinitions styleDefinitions,
            final StyleOptimisation styleOptimisation,
            final NumberingDefinitions numberingDefinitions,
            final ContentCategoriesDetection contentCategoriesDetection,
            final List<StringItem> stringItems
        ) {
            this.conditionalParameters = conditionalParameters;
            this.eventFactory = eventFactory;
            this.presetColorValues = presetColorValues;
            this.highlightColorValues = highlightColorValues;
            this.systemColorValues = systemColorValues;
            this.indexedColors = indexedColors;
            this.theme = theme;
            this.nestedBlockId = nestedBlockId;
            this.styleDefinitions = styleDefinitions;
            this.styleOptimisation = styleOptimisation;
            this.numberingDefinitions = numberingDefinitions;
            this.contentCategoriesDetection = contentCategoriesDetection;
            this.stringItems = stringItems;
        }

        @Override
        public StartDocument startDocument() {
            return this.startDocument;
        }

        @Override
        public StartElement startElement() {
            return this.startElement;
        }

        @Override
        public StringItem stringItemFor(final int index) {
            final StringItem si = this.stringItems.get(index);
            if (null == si) {
                throw new IllegalArgumentException("The requested string item is not available: "
                    .concat(String.valueOf(index)));
            }
            return si;
        }

        @Override
        public EndElement endElement() {
            return this.endElement;
        }

        @Override
        public EndDocument endDocument() {
            return this.endDocument;
        }

        @Override
        public void readWith(final XMLEventReader eventReader) throws XMLStreamException {
            while (eventReader.hasNext()) {
                final XMLEvent e = eventReader.nextEvent();
                if (e.isEndDocument()) {
                    this.endDocument = (EndDocument) e;
                    break;
                }
                if (e.isStartDocument()) {
                    this.startDocument = (StartDocument) e;
                    continue;
                }
                if (e.isEndElement() && e.asEndElement().getName().equals(this.startElement.getName())) {
                    this.endElement = e.asEndElement();
                    continue;
                }
                if (e.isStartElement() && SST.equals(e.asStartElement().getName().getLocalPart())) {
                    this.startElement = this.eventFactory.createStartElement(
                        e.asStartElement().getName(),
                        null, // stripping attributes
                        e.asStartElement().getNamespaces()
                    );
                    continue;
                }
                if (e.isStartElement() && SI.equals(e.asStartElement().getName().getLocalPart())) {
                    final StartElementContext sec = new StartElementContext(
                        e.asStartElement(),
                        eventReader,
                        this.presetColorValues,
                        this.highlightColorValues,
                        this.systemColorValues,
                        this.indexedColors,
                        this.theme,
                        this.eventFactory,
                        this.conditionalParameters
                    );
                    final StringItem stringItem = new StringItemParser(
                        sec,
                        nestedBlockId,
                        styleDefinitions,
                        styleOptimisation,
                        numberingDefinitions,
                        contentCategoriesDetection
                    ).parse();
                    stringItem.optimiseStyles();
                    this.stringItems.add(stringItem);
                }
            }
        }

        @Override
        public void addFrom(final Cells cells) throws XMLStreamException {
            final StartElement startElement = this.eventFactory.createStartElement(
                this.startElement.getName().getPrefix(),
                this.startElement.getName().getNamespaceURI(),
                SI
            );
            final EndElement endElement = this.eventFactory.createEndElement(
                this.startElement.getName().getPrefix(),
                this.startElement.getName().getNamespaceURI(),
                SI
            );
            final Iterator<Cell> iterator = cells.iterator();
            while(iterator.hasNext()) {
                final Cell cell = iterator.next();
                final List<XMLEvent> events = new ArrayList<>(cell.inlineStringEvents().size() + 1);
                events.addAll(cell.inlineStringEvents());
                events.add(endElement);
                final XMLEventReader eventReader = new XMLEventsReader(events);
                final StringItem stringItem = new StringItemParser(
                    new StartElementContext(
                        startElement,
                        eventReader,
                        this.presetColorValues,
                        this.highlightColorValues,
                        this.systemColorValues,
                        this.indexedColors,
                        this.theme,
                        this.eventFactory,
                        this.conditionalParameters
                    ),
                    nestedBlockId,
                    styleDefinitions,
                    styleOptimisation,
                    numberingDefinitions,
                    contentCategoriesDetection
                ).parse();
                stringItem.optimiseStyles();
                final int lastIndex = this.stringItems.size();
                this.stringItems.add(stringItem);
                cell.value().updateFormer(
                    this.eventFactory.createCharacters(String.valueOf(lastIndex))
                );
            }
        }
    }
}