/*
 * =============================================================================
 * Copyright (C) 2010-2021 by the Okapi Framework contributors
 * -----------------------------------------------------------------------------
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =============================================================================
 */
package net.sf.okapi.filters.openxml;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventFactory;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.Set;

interface Cell {
    String NAME = "c";
    CellType type();
    String worksheetName();
    CellReferencesRange cellReferencesRange();
    boolean valuePresent();
    CellValue value();
    List<XMLEvent> inlineStringEvents();
    boolean excluded();
    void refineExcluded();
    MetadataContext metadataContext();
    Attribute styleAttribute();
    Cell copiedWithAdjusted(final String column, final Attribute attribute);
    void readWith(final XMLEventReader reader) throws XMLStreamException;
    Markup asMarkup();

    class Default implements Cell {
        private static final String INLINE_STRING = "is";
        private static final QName CELL_LOCATION_REFERENCE = new QName("r");
        private static final QName CELL_TYPE = new QName("t");
        private static final QName CELL_STYLE = new QName("s");
        private final ConditionalParameters conditionalParameters;
        private final XMLEventFactory eventFactory;
        private final SharedStringsFragments sharedStringsFragments;
        private final boolean date1904;
        private final StyleDefinitions styleDefinitions;
        private final Set<Integer> excludedRows;
        private final Set<String> excludedColumns;
        private final Set<Integer> metadataRows;
        private final Set<String> metadataColumns;
        private final List<CellReferencesRange> cellReferencesRanges;
        private final String worksheetName;
        private final StartElement origStartElement;
        private StartElement startElement;
        private CellType type;
        private CellReferencesRange cellReferencesRange;
        private Formula formula;
        private CellValue value;
        private List<XMLEvent> inlineStringEvents;
        private boolean excluded;
        private MetadataContext metadataContext;
        private EndElement endElement;

        Default(
            final ConditionalParameters conditionalParameters,
            final XMLEventFactory eventFactory,
            final SharedStringsFragments sharedStringsFragments,
            final boolean date1904,
            final StyleDefinitions styleDefinitions,
            final Set<Integer> excludedRows,
            final Set<String> excludedColumns,
            final Set<Integer> metadataRows,
            final Set<String> metadataColumns,
            final List<CellReferencesRange> cellReferencesRanges,
            final String worksheetName,
            final StartElement startElement
        ) {
            this.conditionalParameters = conditionalParameters;
            this.eventFactory = eventFactory;
            this.sharedStringsFragments = sharedStringsFragments;
            this.date1904 = date1904;
            this.styleDefinitions = styleDefinitions;
            this.excludedRows = excludedRows;
            this.excludedColumns = excludedColumns;
            this.metadataRows = metadataRows;
            this.metadataColumns = metadataColumns;
            this.cellReferencesRanges = cellReferencesRanges;
            this.worksheetName = worksheetName;
            this.origStartElement = startElement;
        }

        @Override
        public CellType type() {
            return this.type;
        }

        @Override
        public String worksheetName() { return this.worksheetName; }

        @Override
        public CellReferencesRange cellReferencesRange() {
            return this.cellReferencesRange;
        }

        @Override
        public boolean valuePresent() {
            return null != this.value;
        }

        @Override
        public CellValue value() {
            return this.value;
        }

        @Override
        public List<XMLEvent> inlineStringEvents() {
            return this.inlineStringEvents;
        }

        @Override
        public boolean excluded() {
            return this.excluded;
        }

        @Override
        public void refineExcluded() {
            if (!this.excluded) {
                this.excluded = excludedFor(combinedDifferentialFormatFor(this.startElement));
            }
        }

        @Override
        public MetadataContext metadataContext() {
            if (null == this.metadataContext) {
                boolean metadataRow = false;
                if (!this.metadataRows.isEmpty()) {
                    for (Integer r : this.cellReferencesRange.rows()) {
                        if (this.metadataRows.contains(r)) {
                            metadataRow = true;
                            break;
                        }
                    }
                }
                boolean metadataColumn = false;
                if (!this.metadataColumns.isEmpty()) {
                    for (String c : this.cellReferencesRange.columns()) {
                        if (this.metadataColumns.contains(c)) {
                            metadataColumn = true;
                            break;
                        }
                    }
                }

                if (metadataRow && metadataColumn) {
                    this.metadataContext = MetadataContext.ROW_AND_COLUMN;
                } else if (metadataRow) {
                    this.metadataContext = MetadataContext.ROW;
                } else if (metadataColumn) {
                    this.metadataContext = MetadataContext.COLUMN;
                } else {
                    this.metadataContext = MetadataContext.NONE;
                }
            }
            return this.metadataContext;
        }

        @Override
        public Attribute styleAttribute() {
            return  this.startElement.getAttributeByName(Cell.Default.CELL_STYLE);
        }

        @Override
        public Cell copiedWithAdjusted(final String column, final Attribute attribute) {
            final Attribute referenceAttr = this.startElement.getAttributeByName(CELL_LOCATION_REFERENCE);
            final CellReference cellReference = new CellReference(
                this.cellReferencesRange.first().row(),
                column
            );
            final CellReferencesRange cellReferencesRange = cellReferencesRangeFor(cellReference);
            final List<Attribute> attributes;
            if (Objects.isNull(attribute)) {
                attributes = new ArrayList<>(List.of(
                    this.eventFactory.createAttribute(referenceAttr.getName(), cellReference.toString())
                ));
            } else {
                attributes = new ArrayList<>(List.of(
                    this.eventFactory.createAttribute(referenceAttr.getName(), cellReference.toString()),
                    attribute
                ));
            }
            final StartElement startElement = newStartElementOf(this.startElement, attributes);
            final Cell.Default c = new Cell.Default(
                this.conditionalParameters,
                this.eventFactory,
                this.sharedStringsFragments,
                this.date1904,
                this.styleDefinitions,
                this.excludedRows,
                this.excludedColumns,
                this.metadataRows,
                this.metadataColumns,
                this.cellReferencesRanges,
                this.worksheetName,
                this.origStartElement
            );
            c.startElement = startElement;
            c.type = this.type;
            c.cellReferencesRange = cellReferencesRange;
            c.formula = this.formula;
            final DifferentialFormat.Combined format = combinedDifferentialFormatFor(startElement);
            if (valuePresent()) {
                c.value = this.value.copiedWithAdjusted(format);
            }
            c.inlineStringEvents = this.inlineStringEvents;
            c.excluded = !cellReferencesRange.partialMatch(this.excludedRows, this.excludedColumns)
                || excludedFor(format);
            c.metadataContext = this.metadataContext;
            c.endElement = this.endElement;
            return c;
        }

        @Override
        public void readWith(final XMLEventReader reader) throws XMLStreamException {
            final Attribute typeAttr = this.origStartElement.getAttributeByName(CELL_TYPE);
            this.type = CellType.from(typeAttr);
            if (CellType.INLINE_STRING == this.type) {
                this.startElement = newStartElementOf(
                    this.origStartElement,
                    new ArrayList<>(List.of(
                        this.eventFactory.createAttribute(typeAttr.getName(), CellType.SHARED_STRING.toString())
                    ))
                );
            } else {
                this.startElement = this.origStartElement;
            }
            this.cellReferencesRange = cellReferencesRangeFor(
                new CellReference(this.startElement.getAttributeByName(CELL_LOCATION_REFERENCE).getValue())
            );
            final DifferentialFormat.Combined format = combinedDifferentialFormatFor(this.startElement);
            while (reader.hasNext()) {
                final XMLEvent e = reader.nextEvent();
                if (e.isEndElement() && e.asEndElement().getName().equals(this.startElement.getName())) {
                    this.endElement = e.asEndElement();
                    break;
                }
                if (!e.isStartElement()) {
                    continue;
                }
                final StartElement se = e.asStartElement();
                if (Formula.F.equals(se.getName().getLocalPart())) {
                  this.formula = new Formula.Default(
                      se,
                      new MarkupComponent.Context.Default(this.worksheetName)
                  );
                  this.formula.readWith(reader);
                } else if (CellValue.NAME.equals(se.getName().getLocalPart())) {
                    if (!formulaPresent() && CellType.STRING.equals(this.type)) {
                        this.type = CellType.INLINE_STRING;
                        this.startElement = newStartElementOf(
                            this.startElement,
                            new ArrayList<>(List.of(
                                this.eventFactory.createAttribute(typeAttr.getName(), CellType.SHARED_STRING.toString())
                            ))
                        );
                        this.inlineStringEvents = convertedEventsFor(se, reader);
                        this.value = new CellValue.Default(
                            this.eventFactory,
                            this.date1904,
                            CellType.SHARED_STRING,
                            format,
                            se
                        );
                    } else {
                        this.value = new CellValue.Default(this.eventFactory, this.date1904, this.type, format, se);
                        this.value.readWith(reader);
                    }
                } else if (INLINE_STRING.equals(se.getName().getLocalPart())) {
                    this.inlineStringEvents = Default.innerEventsFor(se, reader);
                    this.value = new CellValue.Default(
                        this.eventFactory,
                        this.date1904,
                        CellType.SHARED_STRING,
                        format,
                        this.eventFactory.createStartElement(
                            se.getName().getPrefix(),
                            se.getName().getNamespaceURI(),
                            CellValue.NAME
                        )
                    );
                }
            }
            this.excluded = !this.cellReferencesRange.partialMatch(this.excludedRows, this.excludedColumns);
            if (CellType.INLINE_STRING != this.type && !this.excluded) {
                this.excluded = excludedFor(format);
            }
        }

        /**
         * Creates a new start element of a provided one with new attributes.
         * @param startElement The start element
         * @param newAttributes The new attributes (must be modifiable)
         * @return A new start element
         */
        private StartElement newStartElementOf(final StartElement startElement, final List<Attribute> newAttributes) {
            final List<Attribute> attributes = new ArrayList<>();
            final Iterator<Attribute> iterator = startElement.getAttributes();
            boolean added;
            while (iterator.hasNext()) {
                final Attribute a = iterator.next();
                added = false;
                if (!newAttributes.isEmpty()) {
                    final Iterator<Attribute> newAttributesIterator = newAttributes.iterator();
                    while (newAttributesIterator.hasNext()) {
                        final Attribute na = newAttributesIterator.next();
                        if (na.getName().equals(a.getName())) {
                            attributes.add(na);
                            newAttributesIterator.remove();
                            added = true;
                            break;
                        }
                    }
                }
                if (!added) {
                    attributes.add(a);
                }
            }
            return this.eventFactory.createStartElement(
                startElement.getName(),
                attributes.iterator(),
                startElement.getNamespaces()
            );
        }

        private CellReferencesRange cellReferencesRangeFor(final CellReference cellReference) {
            for (final CellReferencesRange r : this.cellReferencesRanges) {
                if (r.first().equals(cellReference)) {
                    return r;
                }
            }

            return new CellReferencesRange(cellReference);
        }

        private DifferentialFormat.Combined combinedDifferentialFormatFor(final StartElement startElement) {
            final Attribute styleAttr = startElement.getAttributeByName(CELL_STYLE);
            if (styleAttr == null) {
                return this.styleDefinitions.combinedDifferentialFormatFor(ExcelStyleDefinition.CellFormats.DEFAULT_INDEX);
            }
            final int styleIndex = Integer.parseUnsignedInt(styleAttr.getValue());
            return this.styleDefinitions.combinedDifferentialFormatFor(styleIndex);
        }

        private boolean excludedFor(final DifferentialFormat format) {
            for (final String c : this.conditionalParameters.tsExcelExcludedColors) {
                if ((format.fill().pattern().backgroundColor().value().matches(c)
                    || format.fill().pattern().foregroundColor().value().matches(c))) {
                        return true;
                }
                if (valuePresent() && !this.sharedStringsFragments.stringItemFormattedInlineAt(this.value.asInteger())) {
                    final RunProperty.ColorRunProperty crp = format.font().asRunProperties().getColorRunProperty();
                    if (null != crp && crp.asColorValue().matches(c)) {
                        return true;
                    }
                }
            }
            return false;
        }

        private boolean formulaPresent() {
            return null != formula;
        }

        private List<XMLEvent> convertedEventsFor(final StartElement startElement, final XMLEventReader reader) throws XMLStreamException {
            final List<XMLEvent> events = new ArrayList<>();
            events.add(
                this.eventFactory.createStartElement(
                    startElement.getName().getPrefix(),
                    startElement.getName().getNamespaceURI(),
                    Text.NAME
                )
            );
            events.addAll(innerEventsFor(startElement, reader));
            events.add(
                this.eventFactory.createEndElement(
                    startElement.getName().getPrefix(),
                    startElement.getName().getNamespaceURI(),
                    Text.NAME
                )
            );
            return events;
        }

        private static List<XMLEvent> innerEventsFor(final StartElement startElement, final XMLEventReader reader) throws XMLStreamException {
            final List<XMLEvent> events = new ArrayList<>();
            while (reader.hasNext()) {
                final XMLEvent e = reader.nextEvent();
                if (e.isEndElement() && e.asEndElement().getName().equals(startElement.getName())) {
                    break;
                }
                events.add(e);
            }
            return events;
        }

        @Override
        public Markup asMarkup() {
            final MarkupBuilder mb = new MarkupBuilder(
                new Markup.General(
                    new ArrayList<>(4)
                ),
                new ArrayList<>(2)
            );
            mb.add(this.startElement);
            if (formulaPresent()) {
                mb.add(this.formula);
            }
            if (valuePresent()) {
                mb.add(this.value.asMarkup());
            }
            mb.add(this.endElement);
            return mb.build();
        }
    }
}
