/*
 * =============================================================================
 * Copyright (C) 2010-2021 by the Okapi Framework contributors
 * -----------------------------------------------------------------------------
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =============================================================================
 */
package net.sf.okapi.filters.openxml;

import java.util.Objects;

final class CellReference {
    private static final String EMPTY = "";
    private static final int DEFAULT_ROW = 0;
    private static final String DEFAULT_COLUMN = EMPTY;
    private final String string;
    private int row;
    private String column;
    private boolean split;

    CellReference(final int row, final String column) {
        this(column + row, row, column);
    }

    CellReference(final String string) {
        this(string, DEFAULT_ROW, DEFAULT_COLUMN);
    }

    CellReference(final String string, final int row, final String column) {
        this.string = string;
        this.row = row;
        this.column = column;
    }

    int row() {
        if (!this.split) {
            split();
        }
        return this.row;
    }

    String column() {
        if (!this.split) {
            split();
        }
        return this.column;
    }

    private void split() {
        if (!EMPTY.equals(this.string)
            && (DEFAULT_ROW == this.row || DEFAULT_COLUMN.equals(this.column))) {
            char c;
            for (int j = 1; j < this.string.length(); j++) {
                c = this.string.charAt(j);
                if (Character.isDigit(c)) {
                    this.column = this.string.substring(0, j);
                    this.row = Integer.parseUnsignedInt(this.string.substring(j));
                    break;
                }
            }
        }
        this.split = false;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        final CellReference that = (CellReference) o;
        return string.equals(that.string);
    }

    @Override
    public int hashCode() {
        return Objects.hash(string);
    }

    @Override
    public String toString() {
        return this.string;
    }
}
