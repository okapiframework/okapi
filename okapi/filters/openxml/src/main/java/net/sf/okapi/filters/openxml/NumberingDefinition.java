/*
 * =============================================================================
 * Copyright (C) 2010-2023 by the Okapi Framework contributors
 * -----------------------------------------------------------------------------
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =============================================================================
 */
package net.sf.okapi.filters.openxml;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

interface NumberingDefinition {
    String id();
    List<XMLEvent> eventsBeforeNumberingLevels();
    Iterator<NumberingLevel> numberingLevelsIterator();
    List<XMLEvent> eventsAfterNumberingLevels();
    void readWith(final XMLEventReader reader) throws XMLStreamException;
    void referencedWith(final String numberingLevelId);
    boolean referenced();
    List<XMLEvent> asEvents();

    final class Abstract implements NumberingDefinition {
        static final String NAME = "abstractNum";
        private static final String ABSTRACT_NUM_ID = "abstractNumId";
        private final StartElement startElement;
        private final List<XMLEvent> eventsBeforeNumberingLevels;
        private final Map<String, NumberingLevel> numberingLevels;
        private final List<XMLEvent> eventsAfterNumberingLevels;
        private String id;
        private boolean referenced;

        Abstract(final StartElement startElement) {
            this(
                startElement,
                new LinkedList<>(),
                new LinkedHashMap<>(),
                new LinkedList<>()
            );
        }

        Abstract(
            final StartElement startElement,
            final List<XMLEvent> eventsBeforeNumberingLevels,
            final Map<String, NumberingLevel> numberingLevels,
            final List<XMLEvent> eventsAfterNumberingLevels
        ) {
            this.startElement = startElement;
            this.eventsBeforeNumberingLevels = eventsBeforeNumberingLevels;
            this.numberingLevels = numberingLevels;
            this.eventsAfterNumberingLevels = eventsAfterNumberingLevels;
        }

        @Override
        public String id() {
            if (null == id) {
                this.id = this.startElement.getAttributeByName(
                    new QName(
                        this.startElement.getName().getNamespaceURI(),
                        Abstract.ABSTRACT_NUM_ID,
                        this.startElement.getName().getPrefix()
                    )
                ).getValue();
            }
            return this.id;
        }

        @Override
        public List<XMLEvent> eventsBeforeNumberingLevels() {
            return this.eventsBeforeNumberingLevels;
        }

        @Override
        public Iterator<NumberingLevel> numberingLevelsIterator() {
            return this.numberingLevels.values().iterator();
        }

        @Override
        public List<XMLEvent> eventsAfterNumberingLevels() {
            return this.eventsAfterNumberingLevels;
        }

        @Override
        public void readWith(final XMLEventReader reader) throws XMLStreamException {
            boolean beforeNumberingLevels = true;
            this.eventsBeforeNumberingLevels.add(this.startElement);
            int defaultIndex = 0;
            while (reader.hasNext()) {
                final XMLEvent e = reader.nextEvent();
                if (e.isStartElement()) {
                    final StartElement el = e.asStartElement();
                    if (NumberingLevel.LVL_NAME.equals(el.getName().getLocalPart())) {
                        final NumberingLevel nl = new NumberingLevel.Default(el, defaultIndex);
                        nl.readWith(reader);
                        this.numberingLevels.put(nl.id(), nl);
                        defaultIndex++;
                        beforeNumberingLevels = false;
                    } else if (beforeNumberingLevels) {
                        this.eventsBeforeNumberingLevels.add(e);
                    } else {
                        this.eventsAfterNumberingLevels.add(e);
                    }
                } else if (e.isEndElement()) {
                    final EndElement el = e.asEndElement();
                    if (el.getName().equals(this.startElement.getName())) {
                        this.eventsAfterNumberingLevels.add(e);
                        break;
                    } else if (beforeNumberingLevels) {
                        this.eventsBeforeNumberingLevels.add(e);
                    } else {
                        this.eventsAfterNumberingLevels.add(e);
                    }
                }
            }
        }

        @Override
        public void referencedWith(final String numberingLevelId) {
            if (this.numberingLevels.containsKey(numberingLevelId)) {
                this.referenced = true;
                final NumberingLevel nl = this.numberingLevels.get(numberingLevelId);
                if (nl.numberingTextPresent()) {
                    nl.numberingText().markAsReferenced();
                }
            }
        }

        @Override
        public boolean referenced() {
            return this.referenced;
        }

        @Override
        public List<XMLEvent> asEvents() {
            final List<XMLEvent> events = new LinkedList<>();
            events.addAll(this.eventsBeforeNumberingLevels);
            final Iterator<NumberingLevel> nli = numberingLevelsIterator();
            while (nli.hasNext()) {
                events.addAll(nli.next().asEvents());
            }
            events.addAll(this.eventsAfterNumberingLevels);
            return events;
        }
    }

    final class Instance implements NumberingDefinition {
        static final String NAME = "num";
        private static final String NUM_ID = "numId";
        private static final String VAL = "val";
        private final StartElement startElement;
        private final Map<String, NumberingDefinition> abstractNumberingDefinitions;
        private final List<XMLEvent> abstractNumIdEvents;
        private final Map<String, NumberingLevel> numberingLevelOverrides;
        private EndElement endElement;
        private String id;
        private String abstractNumId;
        private boolean referenced;

        Instance(final StartElement startElement, final Map<String, NumberingDefinition> abstractNumberingDefinitions) {
            this(
                startElement,
                abstractNumberingDefinitions,
                new LinkedList<>(),
                new LinkedHashMap<>()
            );
        }

        Instance(
            final StartElement startElement,
            final Map<String, NumberingDefinition> abstractNumberingDefinitions,
            final List<XMLEvent> abstractNumIdEvents,
            final Map<String, NumberingLevel> numberingLevelOverrides
        ) {
            this.startElement = startElement;
            this.abstractNumberingDefinitions = abstractNumberingDefinitions;
            this.abstractNumIdEvents = abstractNumIdEvents;
            this.numberingLevelOverrides = numberingLevelOverrides;
        }

        @Override
        public String id() {
            if (null == id) {
                this.id = this.startElement.getAttributeByName(
                    new QName(
                        this.startElement.getName().getNamespaceURI(),
                        Instance.NUM_ID,
                        this.startElement.getName().getPrefix()
                    )
                ).getValue();
            }
            return this.id;
        }

        @Override
        public List<XMLEvent> eventsBeforeNumberingLevels() {
            final List<XMLEvent> events = new LinkedList<>();
            events.add(this.startElement);
            events.addAll(this.abstractNumIdEvents);
            return events;
        }

        @Override
        public Iterator<NumberingLevel> numberingLevelsIterator() {
            return this.numberingLevelOverrides.values().iterator();
        }

        @Override
        public List<XMLEvent> eventsAfterNumberingLevels() {
            return Collections.singletonList(this.endElement);
        }

        @Override
        public void readWith(final XMLEventReader reader) throws XMLStreamException {
            int defaultIndex = 0;
            while (reader.hasNext()) {
                final XMLEvent e = reader.nextEvent();
                if (e.isStartElement()) {
                    final StartElement el = e.asStartElement();
                    if (Abstract.ABSTRACT_NUM_ID.equals(el.getName().getLocalPart())) {
                        this.abstractNumIdEvents.add(e);
                        this.abstractNumIdEvents.add(reader.nextTag());
                        this.abstractNumId = el.getAttributeByName(
                            new QName(
                                el.getName().getNamespaceURI(),
                                Instance.VAL,
                                el.getName().getPrefix()
                            )
                        ).getValue();
                    } else if (NumberingLevel.LVL_OVERRIDE_NAME.equals(el.getName().getLocalPart())) {
                        final NumberingLevel nl = new NumberingLevel.Default(el, defaultIndex);
                        nl.readWith(reader);
                        this.numberingLevelOverrides.put(nl.id(), nl);
                        defaultIndex++;
                    }
                } else if (e.isEndElement()) {
                    final EndElement el = e.asEndElement();
                    if (this.startElement.getName().equals(el.getName())) {
                        this.endElement = e.asEndElement();
                        break;
                    }
                }
            }
        }

        @Override
        public void referencedWith(final String numberingLevelId) {
            if (this.numberingLevelOverrides.containsKey(numberingLevelId)) {
                this.referenced = true;
                final NumberingLevel nl = this.numberingLevelOverrides.get(numberingLevelId);
                if (nl.numberingTextPresent()) {
                    nl.numberingText().markAsReferenced();
                } else {
                    final NumberingDefinition and = this.abstractNumberingDefinitions.get(this.abstractNumId);
                    and.referencedWith(numberingLevelId);
                }
            } else {
                final NumberingDefinition and = this.abstractNumberingDefinitions.get(this.abstractNumId);
                and.referencedWith(numberingLevelId);
            }
        }

        @Override
        public boolean referenced() {
            return this.referenced;
        }

        @Override
        public List<XMLEvent> asEvents() {
            final List<XMLEvent> l = new LinkedList<>();
            l.add(this.startElement);
            l.addAll(this.abstractNumIdEvents);
            final Iterator<NumberingLevel> nli = numberingLevelsIterator();
            while (nli.hasNext()) {
                l.addAll(nli.next().asEvents());
            }
            l.add(this.endElement);
            return l;
        }
    }
}
