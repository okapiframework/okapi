/*===========================================================================
  Copyright (C) 2016-2017 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
===========================================================================*/

package net.sf.okapi.filters.openxml;

import static net.sf.okapi.filters.openxml.XMLEventHelpers.LOCAL_BREAK;
import static net.sf.okapi.filters.openxml.XMLEventHelpers.LOCAL_TAB;
import static net.sf.okapi.filters.openxml.XMLEventHelpers.createQName;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.List;
import java.util.Map;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventFactory;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.XMLEvent;

import net.sf.okapi.common.LocaleId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.sf.okapi.common.resource.Code;
import net.sf.okapi.common.resource.Segment;
import net.sf.okapi.common.resource.TextContainer;
import net.sf.okapi.common.resource.TextFragment;

class BlockTextUnitWriter implements TextUnitWriter {
	private static final String EMPTY = "";
	private final Logger LOGGER = LoggerFactory.getLogger(BlockTextUnitWriter.class);

	private final ConditionalParameters parameters;
	private final XMLEventFactory eventFactory;
	private final LocaleId locale;
	private final QName runName;
	private final QName textName;
	private final RunPropertiesPairWithDetectedRunFonts baseRunPropertiesPairWithDetectedRunFonts;
	private final List<XMLEvents> hiddenCodes;
	private final Map<Integer, XMLEvents> visibleCodes;
	private final AdditiveCollection<XMLEvent> xmlEvents;
	private final RunPropertiesClarification runPropertiesClarification;
	private final Deque<RunPropertiesPairWithDetectedRunFonts> currentRunPropertiesPairWithDetectedRunFonts;
	private StringBuilder textContent;
	private boolean runOpen;

	BlockTextUnitWriter(
		final ConditionalParameters parameters,
		final XMLEventFactory eventFactory,
		final LocaleId locale,
		final QName runName,
		final QName textName,
		final RunPropertiesPairWithDetectedRunFonts baseRunPropertiesPairWithDetectedRunFonts,
		final List<XMLEvents> hiddenCodes,
		final Map<Integer, XMLEvents> visibleCodes,
		final AdditiveCollection<XMLEvent> xmlEvents,
		final RunPropertiesClarification runPropertiesClarification
	) {
		this(
			parameters,
			eventFactory,
			locale,
			runName,
			textName,
			baseRunPropertiesPairWithDetectedRunFonts,
			hiddenCodes,
			visibleCodes,
			xmlEvents,
			runPropertiesClarification,
			new ArrayDeque<>()
		);
	}

	BlockTextUnitWriter(
		final ConditionalParameters parameters,
		final XMLEventFactory eventFactory,
		final LocaleId locale,
		final QName runName,
		final QName textName,
		final RunPropertiesPairWithDetectedRunFonts baseRunPropertiesPairWithDetectedRunFonts,
		final List<XMLEvents> hiddenCodes,
		final Map<Integer, XMLEvents> visibleCodes,
		final AdditiveCollection<XMLEvent> xmlEvents,
		final RunPropertiesClarification runPropertiesClarification,
		final Deque<RunPropertiesPairWithDetectedRunFonts> currentRunPropertiesPairWithDetectedRunFonts
	) {
		this.parameters = parameters;
		this.eventFactory = eventFactory;
		this.locale = locale;
		this.runName = runName;
		this.textName = textName;
		this.baseRunPropertiesPairWithDetectedRunFonts = baseRunPropertiesPairWithDetectedRunFonts;
		this.hiddenCodes = hiddenCodes;
		this.visibleCodes = visibleCodes;
		this.xmlEvents = xmlEvents;
		this.runPropertiesClarification = runPropertiesClarification;
		this.currentRunPropertiesPairWithDetectedRunFonts = currentRunPropertiesPairWithDetectedRunFonts;
	}

	public void write(TextContainer tc) {
		this.textContent = new StringBuilder();
		boolean firstSegmentWritten = false;

		for (Segment segment : tc.getSegments()) {
			if (!firstSegmentWritten) {
				writeFirstSegment(tc.getFirstSegment());
				firstSegmentWritten = true;
				continue;
			}
			writeSegment(segment);
		}
		flushText(true);
	}

	private void writeFirstSegment(final Segment segment) {
		for (final XMLEvents events : this.hiddenCodes) {
			this.xmlEvents.addAll(events.getEvents());
		}
		writeSegment(segment);
	}

	private void writeSegment(Segment segment) {
		try {
			TextFragment content = segment.getContent();
			String codedText = content.getCodedText();
			List<Code> codes = content.getCodes();
			for (int i = 0; i < codedText.length(); i++) {
				char c = codedText.charAt(i);
				if (TextFragment.isMarker(c)) {
					int codeIndex = TextFragment.toIndex(codedText.charAt(++i));
					writeCode(codes.get(codeIndex));
				}
				else {
					writeChar(c);
				}
			}
		}
		catch (Exception e) {
			LOGGER.error("Threw {} writing segment id {} '{}'", e.getClass().getSimpleName(),
						 segment.getId(), segment.toString());
			throw e;
		}
	}

	private void writeChar(char c) {
		textContent.append(c);
	}

	private void writeCode(Code code) {
		// is the Code really an escaped Okapi Marker?
        if (code.isMarkerMasking()) {
			String data = code.getData();
			for (int i = 0; i < data.length(); i++) {
				char c = data.charAt(i);
				writeChar(c);
			}
			return;
		}

		// Cases:
		// - Open
		//   - Terminate current run
		//   - Do something content-dependent:   
		//	    - If it's RunProperties, update run properties
		//	    - If it's a RunContainer, write opening tag
		// - Closed
		//   - Terminate current run
		//   - Handling this is actually optional in many cases
		//	 - If it's a RunContainer, write the closing tag
		// - Isolated
		//   - Terminate current run
		//   - Write out the corresponding markup (for Run, Run.RunMarkup, Block.BlockMarkup
		int id = code.getId();
		XMLEvents codeEvents = visibleCodes.get(id);
		switch (code.getTagType()) {
			case OPENING:
				flushText(true);
				if (codeEvents instanceof RunPropertiesPairWithDetectedRunFonts) {
					this.currentRunPropertiesPairWithDetectedRunFonts.push((RunPropertiesPairWithDetectedRunFonts) codeEvents);
				}
				else if (codeEvents instanceof RunContainer) {
					RunContainer rc = (RunContainer)codeEvents;
					this.xmlEvents.addAll(rc.startMarkupEvents());
					this.currentRunPropertiesPairWithDetectedRunFonts.push(rc.defaultRunPropertiesPairWithDetectedRunFonts());
				}
				else {
					throw new IllegalStateException("Unexpected code contents for opening code '" +
													code.toString() + "':" + codeEvents );
				}
				break;
			case PLACEHOLDER:
				// If this is RunMarkup (markup contained within a run), we should
				// keep the current run open.  Otherwise, close it.
				boolean isRunMarkup = (codeEvents instanceof Run.Markup);
				if (isRunMarkup) {
					flushText(false);
					flushRunStart();
				} else {
					flushText(true);
				}
				this.xmlEvents.addAll(codeEvents.getEvents());
				break;
			case CLOSING:
				flushText(true);
				if (codeEvents instanceof RunPropertiesPairWithDetectedRunFonts) {
					// XXX What if it's not on the top of the stack?  It's probably a corrupt target.
					this.currentRunPropertiesPairWithDetectedRunFonts.pop();
				}
				else if (codeEvents instanceof RunContainer) {
					RunContainer rc = (RunContainer) codeEvents;
					this.xmlEvents.addAll(rc.endMarkupEvents());
					this.currentRunPropertiesPairWithDetectedRunFonts.pop(); // Pop RunContainer properties
				}
				else {
					throw new IllegalStateException("Unexpected code contents for closing code '" +
													code.toString() + "':" + codeEvents );
				}

				break;
		}
	}

	private void flushRunStart() {
		if (!runOpen) {
			writeRunStart();
		}
	}

	private void flushRunEnd() {
		if (runOpen) {
			writeRunEnd();
		}
	}

	private void flushText(boolean terminateRun) {
		final RunPropertiesPairWithDetectedRunFonts rppwdrf = runPropertiesPairWithDetectedRunFonts();
		if (textContent.length() > 0) {
			final String text = textContent.toString();
			this.runPropertiesClarification.prepareContextWith(
				rppwdrf.combined(),
				rppwdrf.detectedRunFonts(),
				text
			);
			flushRunStart();
			writeRunText(text);
			textContent = new StringBuilder();
		} else {
			this.runPropertiesClarification.prepareContextWith(
				rppwdrf.combined(),
				rppwdrf.detectedRunFonts(),
				EMPTY
			);
		}
		if (terminateRun) {
			flushRunEnd();
		}
	}

	private RunPropertiesPairWithDetectedRunFonts runPropertiesPairWithDetectedRunFonts() {
		return this.currentRunPropertiesPairWithDetectedRunFonts.isEmpty()
			? this.baseRunPropertiesPairWithDetectedRunFonts
			: this.currentRunPropertiesPairWithDetectedRunFonts.peek();
	}

	private void writeRunStart() {
		final RunPropertiesPairWithDetectedRunFonts rpp = runPropertiesPairWithDetectedRunFonts();
		xmlEvents.add(eventFactory.createStartElement(runName, null, null));
		runPropertiesClarification.performFor(rpp.direct());
		xmlEvents.addAll(rpp.direct().getEvents());
		runOpen = true;
	}

	// Would be better to have a separate hierarcy for the MS Word BlockTextUnitWrite.java and for the Excel
	// BlockTextUnitWrite.java but...
	private void writeRunText(String text) {
		// MS Excel doesn't support the line breaks inside text run
		// We should save a content as is
		// Current implementation of ms excel text run has <t> without prefix
		// We are using this fact to catch an excel text runs
		if (textName.getPrefix().isEmpty()) {
			writeTextIfNeeded(new StringBuilder(text));
			return;
		}

		// MS Word text runs can contain the line breaks
		// The text run of ms word looks like <w:t>
		// The prefix "w" says us that is ms word text tun
		StringBuilder sb = new StringBuilder();
		for (char c : text.toCharArray()) {
			if (c == parameters.getLineSeparatorReplacement() && parameters.getAddLineSeparatorCharacter()) {
				writeTextIfNeeded(sb);
				writeLineBreakAt(sb.length());
				sb.setLength(0);
			} else if (c == '\t' && parameters.getAddTabAsCharacter()
					&& Namespace.PREFIX_W.equals(this.textName.getPrefix())) {
				writeTextIfNeeded(sb);
				sb.setLength(0);
				writeTab();
			} else {
				sb.append(c);
			}
		}
		writeTextIfNeeded(sb);
	}

	private void writeTextIfNeeded(StringBuilder buffer) {
		if (buffer.length() > 0) {
			flushRunStart();
			writeText(buffer.toString());
		}
	}

	private void writeTab() {
		QName br = createQName(LOCAL_TAB, textName);
		xmlEvents.add(eventFactory.createStartElement(br, null, null));
		xmlEvents.add(eventFactory.createEndElement(br, null));
	}

	private void writeLineBreakAt(final int position) {
		// Word and PowerPoint seems to always start a new run before the break element. Although
		// this is not enforced by specification we behave like Word. This prevents some strange
		// behaviour and broken documents.
		if (!this.runOpen) {
			writeRunStart();
		}
		if (Namespace.PREFIX_A.equals(this.textName.getPrefix())) {
			// The a:br element has to be on the same level as a:r.
			if (this.runOpen && 0 < position) {
				writeRunEnd();
				writeRunStart();
			}
			writeText("");
			writeRunEnd();
		} else {
			writeRunEnd();
			writeRunStart();
		}
		QName br = createQName(LOCAL_BREAK, textName);
		xmlEvents.add(eventFactory.createStartElement(br, null, null));
		if (Namespace.PREFIX_A.equals(this.textName.getPrefix())) {
			final RunPropertiesPairWithDetectedRunFonts rpp = runPropertiesPairWithDetectedRunFonts();
			runPropertiesClarification.performFor(rpp.direct());
			xmlEvents.addAll(rpp.direct().getEvents());
		}
		xmlEvents.add(eventFactory.createEndElement(br, null));
	}

	private void writeText(String text) {
		boolean needsPreserveSpace = needsXmlSpacePreserve(text);
		ArrayList<Attribute> attrs = new ArrayList<>();
		// DrawingML <a:t> does not use the xml:space="preserve" attribute
		if (needsPreserveSpace && !Namespaces.DrawingML.containsName(textName)) {
			attrs.add(eventFactory.createAttribute("xml", Namespaces.XML.getURI(), "space", "preserve"));
		}
		xmlEvents.add(eventFactory.createStartElement(textName, attrs.iterator(), null));
		xmlEvents.add(eventFactory.createCharacters(text));
		xmlEvents.add(eventFactory.createEndElement(textName, null));
	}

	private void writeRunEnd() {
		xmlEvents.add(eventFactory.createEndElement(runName, null));
		runOpen = false;
	}

	/**
	 * Returns true if the given text contains a space, tab or no-break space. In that case you
	 * have to add {@code xml:space="preserve"} to the {@code &lt;w:t&gt;} element.
	 *
	 * @param text text
	 * @return true if the given text contains a space, tab or no-break space
	 */
	static boolean needsXmlSpacePreserve(String text) {
		for (char c : text.toCharArray()) {
			// This catches things like ideographic space (U+3000).  NBSP
			// isn't flagged as whitespace in unicode, so we have to special-case it.
			if (Character.isWhitespace(c) || c == '\u00A0') return true;
		}
		return false;
	}
}
