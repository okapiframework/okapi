/*
 * =============================================================================
 * Copyright (C) 2010-2022 by the Okapi Framework contributors
 * -----------------------------------------------------------------------------
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =============================================================================
 */
package net.sf.okapi.filters.openxml;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.XMLEvent;
import java.util.ArrayList;

interface Theme {
    String rgbColorValueFor(final int index);
    void readWith(final XMLEventReader reader) throws XMLStreamException;
    Markup asMarkup();

    final class Empty implements Theme {
        private static final String EMPTY = "";

        @Override
        public String rgbColorValueFor(final int index) {
            return EMPTY;
        }

        @Override
        public void readWith(final XMLEventReader reader) throws XMLStreamException {
        }

        @Override
        public Markup asMarkup() {
            return new Markup.Empty();
        }
    }

    final class Default implements Theme {
        private final PresetColorValues presetColorValues;
        private ColorScheme colorScheme;
        private MarkupBuilder markupBuilder;

        Default(final PresetColorValues presetColorValues) {
            this.presetColorValues = presetColorValues;
        }

        @Override
        public String rgbColorValueFor(final int index) {
            return this.colorScheme().colorValueFor(index);
        }

        private ColorScheme colorScheme() {
            if (null == colorScheme) {
                this.colorScheme = new ColorScheme.Empty();
            }
            return this.colorScheme;
        }

        @Override
        public void readWith(final XMLEventReader reader) throws XMLStreamException {
            this.markupBuilder = new MarkupBuilder(new Markup.General(new ArrayList<>()));
            while (reader.hasNext()) {
                final XMLEvent e = reader.nextEvent();
                if (e.isStartElement() && ColorScheme.NAME.equals(e.asStartElement().getName().getLocalPart())) {
                    this.colorScheme = new ColorScheme.Default(
                        this.presetColorValues,
                        e.asStartElement()
                    );
                    this.colorScheme.readWith(reader);
                    this.markupBuilder.add(this.colorScheme.asMarkup());
                    continue;
                }
                this.markupBuilder.add(e);
            }
        }

        @Override
        public Markup asMarkup() {
            final Markup markup;
            if (null == this.markupBuilder) {
                markup = new Markup.Empty();
            } else {
                markup = this.markupBuilder.build();
            }
            return markup;
        }
    }
}
