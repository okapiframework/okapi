/*
 * =============================================================================
 * Copyright (C) 2010-2021 by the Okapi Framework contributors
 * -----------------------------------------------------------------------------
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =============================================================================
 */
package net.sf.okapi.filters.openxml;

import net.sf.okapi.common.filters.fontmappings.FontMappings;

import javax.xml.stream.XMLEventFactory;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.util.Collections;
import java.util.List;

interface Font extends Chunk {
    String NAME = "font";
    void readWith(final XMLEventReader reader) throws XMLStreamException;
    RunProperties asRunProperties();
    Markup asMarkup();

    final class Empty implements Font {
        @Override
        public void apply(final FontMappings fontMappings) {
        }

        @Override
        public List<XMLEvent> getEvents() {
            return Collections.emptyList();
        }

        @Override
        public void readWith(final XMLEventReader reader) throws XMLStreamException {
        }

        @Override
        public RunProperties asRunProperties() {
            return new RunProperties.Empty();
        }

        @Override
        public Markup asMarkup() {
            return new Markup.Empty();
        }
    }

    final class Default implements Font {
        private final ConditionalParameters conditionalParameters;
        private final XMLEventFactory eventFactory;
        private final PresetColorValues presetColorValues;
        private final PresetColorValues highlightColorValues;
        private final SystemColorValues systemColorValues;
        private final IndexedColors indexedColors;
        private final Theme theme;
        private final StartElement startElement;
        private RunProperties fontAsRunProperties;;

        Default(
            final ConditionalParameters conditionalParameters,
            final XMLEventFactory eventFactory,
            final PresetColorValues presetColorValues,
            final PresetColorValues highlightColorValues,
            final SystemColorValues systemColorValues,
            final IndexedColors indexedColors,
            final Theme theme,
            final StartElement startElement
        ) {
            this.conditionalParameters = conditionalParameters;
            this.eventFactory = eventFactory;
            this.presetColorValues = presetColorValues;
            this.highlightColorValues = highlightColorValues;
            this.systemColorValues = systemColorValues;
            this.indexedColors = indexedColors;
            this.theme = theme;
            this.startElement = startElement;
        }

        @Override
        public void apply(final FontMappings fontMappings) {
            this.fontAsRunProperties.apply(fontMappings);
        }

        @Override
        public List<XMLEvent> getEvents() {
            return this.fontAsRunProperties.getEvents();
        }

        @Override
        public void readWith(final XMLEventReader reader) throws XMLStreamException {
            final StartElementContext startElementContext = new StartElementContext(
                this.startElement,
                reader,
                this.presetColorValues,
                this.highlightColorValues,
                this.systemColorValues,
                this.indexedColors,
                this.theme,
                this.eventFactory,
                this.conditionalParameters
            );
            this.fontAsRunProperties = new RunPropertiesParser(
                startElementContext,
                new RunSkippableElements(startElementContext)
            ).parse();
        }

        @Override
        public RunProperties asRunProperties() {
            return this.fontAsRunProperties;
        }

        @Override
        public Markup asMarkup() {
            return new Markup.General(Collections.singletonList(this.fontAsRunProperties));
        }
    }
}
