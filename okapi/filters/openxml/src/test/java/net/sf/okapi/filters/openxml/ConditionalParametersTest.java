/*
 * =============================================================================
 * Copyright (C) 2010-2020 by the Okapi Framework contributors
 * -----------------------------------------------------------------------------
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =============================================================================
 */
package net.sf.okapi.filters.openxml;

import net.sf.okapi.common.filters.fontmappings.DefaultFontMapping;
import net.sf.okapi.common.filters.fontmappings.DefaultFontMappings;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.TreeSet;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(JUnit4.class)
public class ConditionalParametersTest {

    @Test
    public void testFromStringForTsComplexFieldDefinitionsToExtract() {
        String yaml =
            "#v1\n" +
                "tsComplexFieldDefinitionsToExtract.i=3\n" +
                "cfd0=HYPERLINK\n" +
                "cfd1=FORMTEXT\n" +
                "cfd2=TOC\n";
        ConditionalParameters params = new ConditionalParameters();
        params.fromString(yaml);
        HashSet<String> expected1 = new HashSet<>();
        expected1.add("HYPERLINK");
        expected1.add("FORMTEXT");
        expected1.add("TOC");
        assertEquals(expected1, params.tsComplexFieldDefinitionsToExtract);
    }

    @Test
    public void testToStringForTsComplexFieldDefinitionsToExtract() {
        ConditionalParameters params = new ConditionalParameters();
        params.tsComplexFieldDefinitionsToExtract.add("FORMTEXT");

        String yaml = params.toString();

        List<String> lines = Arrays.asList(yaml.split("\\\n"));
        assertTrue(lines.contains("tsComplexFieldDefinitionsToExtract.i=2"));
        assertTrue(lines.contains("cfd0=FORMTEXT"));
        assertTrue(lines.contains("cfd1=HYPERLINK"));
    }

    @Test
    public void testMigrateLegacyDefaultTranslatableField() {
        String yaml =
            "#v1\n" +
                "bPreferenceTranslateDocProperties.b=false\n" +
                "bPreferenceReorderDocProperties.b=false\n" +
                "bPreferenceTranslateComments.b=false\n" +
                "bPreferenceTranslatePowerpointNotes.b=true\n" +
                "bPreferenceTranslatePowerpointMasters.b=true\n" +
                "bPreferenceTranslateWordHeadersFooters.b=true\n" +
                "bPreferenceTranslateWordHidden.b=false\n" +
                "bPreferenceTranslateExcelExcludeColors.b=false\n" +
                "bPreferenceTranslateExcelExcludeColumns.b=true\n" +
                "tsExcelExcludedColors.i=0\n" +
                "tsExcelExcludedColumns.i=4\n" +
                "tsExcludeWordStyles.i=0\n" +
                "zzz0=1A\n" +
                "zzz1=1B\n" +
                "zzz2=3A\n" +
                "zzz3=3D\n";
        ConditionalParameters params = new ConditionalParameters();
        params.fromString(yaml);
        HashSet<String> expected1 = new HashSet<>();
        expected1.add("HYPERLINK");
        assertEquals(expected1, params.tsComplexFieldDefinitionsToExtract);
    }

    @Test
    public void testIncludedSlidesOnly() throws Exception {
        ConditionalParameters params = new ConditionalParameters();
        params.setPowerpointIncludedSlideNumbersOnly(true);
        params.tsPowerpointIncludedSlideNumbers = new TreeSet<>(Arrays.asList(1, 2, 3, 5, 7, 11));

        String paramsToString = params.toString();

        ConditionalParameters params2 = new ConditionalParameters();
        params2.fromString(paramsToString);

        assertThat(params2.getPowerpointIncludedSlideNumbersOnly()).isTrue();
        assertThat(params2.tsPowerpointIncludedSlideNumbers)
            .containsExactly(1, 2, 3, 5, 7, 11);
    }

    @Test
    public void defaultValuesExposedAsString() {
        Assert.assertEquals(
            "#v1\n" +
            "maxAttributeSize.i=4194304\n" +
            "bPreferenceTranslateDocProperties.b=true\n" +
            "translatePowerpointDocProperties.b=true\n" +
            "reorderPowerpointDocProperties.b=false\n" +
            "reorderPowerpointRelationships.b=false\n" +
            "translatePowerpointDiagramData.b=true\n" +
            "reorderPowerpointDiagramData.b=false\n" +
            "translatePowerpointCharts.b=true\n" +
            "reorderPowerpointCharts.b=false\n" +
            "bPreferenceTranslatePowerpointNotes.b=true\n" +
            "bPreferenceReorderPowerpointNotes.b=false\n" +
            "bPreferenceTranslateComments.b=true\n" +
            "translatePowerpointComments.b=true\n" +
            "reorderPowerpointComments.b=false\n" +
            "bPreferenceTranslatePowerpointMasters.b=true\n" +
            "bPreferenceIgnorePlaceholdersInPowerpointMasters.b=false\n" +
            "bPreferenceTranslateWordHeadersFooters.b=true\n" +
            "translateWordNumberingLevelText.b=false\n" +
            "bPreferenceTranslateWordHidden.b=false\n" +
            "bPreferenceTranslateWordExcludeGraphicMetaData.b=false\n" +
            "translatePowerpointGraphicMetadata.b=false\n" +
            "bPreferenceTranslatePowerpointHidden.b=false\n" +
            "bPreferenceTranslateExcelHidden.b=false\n" +
            "bPreferenceTranslateExcelExcludeColors.b=false\n" +
            "bPreferenceTranslateExcelSheetNames.b=false\n" +
            "translateExcelCellsCopied.b=true\n" +
            "bPreferenceAddLineSeparatorAsCharacter.b=false\n" +
            "sPreferenceLineSeparatorReplacement=$0a$\n" +
            "bPreferenceReplaceNoBreakHyphenTag.b=false\n" +
            "bPreferenceIgnoreSoftHyphenTag.b=false\n" +
            "bPreferenceAddTabAsCharacter.b=false\n" +
            "bPreferenceAggressiveCleanup.b=false\n" +
            "ignoreWhitespaceStyles.b=false\n" +
            "bPreferenceAutomaticallyAcceptRevisions.b=true\n" +
            "bExtractExternalHyperlinks.b=false\n" +
            "bPreferencePowerpointIncludedSlideNumbersOnly.b=false\n" +
            "bPreferenceTranslateExcelDiagramData.b=false\n" +
            "bPreferenceTranslateExcelDrawings.b=false\n" +
            "subfilter=\n" +
            "bInExcludeMode.b=true\n" +
            "bInExcludeHighlightMode.b=true\n" +
            "bPreferenceTranslateWordExcludeColors.b=false\n" +
            "bReorderPowerpointNotesAndComments.b=false\n" +
            "ignoreWordFontColors.b=false\n" +
            "allowWordStyleOptimisation.b=true\n" +
            "preserveExcelStylesInTargetColumns.b=false\n" +
            "bPreferenceAllowEmptyTargets.b=false\n" +
            "tsComplexFieldDefinitionsToExtract.i=1\n" +
            "cfd0=HYPERLINK\n" +
            "tsExcelExcludedColors.i=0\n" +
            "tsExcludeWordStyles.i=0\n" +
            "tsWordHighlightColors.i=0\n" +
            "tsWordExcludedColors.i=0\n" +
            "tsPowerpointIncludedSlideNumbers.i=0",
            new ConditionalParameters().toString()
        );
    }

    @Test
    public void fontMappingsExposedAsString() {
        final ConditionalParameters conditionalParameters = new ConditionalParameters();
        conditionalParameters.fontMappings(
            new DefaultFontMappings(
                new DefaultFontMapping(".*", ".*", "Times.*", "Arial")
            )
        );
        Assert.assertEquals(
            "#v1\n" +
            "maxAttributeSize.i=4194304\n" +
            "bPreferenceTranslateDocProperties.b=true\n" +
            "translatePowerpointDocProperties.b=true\n" +
            "reorderPowerpointDocProperties.b=false\n" +
            "reorderPowerpointRelationships.b=false\n" +
            "translatePowerpointDiagramData.b=true\n" +
            "reorderPowerpointDiagramData.b=false\n" +
            "translatePowerpointCharts.b=true\n" +
            "reorderPowerpointCharts.b=false\n" +
            "bPreferenceTranslatePowerpointNotes.b=true\n" +
            "bPreferenceReorderPowerpointNotes.b=false\n" +
            "bPreferenceTranslateComments.b=true\n" +
            "translatePowerpointComments.b=true\n" +
            "reorderPowerpointComments.b=false\n" +
            "bPreferenceTranslatePowerpointMasters.b=true\n" +
            "bPreferenceIgnorePlaceholdersInPowerpointMasters.b=false\n" +
            "bPreferenceTranslateWordHeadersFooters.b=true\n" +
            "translateWordNumberingLevelText.b=false\n" +
            "bPreferenceTranslateWordHidden.b=false\n" +
            "bPreferenceTranslateWordExcludeGraphicMetaData.b=false\n" +
            "translatePowerpointGraphicMetadata.b=false\n" +
            "bPreferenceTranslatePowerpointHidden.b=false\n" +
            "bPreferenceTranslateExcelHidden.b=false\n" +
            "bPreferenceTranslateExcelExcludeColors.b=false\n" +
            "bPreferenceTranslateExcelSheetNames.b=false\n" +
            "translateExcelCellsCopied.b=true\n" +
            "bPreferenceAddLineSeparatorAsCharacter.b=false\n" +
            "sPreferenceLineSeparatorReplacement=$0a$\n" +
            "bPreferenceReplaceNoBreakHyphenTag.b=false\n" +
            "bPreferenceIgnoreSoftHyphenTag.b=false\n" +
            "bPreferenceAddTabAsCharacter.b=false\n" +
            "bPreferenceAggressiveCleanup.b=false\n" +
            "ignoreWhitespaceStyles.b=false\n" +
            "bPreferenceAutomaticallyAcceptRevisions.b=true\n" +
            "bExtractExternalHyperlinks.b=false\n" +
            "bPreferencePowerpointIncludedSlideNumbersOnly.b=false\n" +
            "bPreferenceTranslateExcelDiagramData.b=false\n" +
            "bPreferenceTranslateExcelDrawings.b=false\n" +
            "subfilter=\n" +
            "bInExcludeMode.b=true\n" +
            "bInExcludeHighlightMode.b=true\n" +
            "bPreferenceTranslateWordExcludeColors.b=false\n" +
            "bReorderPowerpointNotesAndComments.b=false\n" +
            "ignoreWordFontColors.b=false\n" +
            "allowWordStyleOptimisation.b=true\n" +
            "preserveExcelStylesInTargetColumns.b=false\n" +
            "bPreferenceAllowEmptyTargets.b=false\n" +
            "tsComplexFieldDefinitionsToExtract.i=1\n" +
            "cfd0=HYPERLINK\n" +
            "tsExcelExcludedColors.i=0\n" +
            "tsExcludeWordStyles.i=0\n" +
            "tsWordHighlightColors.i=0\n" +
            "tsWordExcludedColors.i=0\n" +
            "tsPowerpointIncludedSlideNumbers.i=0\n" +
            "fontMappings.0.sourceLocalePattern=.*\n" +
            "fontMappings.0.targetLocalePattern=.*\n" +
            "fontMappings.0.sourceFontPattern=Times.*\n" +
            "fontMappings.0.targetFont=Arial\n" +
            "fontMappings.number.i=1",
            conditionalParameters.toString()
        );
    }
}
