package net.sf.okapi.filters.openxml;

import net.sf.okapi.common.FileLocation;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import javax.xml.stream.XMLEventFactory;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(JUnit4.class)
public class PresentationFragmentsTest {
	private final PartPath basePath;
	private final FileLocation root;
	private final XMLInputFactory inputFactory;
	private final XMLEventFactory eventFactory;
	private final PresetColorValues presetColorValues;
	private final PresetColorValues highlightColorValues;
	private final SystemColorValues systemColorValues;
	private final IndexedColors indexedColors;
	private final Theme theme;
	private PresentationFragments presentationFragments;

	public PresentationFragmentsTest() {
		this.basePath = new PartPath.Default("/ppt");
		this.root = FileLocation.fromClass(getClass());
		this.inputFactory = XMLInputFactory.newInstance();
		this.eventFactory = XMLEventFactory.newInstance();
		this.presetColorValues = new PresetColorValues.Default();
		this.highlightColorValues = new PresetColorValues.Default(Collections.emptyList());
		this.systemColorValues = new SystemColorValues.Default();
		this.indexedColors = new IndexedColors.Default(this.systemColorValues);
		this.theme = new Theme.Empty();
	}

	@Before
	public void setUp() throws XMLStreamException, IOException {
		final Relationships relationships = new Relationships.Default(
			this.eventFactory,
			new RelationshipIdGeneration.Default(),
			this.basePath
		);
		try (final Reader reader = readerFor("/presentation.xml.rels")) {
			relationships.readWith(this.inputFactory.createXMLEventReader(reader));
		}
		this.presentationFragments = new PresentationFragments.Default(
			new ConditionalParameters(),
			this.eventFactory,
            this.presetColorValues,
			this.highlightColorValues,
			this.systemColorValues,
			this.indexedColors,
			this.theme,
			relationships
		);
		try (final Reader reader = readerFor("/presentation.xml")) {
			this.presentationFragments.readWith(this.inputFactory.createXMLEventReader(reader));
		}
	}

	private Reader readerFor(final String resource) {
		InputStream input = root.in(resource).asInputStream();
		return new InputStreamReader(input, StandardCharsets.UTF_8);
	}

	@Test
	public void slideMasterNamesDetermined() {
		final List<String> slideMasterNames = this.presentationFragments.slideMasterNames();
		assertEquals(1, slideMasterNames.size());
		assertEquals("/ppt/slideMasters/slideMaster1.xml", slideMasterNames.get(0));
	}

	@Test
	public void notesMasterNamesDetermined() {
		final List<String> notesMasterNames = this.presentationFragments.notesMasterNames();
		assertEquals(1, notesMasterNames.size());
		assertEquals("/ppt/notesMasters/notesMaster1.xml", notesMasterNames.get(0));
	}

	@Test
	public void slideNamesDetermined() {
		final List<String> slideNames = this.presentationFragments.slideNames();
		assertEquals(4, slideNames.size());
		assertEquals("/ppt/slides/slide1.xml", slideNames.get(0));
		assertEquals("/ppt/slides/slide2.xml", slideNames.get(1));
		assertEquals("/ppt/slides/slide3.xml", slideNames.get(2));
		assertEquals("/ppt/slides/slide4.xml", slideNames.get(3));
	}

	@Test
	public void defaultTextStyleDetermined() {
		final StyleDefinitions styleDefinitions = this.presentationFragments.defaultTextStyle();
		assertNotNull(styleDefinitions);
	}
}
