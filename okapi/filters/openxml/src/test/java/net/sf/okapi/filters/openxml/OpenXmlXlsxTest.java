package net.sf.okapi.filters.openxml;

import net.sf.okapi.common.Event;
import net.sf.okapi.common.FileLocation;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.annotation.XLIFFContextGroup;
import net.sf.okapi.common.exceptions.OkapiBadFilterParametersException;
import net.sf.okapi.common.exceptions.OkapiUnexpectedRevisionException;
import net.sf.okapi.common.filters.FilterTestDriver;
import net.sf.okapi.common.resource.INameable;
import net.sf.okapi.common.resource.ITextUnit;
import net.sf.okapi.common.resource.RawDocument;
import org.assertj.core.api.Assertions;
import org.assertj.core.api.iterable.Extractor;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import static net.sf.okapi.filters.openxml.OpenXMLTestHelpers.textUnitSourceExtractor;
import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author jpmaas
 * @since 17.08.2017
 */
@RunWith(JUnit4.class)
public class OpenXmlXlsxTest {

    private final LocaleId locENUS = LocaleId.fromString("en-US");
	private FileLocation root;

	@Before
	public void setUp() {
		root = FileLocation.fromClass(getClass());
	}

    @Test
    public void worksheetRowsAndColumnsIdentificationClarified() throws Exception {
        eventsFor("/1325-rows-and-columns-identification.xlsx", new ConditionalParameters());
    }

    @Test
    public void testTextFields() throws Exception {
        ConditionalParameters parameters = new ConditionalParameters();
        parameters.setTranslateExcelDrawings(true);
        parameters.setTranslateDocProperties(false);
        List<Event> actual = eventsFor("/textfield.xlsx", parameters);
        List<ITextUnit> textUnits = FilterTestDriver.filterTextUnits(actual);
        assertThat(textUnits).extracting((Extractor<ITextUnit, Object>) input -> input.getSource().toString()).containsExactly(
                "Hallo Welt!",
                "Ich bin ein Textfeld!");
    }

    @Test
    public void testExcelWorksheetTransUnitProperty() throws Exception {
        ConditionalParameters parameters = new ConditionalParameters();
        parameters.setTranslateExcelDrawings(true);
        parameters.setTranslateDocProperties(false);
        List<Event> actual = eventsFor("/textfield.xlsx", parameters);
        List<ITextUnit> textUnits = FilterTestDriver.filterTextUnits(actual);
        assertThat(textUnits).extracting((Extractor<ITextUnit, Object>) input -> input.hasProperty(
            ExcelWorksheetTransUnitProperty.CELL_REFERENCE.getKeyName())).containsExactly(
            true,
            false);
        assertThat(textUnits).extracting((Extractor<ITextUnit, Object>) input -> String.valueOf(input.getProperty(
            ExcelWorksheetTransUnitProperty.CELL_REFERENCE.getKeyName()))).containsExactly(
            "A1",
            "null");
        assertThat(textUnits).extracting((Extractor<ITextUnit, Object>) input -> String.valueOf(input.getProperty(
            ExcelWorksheetTransUnitProperty.SHEET_NAME.getKeyName()))).containsExactly(
            "Tabelle1",
            "null");
        assertThat(textUnits).extracting((Extractor<ITextUnit, Object>) INameable::getName).containsExactly(
            "Tabelle1!A1",
            null);
    }

    @Test
    public void testSmartArt() throws Exception {
        ConditionalParameters parameters = new ConditionalParameters();
        parameters.setTranslateExcelDiagramData(true);
        parameters.setTranslateDocProperties(false);
        List<Event> actual = eventsFor("/smartart.xlsx", parameters);
        List<ITextUnit> textUnits = FilterTestDriver.filterTextUnits(actual);
        assertThat(textUnits).extracting((Extractor<ITextUnit, Object>) input -> input.getSource().toString()).containsExactly(
            "Hallo Welt!", "Ich", "bin", "ein", "Smart", "Art"
        );
    }

    @Test
    public void testSmartArtHidden() throws Exception {
        ConditionalParameters parameters = new ConditionalParameters();
        parameters.setTranslateExcelDiagramData(true);
        parameters.setTranslateDocProperties(false);
        parameters.setTranslateExcelHidden(false);
        List<Event> actual = eventsFor("/SmartArt3Sheets.xlsx", parameters);
        List<ITextUnit> textUnits = FilterTestDriver.filterTextUnits(actual);
        assertThat(textUnits).extracting((Extractor<ITextUnit, Object>) input -> input.getSource().toString()).containsExactly(
            "Zelle 1", "Zelle 3", "Smart Art 1", "Smart Art 3"
        );
    }

    @Test
    public void testTextFieldsHidden() throws Exception {
        ConditionalParameters parameters = new ConditionalParameters();
        parameters.setTranslateExcelDrawings(true);
        parameters.setTranslateDocProperties(false);
        parameters.setTranslateExcelHidden(false);
        List<Event> actual = eventsFor("/Textfeld3Sheets.xlsx", parameters);
        List<ITextUnit> textUnits = FilterTestDriver.filterTextUnits(actual);
        assertThat(textUnits).extracting((Extractor<ITextUnit, Object>) input -> input.getSource().toString()).containsExactly(
                "Zelle 1",
                "Zelle 3",
                "Textfeld 1",
                "Textfeld 3");
    }

    @Test
    public void testSheetNamesHiddenExclude() throws Exception {
        ConditionalParameters parameters = new ConditionalParameters();
        parameters.setTranslateExcelSheetNames(true);
        List<Event> actual = eventsFor("/SheetNameHidden.xlsx", parameters);
        List<ITextUnit> textUnits = FilterTestDriver.filterTextUnits(actual);
        assertThat(textUnits).extracting((Extractor<ITextUnit, Object>) input -> input.getSource().toString()).containsExactly(
            "Cell Visible",
            "Sheet Visible"
        );
    }

    @Test
    public void testSheetNamesHiddenInclude() throws Exception {
        ConditionalParameters parameters = new ConditionalParameters();
        parameters.setTranslateExcelSheetNames(true);
        parameters.setTranslateExcelHidden(true);
        List<Event> actual = eventsFor("/SheetNameHidden.xlsx", parameters);
        List<ITextUnit> textUnits = FilterTestDriver.filterTextUnits(actual);
        assertThat(textUnits).extracting((Extractor<ITextUnit, Object>) input -> input.getSource().toString()).containsExactly(
            "Cell Visible",
            "Cell Hidden",
            "Sheet Visible",
            "Sheet Hidden"
        );
    }

    @Test
    public void groupsOfWorksheetsAndRowsExtracted() throws Exception {
        final List<Event> actual = eventsFor("/1059.xlsx", new ConditionalParameters());
        Assertions.assertThat(actual.size()).isEqualTo(33);
        Assertions.assertThat(actual.get(6).isStartGroup()).isTrue();
        Assertions.assertThat(actual.get(6).getStartGroup().getName()).isEqualTo("2");
        Assertions.assertThat(actual.get(7).isTextUnit()).isTrue();
        Assertions.assertThat(actual.get(7).isTextUnit()).isTrue();
        Assertions.assertThat(actual.get(9).isTextUnit()).isTrue();
        Assertions.assertThat(actual.get(10).isTextUnit()).isTrue();
        Assertions.assertThat(actual.get(11).isEndGroup()).isTrue();
        Assertions.assertThat(actual.get(11).getEndGroup().getId()).isEqualTo(actual.get(6).getStartGroup().getId());
        Assertions.assertThat(actual.get(18).isEndGroup()).isTrue();
        Assertions.assertThat(actual.get(18).getEndGroup().getId()).isEqualTo(actual.get(5).getStartGroup().getId());
    }

    @Test
    public void testFormattings() throws Exception {
        ConditionalParameters params = new ConditionalParameters();
        params.setTranslateDocProperties(false);
        params.setTranslatePowerpointMasters(false);
        List<Event> events = eventsFor("/Formattings.xlsx", params);

        List<ITextUnit> textUnits = FilterTestDriver.filterTextUnits(events);
        assertThat(textUnits).extracting(textUnitSourceExtractor()).containsExactlyInAnyOrder(
                "This is a <run1>bold formatting</run1>",
                "This is an <run1>italics formatting</run1>",
                "This is an <run1>underlined formatting</run1>",
                "This is a hyperlink"
        );

        assertThat(
                textUnits.get(0).getSource().getParts().get(0).getContent().getCodes()
        ).hasSize(2).extracting("type").containsExactly(
            "x-bold;color:FFFFFF;fonts:Calibri;",
            "x-bold;color:FFFFFF;fonts:Calibri;"
        );
        assertThat(
                textUnits.get(1).getSource().getParts().get(0).getContent().getCodes()
        ).hasSize(2).extracting("type").containsExactly(
            "x-italic;color:FFFFFF;fonts:Calibri;",
            "x-italic;color:FFFFFF;fonts:Calibri;"
        );
        assertThat(
                textUnits.get(2).getSource().getParts().get(0).getContent().getCodes()
        ).hasSize(2).extracting("type").containsExactly(
            "x-underline:single;color:FFFFFF;fonts:Calibri;",
            "x-underline:single;color:FFFFFF;fonts:Calibri;"
        );
        assertThat(
                textUnits.get(3).getSource().getParts().get(0).getContent().getCodes()
        ).hasSize(0);
    }

    @Test
    public void fontsInfoExtracted() throws Exception {
        ConditionalParameters params = new ConditionalParameters();
        params.setTranslatePowerpointDocProperties(false);
        List<Event> events = eventsFor("/1312-fonts-info.xlsx", params);
        List<ITextUnit> textUnits = FilterTestDriver.filterTextUnits(events);
        assertThat(textUnits).extracting(textUnitSourceExtractor()).containsExactlyInAnyOrder(
            "Fonts <run1>code info</run1>",
            "User"
        );
        assertThat(
            textUnits.get(0).getSource().getParts().get(0).getContent().getCodes()
        ).hasSize(2).extracting("type").containsExactly(
            "x-color:FFFFFF;fonts:Times New Roman;",
            "x-color:FFFFFF;fonts:Times New Roman;"
        );
    }

    @Test
    public void maxWidthAndSizeUnitPropertiesSpecified() throws Exception {
        final ConditionalParameters params = new ConditionalParameters();
        params.worksheetConfigurations(
            new WorksheetConfigurations.Default(
                new WorksheetConfiguration.Default(
                    ".*",
                    Collections.emptyList(),
                    Arrays.asList("B", "C"),
                    Arrays.asList("5", "5"),
                    Collections.emptyList(),
                    Collections.emptyList(),
                    Collections.emptyList(),
                    Collections.emptyList()
                )
            )
        );
        List<Event> events = eventsFor("/1386-styled-and-plain-strings.xlsx", params);
        List<ITextUnit> textUnits = FilterTestDriver.filterTextUnits(events);
        assertThat(textUnits).extracting(textUnitSourceExtractor()).containsExactly(
            "Text 1", "B1",
            "Text 2", "C2",
            "B3",
            "User"
        );
        assertThat(textUnits.get(0).hasProperty("maxwidth")).isFalse();
        assertThat(textUnits.get(0).hasProperty("size-unit")).isFalse();
        assertThat(textUnits.get(1).getProperty("maxwidth").getValue()).isEqualTo("5");
        assertThat(textUnits.get(1).getProperty("size-unit").getValue()).isEqualTo("char");
        assertThat(textUnits.get(2).hasProperty("maxwidth")).isFalse();
        assertThat(textUnits.get(0).hasProperty("size-unit")).isFalse();
        assertThat(textUnits.get(3).getProperty("maxwidth").getValue()).isEqualTo("5");
        assertThat(textUnits.get(3).getProperty("size-unit").getValue()).isEqualTo("char");
        assertThat(textUnits.get(4).getProperty("maxwidth").getValue()).isEqualTo("5");
        assertThat(textUnits.get(4).getProperty("size-unit").getValue()).isEqualTo("char");
    }

    @Test
    public void sourceColumnsIdentifiedAndExtractedAsTargetColumns() throws Exception {
        final ConditionalParameters params = new ConditionalParameters();
        params.worksheetConfigurations(
            new WorksheetConfigurations.Default(
                new WorksheetConfiguration.Default(
                    ".*",
                    Collections.singletonList("A"),
                    Arrays.asList("B", "C"),
                    Collections.emptyList(),
                    Collections.emptyList(),
                    Collections.emptyList(),
                    Collections.emptyList(),
                    Collections.emptyList()
                )
            )
        );
        List<Event> events = eventsFor("/1334-source-and-target-columns.xlsx", params);
        List<ITextUnit> textUnits = FilterTestDriver.filterTextUnits(events);
        assertThat(textUnits).extracting(textUnitSourceExtractor()).containsExactly(
            "<run1>Formatted</run1> text", "c1",
            "c2",
            "Text", "c3",
            "User"
        );
        params.worksheetConfigurations(
            new WorksheetConfigurations.Default(
                new WorksheetConfiguration.Default(
                    ".*",
                    Arrays.asList("C", "B"),
                    Collections.singletonList("A"),
                    Collections.emptyList(),
                    Collections.emptyList(),
                    Collections.emptyList(),
                    Collections.emptyList(),
                    Collections.emptyList()
                )
            )
        );
        events = eventsFor("/1334-source-and-target-columns.xlsx", params);
        textUnits = FilterTestDriver.filterTextUnits(events);
        assertThat(textUnits).extracting(textUnitSourceExtractor()).containsExactly(
            "c1", "B1",
            "c2", "B2",
            "c3", "B3",
            "User"
        );
        params.setTranslateExcelCellsCopied(false);
        params.worksheetConfigurations(
            new WorksheetConfigurations.Default(
                new WorksheetConfiguration.Default(
                    ".*",
                    Arrays.asList("C", "B"),
                    Collections.singletonList("A"),
                    Collections.emptyList(),
                    Collections.emptyList(),
                    Collections.emptyList(),
                    Collections.emptyList(),
                    Collections.emptyList()
                )
            )
        );
        events = eventsFor("/1334-source-and-target-columns.xlsx", params);
        textUnits = FilterTestDriver.filterTextUnits(events);
        assertThat(textUnits).extracting(textUnitSourceExtractor()).containsExactly(
            "c1", "B1",
            "c2", "B2",
            "c3", "B3",
            "User"
        );
    }

    @Test
    public void rowsExcluded() throws Exception {
        final ConditionalParameters params = new ConditionalParameters();
        params.worksheetConfigurations(
            new WorksheetConfigurations.Default(
                new WorksheetConfiguration.Default(
                    ".*",
                    Collections.emptySet(),
                    Collections.emptySet(),
                    Collections.emptyList(),
                    Collections.singleton(2),
                    Collections.emptySet(),
                    Collections.emptySet(),
                    Collections.emptySet()
                )
            )
        );;
        List<Event> events = eventsFor("/1060.xlsx", params);
        List<ITextUnit> textUnits = FilterTestDriver.filterTextUnits(events);
        assertThat(textUnits).extracting(textUnitSourceExtractor()).containsExactlyInAnyOrder(
            "A3", "B3", "common", "D4",
            "User"
        );
        params.worksheetConfigurations(
            new WorksheetConfigurations.Default(
                new WorksheetConfiguration.Default(
                    ".*",
                    Collections.singleton("C"),
                    Collections.singleton("A"),
                    Collections.emptyList(),
                    Collections.singleton(2),
                    Collections.emptySet(),
                    Collections.emptySet(),
                    Collections.emptySet()
                )
            )
        );;
        events = eventsFor("/1060.xlsx", params);
        textUnits = FilterTestDriver.filterTextUnits(events);
        assertThat(textUnits).extracting(textUnitSourceExtractor()).containsExactlyInAnyOrder(
            "common", "B3", "D4",
            "User"
        );
    }

    @Test
    public void columnsExcluded() throws Exception {
        final ConditionalParameters params = new ConditionalParameters();
        params.worksheetConfigurations(
            new WorksheetConfigurations.Default(
                new WorksheetConfiguration.Default(
                    ".*",
                    Collections.emptyList(),
                    Collections.emptyList(),
                    Collections.emptyList(),
                    Collections.emptyList(),
                    Arrays.asList("B", "C", "D"),
                    Collections.emptyList(),
                    Collections.emptyList()
                )
            )
        );;
        List<Event> events = eventsFor("/1060.xlsx", params);
        List<ITextUnit> textUnits = FilterTestDriver.filterTextUnits(events);
        assertThat(textUnits).extracting(textUnitSourceExtractor()).containsExactlyInAnyOrder(
            "A2",
            "A3",
            "User"
        );
        params.worksheetConfigurations(
            new WorksheetConfigurations.Default(
                new WorksheetConfiguration.Default(
                    ".*",
                    Collections.singletonList("A"),
                    Collections.singletonList("B"),
                    Collections.emptyList(),
                    Collections.emptyList(),
                    Arrays.asList("B", "C", "D"),
                    Collections.emptyList(),
                    Collections.emptyList()
                )
            )
        );;
        events = eventsFor("/1060.xlsx", params);
        textUnits = FilterTestDriver.filterTextUnits(events);
        assertThat(textUnits).extracting(textUnitSourceExtractor()).containsExactlyInAnyOrder(
            "User"
        );
    }

    @Test
    public void rowsAndColumnsExcluded() throws Exception {
        final ConditionalParameters params = new ConditionalParameters();
        params.worksheetConfigurations(
            new WorksheetConfigurations.Default(
                new WorksheetConfiguration.Default(
                    ".*",
                    Collections.emptyList(),
                    Collections.emptyList(),
                    Collections.emptyList(),
                    Collections.singletonList(2),
                    Arrays.asList("B", "C", "D"),
                    Collections.emptyList(),
                    Collections.emptyList()
                )
            )
        );;
        final List<Event> events = eventsFor("/1060.xlsx", params);
        final List<ITextUnit> textUnits = FilterTestDriver.filterTextUnits(events);
        assertThat(textUnits).extracting(textUnitSourceExtractor()).containsExactlyInAnyOrder(
            "A3",
            "User"
        );
    }

    @Test
    public void metadataMarked() throws Exception {
        final ConditionalParameters params = new ConditionalParameters();
        params.worksheetConfigurations(
            new WorksheetConfigurations.Default(
                new WorksheetConfiguration.Default(
                    ".*",
                    Collections.emptyList(),
                    Collections.emptyList(),
                    Collections.emptyList(),
                    Collections.emptyList(),
                    Collections.emptyList(),
                    Arrays.asList(1, 2),
                    Arrays.asList("D", "E")
                )
            )
        );
        final List<Event> events = eventsFor("/1062-1.xlsx", params);
        assertThat(events.get(5).getStartGroup().getAnnotations().isEmpty()).isTrue();

        assertThat(events.get(17).getStartGroup().getAnnotations().isEmpty()).isFalse();
        XLIFFContextGroup group = events.get(17).getStartGroup().getAnnotation(XLIFFContextGroup.class);
        assertThat(group.name()).isEqualTo("row-metadata");
        Iterator<XLIFFContextGroup.Context> contextsIterator = group.iterator();
        XLIFFContextGroup.Context context = contextsIterator.next();
        assertThat(context.type()).isEqualTo("Metadata Header D1;Metadata Header D2");
        assertThat(context.value()).isEqualTo("Metadata D3");
        context = contextsIterator.next();
        assertThat(context.type()).isEqualTo("Metadata Header D1;Metadata Header E2");
        assertThat(context.value()).isEqualTo("Metadata E3");

        assertThat(events.get(44).getStartGroup().getAnnotations().isEmpty()).isFalse();
        group = events.get(44).getStartGroup().getAnnotation(XLIFFContextGroup.class);
        assertThat(group.name()).isEqualTo("row-metadata");
        contextsIterator = group.iterator();
        context = contextsIterator.next();
        assertThat(context.type()).isEqualTo("Metadata Header D1;Metadata Header D2");
        assertThat(context.value()).isEqualTo("Metadata D3");
        context = contextsIterator.next();
        assertThat(context.type()).isEqualTo("Metadata Header D1;Metadata Header E2");
        assertThat(context.value()).isEqualTo("Metadata E3");

        final List<ITextUnit> textUnits = FilterTestDriver.filterTextUnits(events);
        assertThat(textUnits).extracting(textUnitSourceExtractor()).containsExactlyInAnyOrder(
            "A3", "B3", "C3",
            "A4", "B4", "C4",
            "A3", "B3", "C3",
            "A4", "B4", "C4",
            "User"
        );
    }

    @Test
    public void mergedCellsAsMetadataMarked() throws Exception {
        final ConditionalParameters params = new ConditionalParameters();
        params.worksheetConfigurations(
            new WorksheetConfigurations.Default(
                new WorksheetConfiguration.Default(
                    "Sheet2",
                    Collections.emptyList(),
                    Collections.emptyList(),
                    Collections.emptyList(),
                    Collections.emptyList(),
                    Collections.emptyList(),
                    Arrays.asList(3, 4, 5),
                    Arrays.asList("A", "B")
                )
            )
        );
        List<Event> events = eventsFor("/1062-2.xlsx", params);
        assertThat(events.get(49).getStartGroup().getAnnotations().isEmpty()).isFalse();
        XLIFFContextGroup group = events.get(49).getStartGroup().getAnnotation(XLIFFContextGroup.class);
        Iterator<XLIFFContextGroup.Context> contextsIterator = group.iterator();
        XLIFFContextGroup.Context context = contextsIterator.next();
        assertThat(context.type()).isEqualTo("A3;B3;A4;B4;B5:C5");
        assertThat(context.value()).isEqualTo("A6:B7");

        params.worksheetConfigurations(
            new WorksheetConfigurations.Default(
                new WorksheetConfiguration.Default(
                    "Sheet2",
                    Collections.emptyList(),
                    Collections.emptyList(),
                    Collections.emptyList(),
                    Collections.emptyList(),
                    Collections.emptyList(),
                    Arrays.asList(3, 4, 5),
                    Arrays.asList("C", "D", "E", "F")
                )
            )
        );
        events = eventsFor("/1062-2.xlsx", params);
        assertThat(events.get(49).getStartGroup().getAnnotations().isEmpty()).isFalse();
        group = events.get(49).getStartGroup().getAnnotation(XLIFFContextGroup.class);
        contextsIterator = group.iterator();
        context = contextsIterator.next();
        assertThat(context.type()).isEqualTo("C3:D4;E4:E5;B5:C5;D5");
        assertThat(context.value()).isEqualTo("C6:E6");
        context = contextsIterator.next();
        assertThat(context.type()).isEqualTo("Hidden F3:H5");
        assertThat(context.value()).isEqualTo("F6:G6");

        params.worksheetConfigurations(
            new WorksheetConfigurations.Default(
                new WorksheetConfiguration.Default(
                    "Sheet2",
                    Collections.emptyList(),
                    Collections.emptyList(),
                    Collections.emptyList(),
                    Collections.emptyList(),
                    Collections.emptyList(),
                    Arrays.asList(3, 4, 5),
                    Arrays.asList("E", "F")
                )
            )
        );
        events = eventsFor("/1062-2.xlsx", params);
        assertThat(events.get(49).getStartGroup().getAnnotations().isEmpty()).isFalse();
        group = events.get(49).getStartGroup().getAnnotation(XLIFFContextGroup.class);
        contextsIterator = group.iterator();
        context = contextsIterator.next();
        assertThat(context.type()).isEqualTo("E4:E5");
        assertThat(context.value()).isEqualTo("C6:E6");
        context = contextsIterator.next();
        assertThat(context.type()).isEqualTo("Hidden F3:H5");
        assertThat(context.value()).isEqualTo("F6:G6");

        params.worksheetConfigurations(
            new WorksheetConfigurations.Default(
                new WorksheetConfiguration.Default(
                    "Sheet2",
                    Collections.emptyList(),
                    Collections.emptyList(),
                    Collections.emptyList(),
                    Collections.emptyList(),
                    Collections.emptyList(),
                    Collections.emptyList(),
                    Arrays.asList("C", "D", "E", "F")
                )
            )
        );
        events = eventsFor("/1062-2.xlsx", params);
        assertThat(events.get(45).getStartGroup().getAnnotations().isEmpty()).isFalse();
        group = events.get(45).getStartGroup().getAnnotation(XLIFFContextGroup.class);
        contextsIterator = group.iterator();
        context = contextsIterator.next();
        assertThat(context.type()).isEqualTo("C");
        assertThat(context.value()).isEqualTo("B5:C5");

        assertThat(events.get(49).getStartGroup().getAnnotations().isEmpty()).isFalse();
        group = events.get(49).getStartGroup().getAnnotation(XLIFFContextGroup.class);
        contextsIterator = group.iterator();
        context = contextsIterator.next();
        assertThat(context.type()).isEqualTo("C;D;E");
        assertThat(context.value()).isEqualTo("C6:E6");
        context = contextsIterator.next();
        assertThat(context.type()).isEqualTo("F");
        assertThat(context.value()).isEqualTo("F6:G6");
    }

    @Test
    public void booleansAndNumbersExtractedAsMetadata() throws Exception {
        final ConditionalParameters params = new ConditionalParameters();
        params.worksheetConfigurations(
            new WorksheetConfigurations.Default(
                new WorksheetConfiguration.Default(
                    ".*",
                    Collections.emptyList(),
                    Collections.emptyList(),
                    Collections.emptyList(),
                    Collections.emptyList(),
                    Collections.emptyList(),
                    Arrays.asList(1, 2),
                    Arrays.asList("B", "C", "D", "E")
                )
            )
        );
        List<Event> events = eventsFor("/1096.xlsx", params);
        assertThat(events.get(10).getStartGroup().getAnnotations().isEmpty()).isFalse();
        XLIFFContextGroup group = events.get(10).getStartGroup().getAnnotation(XLIFFContextGroup.class);
        Iterator<XLIFFContextGroup.Context> contextsIterator = group.iterator();
        XLIFFContextGroup.Context context = contextsIterator.next();
        assertThat(context.type()).isEqualTo("111;true");
        assertThat(context.value()).isEqualTo("44461");
        context = contextsIterator.next();
        assertThat(context.type()).isEqualTo("100111;09-23-21");
        assertThat(context.value()).isEqualTo("true");
        context = contextsIterator.next();
        assertThat(context.type()).isEqualTo("100111;Metadata Header D2");
        assertThat(context.value()).isEqualTo("30001");
        context = contextsIterator.next();
        assertThat(context.type()).isEqualTo("100111;9/24/21 1:01");
        assertThat(context.value()).isEqualTo("3000144461");

        group = events.get(13).getStartGroup().getAnnotation(XLIFFContextGroup.class);
        contextsIterator = group.iterator();
        context = contextsIterator.next();
        assertThat(context.value()).isEqualTo("9/22/21 0:00");
        context = contextsIterator.next();
        assertThat(context.value()).isEqualTo("false");
        context = contextsIterator.next();
        assertThat(context.value()).isEqualTo("Metadata D4");
        context = contextsIterator.next();
        assertThat(context.value()).isEqualTo("400001");

        group = events.get(17).getStartGroup().getAnnotation(XLIFFContextGroup.class);
        contextsIterator = group.iterator();
        context = contextsIterator.next();
        assertThat(context.value()).isEqualTo("false");
        context = contextsIterator.next();
        assertThat(context.value()).isEqualTo("true");
        context = contextsIterator.next();
        assertThat(context.value()).isEqualTo("9/23/21 6:00");
        context = contextsIterator.next();
        assertThat(context.value()).isEqualTo("400006");
    }

    @Test
    public void inlineStringsExtracted() throws Exception {
        final ConditionalParameters parameters = new ConditionalParameters();
        List<Event> actual = eventsFor("/982-1.xlsx", parameters);
        List<ITextUnit> textUnits = FilterTestDriver.filterTextUnits(actual);
        assertThat(textUnits).extracting(input -> input.getSource().toString()).containsExactly(
            "Direct formatting with <run1>Times New Roman font</run1>",
            "Rich text inline with <run1>Times New Roman</run1>",
            "User",
            "<run1>User:</run1>\nA comment with <run2>Times New Roman font</run2>"
        );
        parameters.setTranslateExcelCellsCopied(false);
        actual = eventsFor("/982-2.xlsx", parameters);
        textUnits = FilterTestDriver.filterTextUnits(actual);
        assertThat(textUnits).extracting(input -> input.getSource().toString()).containsExactly(
            "Direct formatting with <run1>Times New Roman font</run1>",
            "Rich text inline with <run1>Times New Roman</run1>",
            "User",
            "<run1>User:</run1>\nA comment with <run2>Times New Roman font</run2>"
        );
    }

    @Test
    public void valuesFromCellsOfStringTypeWithEmptyFormulasTreatedAsInlineStrings() throws Exception {
        final ConditionalParameters parameters = new ConditionalParameters();
        final List<Event> actual = eventsFor("/1116.xlsx", parameters);
        final List<ITextUnit> textUnits = FilterTestDriver.filterTextUnits(actual);
        assertThat(textUnits).extracting((Extractor<ITextUnit, Object>) input -> input.getSource().toString()).containsExactly(
            "hello", "world"
        );
    }

    @Test
    public void excelDocumentRevisionsAcceptedWithAllReviewed() throws Exception {
        final ConditionalParameters parameters = new ConditionalParameters();
        parameters.setAutomaticallyAcceptRevisions(false);
        eventsFor("/983-1a.xlsx", parameters);
        eventsFor("/983-2a.xlsx", parameters);
    }

    @Test(expected = OkapiUnexpectedRevisionException.class)
    public void excelDocumentRevisionsNotAcceptedWithNotAllReviewed() throws Exception {
        final ConditionalParameters parameters = new ConditionalParameters();
        parameters.setAutomaticallyAcceptRevisions(false);
        eventsFor("/983-3.xlsx", parameters);
    }


    @Test
    public void colorExclusionConsideredForThemes() throws Exception {
        final ConditionalParameters cp = new ConditionalParameters();
        cp.setTranslateExcelExcludeColors(true);
        cp.tsExcelExcludedColors.addAll(Arrays.asList(
            "FF0000", "C0504D", // exclude "red background", "red background *theme"
            "00B050", "9BBB59", // exclude "green background", "green background *theme"
            "0070C0", "4F81BD"  // exclude "blue background", "blue background *theme"
        ));
        final List<Event> events = eventsFor("/1154.xlsx", cp);
        final List<ITextUnit> textUnits = FilterTestDriver.filterTextUnits(events);
        Assertions.assertThat(textUnits).extracting(OpenXMLTestHelpers.textUnitSourceExtractor()).containsExactly(
            "User"
        );
    }

    @Test
    public void benchmarkXLSX() throws Exception {
        List<Event> events = eventsFor("/large.xlsx", new ConditionalParameters());
        Assertions.assertThat(events).isNotEmpty();
    }

    @Test
    public void sameCellDataNotCopied() throws Exception {
        final ConditionalParameters cp = new ConditionalParameters();
        cp.setTranslateExcelCellsCopied(false);
        final List<Event> events = eventsFor("/1333-same-strings-in-different-cells.xlsx", cp);
        final List<ITextUnit> textUnits = FilterTestDriver.filterTextUnits(events);
        Assertions.assertThat(textUnits).extracting(OpenXMLTestHelpers.textUnitSourceExtractor()).containsExactly(
            "<run1>Formatted</run1> text",
            "Text",
            "User"
        );
    }

    @Test
    public void tintedColorsHandlingClarified() throws Exception {
        eventsFor("/1378-color-with-tint.xlsx", new ConditionalParameters());
    }

    private List<Event> eventsFor(final String path, final ConditionalParameters params) throws Exception {
        final RawDocument doc = new RawDocument(
            root.in(path).asUrl().toURI(),
            StandardCharsets.UTF_8.name(),
            locENUS
        );
        return FilterTestDriver.getEvents(new OpenXMLFilter(), doc, params);
    }
}
