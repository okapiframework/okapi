/*===========================================================================
  Copyright (C) 2013 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
===========================================================================*/
package net.sf.okapi.filters.openxml;

import static org.junit.Assert.assertEquals;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import javax.xml.stream.XMLEventFactory;
import javax.xml.stream.XMLInputFactory;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import net.sf.okapi.common.FileLocation;

@RunWith(JUnit4.class)
public class TestContentTypes {

	private FileLocation root;

	@Before
	public void setUp() {
		root = FileLocation.fromClass(getClass());
	}

	@Test
	public void testRels() throws Exception {
		final InputStream input = root.in("/Content_Types.xml").asInputStream();
		final ContentTypes ct = new ContentTypes.Default(XMLEventFactory.newInstance());
		try (final Reader reader = new InputStreamReader(input, OpenXMLFilter.ENCODING)) {
			ct.readWith(XMLInputFactory.newInstance().createXMLEventReader(reader));
		}
		// Test defaults
		assertEquals("application/vnd.openxmlformats-package.relationships+xml", 
					 ct.with("/a/b.xml.rels").iterator().next().value());
		assertEquals("application/xml", ct.with("/a/b.xml").iterator().next().value());

		// Test overrides
		assertEquals("application/vnd.openxmlformats-officedocument.presentationml.slideLayout+xml",
					 ct.with("/ppt/slideLayouts/slideLayout1.xml").iterator().next().value());
		assertEquals("application/vnd.openxmlformats-officedocument.presentationml.slideLayout+xml",
				 ct.with("/ppt/slideLayouts/slideLayout2.xml").iterator().next().value());
		assertEquals("application/vnd.openxmlformats-officedocument.presentationml.slideLayout+xml",
				 ct.with("/ppt/slideLayouts/slideLayout3.xml").iterator().next().value());
		assertEquals("application/vnd.openxmlformats-officedocument.presentationml.slideLayout+xml",
				 ct.with("/ppt/slideLayouts/slideLayout4.xml").iterator().next().value());
		assertEquals("application/vnd.openxmlformats-officedocument.presentationml.slideLayout+xml",
				 ct.with("/ppt/slideLayouts/slideLayout5.xml").iterator().next().value());
		assertEquals("application/vnd.openxmlformats-officedocument.presentationml.slideLayout+xml",
				 ct.with("/ppt/slideLayouts/slideLayout6.xml").iterator().next().value());
		
		assertEquals("application/vnd.openxmlformats-officedocument.presentationml.notesSlide+xml",
					 ct.with("/ppt/notesSlides/notesSlide1.xml").iterator().next().value());
		assertEquals("application/vnd.openxmlformats-officedocument.presentationml.notesSlide+xml",
				 ct.with("/ppt/notesSlides/notesSlide2.xml").iterator().next().value());
		assertEquals("application/vnd.openxmlformats-officedocument.presentationml.notesSlide+xml",
				 ct.with("/ppt/notesSlides/notesSlide3.xml").iterator().next().value());
		assertEquals("application/vnd.openxmlformats-officedocument.presentationml.notesSlide+xml",
				 ct.with("/ppt/notesSlides/notesSlide4.xml").iterator().next().value());
		
		assertEquals("application/vnd.openxmlformats-officedocument.presentationml.slideMaster+xml",
				ct.with("/ppt/slideMasters/slideMaster1.xml").iterator().next().value());

		assertEquals("application/vnd.openxmlformats-officedocument.presentationml.slide+xml",
				ct.with("/ppt/slides/slide1.xml").iterator().next().value());
		assertEquals("application/vnd.openxmlformats-officedocument.presentationml.slide+xml",
				ct.with("/ppt/slides/slide2.xml").iterator().next().value());
		assertEquals("application/vnd.openxmlformats-officedocument.presentationml.slide+xml",
				ct.with("/ppt/slides/slide3.xml").iterator().next().value());
		assertEquals("application/vnd.openxmlformats-officedocument.presentationml.slide+xml",
				ct.with("/ppt/slides/slide4.xml").iterator().next().value());
		assertEquals("application/vnd.openxmlformats-officedocument.presentationml.tableStyles+xml",
				ct.with("/ppt/tableStyles.xml").iterator().next().value());

		// Try a couple without the leading slash
		assertEquals("application/vnd.openxmlformats-officedocument.presentationml.presentation.main+xml",
					 ct.with("ppt/presentation.xml").iterator().next().value());
		assertEquals("application/vnd.openxmlformats-officedocument.presentationml.presProps+xml",
					 ct.with("ppt/presProps.xml").iterator().next().value());
		assertEquals("application/vnd.openxmlformats-officedocument.theme+xml",
					 ct.with("ppt/theme/theme1.xml").iterator().next().value());
		assertEquals("application/vnd.openxmlformats-officedocument.theme+xml",
				 ct.with("ppt/theme/theme2.xml").iterator().next().value());
		assertEquals("application/vnd.openxmlformats-officedocument.theme+xml",
				 ct.with("ppt/theme/theme3.xml").iterator().next().value());

	}
}
