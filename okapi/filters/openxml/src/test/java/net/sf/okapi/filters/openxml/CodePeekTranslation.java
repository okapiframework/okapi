/*===========================================================================
  Copyright (C) 2009 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
===========================================================================*/
package net.sf.okapi.filters.openxml;

import java.util.List;
import java.util.Set;

import net.sf.okapi.common.Event;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.encoder.EncoderContext;
import net.sf.okapi.common.resource.Code;
import net.sf.okapi.common.resource.ITextUnit;
import net.sf.okapi.common.resource.TextContainer;
import net.sf.okapi.common.resource.TextFragment;

import net.sf.okapi.common.skeleton.GenericSkeletonWriter;
import org.slf4j.Logger;

/**
 * Implements ITranslator and modifies text to be translated to
 * show the tags the translator will see while translating.  This
 * is used in OpenXMLRoundTripTest, so the tags are shown in the
 * orignal file format.
 */
final class CodePeekTranslation extends GenericSkeletonWriter implements Translation {
	  // extends GenericSkeletonWriter because expandCodeContent is protected
	private static final String CONS="BCDFGHJKLMNPQRSTVWXYZbcdfghjklmnpqrstvwxyz";
	private static final String NUM="0123456789";
	private static final String wordon="<w:r><w:t>";
	private static final String wordoff="</w:t></w:r>";
	private static final String lbrac="{";
	private static final String rbrac="}";
	private static final LocaleId locENUS = LocaleId.fromString("en-us");

	private final Translation.Copy copyTranslation;
	private final ParseType parseType;
	private final Logger logger;

	CodePeekTranslation(
		final Translation.Copy copyTranslation,
		final ParseType parseType,
		final Logger logger
	) {
		this.copyTranslation = copyTranslation;
		this.parseType = parseType;
		this.logger = logger;
	}

	@Override
	public Event applyTo(final Event event) {
		final Event e = this.copyTranslation.applyTo(event);
		final ITextUnit tu = event.getTextUnit();
		final Set<LocaleId> locales = tu.getTargetLocales();
		locales.forEach(l -> {
			final TextContainer tc = tu.getTarget(l);
			final TextFragment tf = applyTo(tc.getFirstContent());
			tc.setContent(tf);
		});
		return e;
	}

	@Override
	public TextFragment applyTo(final TextFragment textFragment) {
		final TextFragment tf = this.copyTranslation.applyTo(textFragment);
		String s = tf.getCodedText();
		String rslt=s,ss="",slow,sss;
		int i,j,k,len,codenum;
		char carrot;
		int nSurroundingCodes=0; // DWH 4-8-09
		Code code;
		char ch;
		try
		{
			len = s.length();
			List<Code> codes = tf.getCodes();
			if (len>1)
			{
				for(i=0;i<len;i++)
				{
					ch = s.charAt(i);
					code = null;
					switch ( ch )
					{
						case TextFragment.MARKER_OPENING:
							sss = s.substring(i,i+2);
							codenum = TextFragment.toIndex(s.charAt(++i));
							code = codes.get(codenum);
							ss += sss + lbrac + "g" + codenum + ":" + eggspand(code) + rbrac;
							nSurroundingCodes++;
							break;
						case TextFragment.MARKER_CLOSING:
							sss = s.substring(i,i+2);
							codenum = TextFragment.toIndex(s.charAt(++i));
							code = codes.get(codenum);
							ss += lbrac + "/g" + codenum + ":" + eggspand(code) + rbrac + sss;
							nSurroundingCodes--;
							break;
						case TextFragment.MARKER_ISOLATED:
							sss = s.substring(i,i+2);
							codenum = TextFragment.toIndex(s.charAt(++i));
							code = codes.get(codenum);
							if (code.getTagType()==TextFragment.TagType.OPENING)
								nSurroundingCodes++;
							else if (code.getTagType()==TextFragment.TagType.CLOSING)
								nSurroundingCodes--;
							if (nSurroundingCodes>0)
							{
								if (ParseType.MSWORD == this.parseType)
								{
									ss += sss + "lbrac + x" + codenum + ":" + eggspand(code) + rbrac;
								}
								else
									ss += sss;								
							}
							else
							{
								if (ParseType.MSWORD == this.parseType)
								{
									if (code.getTagType()==TextFragment.TagType.OPENING)
										ss += wordon + lbrac + "x" + codenum + ":" + eggspand(code) + rbrac + wordoff + sss;
									else if (code.getTagType()==TextFragment.TagType.OPENING)
										ss += sss + wordon + lbrac + "x" + codenum + ":" + eggspand(code) + rbrac + wordoff;
									else
										ss += sss + lbrac + "x" + codenum  + ":" + eggspand(code)+ rbrac;
								}
								else
									ss += sss;
							}
							break;
//TODO: Does it need to be implemented with new TextContainer?
//						case TextFragment.MARKER_SEGMENT:
//							sss = s.substring(i,i+2);
//							codenum = TextFragment.toIndex(s.charAt(++i));
//							code = codes.get(codenum);
//							if (code.getTagType()==TextFragment.TagType.OPENING)
//								nSurroundingCodes++;
//							else if (code.getTagType()==TextFragment.TagType.CLOSING)
//								nSurroundingCodes--;
//							ss += sss /* + lbrac + "y" + codenum + rbrac */;
//							break;
					}
					if (code!=null)
						continue;
					if (i+2<len && s.substring(i,i+3).equals("---"))
					{
						ss += "---";
						i += 2;
					}
					else if (i+1<len && s.substring(i,i+2).equals("--"))
					{
						ss += "--";
						i += 1;				
					}
					else
					{
						j = hominyOf(s.substring(i),NUM);
						if (j>0)
						{
							ss += s.substring(i,i+j);
							i += j-1;
							continue;
						}
						j = hominyOf(s.substring(i),CONS);
						if (j>0)
						{
							k = hominyLetters(s.substring(i+j));
							slow = s.substring(i,i+j).toLowerCase();
							if (k > -1)
							{
								ss += s.substring(i+j,i+j+k);
								i += k;
							}
							ss += slow+"ay";
							i += j-1;
							continue;
						}
						else
						{
							k = hominyLetters(s.substring(i));
							if (k>0)
							{
								ss += s.substring(i,i+k)+"hay";
								i += k-1;
							}
							else
							{
								carrot = s.charAt(i);
								if (carrot=='&') // DWH 4-21-09 handle entities
								{
									k = s.indexOf(';', i);
									if (k>=0 && (k-i<=5 || (k-i<=7 && s.charAt(i+1)=='#')))
										// entity: leave it alone
									{
										ss += s.substring(i,k+1);
										i += k-i;
									}
									else
										ss += carrot;
								}
								else if (TextFragment.isMarker(carrot))
								{
									ss += s.substring(i,i+2);
									i++;
								}
								else
									ss += carrot;
							}
						}
					}			
				}
				rslt = ss;
			}
		}
		catch(Throwable e)
		{
			this.logger.warn("Tag Translator failed on {}", s);
		}
		tf.setCodedText(rslt);
		return tf;
	}

	private int hominyOf(String s, String of)
	{
		int i=0,len=s.length();
		char carrot;
		for(i=0;i<len;i++)
		{
			carrot = s.charAt(i);
			if (of.indexOf(carrot) < 0)
				break;
		}
		return i;
	}
	private int hominyLetters(String s)
	{
		int i=0,len=s.length();
		char carrot;
		for(i=0;i<len;i++)
		{
			carrot = s.charAt(i);
			if (!Character.isLetter(carrot) && carrot!='\'')
				break;
		}
		return i;		
	}
	private String eggspand(Code code)
	{
		String s,ss="";
		int len;
		char carrot;
		s = expandCodeContent(code, locENUS, EncoderContext.SKELETON);
		len = s.length();
		for(int i=0; i<len; i++)
		{
			carrot = s.charAt(i);
//			if (carrot=='<')
//				ss += "&lt;";
//			else
				ss += carrot;
		}
		return ss;
	}
}
