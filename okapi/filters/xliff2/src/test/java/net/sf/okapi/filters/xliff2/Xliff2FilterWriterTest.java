/*===========================================================================
  Copyright (C) 2024 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
===========================================================================*/

package net.sf.okapi.filters.xliff2;

import net.sf.okapi.common.Event;
import net.sf.okapi.common.FileLocation;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.filters.FilterTestDriver;
import net.sf.okapi.common.filterwriter.IFilterWriter;
import net.sf.okapi.common.filterwriter.XLIFFWriterParameters;
import net.sf.okapi.common.resource.RawDocument;
import net.sf.okapi.filters.html.HtmlFilter;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.xmlunit.builder.Input;
import org.xmlunit.matchers.CompareMatcher;

import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;

@RunWith(JUnit4.class)
public class Xliff2FilterWriterTest {
    private HtmlFilter filter;
    private XLIFF2FilterWriter writer;
    private FileLocation fl;
    private final LocaleId locEN = LocaleId.fromString("en");
    private final LocaleId locFR = LocaleId.fromString("fr");

    @Before
    public void setUp() {
        filter = new HtmlFilter();
        fl = FileLocation.fromClass(this.getClass());
    }

    @Test
    public void testWriteHTMLAsXliff2() throws Exception {
        RawDocument rd = new RawDocument(fl.in("/burlington_ufo_center.html").asUri(), "UTF-8", LocaleId.ENGLISH);
        rd.setTargetLocale(LocaleId.FRENCH);
        List<Event> events = FilterTestDriver.getEvents(filter, rd, null);

        Path temp = Files.createTempFile("okapi~xliff2~2", ".xlf");
        try (IFilterWriter w = new XLIFF2FilterWriter(new Parameters(), filter.getEncoderManager());
             OutputStream os = Files.newOutputStream(temp, StandardOpenOption.CREATE)) {
            w.setOptions(LocaleId.FRENCH, "UTF-8");
            w.getParameters().setBoolean(XLIFFWriterParameters.COPYSOURCE, true);
            w.setOutput(os);
            events.forEach(w::handleEvent);
        }
        try (Reader g = new InputStreamReader(fl.in("/gold/burlington_ufo_center.html.xlf").asInputStream(), StandardCharsets.UTF_8);
             Reader o = Files.newBufferedReader(temp, StandardCharsets.UTF_8)) {
            assertThat(Input.fromReader(g), CompareMatcher.isIdenticalTo(Input.fromReader(o))
                    .ignoreComments().ignoreWhitespace());
        } finally {
            Files.delete(temp);
            rd.close();
        }
    }
}
