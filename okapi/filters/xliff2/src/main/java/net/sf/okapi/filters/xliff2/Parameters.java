/*===========================================================================
  Copyright (C) 2014 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
============================================================================*/

package net.sf.okapi.filters.xliff2;

import net.sf.okapi.common.EditorFor;
import net.sf.okapi.common.ParametersDescription;
import net.sf.okapi.common.StringParameters;
import net.sf.okapi.common.filters.InlineCodeFinder;
import net.sf.okapi.common.uidescription.CheckboxPart;
import net.sf.okapi.common.uidescription.CodeFinderPart;
import net.sf.okapi.common.uidescription.EditorDescription;
import net.sf.okapi.common.uidescription.IEditorDescriptionProvider;

@EditorFor(Parameters.class)
public class Parameters extends StringParameters implements IEditorDescriptionProvider {

	public static final String MAXVALIDATION = "maxValidation";
	private static final String USECODEFINDER = "useCodeFinder";
	private static final String CODEFINDERRULES = "codeFinderRules";
	private static final String SIMPLIFY_TAGS = "simplifyTags";
	private static final String NEEDS_SEGMENTATION = "needsSegmentation";
	private static final String FORCE_UNIQUE_IDS = "forceUniqueIds";
	private static final String IGNORE_TAG_TYPE_MATCH = "ignoreTagTypeMatch";
	private static final String DISCARD_INVALID_TAGS = "discardInvalidTargets";
	private static final String WRITE_ORIGINAL_DATA = "writeOriginalData";
	private static final String SUBFILTER = "subfilter";
	private static final String SUBFILTER_OVERWRITE_TARGET = "subfilterOverwriteTarget";


	private InlineCodeFinder codeFinder; // Initialized in reset()

	public Parameters () {
		super();
	}

	public boolean getMaxValidation () {
		return getBoolean(MAXVALIDATION);
	}

	public void setMaxValidation (boolean maxValidation) {
		setBoolean(MAXVALIDATION, maxValidation);
	}

	public boolean getUseCodeFinder () {
		return getBoolean(USECODEFINDER);
	}

	public void setUseCodeFinder (boolean useCodeFinder) {
		setBoolean(USECODEFINDER, useCodeFinder);
	}

	public InlineCodeFinder getCodeFinder () {
		return codeFinder;
	}
	
	public String getCodeFinderRules () {
		return codeFinder.toString();
	}

	public void setCodeFinderRules (String data) {
		codeFinder.fromString(data);
	}

	public boolean getSimplifyTags () {
		return getBoolean(SIMPLIFY_TAGS);
	}
	
	public void setSimplifyTags (boolean simplifyTags) {
		setBoolean(SIMPLIFY_TAGS, simplifyTags);
	}
	
	public boolean getNeedsSegmentation() {
		return getBoolean(NEEDS_SEGMENTATION);
	}
	
	public void setNeedsSegmentation(boolean needsSegmentation) {
		setBoolean(NEEDS_SEGMENTATION, needsSegmentation);
	}

	public boolean getForceUniqueIds() {
		return getBoolean(FORCE_UNIQUE_IDS);
	}

	public void setForceUniqueIds(boolean forceUniqueIds) {
		setBoolean(FORCE_UNIQUE_IDS, forceUniqueIds);
	}

	public boolean getDiscardInvalidTargets() {
		return getBoolean(DISCARD_INVALID_TAGS);
	}

	public void setDiscardInvalidTargets(boolean discardInvalidTags) {
		setBoolean(DISCARD_INVALID_TAGS, discardInvalidTags);
	}

	public boolean getIgnoreTagTypeMatch() {
		return getBoolean(IGNORE_TAG_TYPE_MATCH);
	}

	public void setIgnoreTagTypeMatch(boolean ignoreTagTypeMatch) {
		setBoolean(IGNORE_TAG_TYPE_MATCH, ignoreTagTypeMatch);
	}

	public boolean getWriteOriginalData() {
		return getBoolean(WRITE_ORIGINAL_DATA);
	}

	public void setWriteOriginalData(boolean writeOriginalData) {
		setBoolean(WRITE_ORIGINAL_DATA, writeOriginalData);
	}

	public String getSubfilter () {
		return getString(SUBFILTER);
	}

	public void setSubfilter(String subfilter) {
		setString(SUBFILTER, subfilter);
		if (!"".equals(getSubfilter())) {
			setUseCodeFinder(false);
		}
	}

	public boolean getSubfilterOverwriteTarget () {
		return getBoolean(SUBFILTER_OVERWRITE_TARGET);
	}

	public void setSubfilterOverwriteTarget (boolean subfilterOverwriteTarget) {
		setBoolean(SUBFILTER_OVERWRITE_TARGET, subfilterOverwriteTarget);
	}
	
	@Override
	public void reset () {
		super.reset();
		setMaxValidation(true);
		setUseCodeFinder(false);
		codeFinder = new InlineCodeFinder();
		codeFinder.setSample("&name; <tag></at><tag/> <tag attr='val'> </tag=\"val\">");
		codeFinder.setUseAllRulesWhenTesting(true);
		codeFinder.addRule("</?([A-Z0-9a-z]*)\\b[^>]*>");
		setSimplifyTags(false);
		setNeedsSegmentation(false);
		setForceUniqueIds(false);
		setIgnoreTagTypeMatch(false);
		setDiscardInvalidTargets(false);
		setWriteOriginalData(true);
		setSubfilter(null);
		setSubfilterOverwriteTarget(false);
	}

	@Override
	public void fromString (String data) {
		super.fromString(data);
		codeFinder.fromString(buffer.getGroup(CODEFINDERRULES, ""));
	}

	@Override
	public String toString () {
		buffer.setGroup(CODEFINDERRULES, codeFinder.toString());
		return super.toString();
	}
	
	@Override
	public ParametersDescription getParametersDescription () {
		ParametersDescription desc = new ParametersDescription(this);
		desc.add(MAXVALIDATION, "Perform maximum validation when parsing", null);
		desc.add(FORCE_UNIQUE_IDS, "Ensure tag IDs are unique within units", null);
		desc.add(IGNORE_TAG_TYPE_MATCH, "Ignore tag type mismatch between source and target", null);
		desc.add(DISCARD_INVALID_TAGS, "Discard invalid targets rather than rejecting the file", null);
		desc.add(WRITE_ORIGINAL_DATA, "Output includes original data when available", null);
		desc.add(USECODEFINDER, "Has inline codes as defined below:", null);
		desc.add(CODEFINDERRULES, null, "Rules for inline codes");
		desc.add(SUBFILTER, "Subfilter:", "Name of the subfilter to use for embedded content");
		desc.add(SUBFILTER_OVERWRITE_TARGET, "Subfilter: Always Overwrite Target?", "Should the writer always overwrite an existing target even if state is final?");
		return desc;
	}

	@Override
	public EditorDescription createEditorDescription (ParametersDescription paramDesc) {
		EditorDescription desc = new EditorDescription("XLIFF-2 Filter Parameters", true, false);
		
		desc.addCheckboxPart(paramDesc.get(MAXVALIDATION));
		desc.addCheckboxPart(paramDesc.get(FORCE_UNIQUE_IDS));
		desc.addCheckboxPart(paramDesc.get(IGNORE_TAG_TYPE_MATCH));
		desc.addCheckboxPart(paramDesc.get(DISCARD_INVALID_TAGS));
		desc.addCheckboxPart(paramDesc.get(WRITE_ORIGINAL_DATA));

		CheckboxPart cbp = desc.addCheckboxPart(paramDesc.get(USECODEFINDER));
		CodeFinderPart cfp = desc.addCodeFinderPart(paramDesc.get(CODEFINDERRULES));
		cfp.setMasterPart(cbp, true);
		desc.addTextInputPart(paramDesc.get(SUBFILTER));
		desc.addCheckboxPart(paramDesc.get(SUBFILTER_OVERWRITE_TARGET));
		return desc;
	}

}
