/*===========================================================================
  Copyright (C) 2014 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
===========================================================================*/

package net.sf.okapi.filters.xliff2;

import net.sf.okapi.common.IResource;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.StringUtil;
import net.sf.okapi.common.annotation.GenericAnnotation;
import net.sf.okapi.common.annotation.GenericAnnotationType;
import net.sf.okapi.common.annotation.GenericAnnotations;
import net.sf.okapi.common.resource.Code;
import net.sf.okapi.common.resource.DocumentPart;
import net.sf.okapi.common.resource.ITextUnit;
import net.sf.okapi.common.resource.Segment;
import net.sf.okapi.common.resource.StartGroup;
import net.sf.okapi.common.resource.TextContainer;
import net.sf.okapi.common.resource.TextFragment;
import net.sf.okapi.common.resource.TextPart;
import net.sf.okapi.common.resource.TextUnit;
import net.sf.okapi.common.xliff2.XLIFF2PropertyStrings;
import net.sf.okapi.filters.xliff2.util.PropertiesMapper;
import net.sf.okapi.lib.xliff2.core.CTag;
import net.sf.okapi.lib.xliff2.core.Fragment;
import net.sf.okapi.lib.xliff2.core.MTag;
import net.sf.okapi.lib.xliff2.core.MidFileData;
import net.sf.okapi.lib.xliff2.core.Part;
import net.sf.okapi.lib.xliff2.core.StartGroupData;
import net.sf.okapi.lib.xliff2.core.TagType;
import net.sf.okapi.lib.xliff2.core.Unit;
import net.sf.okapi.lib.xliff2.metadata.IMetadataItem;
import net.sf.okapi.lib.xliff2.metadata.Meta;
import net.sf.okapi.lib.xliff2.metadata.MetaGroup;
import net.sf.okapi.lib.xliff2.metadata.Metadata;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class X2ToOkpConverter {
	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final LocaleId trgLoc;

	/**
	 * Creates a new converter object.
	 * 
	 * @param trgLoc the target locale.
	 */
	public X2ToOkpConverter(LocaleId trgLoc) {
		this.trgLoc = trgLoc;
	}

	public DocumentPart convert(MidFileData midFileData) {
		DocumentPart documentPart = new DocumentPart();

		// Transfer XLIFF 2 metadata module elements into context group annotations
		if (midFileData.hasMetadata()) {
			GenericAnnotation contextGroup = metadataToContextGroup(midFileData.getMetadata());
			documentPart.setAnnotation(new GenericAnnotations(contextGroup));
		}

		return documentPart;
	}

	public StartGroup convert(StartGroupData sgd, String parentId) {
		StartGroup sg = new StartGroup(parentId, sgd.getId());

		sg.setId(sgd.getId());
		sg.setIsTranslatable(sgd.getTranslate());
		sg.setName(sgd.getName());
		sg.setType(sgd.getType());

		// Transfer XLIFF 2 metadata module elements into context group annotations
		if (sgd.hasMetadata()) {
			GenericAnnotation contextGroup = metadataToContextGroup(sgd.getMetadata());
			sg.setAnnotation(new GenericAnnotations(contextGroup));
		}

		return sg;
	}

	// Converts XLIFF 2 metadata module elements into TextUnit annotations
	private GenericAnnotation metadataToContextGroup(Metadata md) {
		GenericAnnotation contextGroup = new GenericAnnotation(GenericAnnotationType.MISC_METADATA);
		int i = 0;
		for (MetaGroup metaGroup : md) {
			addMeta(metaGroup, contextGroup, String.valueOf(i));
			i++;
		}

		return contextGroup;
	}

	private void addMeta(IMetadataItem fromMetadata, GenericAnnotation toContextGroup, String sequenceId) {
		if (fromMetadata.isGroup()) {
			MetaGroup metaGroup = (MetaGroup) fromMetadata;
			Iterator<IMetadataItem> metaIterator = metaGroup.iterator();
			int i = 0;
			while (metaIterator.hasNext()) {
				IMetadataItem next = metaIterator.next();
				addMeta(next, toContextGroup, sequenceId + "." + i);
				i++;
			}
		} else {
			Meta meta = (Meta) fromMetadata;
			// check for duplicate types
			if (toContextGroup.getString(meta.getType()) != null) {
				// use the group and meta sequence numbers to make unique
				toContextGroup.setString(meta.getType() + "." + sequenceId, meta.getData());
			} else {
				toContextGroup.setString(meta.getType(), meta.getData());
			}
		}
	}

	public ITextUnit convert(Unit unit) {
		ITextUnit tu = new TextUnit(unit.getId());
		tu.setName(unit.getName());
		tu.setType(unit.getType());
		tu.setPreserveWhitespaces(unit.getPreserveWS());
		tu.setIsTranslatable(unit.getTranslate());

		// Transfer XLIFF 2 metadata module elements into context group annotations
		if (unit.hasMetadata()) {
			GenericAnnotation contextGroup = metadataToContextGroup(unit.getMetadata());
			tu.setAnnotation(new GenericAnnotations(contextGroup));
		}

		TextContainer src = tu.getSource();
		TextContainer trg = null;

		// Do we have at least one target part?
		boolean hasTarget = false;
		for (Part part : unit) {
			if (part.hasTarget()) {
				hasTarget = true;
				break;
			}
		}
		// Transfer the target if needed
		if (hasTarget) {
			trg = tu.createTarget(trgLoc, false, IResource.CREATE_EMPTY);
		}

		// Must process the src and trg containers together so segments have same id
		// needed for matching later
		convert(unit, src, trg);
		return tu;
	}

	private void convert(Unit unit, TextContainer src, TextContainer trg) {
		List<TextPart> srcTextParts = new ArrayList<>();
		List<TextPart> trgTextParts = new ArrayList<>();
		boolean hasTarget = false;

		for (Part part : unit) {
			// use original xliff2 id if available
			TextPart stp;
			TextPart ttp;
			
			if (part.isSegment()) {
				part.getId(true);
				stp = convertToSegment(part);
				ttp = convertToSegment(part);
				hasTarget = part.hasTarget();
			} else {
				// ignorable or inter-segment text
				if (part.getId() == null) {
					String id = part.getStore().suggestId(false);
					part.setId(id);
				}
				stp = convertToTextPart(part);
				ttp = convertToTextPart(part);
			}

			stp.setPreserveWhitespaces(part.getPreserveWS());
			ttp.setPreserveWhitespaces(part.getPreserveWS());

			convert(part.getSource(), stp);

			Fragment target = part.getTarget();
			// apply the source ignorable content to target unless there
			// exists target ignorable content, but only if we had a target segment
			if (!part.isSegment() && part.getTarget() == null && hasTarget) {
				target = part.getSource();
			}

			srcTextParts.add(stp);
			PropertiesMapper.setPartProperties(part, stp);

			convert(target, ttp);
			trgTextParts.add(ttp);
			PropertiesMapper.setPartProperties(part, ttp);
		}

		src.setParts(srcTextParts.toArray(new TextPart[0]));
		if (trg != null) {
			trg.setParts(trgTextParts.toArray(new TextPart[0]));
		}
	}

	private Segment convertToSegment(Part part) {
		Segment s = new Segment(part.getId());
		s.setOriginalId(part.getId());
		return s;
	}

	private TextPart convertToTextPart(Part part) {
		TextPart tp = new TextPart(part.getId(), null);
		tp.setOriginalId(part.getId());
		return tp;
	}

	private void convert(Fragment frag, TextPart part) {
		if (frag == null) {
			return;
		}

		final List<Integer> annIds = new ArrayList<>();
		TextFragment tf = part.text;
		for (Object obj : frag) {
			if (obj instanceof String) {
				tf.append(new TextFragment((String) obj), true);
			} else if (obj instanceof CTag) {
				CTag ctag = (CTag) obj;
				Code code = new Code(ctag.getType());
				int id = StringUtil.generateIntId(ctag.getId());
				code.setId(id);
				code.setDeleteable(ctag.getCanDelete());
				code.setCloneable(ctag.getCanCopy());
				code.setData(ctag.getData());
				code.setDisplayText(ctag.getDisp());
				code.setOriginalId(ctag.getId());
				code.setTagType(convertTagType(ctag.getTagType()));
				PropertiesMapper.setCodeProperties(ctag, code);
				tf.append(code);
			} else if (obj instanceof MTag) {
				MTag mtag = (MTag) obj;
				Code code = new Code(mtag.getType());

				int id = StringUtil.generateIntId(mtag.getId());
				code.setId(id);
				code.setOriginalId(mtag.getId());
				code.setTagType(convertTagType(mtag.getTagType()));
				// override below if annotations added
				code.setType(mtag.getType());

				// override type if this is an annotation to match
				// standard Okapi behavior with net.sf.okapi.common.resource.InlineAnnotation
				if (mtag.getTagType() == TagType.OPENING) {
					// Get the annotations
					GenericAnnotations anns = convertMtagToAnnotations(mtag);
					if (anns != null) {
						annIds.add(code.getId());
						GenericAnnotations.addAnnotations(code, anns);
						code.setType(Code.TYPE_ANNOTATION_ONLY);
					}
				} else {
					// can only be CLOSING
					int n;
					id = StringUtil.generateIntId(mtag.getId());
					String origId = mtag.getId();
					code.setOriginalId(origId);
					code.setId(id);

					// find matching OPENING code to see if it has annotations
					if ((n = annIds.indexOf(id)) != -1) {
						annIds.remove(n);
						Code oc = part.getContent().getCode(part.getContent().getIndex(id));
						// must copy annotations to closing code with current design
						GenericAnnotations.addAnnotations(code, oc.getGenericAnnotations());
						code.setType(Code.TYPE_ANNOTATION_ONLY);
					}
				}

				PropertiesMapper.setCodeProperties(mtag, code);
				tf.append(code);
			}
		}
	}

	protected GenericAnnotations convertMtagToAnnotations(MTag mtag)  {
		GenericAnnotations anns = new GenericAnnotations();

		String type = mtag.getType();
		String value = mtag.getValue();

		if (type != null) {
			// generic, comment, term
			if (type.equals(XLIFF2PropertyStrings.TERM)) {
				if (value == null) {
					anns.add(new GenericAnnotation(GenericAnnotationType.TERM, XLIFF2PropertyStrings.TYPE, type));
				} else {
					anns.add(new GenericAnnotation(GenericAnnotationType.TERM, XLIFF2PropertyStrings.TYPE, type,
							XLIFF2PropertyStrings.VALUE, value));
				}
			} else if (type.equals(XLIFF2PropertyStrings.COMMENT)) {
				if (value == null) {
					anns.add(new GenericAnnotation(GenericAnnotationType.LOCNOTE, XLIFF2PropertyStrings.TYPE, type));
				} else {
					anns.add(new GenericAnnotation(GenericAnnotationType.LOCNOTE, XLIFF2PropertyStrings.TYPE, type,
							XLIFF2PropertyStrings.VALUE, value));
				}
			} else {
				// generic or a custom value
				// not an ITS annotation per se but used for the xliff1.2 mtype or xliff 2.0 type attributes
				anns.add(new GenericAnnotation(GenericAnnotationType.CUSTOM_TYPE, GenericAnnotationType.CUSTOM_TYPE, type));
				// FIXME: skip 'value'. if a value is found here not sure how to map to ITS
				// FIXME: skip ref
			}
		}

		return (anns.isEmpty()) ? null : anns;
	}

	private static TextFragment.TagType convertTagType(net.sf.okapi.lib.xliff2.core.TagType tagType) {
		switch (tagType) {
			case CLOSING:
				return TextFragment.TagType.CLOSING;
			case OPENING:
				return TextFragment.TagType.OPENING;
			case STANDALONE:
				return TextFragment.TagType.PLACEHOLDER;
			default:
				LoggerFactory.getLogger(PropertiesMapper.class).warn("TagType {} unrecognized. Treating it as {}.", tagType,
						net.sf.okapi.lib.xliff2.core.TagType.STANDALONE);
				return TextFragment.TagType.PLACEHOLDER;

		}
	}
}
