package net.sf.okapi.filters.table.csv;

import net.sf.okapi.common.IParameters;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.Util;
import net.sf.okapi.common.encoder.EncoderManager;
import net.sf.okapi.common.resource.*;
import net.sf.okapi.common.skeleton.GenericSkeleton;
import net.sf.okapi.common.skeleton.GenericSkeletonWriter;
import net.sf.okapi.common.skeleton.SkeletonUtil;

public class CSVSkeletonWriter extends GenericSkeletonWriter {

	Parameters params = new Parameters();
	
	@Override
	public String processStartDocument(LocaleId outputLocale,
			String outputEncoding, EncoderManager encoderManager, StartDocument resource) {
		// Just get the current document's delimiter and qualifier
		IParameters params = resource.getFilterParameters();
		if (params instanceof net.sf.okapi.filters.table.Parameters) {
			params = ((net.sf.okapi.filters.table.Parameters) params).getActiveParameters();
		}
		this.params = (Parameters) params;
		return super.processStartDocument(outputLocale, outputEncoding, encoderManager, resource);
	}

	private void escapeQualifiers(TextContainer tc) {
		if (tc.getUnSegmentedContentCopy().toText().contains(params.textQualifier)) {
			switch (params.escapingMode) {
				case Parameters.ESCAPING_MODE_DUPLICATION:
					// If there are an odd number of qualifiers in a row, then this will add one to make an even number
					for (TextPart tp : tc.getParts()) {
						TextFragment pf = tp.getContent();
						int i = 0;
						while (i < pf.length()) {
							String pt = pf.toString();
							if (!params.removeQualifiers && tc.getParts().get(0).equals(tp) && i == 0) {
								i += 1;
								continue;
							} else if (!params.removeQualifiers && tc.getParts().get(tc.getParts().size() - 1).equals(tp) && i == pt.length() - 1) {
								break;
							}
							if (pt.substring(i).startsWith(params.textQualifier)) {
								if (i == pt.length() - 1) {
									pf.insert(i, params.textQualifier);
									break;
								} else if (pt.substring(i+params.textQualifier.length()).startsWith(params.textQualifier)) {
									i += 2 * params.textQualifier.length();
								} else {
									pf.insert(i, params.textQualifier);
									i += 2 * params.textQualifier.length();
								}
							} else {
								i += 1;
							}
						}
					}

					break;

				case Parameters.ESCAPING_MODE_BACKSLASH:
					for (TextPart tp : tc.getParts()) {
						TextFragment pf = tp.getContent();
						int i = 0;
						while (i < pf.length()) {
							String pt = pf.toString();
							if (!params.removeQualifiers && tc.getParts().get(0).equals(tp) && i == 0) {
								i += 1;
								continue;
							} else if (!params.removeQualifiers && tc.getParts().get(tc.getParts().size() - 1).equals(tp) && i == pt.length() - 1) {
								break;
							}
							if (pt.substring(i).startsWith(params.textQualifier)) {
								if (i == 0) {
									pf.insert(0, "\\");
									i += 1 + params.textQualifier.length();
								} else if (pt.charAt(i - 1) == '\\') {
									i += params.textQualifier.length();
								} else {
									pf.insert(i, "\\");
									i += 1 + params.textQualifier.length();
								}
							} else {
								i += 1;
							}
						}
					}
					break;
			}
		}
	}
	
	@Override
	public String processTextUnit(ITextUnit tu) {

		TextContainer tc;
		boolean isTarget = tu.hasTarget(outputLoc);
		if (isTarget) {
			tc = tu.getTarget(outputLoc);
		}
		else {
			tc = tu.getSource();
		}

		if (tc == null) {
			return super.processTextUnit(tu);
		}

		escapeQualifiers(tc); // always escape qualifiers

		TextFragment tf = tc.getUnSegmentedContentCopy();
		String text = tf.toText(); // Just to detect "bad" characters
		
		if (tu.hasProperty(CommaSeparatedValuesFilter.PROP_QUALIFIED) &&
				"yes".equals(tu.getProperty(CommaSeparatedValuesFilter.PROP_QUALIFIED).getValue())) {
            return super.processTextUnit(tu);
        }

		if (!params.addQualifiers) {
			return super.processTextUnit(tu);
		}
		
		// If the CSV file has a comma new line or qualifiers in non-qualified field values, qualify them
		if (text.contains(params.fieldDelimiter) || text.contains("\n") || text.contains(params.textQualifier)) {
			if (tc.hasBeenSegmented()) {
				tc.insert(0, new TextPart(params.textQualifier));
				tc.append(new TextPart(params.textQualifier));
			}
			else {
				tf.insert(0, new TextFragment(params.textQualifier));
				tf.append(params.textQualifier);
				tc.setContent(tf);
			}
			tu.setProperty(new Property(CommaSeparatedValuesFilter.PROP_QUALIFIED, 
					"yes",
					Property.FILTER_AND_DISPLAY));
		}

		return super.processTextUnit(tu);
	}

}
