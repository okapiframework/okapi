/*
 * ====================================================================
 *   Copyright (C) $time.year by the Okapi Framework contributors
 * -----------------------------------------------------------------------------
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 * =====================================================================
 */

package net.sf.okapi.filters.messageformat;

import com.ibm.icu.text.MessageFormat;
import net.sf.okapi.common.LocaleId;
import org.junit.Ignore;
import org.junit.Test;

import java.time.Duration;
import java.util.HashMap;

import static org.junit.Assert.assertEquals;

public class MessageFormatToFormattedTest {

    @Test
    public void testSkipSyntaxApostrophe() throws Exception {
        String message = "I have '{count, plural, one {# apple} other {# apples}}'";
        try (MessageFormatParser p = new MessageFormatParser(message)) {
            assertEquals(message, p.toFormatted(LocaleId.ENGLISH));
        }
    }

    @Test
    public void testSkipSyntaxEmbedded() throws Exception {
        String message = "'{0, plural, one {You have {1, plural, one {# apple} other {# apples}}} other {You and # others have {1, plural, one {# apple} other {# apples}}}}'";
        try (MessageFormatParser p = new MessageFormatParser(message)) {
            assertEquals(message, p.toFormatted(LocaleId.ENGLISH));
        }
    }

    @Test
    public void testSkipSyntaxEscapedBraces() throws Exception {
        String message = "This is a '{{literal text}}' in the message.";
        try (MessageFormatParser p = new MessageFormatParser(message)) {
            assertEquals(message, p.toFormatted(LocaleId.ENGLISH));
        }
    }

    @Test
    public void testSkipSyntaxQuotedText() throws Exception {
        String message = "He said, '{quote, select, yes {Yes} no {No}}'";
        try (MessageFormatParser p = new MessageFormatParser(message)) {
            assertEquals(message, p.toFormatted(LocaleId.ENGLISH));
        }
    }

    @Test
    public void testSkipSyntaxComplexPattern() throws Exception {
        String message = "This is a '{complex, choice, simple {simple pattern} complex {complex pattern}}' message.";
        try (MessageFormatParser p = new MessageFormatParser(message)) {
            assertEquals(message, p.toFormatted(LocaleId.ENGLISH));
        }
    }

    @Test
    public void testOffset() throws Exception {
        String message = "{n, plural, offset:2 =0 {No (#) trains are available} one {# train is available} other {# trains are available}}";
        try (MessageFormatParser p = new MessageFormatParser(message)) {
            assertEquals("1 train is available", p.toFormatted(LocaleId.ENGLISH));
        }
    }

    @Test
    public void testRussian() throws Exception {
        String message = "{members, plural, \n" +
                "=0 {Нет доступных членов} \n" +
                "one {Есть один член.} \n" +
                "few {Имеется # члена.} \n" +
                "many {Есть # членов.} \n" +
                "other {# члена.}}";
        try (MessageFormatParser p = new MessageFormatParser()) {
            p.parse(message);
            assertEquals("Имеется 3 члена.", p.toFormatted(LocaleId.RUSSIAN));
        }
    }

    @Test
    public void testWithPlural() throws Exception {
        String message = "Text {count, plural, zero {foo} =1 {bar} =2 {baz} other {bazinga}} {foo} {0} End";
        try (MessageFormatParser p = new MessageFormatParser()) {
            p.parse(message);
            assertEquals("Text bazinga Foo 0 End", p.toFormatted(LocaleId.ENGLISH));
        }
    }

    @Test
    public void testWithSimpleMessage() throws Exception {
        String message = "Hello, {name}!";
        try (MessageFormatParser p = new MessageFormatParser()) {
            p.parse(message);
            assertEquals("Hello, Foo!", p.toFormatted(LocaleId.ENGLISH));
        }
    }

    @Test
    public void testWithPluralMessage() throws Exception {
        String message = "{count, plural, one {There is one item.} other {There are # items.}}";
        try (MessageFormatParser p = new MessageFormatParser()) {
            p.parse(message);
            assertEquals("There are 3 items.", p.toFormatted(LocaleId.ENGLISH));
        }
    }

    @Test
    public void testWithNestedMessage() throws Exception {
        String message = "{level, select, info {Information: {message}} error {Error: {message}} other {Status: {message}}}";
        try (MessageFormatParser p = new MessageFormatParser()) {
            p.parse(message);
            assertEquals("Status: Foo", p.toFormatted(LocaleId.ENGLISH));
        }
    }

    @Test
    public void testWithComplexGenderMessage() throws Exception {
        String message = "{userGender, select, male {{user} has} female {{user} has} other {{user} has}} invited {guestCount, plural, one {one guest} other {# guests}} to the event.";
        try (MessageFormatParser p = new MessageFormatParser()) {
            p.parse(message);
            assertEquals("Foo has invited 3 guests to the event.", p.toFormatted(LocaleId.ENGLISH));
        }
    }

    @Test
    public void testWithComplexPluralMessage() throws Exception {
        String message = "{itemCount, plural, one {There is one item.} other {There are # items. {users, plural, one {One user} other {# users}}}}";
        try (MessageFormatParser p = new MessageFormatParser()) {
            p.parse(message);
            assertEquals("There are 3 items. 3 users", p.toFormatted(LocaleId.ENGLISH));
        }
    }

    @Test
    public void testWithNestedGenderAndPluralMessage() throws Exception {
        String message = "{userGender, select, male {{user} has} female {{user} has} other {{user} has}} invited {guestCount, plural, one {one guest} other {{guestCount, plural, one {# guest} other {# guests}}}} to the {eventType, select, party {party} conference {conference} other {event}}.";
        try (MessageFormatParser p = new MessageFormatParser()) {
            p.parse(message);
            assertEquals("Foo has invited 3 guests to the event.", p.toFormatted(LocaleId.ENGLISH));
        }
    }

    @Test
    public void testWithMixedGenderAndPluralMessage() throws Exception {
        String message = "For {userGender, select, male {him} female {her} other {them}}, there {itemCount, plural, one {is one item} other {are # items}} in the {location, select, home {home} office {office} other {location}}.";
        try (MessageFormatParser p = new MessageFormatParser()) {
            p.parse(message);
            assertEquals("For her, there are 3 items in the location.", p.toFormatted(LocaleId.ENGLISH));
        }
    }

    @Test
    public void testWithEmbeddedPluralMessage() throws Exception {
        String message = "{0, plural, one {You have {1, plural, one {# apple} other {# apples}}} other {You and # others have {1, plural, one {# apple} other {# apples}}}}";
        try (MessageFormatParser p = new MessageFormatParser()) {
            p.parse(message);
            assertEquals("You and 3 others have 3 apples", p.toFormatted(LocaleId.ENGLISH));
        }
    }

    @Test
    public void testSelectMessage() throws Exception {
        String message = "{color, select, red {The color is red.} blue {The color is blue.} green {The color is green.} other {The color is unknown.}}";
        try (MessageFormatParser parser = new MessageFormatParser(message)) {
            assertEquals("The color is unknown.", parser.toFormatted(LocaleId.ENGLISH));
        }
    }

    @Test
    public void testSelectOrdinalMessage() throws Exception {
        String message = "{position, selectordinal, one {You are in the 1st place.} two {You are in the 2nd place.} few {You are in the 3rd place.} other {You are in the #th place.}}";
        try (MessageFormatParser parser = new MessageFormatParser(message)) {
            assertEquals("You are in the 3rd place.", parser.toFormatted(LocaleId.ENGLISH));
        }
    }

    @Test
    public void testShortPluralMessage() throws Exception {
        String message = "There {i, plural, one {was 1 person} other {were {i} people}} checking out that item.";
        try (MessageFormatParser parser = new MessageFormatParser(message)) {
            assertEquals("There were 3 people checking out that item.", parser.toFormatted(LocaleId.ENGLISH));
        }
    }

    @Test
    public void testWithNumber() throws Exception {
        String message = "You have {count, number} new messages.";
        try (MessageFormatParser p = new MessageFormatParser()) {
            p.parse(message);
            assertEquals("You have 1 new messages.", p.toFormatted(LocaleId.ENGLISH));
        }
    }

    @Ignore("Date value changes with the machine timezone")
    public void testWithDate() throws Exception {
        String message = "Today is {date, date}.";
        try (MessageFormatParser p = new MessageFormatParser()) {
            p.parse(message);
            assertEquals("Today is Mar 8, 2023.", p.toFormatted(LocaleId.ENGLISH));
        }
    }

    @Ignore("Date value changes with the machine timezone")
    public void testWithTime() throws Exception {
        String message = "The time is {time, time}.";
        try (MessageFormatParser p = new MessageFormatParser()) {
            p.parse(message);
            assertEquals("The time is 3:33:20 AM.", p.toFormatted(LocaleId.ENGLISH));
        }
    }

    @Test
    public void testWithSelect() throws Exception {
        String message = "The weather today is {weather, select, \n" +
                "  sunny {sunny}\n" +
                "  rainy {rainy}\n" +
                "  other {cloudy}}.";
        try (MessageFormatParser p = new MessageFormatParser()) {
            p.parse(message);
            assertEquals("The weather today is cloudy.", p.toFormatted(LocaleId.ENGLISH));
        }
    }

    @Test
    public void testCurrency() throws Exception {
        String message = "The price is {price, number, currency}.";
        try (MessageFormatParser p = new MessageFormatParser()) {
            p.parse(message);
            assertEquals("The price is ¤1.00.", p.toFormatted(LocaleId.ENGLISH));
        }
    }

    @Test
    public void testPercent() throws Exception {
        String message = "You saved {discount, number, percent}.";
        try (MessageFormatParser p = new MessageFormatParser()) {
            p.parse(message);
            assertEquals("You saved 100%.", p.toFormatted(LocaleId.ENGLISH));
        }
    }


    @Test
    public void testSpellout() throws Exception {
        String message = "You came in {place, spellout} place.";
        try (MessageFormatParser p = new MessageFormatParser()) {
            p.parse(message);
            assertEquals("You came in zero place.", p.toFormatted(LocaleId.ENGLISH));
        }
    }

    @Test
    public void testDuration() throws Exception {
        String message = "Meeting starts in {duration, duration}";
        try (MessageFormatParser p = new MessageFormatParser()) {
            p.parse(message);
            assertEquals("Meeting starts in 30 sec.", p.toFormatted(LocaleId.ENGLISH));
        }
    }
}
