/*===========================================================================
  Copyright (C) 2008-2011 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
===========================================================================*/

package net.sf.okapi.filters.abstractmarkup;

import net.htmlparser.jericho.Attribute;
import net.htmlparser.jericho.Attributes;
import net.htmlparser.jericho.CharacterEntityReference;
import net.htmlparser.jericho.CharacterReference;
import net.htmlparser.jericho.Config;
import net.htmlparser.jericho.EndTag;
import net.htmlparser.jericho.EndTagType;
import net.htmlparser.jericho.LoggerProvider;
import net.htmlparser.jericho.NumericCharacterReference;
import net.htmlparser.jericho.Segment;
import net.htmlparser.jericho.Source;
import net.htmlparser.jericho.StartTag;
import net.htmlparser.jericho.StartTagType;
import net.htmlparser.jericho.StreamedSource;
import net.htmlparser.jericho.Tag;
import net.sf.okapi.common.BOMNewlineEncodingDetector;
import net.sf.okapi.common.Event;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.MimeTypeMapper;
import net.sf.okapi.common.Util;
import net.sf.okapi.common.encoder.QuoteMode;
import net.sf.okapi.common.encoder.XMLEncoder;
import net.sf.okapi.common.exceptions.OkapiBadFilterInputException;
import net.sf.okapi.common.exceptions.OkapiIOException;
import net.sf.okapi.common.filters.*;
import net.sf.okapi.common.filters.PropertyTextUnitPlaceholder.PlaceholderAccessType;
import net.sf.okapi.common.resource.*;
import net.sf.okapi.common.resource.TextFragment.TagType;
import net.sf.okapi.common.skeleton.GenericSkeleton;
import net.sf.okapi.filters.abstractmarkup.ExtractionRuleState.ExtractionRule;
import net.sf.okapi.filters.abstractmarkup.config.TaggedFilterConfiguration;
import net.sf.okapi.filters.abstractmarkup.config.TaggedFilterConfiguration.RULE_TYPE;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

/**
 * Abstract class useful for creating an {@link IFilter} around the Jericho parser. Jericho can parse non-wellformed
 * HTML, XHTML, XML and various server side scripting languages such as PHP, Mason, Perl (all configurable from
 * Jericho). AbstractMarkupFilter takes care of the parser initialization and provides default handlers for each token
 * type returned by the parser.
 * <p>
 * Handling of translatable text, inline tags, translatable and read-only attributes are configurable through a user
 * defined YAML file. See the Okapi HtmlFilter with defaultConfiguration.yml and OpenXml filters for examples.
 * 
 */
public abstract class AbstractMarkupFilter extends AbstractFilter {
	private final Logger LOGGER = LoggerFactory.getLogger(getClass());
	private static final String CDATA_START_REGEX = "<!\\[CDATA\\[";
	private static final String CDATA_END_REGEX = "]]>";
	private static final Pattern CDATA_START_PATTERN = Pattern.compile(CDATA_START_REGEX);
	private static final Pattern CDATA_END_PATTERN = Pattern.compile(CDATA_END_REGEX);
	private static final int PREVIEW_BYTE_COUNT = 8192;

	private final StringBuilder bufferedWhitespace;
	private StreamedSource document;
	private Iterator<Segment> nodeIterator;
	private boolean hasUtf8Bom;
	private boolean hasUtf8Encoding;
	private boolean hasBOM;
	private AbstractMarkupEventBuilder eventBuilder;
	private RawDocument currentRawDocument;
	private ExtractionRuleState ruleState; 
	private String currentId;
	private boolean documentEncoding;
	private String currentDocName;
	private IFilter cdataFilter;
	private IFilter pcdataFilter;
	private int cdataSectionIndex;
	private int pcdataSectionIndex;
	private int prevTagEnd;

	static {
		Config.ConvertNonBreakingSpaces = false;
		Config.IsHTMLEmptyElementTagRecognised = true;
		Config.NewLine = BOMNewlineEncodingDetector.NewlineType.LF.toString();
		Config.LoggerProvider = LoggerProvider.SLF4J;
		Config.CurrentCharacterReferenceEncodingBehaviour=Config.LEGACY_CHARACTER_REFERENCE_ENCODING_BEHAVIOUR;
		// make jericho more tolerant of parsing elements/attributes
		Attributes.setDefaultMaxErrorCount(15);
	}

	/**
	 * Default constructor for {@link AbstractMarkupFilter} using default {@link AbstractMarkupEventBuilder}
	 */
	public AbstractMarkupFilter() {
		super();
		this.bufferedWhitespace = new StringBuilder();
		this.hasUtf8Bom = false;
		this.hasUtf8Encoding = false;
		this.hasBOM = false;
		this.currentId = null;
		this.documentEncoding = false;		
	}

	/**
	 * Get the current {@link TaggedFilterConfiguration}. A TaggedFilterConfiguration is the result of reading in a YAML
	 * configuration file and converting it into Java Objects.
	 * 
	 * @return a {@link TaggedFilterConfiguration}
	 */
	abstract protected TaggedFilterConfiguration getConfig();

	/**
	 * Close the filter and all used resources.
	 */
	@Override
	public void close() {
		super.close();
		
		this.hasUtf8Bom = false;
		this.hasUtf8Encoding = false;
		this.currentId = null;

		if (ruleState != null) {
			ruleState.reset(!getConfig().isGlobalPreserveWhitespace(),
							getConfig().isGlobalExcludeByDefault());
		}

		if (currentRawDocument != null) {
			currentRawDocument.close();
		}

		try {
			if (document != null) {
				document.close();
			}
		} catch (IOException e) {
			throw new OkapiIOException("Could not close " + getDocumentName(), e);
		}
		this.document = null; // help Java GC

		LOGGER.debug("{} has been closed", getDocumentName());
	}

	/*
	 * Get PREVIEW_BYTE_COUNT bytes so we can sniff out any encoding information in XML or HTML files
	 */
	protected Source getParsedHeader(final InputStream inputStream) {
		try {
			// Make sure we grab the same buffer for UTF-16/32
			// this is to avoid round trip problem when not detecting a declaration
			// between a non-UTF-16/32 and a UTF-16/32 input
			int charSize = 1;
			if ( getEncoding().toLowerCase().startsWith("utf-16") ) charSize = 2;
			else if ( getEncoding().toLowerCase().startsWith("utf-32") ) charSize = 4;
			
			final byte[] bytes = new byte[PREVIEW_BYTE_COUNT * charSize];
			int i;
			for (i = 0; i < bytes.length; i++) {
				final int nextByte = inputStream.read();
				if (nextByte == -1)
					break;
				bytes[i] = (byte) nextByte;
			}
			return new Source(new ByteArrayInputStream(bytes, 0, i));
		} catch (IOException e) {
			throw new OkapiIOException("Could not reset the input stream to it's start position", e);
		} finally {
			try {
				inputStream.reset();
			} catch (IOException e) {

			}
		}
	}
	
	protected String detectEncoding(RawDocument input) {
		BOMNewlineEncodingDetector detector = new BOMNewlineEncodingDetector(input.getStream(),
				input.getEncoding());
		// string input has a default BOM defined by java
		// do not remove it
		if (input.getInputCharSequence() != null) {
			detector.detectBom();
		} else {
			detector.detectAndRemoveBom();
		}

		setEncoding(detector.getEncoding());
		hasUtf8Bom = detector.hasUtf8Bom();
		hasUtf8Encoding = detector.hasUtf8Encoding();
		hasBOM = detector.hasBom();
		setNewlineType(detector.getNewlineType().toString());

		Source parsedHeader = getParsedHeader(input.getStream());
		String detectedEncoding = parsedHeader.getDocumentSpecifiedEncoding();
		documentEncoding = detectedEncoding != null;

		if (detectedEncoding == null && getEncoding() != null) {
			detectedEncoding = getEncoding();
			LOGGER.debug("Cannot auto-detect encoding. Using the default encoding ({})", getEncoding());
		} else if (getEncoding() == null) {
			detectedEncoding = parsedHeader.getEncoding(); // get best guess
			LOGGER.debug("Default encoding and detected encoding not found. Using best guess encoding ({})",
							detectedEncoding);
		}

		return detectedEncoding;
	}

	/**
	 * Start a new {@link IFilter} using the supplied {@link RawDocument}.
	 * 
	 * @param input
	 *            - input to the {@link IFilter} (can be a {@link CharSequence}, {@link URI} or {@link InputStream})
	 */
	@Override
	public void open(RawDocument input) {
		open(input, true);
		LOGGER.debug("{} has opened an input document", getName());
	}

	/**
	 * Start a new {@link IFilter} using the supplied {@link RawDocument}.
	 * 
	 * @param input
	 *            - input to the {@link IFilter} (can be a {@link CharSequence}, {@link URI} or {@link InputStream})
	 * @param generateSkeleton
	 *            - true if the {@link IFilter} should store non-translatble blocks (aka skeleton), false otherwise.
	 * 
	 * @throws OkapiBadFilterInputException
	 * @throws OkapiIOException
	 */
	@Override
    public void open(RawDocument input, boolean generateSkeleton) {
		super.open(input, generateSkeleton);
		
		if (currentRawDocument != null) {
			currentRawDocument.close();
		}
		currentRawDocument = input;

		// doc name may be set by sub-classes
		if (getCurrentDocName() != null) {
			setDocumentName(getCurrentDocName());
		} else if (input.getInputURI() != null) {
			setDocumentName(input.getInputURI().getPath());
		}

		try {
			String detectedEncoding = detectEncoding(input);
			input.setEncoding(detectedEncoding);
			setOptions(input.getSourceLocale(), input.getTargetLocale(), detectedEncoding,
					generateSkeleton);
			if (document != null) {
				document.close();
			}
			document = new StreamedSource(input.getReader());
		} catch (IOException e) {
			throw new OkapiIOException("Filter could not open input stream", e);
		}
		
		currentDocName = null;

		startFilter();
	}

	@Override
	public boolean hasNext() {
		return eventBuilder.hasNext();
	}

	/**
	 * Queue up Jericho tokens until we can build an Okapi {@link Event} and return it.
	 */
	@Override
	public Event next() {
		if (eventBuilder.hasQueuedEvents()) {
			return eventBuilder.next();
		}

		while (nodeIterator.hasNext() && !isCanceled()) {
			Segment segment = nodeIterator.next();

			preProcess(segment);

			if (segment instanceof Tag) {
				final Tag tag = (Tag) segment;

				// Check for nested tag-like data that are seen as true tags by Jericho
				if ( tag.getBegin() < prevTagEnd ) {
					// if the start position of a tag is after the end position of the previous one
					// it is some kind of 'tag-like' instruction that is in an attribute of the previous tag
					// We need to discard it or it will be written out as extra data in the output
					continue;
				}
				prevTagEnd = tag.getEnd(); // Remember the end position for this tag for the next check
				
				if (tag.getTagType() == StartTagType.NORMAL
						|| tag.getTagType() == StartTagType.UNREGISTERED) {
					handleStartTag((StartTag) tag);
				} else if (tag.getTagType() == EndTagType.NORMAL
						|| tag.getTagType() == EndTagType.UNREGISTERED) {
					handleEndTag((EndTag) tag);
				} else if (tag.getTagType() == StartTagType.DOCTYPE_DECLARATION) {
					handleDocTypeDeclaration(tag);
				} else if (tag.getTagType() == StartTagType.CDATA_SECTION) {
					handleCdataSection(tag);
				} else if (tag.getTagType() == StartTagType.COMMENT) {
					handleComment(tag);
				} else if (tag.getTagType() == StartTagType.XML_DECLARATION) {
					handleXmlDeclaration(tag);
				} else if (tag.getTagType() == StartTagType.XML_PROCESSING_INSTRUCTION) {
					handleProcessingInstruction(tag);
				} else if (tag.getTagType() == StartTagType.MARKUP_DECLARATION) {
					// SKIP these as we handle them in DOCTYPE_DECLARATION  
					//handleMarkupDeclaration(tag);
				} else if (tag.getTagType() == StartTagType.SERVER_COMMON) {
					handleServerCommon(tag);
				} else if (tag.getTagType() == StartTagType.SERVER_COMMON_ESCAPED) {
					handleServerCommonEscaped(tag);
				}
				else if ( tag.getName().startsWith("%--") ) {
					// Comment tag for ASPX
					handleDocumentPart(tag);
				}
				else { // Not classified explicitly by Jericho
					if (tag instanceof StartTag) {
						handleStartTag((StartTag) tag);
					} else if (tag instanceof EndTag) {
						handleEndTag((EndTag) tag);
					} else {
						handleDocumentPart(tag);
					}
				}
			} else if (segment instanceof CharacterEntityReference) {
				handleCharacterEntity((CharacterEntityReference)segment);
			} else if (segment instanceof NumericCharacterReference) {
				handleNumericEntity((NumericCharacterReference)segment);
			} else {
				// last resort is pure text node
				handleText(segment.toString());
			}

			if (eventBuilder.hasQueuedEvents()) {
				break;
			}
		}

		if (!nodeIterator.hasNext()) {			
			endFilter(); // we are done
		}

		// return one of the waiting events
		return eventBuilder.next();
	}

	/**
	 * Delayed initialization of the {@link EventBuilder}.  This will be called
	 * when the filter is initialized if an EventBuilder was not previously
	 * passed to the constructor.
	 * @return
	 */
	protected AbstractMarkupEventBuilder createEventBuilder() {
		AbstractMarkupEventBuilder eb = new AbstractMarkupEventBuilder(getParentId(), 
				this, getEncoderManager(), getEncoding(), getNewlineType());
		eb.setMimeType(getMimeType());
		return eb;
	}
	
	/**
	 * Initialize the filter for every input and send the {@link StartDocument} {@link Event}
	 */
	protected void startFilter() {
		// order of execution matters
		if (eventBuilder == null) {
			eventBuilder = createEventBuilder();
		} else {
			eventBuilder.reset(getParentId(), this);
		}		

		eventBuilder.addFilterEvent(createStartFilterEvent());		

		// default is to preserve whitespace
		boolean preserveWhitespace = true;
		boolean defaultExcludeRule = false;
		if (getConfig() != null) {
			preserveWhitespace = getConfig().isGlobalPreserveWhitespace();
			defaultExcludeRule = getConfig().isGlobalExcludeByDefault();
		}
		ruleState = new ExtractionRuleState(preserveWhitespace, defaultExcludeRule);
		setPreserveWhitespace(ruleState.isPreserveWhitespaceState());

		// This optimizes memory at the expense of performance
		nodeIterator = document.iterator();

		cdataSectionIndex = 0;
		pcdataSectionIndex = 0;
		prevTagEnd = -1;
		pcdataFilter = null;
		cdataFilter = null;

		// initialize cdata sub-filter
		TaggedFilterConfiguration config = getConfig(); 
		if (config != null && config.getGlobalCDATASubfilter() != null) {
			cdataFilter = getFilterConfigurationMapper().createFilter(
					getConfig().getGlobalCDATASubfilter(), cdataFilter); 
		}
		
		// initialize pcdata sub-filter
		if (config != null && config.getGlobalPCDATASubfilter() != null) {
			String subfilterName = getConfig().getGlobalPCDATASubfilter();
			pcdataFilter = getFilterConfigurationMapper().createFilter(subfilterName, pcdataFilter); 
		}
	}

	/**
	 * End the current filter processing and send the {@link Ending} {@link Event}
	 */
	protected void endFilter() {
		// make sure we flush out any whitespace at the end of the file
		if (bufferedWhitespace.length() > 0) {
			if (isInsideTextRun()) {
				if (ruleState.isInlineExcludedState()) {
					eventBuilder.appendCodeInlineExcludedData(bufferedWhitespace.toString());
				} else {
					addToTextUnit(bufferedWhitespace.toString());
				}
			} else {
				if (ruleState.isInlineExcludedState()) {
					eventBuilder.appendCodeInlineExcludedData(bufferedWhitespace.toString());
				} else {
					addToDocumentPart(bufferedWhitespace.toString());
				}
			}
			bufferedWhitespace.setLength(0);
			bufferedWhitespace.trimToSize();
		}

		// end INLINE_EXCLUDED content if present
		if (isInsideTextRun()) {
			if (eventBuilder.peekMostRecentCode() != null) {
				eventBuilder.endCode();
			}
		}

		// clear out all unended temp events
		eventBuilder.flushRemainingTempEvents();

		// add the final endDocument event
		eventBuilder.addFilterEvent(createEndFilterEvent());
	}

	public ExtractionRule getMainAttributeRule(StartTag tag, String attributeName, Map<String, String> attributes) {
		EnumSet<RULE_TYPE> ruleTypes;
		ExtractionRule rule = new ExtractionRule(tag.getName().toLowerCase(), RULE_TYPE.RULE_NOT_FOUND, true);

		ruleTypes = getConfig().getAttributeRuleTypes(attributeName, tag.getName(), attributes);
		ruleTypes.addAll(getConfig().getAttributeOnElementRuleTypes(tag.getName(), attributeName, attributes));

		// disambiguate multiple rules. ORDER MATTERS!!
		if (ruleTypes.contains(RULE_TYPE.ATTRIBUTE_TRANS) || ruleTypes.contains(RULE_TYPE.ELEMENT_ATTRIBUTE_TRANS)) {
			rule.ruleType = RULE_TYPE.ATTRIBUTE_TRANS;
		} else if (ruleTypes.contains(RULE_TYPE.ATTRIBUTE_WRITABLE) || ruleTypes.contains(RULE_TYPE.ELEMENT_ATTRIBUTE_WRITABLE)) {
			rule.ruleType = RULE_TYPE.ATTRIBUTE_WRITABLE;
		} else if (ruleTypes.contains(RULE_TYPE.ATTRIBUTE_READONLY) || ruleTypes.contains(RULE_TYPE.ELEMENT_ATTRIBUTE_READONLY)) {
			rule.ruleType = RULE_TYPE.ATTRIBUTE_READONLY;
		} else if (ruleTypes.contains(RULE_TYPE.ATTRIBUTE_ID) || ruleTypes.contains(RULE_TYPE.ELEMENT_ATTRIBUTE_ID)) {
			rule.ruleType = RULE_TYPE.ATTRIBUTE_ID;
		} else if (ruleTypes.contains(RULE_TYPE.ATTRIBUTES_ONLY)) {
			rule.ruleType = RULE_TYPE.ATTRIBUTES_ONLY;
		} else if (ruleTypes.contains(RULE_TYPE.ATTRIBUTE_PRESERVE_WHITESPACE)) {
			rule.ruleType = RULE_TYPE.ATTRIBUTE_PRESERVE_WHITESPACE;
		} else if (ruleTypes.contains(RULE_TYPE.ATTRIBUTE_DEFAULT_WHITESPACE)) {
			rule.ruleType = RULE_TYPE.ATTRIBUTE_DEFAULT_WHITESPACE;
		}

		return rule;
	}

	public ExtractionRule disambiguateElementRuleTypes(Tag tag, EnumSet<RULE_TYPE> ruleTypes) {
		ExtractionRule rule = new ExtractionRule(tag.getName().toLowerCase(), RULE_TYPE.RULE_NOT_FOUND, true);

		// must use rule state based on start tag mate
		if (tag instanceof EndTag) {
			return getRuleTypeFromStartTag((EndTag)tag, ruleTypes);
		} else {
			// disambiguate multiple rules. ORDER MATTERS!!
			// TODO: handle cases [GROUP, EXCLUDE], [TEXT_UNIT, EXCLUDE] etc.
			if (ruleTypes.containsAll(TaggedFilterConfiguration.INLINE_AND_INCLUDE)) {
				rule.ruleType = RULE_TYPE.INLINE_INCLUDED_ELEMENT;
			} else if (ruleTypes.containsAll(TaggedFilterConfiguration.INLINE_AND_EXCLUDE)) {
				rule.ruleType = RULE_TYPE.INLINE_EXCLUDED_ELEMENT;
			} else if (ruleTypes.contains(RULE_TYPE.INLINE_ELEMENT)) {
				rule.ruleType = RULE_TYPE.INLINE_ELEMENT;
			} else if (ruleTypes.contains(RULE_TYPE.INCLUDED_ELEMENT)) {
				rule.ruleType = RULE_TYPE.INCLUDED_ELEMENT;
			} else if (ruleTypes.contains(RULE_TYPE.EXCLUDED_ELEMENT)) {
				rule.ruleType = RULE_TYPE.EXCLUDED_ELEMENT;
			} else if (ruleTypes.contains(RULE_TYPE.GROUP_ELEMENT)) {
				rule.ruleType = RULE_TYPE.GROUP_ELEMENT;
			} else if (ruleTypes.contains(RULE_TYPE.TEXT_UNIT_ELEMENT)) {
				rule.ruleType = RULE_TYPE.TEXT_UNIT_ELEMENT;
			}

			// some condition failed ruleApplies = false
			else if (ruleTypes.contains(RULE_TYPE.INLINE_ELEMENT_FAIL)) {
				rule.ruleType = RULE_TYPE.INLINE_ELEMENT;
				rule.ruleApplies = false;
			} else if (ruleTypes.contains(RULE_TYPE.INLINE_INCLUDED_ELEMENT_FAIL)) {
				rule.ruleType = RULE_TYPE.INLINE_INCLUDED_ELEMENT;
				rule.ruleApplies = false;
			} else if (ruleTypes.contains(RULE_TYPE.INLINE_EXCLUDED_ELEMENT_FAIL)) {
				rule.ruleType = RULE_TYPE.INLINE_EXCLUDED_ELEMENT;
				rule.ruleApplies = false;
			} else if (ruleTypes.contains(RULE_TYPE.INCLUDED_ELEMENT_FAIL)) {
				rule.ruleType = RULE_TYPE.INCLUDED_ELEMENT;
				rule.ruleApplies = false;
			} else if (ruleTypes.contains(RULE_TYPE.EXCLUDED_ELEMENT_FAIL)) {
				rule.ruleType = RULE_TYPE.EXCLUDED_ELEMENT;
				rule.ruleApplies = false;
			} else if (ruleTypes.contains(RULE_TYPE.GROUP_ELEMENT_FAIL)) {
				rule.ruleType = RULE_TYPE.GROUP_ELEMENT;
				rule.ruleApplies = false;
			} else if (ruleTypes.contains(RULE_TYPE.TEXT_UNIT_ELEMENT_FAIL)) {
				rule.ruleType = RULE_TYPE.TEXT_UNIT_ELEMENT;
				rule.ruleApplies = false;
			}

			return rule;
		}
	}

	public ExtractionRule getRuleTypeFromStartTag(EndTag endTag, EnumSet<RULE_TYPE> ruleTypes) {
		ExtractionRule rule = new ExtractionRule(endTag.getName().toLowerCase(), RULE_TYPE.RULE_NOT_FOUND, true);

		// search for any start tags that match this end tag
		// return the first found
		for (RULE_TYPE t: ruleTypes) {
			switch (t) {
				case INLINE_EXCLUDED_ELEMENT:
				case INLINE_INCLUDED_ELEMENT:
					rule = ruleState.peekExcludedIncludedInlineRule();
					if (isMatchedTag(rule, endTag)) {
						return rule;
					}
					break;
				case INLINE_ELEMENT:
					rule = ruleState.peekInlineRule();
					if (isMatchedTag(rule, endTag)) {
						return rule;
					}
					break;
				case GROUP_ELEMENT:
					rule = ruleState.peekGroupRule();
					if (isMatchedTag(rule, endTag)) {
						return rule;
					}
					break;
				case INCLUDED_ELEMENT:
				case EXCLUDED_ELEMENT:
					if (ruleTypes.containsAll(TaggedFilterConfiguration.INLINE_AND_EXCLUDE) ||
							ruleTypes.containsAll(TaggedFilterConfiguration.INLINE_AND_INCLUDE)) {
						rule = ruleState.peekExcludedIncludedInlineRule();
						if (isMatchedTag(rule, endTag)) {
							return rule;
						}
						break;
					} else {
						rule = ruleState.peekExcludedIncludedRule();
						if (isMatchedTag(rule, endTag)) {
							return rule;
						}
					}
					break;
				case TEXT_UNIT_ELEMENT:
					rule = ruleState.peekTextUnitRule();
					if (isMatchedTag(rule, endTag)) {
						return rule;
					}
					break;
				default:
					break;
			}
		}

		// did not find a matched tag.
		return rule;
	}

	public ExtractionRule getMainElementRule(Tag tag) {
		EnumSet<RULE_TYPE> ruleTypes;

		// determine rule state if this is a tag
		if (tag instanceof StartTag) {
			StartTag startTag = (StartTag) tag;
			Map<String, String> attributes = new HashMap<>();
			if (startTag.getAttributes() != null) {
				startTag.getAttributes().populateMap(attributes, true);
			}
			ruleTypes = getConfig().getElementRuleTypes(startTag.getName(), attributes, true);
			return disambiguateElementRuleTypes(tag, ruleTypes);
		} else if (tag instanceof EndTag) {
			ruleTypes = getConfig().getElementRuleTypes(tag.getName(), false);
			return disambiguateElementRuleTypes(tag, ruleTypes);
		} else {
			// not a Tag - no rules apply
			return new ExtractionRule(tag.getName().toLowerCase(), RULE_TYPE.RULE_NOT_FOUND, true);
		}
	}

	/**
	 * based on rule state and segment type determine if we are still inside a text run
	 * @param segment
	 * @return
	 */
	public boolean isInline(Segment segment) {
		// check for excluded state, so we don't create an unneeded TU
		if (ruleState.isExcludedState()) {
			return false;
		}

		if (segment instanceof Tag) {
			Tag tag = (Tag)segment;
			ExtractionRule rule = getMainElementRule(tag);
			if (rule != null && rule.ruleApplies && (rule.ruleType == RULE_TYPE.INLINE_ELEMENT ||
					rule.ruleType == RULE_TYPE.INLINE_INCLUDED_ELEMENT ||
					rule.ruleType == RULE_TYPE.INLINE_EXCLUDED_ELEMENT ||
					(getEventBuilder().isInsideTextRun() && (tag.getTagType() == StartTagType.COMMENT ||
							tag.getTagType() == StartTagType.XML_PROCESSING_INSTRUCTION)) ||
					(getConfig().isInlineCdata() && tag.getName().equals("![cdata[")))) {
				return true;
			}
		} else {
			// text
			return true;
		}

		return false;
	}

	/**
	 * Do any handling needed before the current Segment is processed. Default is to do nothing.
	 *
	 * @param segment
	 */
	protected void preProcess(Segment segment) {
		boolean inline = isInline(segment);

		// close INLINE_EXCLUDE if needed
		if (!ruleState.isInlineExcludedState() && eventBuilder.peekMostRecentCode() != null) {
			eventBuilder.endCode();
		}

		// handle buffered whitespace now that we have some context
		if (bufferedWhitespace.length() > 0) {
			if (ruleState.isInlineExcludedState()) {
				eventBuilder.appendCodeInlineExcludedData(bufferedWhitespace.toString());
			} else if (isInsideTextRun()) {
				addToTextUnit(bufferedWhitespace.toString());
			} else {
				if (inline) {
					startTextUnit(bufferedWhitespace.toString());
				} else {
					addToDocumentPart(bufferedWhitespace.toString());
				}
			}

			// reset buffer for next pass
			bufferedWhitespace.setLength(0);
			bufferedWhitespace.trimToSize();
		}
	}

	/**
	 * Do any required post-processing on the TextUnit before the {@link Event} leaves the {@link IFilter}. Default
	 * implementation leaves Event unchanged. Override this method if you need to do format specific handing such as
	 * collapsing whitespace.
	 */
	protected void postProcessTextUnit (ITextUnit textUnit) {}

	/**
	 * Handle any recognized escaped server tags.
	 *
	 * @param tag
	 */
	protected void handleServerCommonEscaped(Tag tag) {
		handleDocumentPart(tag);
	}

	/**
	 * Handle any recognized server tags (i.e., PHP, Mason etc.)
	 *
	 * @param tag
	 */
	protected void handleServerCommon(Tag tag) {
		handleDocumentPart(tag);
	}

	/**
	 * Handle an XML declaration.
	 *
	 * @param tag
	 */
	protected void handleXmlDeclaration(Tag tag) {
		handleDocumentPart(tag);
	}

	/**
	 * Handle the XML doc type declaration (DTD).
	 *
	 * @param tag
	 */
	protected void handleDocTypeDeclaration(Tag tag) {
		handleDocumentPart(tag);
	}

	/**
	 * Handle processing instructions.
	 *
	 * @param tag
	 */
	protected void handleProcessingInstruction(Tag tag) {
		if (ruleState.isExcludedState()) {
			addToDocumentPart(tag.toString());
			return;
		}

		if (isInsideTextRun()) {
			if (ruleState.isInlineExcludedState()) {
				eventBuilder.appendCodeInlineExcludedData(tag.toString());
				return;
			} else {
				addCodeToCurrentTextUnit(tag);
			}
		} else {
			handleDocumentPart(tag);
		}
	}

	/**
	 * Handle comments.
	 *
	 * @param tag
	 */
	protected void handleComment(Tag tag) {
		if (ruleState.isExcludedState()) {
			addToDocumentPart(tag.toString());
			return;
		}
		if (isInsideTextRun()) {
			if (ruleState.isInlineExcludedState()) {
				eventBuilder.appendCodeInlineExcludedData(tag.toString());
				return;
			} else {
				addCodeToCurrentTextUnit(tag);
			}
		} else {
			handleDocumentPart(tag);
		}
	}

	/**
	 * Handle CDATA sections.
	 *
	 * @param tag
	 */
	protected void handleCdataSection(Tag tag) {
		// end any skeleton so we can start CDATA section with subfilter
		if (!getConfig().isInlineCdata() && eventBuilder.hasUnfinishedSkeleton()) {
			endDocumentPart();
		}

		String cdataWithoutMarkers = CDATA_START_PATTERN.matcher(tag.toString()).replaceFirst("");
		cdataWithoutMarkers = CDATA_END_PATTERN.matcher(cdataWithoutMarkers).replaceFirst("");

		if ( ruleState.isExcludedState() ) {
			// Excluded content
			addToDocumentPart(tag.toString());
		} else { // Content to extract

			if (cdataFilter != null) {
				String parentId = eventBuilder.findMostRecentParentId();
				if (parentId == null) parentId = getDocumentId().getLastId();

				String parentName = eventBuilder.findMostRecentParentName();
				if (parentName == null) parentName = getDocumentId().getLastId();

				try (ISubFilter cdataSubfilter = ISubFilter.create(cdataFilter,
						null, // we don't encode cdata
						++cdataSectionIndex, parentId, parentName)) {
					eventBuilder.addFilterEvents(cdataSubfilter.getEvents(new RawDocument(cdataWithoutMarkers, getSrcLoc(), getTrgLoc())));
					// Now write out the CDATA skeleton
					addToDocumentPart("<![CDATA[");
					addToDocumentPart(cdataSubfilter.createRefCode().toString());
					addToDocumentPart("]]>");
				}
			} else if (getConfig().isInlineCdata()) {
				// Add CDATA as if it is a tag
				if (canStartNewTextUnit()) {
					startTextUnit();
				}
				addToTextUnit(new Code(TagType.OPENING, Code.TYPE_CDATA, "<![CDATA["));
				addToTextUnit(cdataWithoutMarkers);
				addToTextUnit(new Code(TagType.CLOSING, Code.TYPE_CDATA, "]]>"));
			} else {
				// we assume the CDATA is plain text take it as is
				startTextUnit(new GenericSkeleton("<![CDATA["));
				addToTextUnit(cdataWithoutMarkers);
				setTextUnitType(ITextUnit.TYPE_CDATA);
				setTextUnitMimeType(MimeTypeMapper.PLAIN_TEXT_MIME_TYPE);
				endTextUnit(new GenericSkeleton("]]>"));
			}
		}
	}

	/**
	 * Handle all text (PCDATA).
	 *
	 * @param text
	 */
	protected void handleText(CharSequence text) {
		// if in excluded state everything is skeleton including text
		if (ruleState.isExcludedState()) {
			addToDocumentPart(text.toString());
			return;
		}

		if (ruleState.isInlineExcludedState()) {
			eventBuilder.appendCodeInlineExcludedData(text.toString());
			return;
		}

		// buffer the whitespace until we have the context to deal with it
		if (isWhiteSpace(text)) {
			bufferedWhitespace.append(text);
			return;
		}

		if (isInsideTextRun()) {
			addToTextUnit(text.toString());
		} else {
			startTextUnit(text.toString());
		}
	}

	protected boolean isWhiteSpace(CharSequence text) {
		for (int i = 0; i < text.length(); i++) {
		 		if (!Segment.isWhiteSpace(text.charAt(i))) {
		 			return false;
		 		}
		 	}
		 	return true;
	}

	/**
	 * Handle all Character entities. Default implementation converts entity to Unicode character.
	 *
	 * @param entity
	 *            - the character entity
	 */
	protected void handleNumericEntity(NumericCharacterReference entity) {
		if (ruleState.isExcludedState() || ruleState.isInlineExcludedState()) {
			handleText(entity.toString());
		} else {
			handleText(CharacterReference.decode(entity.toString(), false));
		}
	}

	/**
	 * Handle all numeric entities. Default implementation converts entity to Unicode character.
	 *
	 * @param entity
	 *            - the numeric entity
	 */
	protected void handleCharacterEntity(CharacterEntityReference entity) {
		if (ruleState.isExcludedState() || ruleState.isInlineExcludedState()) {
			handleText(entity.toString());
		} else {
			handleText(CharacterReference.decode(entity.toString(), false));
		}
	}

	/**
	 * Handle start tags.
	 *
	 * @param startTag
	 */
	protected void handleStartTag(StartTag startTag) {
		ExtractionRule rule = getMainElementRule(startTag);

		// reset after each start tag so that we never
		// set a TextUnit name that id from a far out tag
		currentId = null;

		if (!startTag.isSyntacticalEmptyElementTag()) {
			updateStartTagRuleState(startTag, rule);
		}

		try {
			// if in excluded state everything is skeleton including text
			if (ruleState.isExcludedState() || rule.ruleType == RULE_TYPE.EXCLUDED_ELEMENT) {
				addToDocumentPart(startTag.toString());
				return;
			}

			// check to see if we are inside an inline run that is excluded
			if (ruleState.isInlineExcludedState() || rule.ruleType == RULE_TYPE.INLINE_EXCLUDED_ELEMENT) {
				// special code like: "<ph translate='no'>some protected text with </ph>"
				// where the start tag, text and end tag are all one code
				if (canStartNewTextUnit()) {
					startTextUnit();
				}
				eventBuilder.appendCodeInlineExcludedData(startTag.toString());
				// standalone code: add to TU immediately
				if (startTag.isSyntacticalEmptyElementTag()) {
					eventBuilder.endCode();
				}
				return;
			} else {
				if (eventBuilder.peekMostRecentCode() != null) {
					eventBuilder.endCode();
				}
			}

			List<PropertyTextUnitPlaceholder> propertyTextUnitPlaceholders;
			propertyTextUnitPlaceholders = createPropertyTextUnitPlaceholders(startTag);

			switch (rule.ruleApplies ? rule.ruleType : RULE_TYPE.RULE_NOT_FOUND) {
			case INLINE_ELEMENT:
				// check to see if we are inside a inline run that is excluded
				if (ruleState.isInlineExcludedState()) {
					eventBuilder.appendCodeInlineExcludedData(startTag.toString());
					break;
				}

				if (canStartNewTextUnit()) {
					startTextUnit();
				}
				addCodeToCurrentTextUnit(startTag, startTag.isSyntacticalEmptyElementTag());
				break;
			case INLINE_INCLUDED_ELEMENT:
				// special code like: "<ph translate='no'>some protected text with </ph>"
				// where the start tag, text and end tag are all one code
				if (canStartNewTextUnit()) {
					startTextUnit();
				}
				addCodeToCurrentTextUnit(startTag, startTag.isSyntacticalEmptyElementTag());
				break;
			case TEXT_UNIT_ELEMENT:
				handleAttributesThatAppearAnywhere(propertyTextUnitPlaceholders, startTag);
				setTextUnitType(getConfig().getElementType(startTag));
				break;
			default:
				handleAttributesThatAppearAnywhere(propertyTextUnitPlaceholders, startTag);
			}
		} finally {
			// A TextUnit may have already been created. Update its preserveWS field
			if (eventBuilder.isCurrentTextUnit()) {
				ITextUnit tu = eventBuilder.peekMostRecentTextUnit();
				tu.setPreserveWhitespaces(ruleState.isPreserveWhitespaceState());
			}
		}
	}

	protected void updateStartTagRuleState(StartTag startTag, ExtractionRule rule) {
		String tag = startTag.getName().toLowerCase();

		switch (rule.ruleType) {
		case INLINE_EXCLUDED_ELEMENT:
		case INLINE_INCLUDED_ELEMENT:
			ruleState.pushInlineExcludedIncludedRule(rule);
			break;
		case INLINE_ELEMENT:
			ruleState.pushInlineRule(rule);
			break;
		case GROUP_ELEMENT:
			ruleState.pushGroupRule(rule);
			break;
		case EXCLUDED_ELEMENT:
		case INCLUDED_ELEMENT:
			ruleState.pushExcludedIncludedRule(rule);
			break;
		case TEXT_UNIT_ELEMENT:
			ruleState.pushTextUnitRule(rule);
			break;
		default:
			break;
		}

		// if exclude is true by default then push "not exclude" to enable this rule
		if (getConfig().isGlobalExcludeByDefault() && !startTag.isSyntacticalEmptyElementTag()) {
			switch (rule.ruleType) {
				case TEXT_UNIT_ELEMENT:
				case INLINE_ELEMENT:
				case GROUP_ELEMENT:
					ExtractionRule r = new ExtractionRule(startTag.getName().toLowerCase(),
							RULE_TYPE.INCLUDED_ELEMENT, rule.ruleApplies);
					ruleState.pushExcludedIncludedRule(r);
					break;
				default:
					break;
			}
		}

		// update state like preserve whitespace etc..
		Map<String, String> attributes = new HashMap<>();
		if (startTag.getAttributes() != null) {
			startTag.getAttributes().populateMap(attributes, true);
		}

		// element rules have priority
		EnumSet<RULE_TYPE> ruleTypes = getConfig().getElementRuleTypes(tag, attributes, true);
		if (ruleTypes.contains(RULE_TYPE.PRESERVE_WHITESPACE)) {
			ExtractionRule r = new ExtractionRule(startTag.getName().toLowerCase(),
					RULE_TYPE.PRESERVE_WHITESPACE, rule.ruleApplies);
			ruleState.pushPreserverWhitespaceRule(r);
			setPreserveWhitespace(ruleState.isPreserveWhitespaceState());
			return;
		}

		// attribute rules
		// attributes like xml:space which can override element rules
		for (Attribute attribute : startTag.parseAttributes()) {
			// even if we match an attribute only process it if it has a viable value
			if (Util.isEmpty(attribute.getValue()) || Util.isEmpty(attribute.getValueSegment().toString())) {
				continue;
			}

			boolean preserveWS = getConfig().isPreserveWhitespaceCondition(attribute.getName(), attributes);
			boolean defaultWS = getConfig().isDefaultWhitespaceCondition(attribute.getName(), attributes);
			// if it's not preserve or default then the rule doesn't apply
			if (preserveWS || defaultWS) {
				ExtractionRule r = new ExtractionRule(startTag.getName().toLowerCase(),
						RULE_TYPE.PRESERVE_WHITESPACE, preserveWS);
				ruleState.pushPreserverWhitespaceRule(r);
				setPreserveWhitespace(ruleState.isPreserveWhitespaceState());
			}
		}
	}

	protected void updateEndTagRuleState(EndTag endTag, ExtractionRule rule) {
		if (rule == null) return;

		switch (rule.ruleType) {
		case INLINE_EXCLUDED_ELEMENT:
		case INLINE_INCLUDED_ELEMENT:
			if (ruleState.peekExcludedIncludedInlineRule() != null) {
				ruleState.popInlineExcludedIncludedRule();
			}
			break;
		case INLINE_ELEMENT:
			if (ruleState.peekInlineRule() != null) {
				ruleState.popInlineRule();
			}
			break;
		case GROUP_ELEMENT:
			if (ruleState.peekGroupRule() != null) {
				ruleState.popGroupRule();
			}
			break;
		case INCLUDED_ELEMENT:
		case EXCLUDED_ELEMENT:
			if (ruleState.peekExcludedIncludedRule() != null) {
				ruleState.popExcludedIncludedRule();
			}
			break;
		case TEXT_UNIT_ELEMENT:
			if (ruleState.peekTextUnitRule() != null) {
				ruleState.popTextUnitRule();
			}
			break;
		default:
			break;
		}

		// if exclude is true by default then pop "not exclude" added in updateStartTagRuleState
		if (getConfig().isGlobalExcludeByDefault()) {
			switch (rule.ruleType) {
				case TEXT_UNIT_ELEMENT:
				case INLINE_ELEMENT:
				case GROUP_ELEMENT:
					ruleState.popExcludedIncludedRule();
					break;
				default:
					break;
			}
		}

		// handle preserve whitespace and other non-main rule types
		EnumSet<RULE_TYPE> ruleTypes = getConfig().getElementRuleTypes(endTag.getName(), false);
		ExtractionRule r = ruleState.peekPreserverWhitespaceRule();
		if (ruleState.isPreserveWhitespaceState() && ruleTypes.contains(RULE_TYPE.PRESERVE_WHITESPACE) && isMatchedTag(r, endTag)) {
			ruleState.popPreserverWhitespaceRule();
			setPreserveWhitespace(ruleState.isPreserveWhitespaceState());
			return;
		}
	}

	public boolean isMatchedTag(ExtractionRule currentState, EndTag endTag) {
		if (currentState == null) {
			return false;
		}

		if (endTag.getName().equalsIgnoreCase(currentState.ruleName)) {
			return true;
		}
		return false;
	}

	/*
	 * catch tags which are not listed in the config but have attributes that require processing
	 */
	private void handleAttributesThatAppearAnywhere(List<PropertyTextUnitPlaceholder> propertyTextUnitPlaceholders, StartTag tag) {

		// if a pcdata subfilter is configured, handle the attributes of the start tag
		if (pcdataFilter != null && tag.getAttributes().length() != 0) {
			handleAttributeSubfiltering(tag);
			return;
		}

		ExtractionRule rule = getMainElementRule(tag);
		switch (rule.ruleApplies ? rule.ruleType : RULE_TYPE.RULE_NOT_FOUND) {
		case TEXT_UNIT_ELEMENT:
			if (propertyTextUnitPlaceholders != null && !propertyTextUnitPlaceholders.isEmpty()) {
				startTextUnit(new GenericSkeleton(tag.toString()), propertyTextUnitPlaceholders);
			} else {
				if (!tag.isSyntacticalEmptyElementTag()) {
					startTextUnit(new GenericSkeleton(tag.toString()));
				} else {
					addToDocumentPart(tag.toString());
				}
			}
			break;
		case GROUP_ELEMENT:
			if (propertyTextUnitPlaceholders != null && !propertyTextUnitPlaceholders.isEmpty()) {
				startGroup(new GenericSkeleton(tag.toString()), getConfig().getElementType(tag),
						getSrcLoc(), propertyTextUnitPlaceholders);
			} else {
				if (!tag.isSyntacticalEmptyElementTag()) {
					startGroup(new GenericSkeleton(tag.toString()), getConfig().getElementType(tag));
				} else {
					addToDocumentPart(tag.toString());
				}
			}
			break;
		default:
			if (propertyTextUnitPlaceholders != null && !propertyTextUnitPlaceholders.isEmpty()) {
				startDocumentPart(tag.toString(), tag.getName(), propertyTextUnitPlaceholders);
				endDocumentPart();
			} else {
				addToDocumentPart(tag.toString());
			}
			break;
		}
	}

	/**
	 * Handles the subfiltering of all attributes in 'startTag'.
	 *
	 * @param startTag the Jericho StartTag that contains the attributes
	 */
	private void handleAttributeSubfiltering(StartTag startTag) {

		// begin start tag
		addToDocumentPart("<" + startTag.getNameSegment().toString() + " ");
		endDocumentPart();

		HashMap<String, String> attributeMap = new HashMap<>();
		for (Attribute attr : startTag.parseAttributes()) {
			attributeMap.clear();
			ExtractionRule attrRule = getMainAttributeRule(startTag, attr.getName(),
					startTag.getAttributes().populateMap(attributeMap, true));

			// subfilter the attribute only if it was declared in the config
			// file as a translatable attribute
			if (attrRule.ruleType == RULE_TYPE.ATTRIBUTE_TRANS) {
				// begin attribute
				addToDocumentPart(attr.getName() + "=" + attr.getQuoteChar());
				endDocumentPart();

				String parentId = eventBuilder.findMostRecentParentId();
				if (parentId == null) parentId = getDocumentId().getLastId();

				String parentName = eventBuilder.findMostRecentParentName();
				if (parentName == null) parentName = getDocumentId().getLastId();

				// apply subfilter to attribute
				try (ISubFilter pcdataSubfilter = ISubFilter.create(pcdataFilter,
						new XMLEncoder(getEncoding(), getNewlineType(),
								true,
								true,
								false,
								QuoteMode.fromValue(this.getConfig().getQuoteMode())),
						++pcdataSectionIndex,
						parentId,
						parentName)) {
					eventBuilder.addFilterEvents(pcdataSubfilter.getEvents(new RawDocument(attr.getValue(), getSrcLoc(), getTrgLoc())));

					// end attribute
					addToDocumentPart(attr.getQuoteChar() + " ");
				}

			} else {
				addToDocumentPart(attr.toString() + " ");
			}
		}

		// close start tag
		if (startTag.isSyntacticalEmptyElementTag()) {
			addToDocumentPart("/>");
		} else {
			addToDocumentPart(">");
		}
		endDocumentPart();
	}

	/**
	 * Handle end tags, including empty tags.
	 *
	 * @param endTag
	 */
	protected void handleEndTag(EndTag endTag) {
		ExtractionRule rule = getMainElementRule(endTag);

		// if in excluded state everything is skeleton including text
		if (ruleState.isExcludedState()) {
			addToDocumentPart(endTag.toString());
			updateEndTagRuleState(endTag, rule);
			return;
		}

		// check to see if we are inside an inline run that is excluded
		if (ruleState.isInlineExcludedState()) {
			eventBuilder.appendCodeInlineExcludedData(endTag.toString());
			updateEndTagRuleState(endTag, rule);
			return;
		}

		switch ((rule != null && rule.ruleApplies) ? rule.ruleType : RULE_TYPE.RULE_NOT_FOUND) {
		case INLINE_ELEMENT:
		case INLINE_INCLUDED_ELEMENT:
			if (canStartNewTextUnit()) {
				startTextUnit();
			}
			addCodeToCurrentTextUnit(endTag);
			break;
		case GROUP_ELEMENT:
			if (isInsideTextRun()) {
				eventBuilder.endTextUnit();
			}

			if (eventBuilder.isCurrentGroup()) {
				endGroup(new GenericSkeleton(endTag.toString()));
			} else {
				addToDocumentPart(endTag.toString());
			}
			break;
		case TEXT_UNIT_ELEMENT:
			if (!isInsideTextRun()) {
				endTextUnit(new GenericSkeleton(endTag.toString()));
				break;
			}
			// if a pcdata subfilter is configured let it do the processsing
			if (pcdataFilter != null) {
				// remove the TextUnit we have accumulated since the start tag
				ITextUnit pcdata = peekTempEvent().getTextUnit();
				String parentId = eventBuilder.findMostRecentParentId();
				if (parentId == null) {
					parentId = pcdata.getId();
				}
				if (parentId == null) {
					parentId = getDocumentId().getLastId();
				}

				String parentName = eventBuilder.findMostRecentParentName();
				if (parentName == null) parentName = pcdata.getType(); // tag name
				if (parentName == null) parentName = getDocumentId().getLastId();

				try (ISubFilter pcdataSubfilter = ISubFilter.create(pcdataFilter,
						new XMLEncoder(getEncoding(), getNewlineType(), true, true, false,
									   QuoteMode.UNESCAPED),
						++pcdataSectionIndex, parentId, parentName)) {

					eventBuilder.convertTempTextUnitToDocumentPart();
					eventBuilder.addFilterEvents(pcdataSubfilter.getEvents(new RawDocument(pcdata.getSource().toString(), getSrcLoc(), getTrgLoc())));
					addToDocumentPart(pcdataSubfilter.createRefCode().toString());
					addToDocumentPart(endTag.toString());
				}
			} else {
				// Check for textunits that are empty
				ITextUnit tempTu = peekTempEvent().getTextUnit();
				if (!tempTu.getSource().hasCode() && !tempTu.getSource().hasText(isPreserveWhitespace())) {
					// Tag-only segment.  Don't expose for translation.
					eventBuilder.convertTempTextUnitToDocumentPart();
					addToDocumentPart(endTag.toString());
				}
				else {
					endTextUnit(new GenericSkeleton(endTag.toString()));
				}
			}
			break;
		default:
			addToDocumentPart(endTag.toString());
			break;
		}

		updateEndTagRuleState(endTag, rule);
	}

	/**
	 * Handle anything else not classified by Jericho.
	 *
	 * @param tag
	 */
	protected void handleDocumentPart(Tag tag) {
		addToDocumentPart(tag.toString());
	}

	/**
	 * Some attributes names are converted to Okapi standards such as HTML charset to "encoding" and lang to "language"
	 *
	 * @param attrName
	 *            - the attribute name
	 * @param attrValue
	 *            - the attribute value
	 * @param tag
	 *            - the Jericho {@link Tag} that contains the attribute
	 * @return the attribute name after it as passe through the normalization rules
	 */
	abstract protected String normalizeAttributeName(String attrName, String attrValue, Tag tag);

	/**
	 * Add an {@link Code} to the current {@link TextUnit}. Throws an exception if there is no current {@link TextUnit}.
	 *
	 * @param tag
	 *            - the Jericho {@link Tag} that is converted to a Okpai {@link Code}
	 */
	protected void addCodeToCurrentTextUnit(Tag tag) {
		addCodeToCurrentTextUnit(tag, true);
	}

	/**
	 * Filter specific method for determining {@link TextFragment.TagType}
	 * @param tag Jericho {@link Tag} start or end tag
	 * @return PLACEHOLDER, OPEN, CLOSED {@link TextFragment.TagType}
	 */
	protected TextFragment.TagType determineTagType(Tag tag) {
		TextFragment.TagType codeType;

		// start tag or empty tag
		if (tag.getTagType() == StartTagType.NORMAL
				|| tag.getTagType() == StartTagType.UNREGISTERED) {
			StartTag startTag = ((StartTag) tag);

			// is this an empty tag?
			if (startTag.isSyntacticalEmptyElementTag()) {
				codeType = TextFragment.TagType.PLACEHOLDER;
			} else {
				if (ruleState.isInlineExcludedState()) {
					codeType = TextFragment.TagType.PLACEHOLDER;
				} else {
					codeType = TextFragment.TagType.OPENING;
				}
			}
		} else { // end or unknown tag
			if (tag.getTagType() == EndTagType.NORMAL
					|| tag.getTagType() == EndTagType.UNREGISTERED) {
				codeType = TextFragment.TagType.CLOSING;
			} else {
				codeType = TextFragment.TagType.PLACEHOLDER;
			}
		}

		return codeType;
	}


	/**
	 * Add an {@link Code} to the current {@link TextUnit}. Throws an exception if there is no current {@link TextUnit}.
	 *
	 * @param tag
	 *            - the Jericho {@link Tag} that is converted to a Okpai {@link Code}
	 * @param endCodeNow
	 *            - do we end the code now or delay so we can add more content to the code?
	 *
	 */
	protected void addCodeToCurrentTextUnit(Tag tag, boolean endCodeNow) {
		List<PropertyTextUnitPlaceholder> propertyTextUnitPlaceholders;
		String literalTag = tag.toString();
		TextFragment.TagType codeType = determineTagType(tag);

		Code codeToAdd = new Code(codeType, getConfig().getElementType(tag), literalTag);
		// start tag or empty tag
		if (tag.getTagType() == StartTagType.NORMAL
				|| tag.getTagType() == StartTagType.UNREGISTERED) {
			StartTag startTag = ((StartTag) tag);

			// create a list of Property or Text placeholders for this tag
			// If this list is empty we know that there are no attributes that
			// need special processing
			propertyTextUnitPlaceholders = createPropertyTextUnitPlaceholders(startTag);
			if (propertyTextUnitPlaceholders != null && !propertyTextUnitPlaceholders.isEmpty()
					&& !ruleState.isInlineExcludedState()) {
				// add code and process actionable attributes
				addToTextUnit(codeToAdd, endCodeNow, propertyTextUnitPlaceholders);
			} else {
				// no actionable attributes, just add the code as-is
				addToTextUnit(codeToAdd, endCodeNow);
			}
		} else { // end or unknown tag
			addToTextUnit(codeToAdd);
		}
	}

	/**
	 * For the given Jericho {@link StartTag} parse out all the actionable attributes and and store them as
	 * {@link PropertyTextUnitPlaceholder}. {@link PlaceholderAccessType} are set based on the filter configuration for
	 * each attribute. for the attribute name and value.
	 *
	 * @param startTag
	 *            - Jericho {@link StartTag}
	 * @return all actionable (translatable, writable or read-only) attributes found in the {@link StartTag}
	 */
	protected List<PropertyTextUnitPlaceholder> createPropertyTextUnitPlaceholders(StartTag startTag) {
		// list to hold the properties or TextUnits
		List<PropertyTextUnitPlaceholder> propertyOrTextUnitPlaceholders = new LinkedList<>();
		HashMap<String, String> attributeMap = new HashMap<>();
		for (Attribute attribute : startTag.parseAttributes()) {
			attributeMap.clear();

			// even if we match an attribute only process it if it has a viable value
			if (Util.isEmpty(attribute.getValue()) || Util.isEmpty(attribute.getValueSegment().toString())) {
			    continue;
			}

			ExtractionRule attributeRule = getMainAttributeRule(startTag, attribute.getName(),
					startTag.getAttributes().populateMap(attributeMap, true));
			switch (attributeRule.ruleType) {
				case ATTRIBUTE_TRANS:
				propertyOrTextUnitPlaceholders.add(createPropertyTextUnitPlaceholder(
						PlaceholderAccessType.TRANSLATABLE, attribute.getName(),
						attribute.getValue(), startTag, attribute));
				break;
			case ATTRIBUTE_WRITABLE:
				// for these non-translatable (but localizable) attributes use the raw value
				// given by attribute.getValueSegment() to avoid any entity unescaping that might
				// produce illegal output
				propertyOrTextUnitPlaceholders.add(createPropertyTextUnitPlaceholder(
						PlaceholderAccessType.WRITABLE_PROPERTY, attribute.getName(),
						attribute.getValueSegment().toString(), startTag, attribute));
				break;
			case ATTRIBUTE_READONLY:
				propertyOrTextUnitPlaceholders.add(createPropertyTextUnitPlaceholder(
						PlaceholderAccessType.READ_ONLY_PROPERTY, attribute.getName(),
						attribute.getValue(), startTag, attribute));
				break;
			case ATTRIBUTE_ID:
				propertyOrTextUnitPlaceholders.add(createPropertyTextUnitPlaceholder(
						PlaceholderAccessType.NAME, attribute.getName(), attribute.getValue(),
						startTag, attribute));
				currentId = attribute.getValue() + "-" + attribute.getName();
				break;
			case ATTRIBUTE_PRESERVE_WHITESPACE:
				propertyOrTextUnitPlaceholders.add(createPropertyTextUnitPlaceholder(
						PlaceholderAccessType.WRITABLE_PROPERTY, attribute.getName(),
						attribute.getValue(), startTag, attribute));
				break;
			default:
				break;
			}
		}

		return propertyOrTextUnitPlaceholders;
	}

	/**
	 * Create a {@link PropertyTextUnitPlaceholder} given the supplied type, name and Jericho {@link Tag} and
	 * {@link Attribute}.
	 *
	 * @param type
	 *            - {@link PlaceholderAccessType} is one of TRANSLATABLE, READ_ONLY_PROPERTY, WRITABLE_PROPERTY
	 * @param name
	 *            - attribute name
	 * @param value
	 *            - attribute value
	 * @param tag
	 *            - Jericho {@link Tag} which contains the attribute
	 * @param attribute
	 *            - attribute as a Jericho {@link Attribute}
	 * @return a {@link PropertyTextUnitPlaceholder} representing the attribute
	 */
	protected PropertyTextUnitPlaceholder createPropertyTextUnitPlaceholder(
			PlaceholderAccessType type, String name, String value, Tag tag, Attribute attribute) {
		// offset of attribute
		int mainStartPos = attribute.getBegin() - tag.getBegin();
		int mainEndPos = attribute.getEnd() - tag.getBegin();

		// offset of value of the attribute
		int valueStartPos = attribute.getValueSegment().getBegin() - tag.getBegin();
		int valueEndPos = attribute.getValueSegment().getEnd() - tag.getBegin();

		return new PropertyTextUnitPlaceholder(type, normalizeAttributeName(name, value, tag),
				value, mainStartPos, mainEndPos, valueStartPos, valueEndPos);
	}

	/**
	 * Is the input encoded as UTF-8?
	 *
	 * @return true if the document is in utf8 encoding.
	 */
	@Override
	protected boolean isUtf8Encoding() {
		return hasUtf8Encoding;
	}

	/**
	 * Does the input have a UTF-8 Byte Order Mark?
	 *
	 * @return true if the document has a utf-8 byte order mark.
	 */
	@Override
	protected boolean isUtf8Bom() {
		return hasUtf8Bom;
	}

	/**
	 * Does the input have a BOM?
	 *
	 * @return true if the document has a BOM.
	 */
	protected boolean isBOM() {
		return hasBOM;
	}

	/**
	 * Does this document have a document encoding specified?
	 * @return true if has meta tag with encoding, false otherwise
	 */
	protected boolean isDocumentEncoding() {
		return documentEncoding;
	}

	/**
	 *
	 * @return the preserveWhitespace boolean.
	 */
	protected boolean isPreserveWhitespace() {
		return ruleState.isPreserveWhitespaceState();
	}

	protected void setPreserveWhitespace(boolean preserveWhitespace) {
		eventBuilder.setPreserveWhitespace(preserveWhitespace);
	}

	protected void addToDocumentPart(String part) {
		if (Util.isEmpty(part)) return;
		eventBuilder.addToDocumentPart(part);
	}

	protected void addToTextUnit(String text) {
		if (Util.isEmpty(text)) return;
		eventBuilder.addToTextUnit(text);
	}

	protected void startTextUnit(String text) {
		if (Util.isEmpty(text)) return;
		eventBuilder.startTextUnit(text);
		setTextUnitName(currentId);
	}

	protected void setTextUnitName(String name) {
		String n = name;
		if (name == null) {
			// we have no name for this TU but lets check for a parent
			// name. All children will inherit this
			n = eventBuilder.findMostRecentTextUnitName();
		}
		eventBuilder.setTextUnitName(n);
		currentId = null;
	}

	protected void setTextUnitType(String type) {
		eventBuilder.setTextUnitType(type);
	}

	protected void setCurrentDocName(String currentDocName) {
		this.currentDocName = currentDocName;
	}

	protected String getCurrentDocName() {
		return currentDocName;
	}

	protected boolean canStartNewTextUnit() {
		return eventBuilder.canStartNewTextUnit();
	}

	protected boolean isInsideTextRun() {
		return eventBuilder.isInsideTextRun();
	}

	protected void addToTextUnit(Code code, boolean endCodeNow) {
		eventBuilder.addToTextUnit(code, endCodeNow);
	}

	protected void addToTextUnit(Code code) {
		eventBuilder.addToTextUnit(code);
	}

	protected void addToTextUnit(Code code, boolean endCodeNow,
			List<PropertyTextUnitPlaceholder> propertyTextUnitPlaceholders) {
		eventBuilder.addToTextUnit(code, endCodeNow, propertyTextUnitPlaceholders);
	}

	protected void endDocumentPart() {
		eventBuilder.endDocumentPart();
	}

	protected void startDocumentPart(String part, String name,
			List<PropertyTextUnitPlaceholder> propertyTextUnitPlaceholders) {
		eventBuilder.startDocumentPart(part, name, propertyTextUnitPlaceholders);
	}

	protected void startGroup(GenericSkeleton startMarker, String commonTagType) {
		eventBuilder.startGroup(startMarker, commonTagType);
	}

	protected void startGroup(GenericSkeleton startMarker, String commonTagType, LocaleId locale,
			List<PropertyTextUnitPlaceholder> propertyTextUnitPlaceholders) {
		eventBuilder.startGroup(startMarker, commonTagType, locale, propertyTextUnitPlaceholders);
	}

	protected void startTextUnit(GenericSkeleton startMarker) {
		eventBuilder.startTextUnit(startMarker);
		setTextUnitName(currentId);
	}

	protected void startTextUnit(GenericSkeleton startMarker,
			List<PropertyTextUnitPlaceholder> propertyTextUnitPlaceholders) {
		eventBuilder.startTextUnit(startMarker, propertyTextUnitPlaceholders);
		setTextUnitName(currentId);
	}

	protected void endTextUnit(GenericSkeleton endMarker) {
		eventBuilder.endTextUnit(endMarker);
	}

	protected void endGroup(GenericSkeleton endMarker) {
		eventBuilder.endGroup(endMarker);
	}

	protected void startTextUnit() {
		eventBuilder.startTextUnit();
		setTextUnitName(currentId);
	}

	protected long getTextUnitId() {
		return eventBuilder.getTextUnitId();
	}

	protected void setTextUnitMimeType(String mimeType) {
		eventBuilder.setTextUnitMimeType(mimeType);
	}

	protected void setDocumentPartId(long id) {
		eventBuilder.setDocumentPartId(id);
	}

	protected Event peekTempEvent() {
		return eventBuilder.peekTempEvent();
	}

	protected ExtractionRuleState getRuleState() {
		return ruleState;
	}

	/**
	 * @return the eventBuilder
	 */
	public AbstractMarkupEventBuilder getEventBuilder() {
		return eventBuilder;
	}

	/**
	 * Sets the input document mime type.
	 *
	 * @param mimeType
	 *            the new mime type
	 */
	@Override
	public void setMimeType(String mimeType) {
		super.setMimeType(mimeType);
	}
}
