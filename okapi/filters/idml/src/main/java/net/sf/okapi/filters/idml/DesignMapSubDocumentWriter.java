/*
 * =============================================================================
 * Copyright (C) 2010-2022 by the Okapi Framework contributors
 * -----------------------------------------------------------------------------
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =============================================================================
 */
package net.sf.okapi.filters.idml;

import net.sf.okapi.common.Event;
import net.sf.okapi.common.IParameters;
import net.sf.okapi.common.ISkeleton;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.Util;
import net.sf.okapi.common.encoder.EncoderManager;
import net.sf.okapi.common.exceptions.OkapiIOException;
import net.sf.okapi.common.filterwriter.IFilterWriter;
import net.sf.okapi.common.resource.ITextUnit;
import net.sf.okapi.common.skeleton.ISkeletonWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.stream.XMLEventWriter;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.XMLEvent;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

final class DesignMapSubDocumentWriter implements IFilterWriter {
    private static final String ERROR_CREATING_SUB_DOCUMENT_WRITER = "Error creating sub document writer";
    private static final String ERROR_ADDING_EVENTS_TO_SUB_DOCUMENT_WRITER = "Error adding events to sub document writer";
    private static final String ERROR_CLOSING_SUB_DOCUMENT_WRITER = "Error closing sub document writer";
    private static final String ERROR_CLOSING_OUTPUT_STREAM = "Error closing output stream";

    private final Logger logger;
    private final Parameters parameters;
    private final XMLOutputFactory outputFactory;
    private final String outputPath;
    private final String encoding;
    private final TextSkeletonMerging textSkeletonMerging;
    private XMLEventWriter eventWriter;
    private OutputStream outputStream;

    DesignMapSubDocumentWriter(
        final Parameters parameters,
        final XMLOutputFactory outputFactory,
        final String encoding,
        final String outputPath,
        final TextSkeletonMerging textSkeletonMerging
    ) {
        this.logger = LoggerFactory.getLogger(getClass());
        this.parameters = parameters;
        this.outputFactory = outputFactory;
        this.encoding = encoding;
        this.outputPath = outputPath;
        this.textSkeletonMerging = textSkeletonMerging;
    }

    @Override
    public String getName() {
        return getClass().getSimpleName();
    }

    @Override
    public void setOptions(LocaleId locale, String defaultEncoding) {
    }

    @Override
    public void setOutput(String path) {
    }

    @Override
    public void setOutput(OutputStream output) {
    }

    @Override
    public Event handleEvent(Event event) {
        switch (event.getEventType()) {
            case START_DOCUMENT:
                createOutputStreamAndEventWriter();
                break;
            case DOCUMENT_PART:
                handleDocumentPartEvent(event);
                break;
            case TEXT_UNIT:
                handleTextUnitEvent(event);
                break;
            case END_DOCUMENT:
                close();
                break;
            case START_GROUP:
            case END_GROUP:
            case MULTI_EVENT:
            case START_SUBDOCUMENT:
            case END_SUBDOCUMENT:
            case START_SUBFILTER:
            case END_SUBFILTER:
            default:
                break;
        }
        return event;
    }

    private void createOutputStreamAndEventWriter() {
        Util.createDirectories(this.outputPath);
        try {
            this.outputStream = new FileOutputStream(this.outputPath);
            this.eventWriter = this.outputFactory.createXMLEventWriter(this.outputStream, this.encoding);
        } catch (XMLStreamException | FileNotFoundException e) {
            throw new OkapiIOException(ERROR_CREATING_SUB_DOCUMENT_WRITER, e);
        }
    }

    private void handleDocumentPartEvent(final Event event) {
        final ISkeleton skeleton = event.getDocumentPart().getSkeleton();
        try {
            if (skeleton instanceof MarkupSkeleton) {
                write(((MarkupSkeleton) skeleton).getMarkup().getEvents());
            } else {
                throw new IllegalStateException(ParsingIdioms.UNEXPECTED_STRUCTURE);
            }
        } catch (XMLStreamException e) {
            throw new OkapiIOException(ERROR_ADDING_EVENTS_TO_SUB_DOCUMENT_WRITER, e);
        }
    }

    private void handleTextUnitEvent(final Event event) {
        final ITextUnit tu = event.getTextUnit();
        this.textSkeletonMerging.performFor(tu);
        try {
            write(((TextSkeleton) tu.getSkeleton()).element().getEvents());
        } catch (XMLStreamException e) {
            throw new OkapiIOException(ERROR_ADDING_EVENTS_TO_SUB_DOCUMENT_WRITER, e);
        }
    }

    private void write(final List<XMLEvent> events) throws XMLStreamException {
        for (final XMLEvent event : events) {
            this.eventWriter.add(event);
        }
    }

    @Override
    public void close() {
        try {
            if (this.eventWriter != null) {
                this.eventWriter.close();
            }
            if (this.outputStream != null) {
                try {
                    this.outputStream.close();
                } catch (IOException e) {
                    this.logger.warn(ERROR_CLOSING_OUTPUT_STREAM, e);
                }
                this.outputStream = null;
            }
        } catch (XMLStreamException e) {
            throw new OkapiIOException(ERROR_CLOSING_SUB_DOCUMENT_WRITER, e);
        }
    }

    @Override
    public Parameters getParameters() {
        return parameters;
    }

    @Override
    public void setParameters(IParameters params) {
    }

    @Override
    public void cancel() {
    }

    @Override
    public EncoderManager getEncoderManager() {
        return null;
    }

    @Override
    public ISkeletonWriter getSkeletonWriter() {
        return null;
    }
}

