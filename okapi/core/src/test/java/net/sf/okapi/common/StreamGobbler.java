package net.sf.okapi.common;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;

public class StreamGobbler extends Thread {
    private final InputStream inputStream;
    private final PrintStream outputStream;

    public StreamGobbler(final InputStream inputStream, final PrintStream outputStream) {
        this.inputStream = inputStream;
        this.outputStream = outputStream;
    }

    public void run() {
        try {
            final InputStreamReader isr = new InputStreamReader(this.inputStream);
            final BufferedReader br = new BufferedReader(isr);
            String s;
            while ((s = br.readLine()) != null) {
                this.outputStream.println(s);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
