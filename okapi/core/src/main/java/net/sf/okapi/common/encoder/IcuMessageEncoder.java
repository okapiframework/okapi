/*===========================================================================
  Copyright (C) 2008-2013 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
===========================================================================*/

package net.sf.okapi.common.encoder;

import net.sf.okapi.common.IParameters;

import java.nio.charset.Charset;
import java.nio.charset.CharsetEncoder;
import java.nio.charset.StandardCharsets;
import java.util.Stack;

/**
 * Implements {@link IEncoder} for ICU Message string format.
 */
public class IcuMessageEncoder implements IEncoder {

	private CharsetEncoder chsEnc;
	private String lineBreak = "\n";
	private String encoding;
	private IParameters params;

	public IcuMessageEncoder() {
		chsEnc = StandardCharsets.UTF_8.newEncoder();
	}

	@Override
	public void reset() {
	}

	@Override
	public void setOptions(IParameters params, String encoding, String lineBreak) {
		chsEnc = Charset.forName(encoding).newEncoder();
		this.lineBreak = lineBreak;
		this.encoding = encoding;
		this.params = params;
		// Get the output options
		if (params != null) {
		}
	}

	@Override
	public String encode(String text, EncoderContext context) {
		if (text == null || text.isEmpty()) {
			return text;
		}
		if (text == null || text.isEmpty()) {
			return text;
		}
		return encode(text);
	}

	/**
	 * Escapes ICU messages by adding single quotes around
	 * the longest common subsequence of curly braces and
	 * doubling single quotes.
	 *
	 * @param input the input string to escape
	 * @return the escaped string
	 */
	private String encode(String input) {
		StringBuilder sb = new StringBuilder();
		Stack<Integer> stack = new Stack<>();

		int i = 0;
		while (i < input.length()) {
			char c = input.charAt(i);
			if (c == '{') {
				// Add single quote if stack is empty
				if (stack.isEmpty()) {
					sb.append("'");
				}
				stack.push(sb.length());
				sb.append(c);
			} else if (c == '}') {
				// Append closing brace and add single quote if stack is not empty
				if (!stack.isEmpty()) {
					sb.append(c);
					int start = stack.pop();
					if (stack.isEmpty()) {
						sb.append("'");
					}
				}
			} else if (c == '\'') {
				// Double single quote if stack is empty
				if (stack.isEmpty()) {
					sb.append("''");
				} else {
					sb.append(c);
				}
			} else {
				sb.append(c);
			}
			i++;
		}

		return sb.toString();
	}

	@Override
	public String encode(char value, EncoderContext context) {
        return _encode(value, context);
	}

	protected String _encode(char value, EncoderContext context) {
		if (value > 127) {
			// Store high surrogate for future use
			if (Character.isHighSurrogate(value)) {
				return "";
			}
			return String.valueOf(value);
		} else {
			switch (value) {
				case '\'':
					return "''";
				case '{':
					return "'{'";
				case '}':
					return "'}'";
				default:
					return String.valueOf(value);
			}
		}
	}

	@Override
	public String encode(int value, EncoderContext context) {
		if (Character.isSupplementaryCodePoint(value)) {
            return new String(Character.toChars(value));
		}
		return encode((char) value, context);
	}

	@Override
	public String toNative(String propertyName, String value) {
		// No changes for the other values
		return value;
	}

	@Override
	public String getLineBreak() {
		return lineBreak;
	}

	@Override
	public CharsetEncoder getCharsetEncoder() {
		return chsEnc;
	}

	@Override
	public IParameters getParameters() {
		return params;
	}

	@Override
	public String getEncoding() {
		return encoding;
	}
}
