/*===========================================================================
  Copyright (C) 2016-2017 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
===========================================================================*/

package net.sf.okapi.common.resource;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import static net.sf.okapi.common.resource.Code.EXTENDED_CODE_TYPE_DELIMITER;
import static net.sf.okapi.common.resource.Code.EXTENDED_CODE_TYPE_PREFIX;
import static net.sf.okapi.common.resource.Code.EXTENDED_CODE_TYPE_VALUE_DELIMITER;

/**
 * Provides a code type builder.
 */
public class CodeTypeBuilder {
    private static final String EMPTY_VALUE = "";
    private static final String VALUES_DELIMITER = ",";

    private final Set<String> codeTypes;
    private final Map<String, String> codeTypesAndValues;
    private final boolean addExtendedCodeTypePrefix;

    public CodeTypeBuilder(final boolean addExtendedCodeTypePrefix) {
        this(
            new LinkedHashSet<>(),
            new LinkedHashMap<>(),
            addExtendedCodeTypePrefix
        );
    }

    public CodeTypeBuilder(
        final Set<String> codeTypes,
        final Map<String, String> codeTypesAndValues,
        final boolean addExtendedCodeTypePrefix
    ) {
        this.codeTypes = codeTypes;
        this.codeTypesAndValues = codeTypesAndValues;
        this.addExtendedCodeTypePrefix = addExtendedCodeTypePrefix;
    }

    public void addType(String type) {
        codeTypes.add(type);
    }

    public void addType(String type, String value) {
        final String currentValue = this.codeTypesAndValues.get(type);
        if (null == currentValue) {
            this.codeTypesAndValues.put(type, value);
        } else {
            final Set<String> values = new LinkedHashSet<>(Arrays.asList(currentValue.split(VALUES_DELIMITER)));
            if (!values.contains(value)) {
                values.add(value);
                this.codeTypesAndValues.put(type, String.join(VALUES_DELIMITER, values));
            }
        }
    }

    public String build() {
        if (codeTypes.isEmpty() && codeTypesAndValues.isEmpty()) {
            return EMPTY_VALUE;
        }

        StringBuilder codeTypeBuilder = new StringBuilder(addExtendedCodeTypePrefix ? EXTENDED_CODE_TYPE_PREFIX : EMPTY_VALUE);

        for (String codeType : codeTypes) {
            codeTypeBuilder.append(codeType).append(EXTENDED_CODE_TYPE_DELIMITER);
        }

        for (Map.Entry<String, String> codeTypeAndValue : codeTypesAndValues.entrySet()) {
            codeTypeBuilder.append(codeTypeAndValue.getKey()).append(EXTENDED_CODE_TYPE_VALUE_DELIMITER)
                    .append(codeTypeAndValue.getValue()).append(EXTENDED_CODE_TYPE_DELIMITER);
        }

        return codeTypeBuilder.toString();
    }
}
