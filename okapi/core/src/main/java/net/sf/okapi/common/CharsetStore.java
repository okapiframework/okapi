package net.sf.okapi.common;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

/**
 * Purpose of this class is to have it “hidden” inside existing classes that have charsets to reconcile
 * situations when one does a setCharset (or in constructor) with a Charset and then a
 * getCharset as a String. Or the other way around.
 * Example:
 * <p>
 * <code>
 * Foo foo = new Foo(…, "utf-8");
 * Charset cs = foo.getCharset();
 * String srtCs = foo.CharsetAsString();
 * <code/>
 * And that would open the door to adding more public APIs taking / returning Charset
 */
public class CharsetStore {
    public final static CharsetStore UTF_8 = new CharsetStore(StandardCharsets.UTF_8);
    public final static CharsetStore UTF_16 = new CharsetStore(StandardCharsets.UTF_16);
    public final static CharsetStore UTF_16BE = new CharsetStore(StandardCharsets.UTF_16BE);
    public final static CharsetStore UTF_16LE = new CharsetStore(StandardCharsets.UTF_16LE);
    public final static String UNKNOWN_CHARSET_NAME = "null";
    public final static CharsetStore UNKNOWN = new CharsetStore(UNKNOWN_CHARSET_NAME);
    private final Logger logger = LoggerFactory.getLogger(getClass());
    private String charsetName;
    private Charset charset;

    public CharsetStore(Charset charset) {
        if (charset == null) {
            logger.warn("CharsetStore constructor called with null Charset");
            this.charset = null;
            this.charsetName = UNKNOWN_CHARSET_NAME;
        } else {
            this.charset = charset;
            this.charsetName = null;
        }
    }

    public CharsetStore(String charsetName) {
        if (charsetName == null) {
            logger.warn("CharsetStore constructor called with null charsetName");
            this.charset = null;
            this.charsetName = UNKNOWN_CHARSET_NAME;
        } else {
            this.charsetName = charsetName;
            this.charset = null;
        }
    }

    public Charset getCharset() {
        if (charset == null) {
            charset = Charset.forName(charsetName);
        }
        return charset;
    }

    public String getCharsetName() {
        if (charsetName == null) {
            charsetName = charset.name();
        }
        return charsetName;
    }

    @Override
    public String toString() {
        return getCharsetName();
    }
}
