/*
 * =============================================================================
 * Copyright (C) 2010-2023 by the Okapi Framework contributors
 * -----------------------------------------------------------------------------
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =============================================================================
 */
package net.sf.okapi.common.encoder;

import net.sf.okapi.common.IParameters;

public class BypassEncoder implements IEncoder {
    @Override
    public void reset() {
    }

    @Override
    public void setOptions(final IParameters params, final String encoding, final String lineBreak) {
    }

    @Override
    public String encode(final String text, final EncoderContext context) {
        return text;
    }

    @Override
    public String encode(final int codePoint, final EncoderContext context) {
        if (Character.isSupplementaryCodePoint(codePoint)) {
            return encode(new String(Character.toChars(codePoint)), context);
        }
        return encode((char) codePoint, context);
    }

    @Override
    public String encode(final char value, final EncoderContext context) {
        return String.valueOf(value);
    }

    @Override
    public IParameters getParameters() {
        return null;
    }
}
