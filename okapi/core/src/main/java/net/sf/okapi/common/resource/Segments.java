/*===========================================================================
  Copyright (C) 2010 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
===========================================================================*/


package net.sf.okapi.common.resource;

import net.sf.okapi.common.IResource;
import net.sf.okapi.common.Range;
import net.sf.okapi.common.Util;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

public class Segments implements ISegments {
	// assume segments are always aligned when created
	private AlignmentStatus alignmentStatus = AlignmentStatus.ALIGNED;
	private TextContainer parent;
	private List<TextPart> parts;

	public Segments() {
	}

    /**
	 * Creates an uninitialized Segments object.
	 * <p>
	 * <b>IMPORTANT:</b> setParts() must be called with a non-null argument before
	 * calling any other methods.
	 * 
	 * @param parent the parent {@link TextContainer}.
	 */
	public Segments(TextContainer parent) {
		this.parent = parent;

	}

	/**
	 * Sets the list of TextPart objects in which the segments for this Segments
	 * object are located. Parts must be set after construction before any other
	 * methods are invoked.
	 *
	 * @param parts the list of {@link TextPart}s where the segments are stored.
	 */
	public void setParts(List<TextPart> parts) {
		this.parts = parts;
	}

	@Override
	public Iterator<Segment> iterator() {
		return new Iterator<>() {
			int current = foundNext(-1);

			private int foundNext(int start) {
				for (int i = start + 1; i < parts.size(); i++) {
					if (parts.get(i).isSegment()) {
						return i;
					}
				}
				return -1;
			}

			@Override
			public void remove() {
				throw new UnsupportedOperationException("The method remove() not supported.");
			}

			@Override
			public Segment next() {
				if (current == -1) {
					throw new NoSuchElementException("No more content parts.");
				}
				final int n = current;
				// Get next here because hasNext() could be called several times
				current = foundNext(current);
				// Return 'previous' current
				return (Segment) parts.get(n);
			}

			@Override
			public boolean hasNext() {
				return (current != -1);
			}
		};
	}

	@Override
	public List<Segment> asList() {
		final ArrayList<Segment> segments = new ArrayList<>(parts.size());
		for ( final TextPart part : parts ) {
			if ( part.isSegment() ) {
				segments.add((Segment)part);
			}
		}
		return segments;
	}

	@Override
	public void swap(int segIndex1, int segIndex2) {
		final int partIndex1 = getPartIndex(segIndex1);
		final int partIndex2 = getPartIndex(segIndex2);
		if (( partIndex1 == -1 ) || ( partIndex2 == -1 )) {
			return; // At least one index is wrong: do nothing
		}
		final TextPart tmp = parts.get(partIndex1);
		parts.set(partIndex1, parts.get(partIndex2));
		parts.set(partIndex2, tmp);
	}


	@Override
	public void append(Segment segment, boolean collapseIfPreviousEmpty) {
		append(segment, null, collapseIfPreviousEmpty);
	}

	@Override
	public void append(Segment segment) {
		append(segment, true);
	}

	@Override
	public void append(Segment segment,
			String textBefore,
			boolean collapseIfPreviousEmpty) {
		// Add the text before if needed
		if ( !Util.isEmpty(textBefore) ) {
			if (( parts.get(parts.size()-1).getContent().isEmpty() )
					&& !parts.get(parts.size()-1).isSegment() )
			{
				parts.set(parts.size()-1, new TextPart(textBefore));
			}
			else {
				parts.add(new TextPart(textBefore));
			}
		}

		// If the last segment is empty and at the end of the content: re-use it
		if ( collapseIfPreviousEmpty ) {
			if (( parts.get(parts.size()-1).getContent().isEmpty() )
					&& parts.get(parts.size()-1).isSegment() )
			{
				parts.set(parts.size()-1, segment);
			}
			else {
				parts.add(segment);
			}
		}
		else {
			parts.add(segment);
		}

		validateTextPartId(segment);
		parent.setHasBeenSegmentedFlag(true);
	}

	@Override
	public void append(Segment segment, String textBefore) {
		append(segment, textBefore, true);
	}

	@Override
	public void append(TextFragment fragment, boolean collapseIfPreviousEmpty) {
		append(new Segment(null, fragment), collapseIfPreviousEmpty);
	}

	@Override
	public void append(TextFragment fragment) {
		append(fragment, true);
	}

	@Override
	public void set(int index, Segment seg) {
		final int n = getPartIndex(index);
		if ( n < -1 ) {
			throw new IndexOutOfBoundsException("Invalid segment index: "+index);
		}
		parts.set(n, seg);
		validateTextPartId(seg);
	}

	@Override
	public void insert(int index, Segment seg) {
		// If the index is the one after the last segment: we append
		if ( index == count() ) {
			append(seg, true);
			return;
		}
		// Otherwise it has to exist
		final int n = getPartIndex(index);
		if ( n < -1 ) {
			throw new IndexOutOfBoundsException("Invalid segment index: "+index);
		}
		parts.add(n, seg);
		validateTextPartId(seg);
	}

	@Override
	public int create (List<Range> ranges) {
		return create(ranges, false);
	}

	@Override
	public int create(List<Range> ranges, boolean allowEmptySegments) {
		return create(ranges, allowEmptySegments, MetaCopyStrategy.DEFAULT);
	}

	@Override
	public int create (List<Range> ranges, boolean allowEmptySegments, MetaCopyStrategy strategy)
	{
		// Do nothing if null or empty
		if (( ranges == null ) || ranges.isEmpty() ) return 0;

		List<Range> originalRanges;

		// If the current content is a single segment we start from it
		TextFragment holder;
		if ( parts.size() == 1  ) {
			holder = parts.get(0).getContent();
			originalRanges = List.of(new Range(0, holder.length(), parts.get(0).getId()));
		}
		else {
			originalRanges = new ArrayList<>(parts.size());
			holder = createJoinedContent(originalRanges, true);
		}

		// clone the current parts for the DEEPEN strategy
		List<TextPart> originalParts = null;
		if (strategy == MetaCopyStrategy.DEEPEN) {
			originalParts = new ArrayList<>(parts.size());
			for (TextPart p : parts) {
				originalParts.add(p.clone());
			}
		}

		// Reset the segments
		parts.clear();

		// Extract the segments using the ranges
		int start = 0;
		int id = 0;
		for ( final Range range : ranges ) {
			if ( range.end == -1 ) {
				range.end = holder.text.length();
			}
			// Check boundaries
			if ( range.end < range.start ) {
				throw new InvalidPositionException(String.format(
						"Invalid segment boundaries: start=%d, end=%d.", range.start, range.end));
			}
			if ( start > range.start ) {
				throw new InvalidPositionException("Invalid range order.");
			}
			if ( range.end == range.start ) {
				// If empty segments are not allowed, we skip this one
				if ( !allowEmptySegments ) continue;
				// Otherwise we proceed
			}

			// Create the part for the segment
			// Use existing id if possible, otherwise use local counter
			TextPart p;

			// If there is an interstice: creates the corresponding part
			if ( start < range.start ) {
				p = new TextPart(((range.id == null) ? String.valueOf(id++) : range.id), holder.subSequence(start, range.start));
				validateTextPartId(p);
				parts.add(p);
			}

			// if the range does not store the original part then we assume this is a Segment
			if (range.part == null) {
				p = new Segment(((range.id == null) ? String.valueOf(id++) : range.id),
						holder.subSequence(range.start, range.end));
				validateTextPartId(p);
			} else {
				// since the range carries a part this normally means it's a case where
				// the TextContainer was already segmented when we calculated the ranges, and we want to remember the
				// original TextParts (main use case is ITextUnitMerger)
				if (range.part.isSegment()) {
					p = new Segment(((range.part.id == null) ? String.valueOf(id++) : range.part.id),
							holder.subSequence(range.start, range.end));
					validateTextPartId(p);
				} else {
					p = new TextPart(((range.part.id == null) ? String.valueOf(id++) : range.part.id),
							holder.subSequence(range.start, range.end));
					validateTextPartId(p);
				}
			}
			parts.add(p);
			start = range.end;
			parent.setHasBeenSegmentedFlag(true);
		}

		// Check if we have remaining text after the last segment
		TextPart p;
		if ( start < holder.text.length() ) {
			if ( start == 0 ) { // If the remainder is the whole content: make it a segment
				if (!parts.isEmpty()) {
					p = new TextPart(String.valueOf(++id),holder.subSequence(start, -1));
					validateTextPartId(p);
					parts.add(p);
				}
				else {
					p = new Segment(String.valueOf(++id), holder);
					validateTextPartId(p);
					parts.add(p);
				}
			}
			else { // Otherwise: make it an interstice
				p = new TextPart(String.valueOf(++id),holder.subSequence(start, -1));
				validateTextPartId(p);
				parts.add(p);
			}
		}

		switch(strategy) {
			case DEEPEN:
				// split segments inherit parent metadata and id's are adjusted
				deepenCopyMetaData(originalParts, originalRanges, ranges);
				break;
			case IDENTITY:
				identityCopyMetadata(ranges);
				break;
			case DEFAULT:
				// default case - currently do nothing
				break;
		}

		return parts.size();
	}

	/**
	 * Copy metadata for use cases where the original segments were the exact same as the new ones.
	 * Must be a one to one match between ranges and parts.
	 */
	private void identityCopyMetadata(List<Range> ranges) {
		assert(ranges.size() == parts.size());
		for (int pi = 0; pi < ranges.size(); pi++) {
			Range r = ranges.get(pi);
			TextPart part = parts.get(pi);
			part.id = r.part.id;
			part.originalId = r.part.originalId;
			part.whitespaceStrategy = r.part.whitespaceStrategy;
			IResource.copy(r.part, part);
		}
	}

	private List<Range> fillInRanges(List<Range> ranges) {
		// update ranges to include non-Segments so everything matches up
		List<Range> newRanges = new ArrayList<Range>(ranges.size());
		int ri = 0;
		for (TextPart p : parts) {
			if (p.isSegment()) {
				newRanges.add(ranges.get(ri++));
			} else {
				// this is a TextPart
				if (ri == 0) {
					// this is the first part
					newRanges.add(new Range(0, p.text.length()));
				} else {
					// this is not the first part
					newRanges.add(new Range(newRanges.get(ri - 1).end, newRanges.get(ri - 1).end + p.text.length()));
				}
			}
		}
		return newRanges;
	}

	/**
	 * Copy metadata for deepen existing segmentation use case. Split segments inherit meta from parent segments.
	 */
	private void deepenCopyMetaData(List<TextPart> originalParts, List<Range> originalRanges, List<Range> ranges) {
		List<Range> newRanges = fillInRanges(ranges);
		//we should now have one to one match between ranges and parts
		assert(newRanges.size() == parts.size());
		for (int pi = 0; pi < newRanges.size(); pi++) {
			Range r = newRanges.get(pi);
			for (int oi = 0; oi < originalRanges.size(); oi++) {
				Range op = originalRanges.get(oi);
				TextPart part = parts.get(pi);
				TextPart originalPart = originalParts.get(oi);
				if (op.equals(r)) {
					// Range may already have an id, if so keep it
					part.id = Util.isEmpty(originalPart.id) ? part.id : originalPart.id;
					part.originalId = originalPart.originalId;
					part.whitespaceStrategy = originalPart.whitespaceStrategy;
					IResource.copy(originalPart, part);
				} else if (op.contains(r)) {
					// use case for deepening segmentation (split segments) etc.
					part.id = Util.isEmpty(originalPart.id) ?  part.id : String.format("%s.%d", originalPart.id, pi);
					part.whitespaceStrategy = originalPart.whitespaceStrategy;
					IResource.copy(originalPart, part);
				}
			}
		}
	}

	@Override
	public int create(int start, int end) {
		return create(List.of(new Range(start, end)));
	}

	@Override
	public int count() {
		int count = 0;
		for ( final TextPart part : parts ) {
			if ( part.isSegment() ) {
				count++;
			}
		}
		return count;
	}

	@Override
	public TextFragment getFirstContent() {
		for ( final TextPart part : parts ) {
			if ( part.isSegment() ) {
				return part.getContent();
			}
		}
		// Should never occur
		return null;
	}

	@Override
	public TextFragment getLastContent() {
		for ( int i=parts.size()-1; i>=0; i-- ) {
			if ( parts.get(i).isSegment() ) {
				return parts.get(i).getContent();
			}
		}
		// Should never occur
		return null;
	}

	@Override
	public Segment getLast() {
		for ( int i=parts.size()-1; i>=0; i-- ) {
			if ( parts.get(i).isSegment() ) {
				return (Segment)parts.get(i);
			}
		}
		// Should never occur
		return null;
	}

	@Override
	public Segment get(String id) {
		for ( final TextPart part : parts ) {
			if ( part.isSegment() && part.id.equals(id) ) {
				return (Segment)part;
			}
		}
		// Should never occur
		return null;
	}

	@Override
	public Segment get(int index) {
		int tmp = -1;
		for ( final TextPart part : parts ) {
			if ( part.isSegment() ) {
				if ( ++tmp == index ) {
					return (Segment)part;
				}
			}
		}
		throw new IndexOutOfBoundsException("Index: " + index + ", Size: " + ++tmp);
	}

	@Override
	public void joinAll() {
		// Merge but don't remember the ranges
		//parent.setContent(createJoinedContent(null));
		joinAll(null);
	}

	@Override
	public void joinAll(boolean keepCodeIds) {
		parent.setContent(createJoinedContent(null, keepCodeIds));
	}

	@Override
	public void joinAll(List<Range> ranges) {
		parent.setContent(createJoinedContent(ranges));
	}

	@Override
	public void joinAll(List<Range> ranges, boolean keepCodeIds) {
		parent.setContent(createJoinedContent(ranges, keepCodeIds));
	}

	@Override
	public List<Range> getRanges() {
		return this.getRanges(false);
	}

	@Override
	public List<Range> getRanges(boolean keepCodeIds) {
		final List<Range> ranges = new ArrayList<>(parts.size());
		createJoinedContent(ranges, keepCodeIds);
		return ranges;
	}

	@Override
	public int joinWithNext(int segmentIndex) {
		return joinWithNext(segmentIndex, false);
	}

	@Override
	public int joinWithNext(int segmentIndex, boolean keepCodeIds) {
		// Check if we have something to join to
		if (parts.size() == 1) {
			return 0; // Nothing to do
		}

		// Find the part for the segment index
		final int start = getPartIndex(segmentIndex);
		// Check if we have a segment at such index
		if (start == -1) {
			return 0; // Not found
		}

		// Find the next segment
		int end = -1;
		for (int i = start + 1; i < parts.size(); i++) {
			if (parts.get(i).isSegment()) {
				end = i;
				break;
			}
		}

		// Check if we have a next segment
		if (end == -1) {
			// No more segment to join
			return 0;
		}

		final TextFragment tf = parts.get(start).getContent();
		final int count = (end - start);
		int i = 0;
		while (i < count) {
			tf.append(parts.get(start + 1).getContent(), keepCodeIds);
			parts.remove(start + 1);
			i++;
		}

		// Do not reset segApplied if one part only: keep the info that is was segmented
		return count;
	}

	@Override
	public int getPartIndex(int segIndex) {
		int n = -1;
		for ( int i=0; i<parts.size(); i++ ) {
			if ( parts.get(i).isSegment() ) {
				n++;
				if ( n == segIndex ) return i;
			}
		}
		return -1; // Not found
	}

	@Override
	public int getIndex(String segId) {
		int n = 0;
		for (final TextPart part : parts) {
			if ( part.isSegment() ) {
				if ( segId.equals(part.id) ) return n;
				// Else, move to the next
				n++;
			}
		}
		return -1; // Not found
	}

	@Override
	public AlignmentStatus getAlignmentStatus() {
		return alignmentStatus;
	}

	@Override
	public void setAlignmentStatus(AlignmentStatus alignmentStatus) {
		this.alignmentStatus = alignmentStatus;
	}

	/**
	 * Checks if the id of a given TextPart is empty, null or a duplicate. If it is, the id
	 * is automatically set to a new value auto-generated.
	 * @param part the {@link TextPart} to verify and possibly modify.
	 */
	public void validateTextPartId(TextPart part) {
		if ( !Util.isEmpty(part.id) ) {
			// If not null or empty: check if it is a duplicate
			boolean duplicate = false;
			for ( final TextPart tmp : parts ) {
				if ( !tmp.isSegment() ) continue;
				if ( part == tmp ) continue;
				if ( part.id.equals(tmp.id) ) {
					duplicate = true;
					break;
				}
			}
			if ( !duplicate ) return; // Not a duplicate, nothing to do
		}

		// If duplicate or empty or null: assign a default id
		int value = 0;
		for ( final TextPart tmp : parts ) {
			if ( tmp == part ) continue; // Skip over the actual segment
			// If it starts with a digit, it's probably a number
			if (tmp.id != null && Character.isDigit(tmp.id.charAt(0)) ) {
				// try to convert the id to a integer
				try {
					final int val = Integer.parseInt(tmp.id);
					// Make the new id the same +1
					if ( value <= val ) value = val+1;
				}
				catch ( final NumberFormatException ignore ) {
					// Not really an error, just a non-numeric id
				}
			}
		}
		// Set the auto-value
		part.id = String.valueOf(value);
	}

	// WARNING: All TextPart annotations and Properties are lost after joining
	private TextFragment createJoinedContent(List<Range> ranges) {
		return createJoinedContent(ranges, false);
	}

	private TextFragment createJoinedContent(List<Range> ranges, boolean keepCodeIds) {
		// Clear the ranges if needed
		if ( ranges != null ) {
			ranges.clear();
		}
		// Join all segment into a new TextFragment
		int start = 0;
		final TextFragment tf = new TextFragment();
		for ( final TextPart part : parts ) {
			if (ranges != null) {
				Range r = new Range(start, start + part.text.text.length(), part.id);
				// remember original part as create(List<Range>...) nukes all original TextPart metadata
				r.part = part;
				ranges.add(r);
			}
			start += part.text.text.length();
			tf.append(part.getContent(), keepCodeIds);
		}
		return tf;
	}

	public TextContainer getParent() {
		return parent;
	}

	public List<TextPart> getParts() {
		return parts;
	}
}
