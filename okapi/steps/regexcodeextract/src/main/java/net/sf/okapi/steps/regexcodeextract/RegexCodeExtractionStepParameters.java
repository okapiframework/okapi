/*===========================================================================
  Copyright (C) 2023 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
===========================================================================*/

package net.sf.okapi.steps.regexcodeextract;

import net.sf.okapi.common.ParametersString;
import net.sf.okapi.common.StringParameters;
import net.sf.okapi.common.filters.InlineCodeFinder;

import java.util.ArrayList;
import java.util.Collection;

/**
 * This class is basically a Parameters class for InlineCodeFinder,
 * which it owns.
 * The #addCodeFinderRule and #addCodeFinderRule call
 * #addRule and #addRules of the internal code finder.
 */
public class RegexCodeExtractionStepParameters extends StringParameters {
  // It's a bit strange but following RegExFilter, this class owns the InlineCodeFinder.
  protected InlineCodeFinder codeFinder = new InlineCodeFinder();

  public RegexCodeExtractionStepParameters() {
    reset();
  }

  @Override
  public void reset() {
    super.reset();
    if (codeFinder == null) {
      codeFinder = new InlineCodeFinder();
    }
    codeFinder.reset();
  }


  /**
   * Passes the parameters to the fromString method of the InlineCodeFinder.
   * The string must look like this:
   * <code>
   #v1
   count.i=2
   rule0=\{\{.*?}}
   rule1=</?[a-zA-Z0-9]+?>
   </code>
   * @param value configuration string
   */
  public void setCodeFinderRules (String value) {
    codeFinder.fromString(value);
    codeFinder.compile();
  }

  public void addCodeFinderRule(String regexPattern) {
    codeFinder.addRule(regexPattern);
    codeFinder.compile();
  }

  public void addCodeFinderRules(final Collection<String> regexPatterns) {
    codeFinder.addRules(regexPatterns);
    codeFinder.compile();
  }

  public ArrayList<String> getCodeFinderRules() {
    return codeFinder.getRules();
  }

  @Override
  public String toString () {
    ParametersString tmp = new ParametersString();
    tmp.setGroup("codeFinderRules", codeFinder.toString());
    return tmp.toString();
  }

  @Override
  public void fromString (String data) {
    ParametersString tmp = new ParametersString(data);
    codeFinder.fromString(tmp.getGroup("codeFinderRules", ""));
  }
}
