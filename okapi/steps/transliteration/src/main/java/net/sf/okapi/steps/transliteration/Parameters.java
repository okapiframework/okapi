/*===========================================================================
  Copyright (C) 2021 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
===========================================================================*/

package net.sf.okapi.steps.transliteration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ibm.icu.text.Transliterator;
import com.ibm.icu.util.ULocale;

import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.StringParameters;

/**
 * Use cases (let's take {@code sr-Cyrl} to {@code sr-Latn} transliteration)
 *
 * <ul>
 * <li>take the source and "translate" (transliterate) it into target<br>
 * (source locale should be {@code sr-Cyrl}, target locale should be
 * {@code sr-Latn})
 * <li>take an existing target and convert in place (but then the target locale
 * is wrong, or was wrong)<br>
 * (source locale could be anything, a {@code sr-Cyrl} (or {@code sr-Latn}?)
 * target should already exist)
 * <li>take an existing target and create a new target with a new locale<br>
 * (source locale could be anything, a {@code sr-Cyrl} target locale should
 * already exist)
 * <ul>
 * <li>override new target locale ({@code sr-Latn}) if it exists already
 * ({@code boolean})
 * <li>remove / keep the original target locale ({@code sr-Cyrl})
 * </ul>
 * </ul>
 */
public class Parameters extends StringParameters {
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	public static final String NO_TRANSLITERATION = "";

	private static final String ICU_TRANSLITERATOR_ID = "icuTransliteratorId";
	private static final String FROM_TARGET_LOCALE = "fromTargetLocale";
	private static final String TO_TARGET_LOCALE = "toTargetLocale";

	private Boolean lastValidation = null;
	private Transliterator trans = null;

	public Parameters() {
		super();
	}

	public String getIcuTransliteratorId() {
		return getString(ICU_TRANSLITERATOR_ID);
	}

	public LocaleId getFromTargetLocale() {
		return LocaleId.fromBCP47(getString(FROM_TARGET_LOCALE));
	}

	public LocaleId getToTargetLocale() {
		return LocaleId.fromBCP47(getString(TO_TARGET_LOCALE));
	}

	public boolean setIcuTransliteratorId(String icuTransliteratorId) {
		setString(ICU_TRANSLITERATOR_ID, icuTransliteratorId);

		if (NO_TRANSLITERATION.equals(icuTransliteratorId)) {
			lastValidation = false;
			return lastValidation;
		}
		lastValidation = null;

		try {
			trans = Transliterator.getInstance(icuTransliteratorId);
		} catch (IllegalArgumentException e) {
			trans = null;
			lastValidation = false;
			logger.error(e.getMessage());
			return false;
		}
		return true;
	}

	public void setFromTargetLocale(LocaleId fromTargetLocale) {
		setString(FROM_TARGET_LOCALE, fromTargetLocale.toBCP47());
		lastValidation = null;
	}

	public void setToTargetLocale(LocaleId toTargetLocale) {
		setString(TO_TARGET_LOCALE, toTargetLocale.toBCP47());
		lastValidation = null;
	}

	Transliterator getTransliterator() {
		return trans;
	}

	public void reset() {
		super.reset();
		setIcuTransliteratorId(NO_TRANSLITERATION);
		setFromTargetLocale(LocaleId.EMPTY);
		setToTargetLocale(LocaleId.EMPTY);
	}

	public boolean isValid() {
		if (lastValidation == null)
			lastValidation = isValidImpl();
		return lastValidation;
	}

	private boolean isValidImpl() {
		if (lastValidation == null) {

			if (trans == null)
				return false;

			String transId = getIcuTransliteratorId();
			if (transId == null || Parameters.NO_TRANSLITERATION.equals(transId))
				return false;

			LocaleId fromTargetLocale = getFromTargetLocale();
			if (fromTargetLocale == null || LocaleId.EMPTY.equals(fromTargetLocale))
				return false;

			LocaleId toTargetLocale = getToTargetLocale();
			if (toTargetLocale == null || LocaleId.EMPTY.equals(toTargetLocale))
				return false;
			
			final ULocale fromTargetLocaleFull = ULocale.addLikelySubtags(fromTargetLocale.toIcuLocale());
			final ULocale toTargetLocaleFull = ULocale.addLikelySubtags(toTargetLocale.toIcuLocale());
			if (toTargetLocaleFull.equals(fromTargetLocaleFull))
				return false;
		}

		return true;
	}
}
