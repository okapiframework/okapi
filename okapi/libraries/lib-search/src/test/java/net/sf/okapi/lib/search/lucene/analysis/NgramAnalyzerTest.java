/*===========================================================================
  Copyright (C) 2008-2009 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
===========================================================================*/

package net.sf.okapi.lib.search.lucene.analysis;


import java.io.Reader;
import java.io.StringReader;
import net.sf.okapi.lib.search.Helper;

import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static org.junit.Assert.*;

/**
 *
 * @author HaslamJD
 */
@RunWith(JUnit4.class)
public class NgramAnalyzerTest {

    @Test
    public void testConstructor() throws Exception {
        NgramAnalyzer nga = new NgramAnalyzer(5);
//        assertEquals("Locale", Locale.CANADA, Helper.getPrivateMember(nga, "locale"));
        assertEquals("Ngram length", 5, (int) (Integer) Helper.getPrivateMember(nga, "ngramLength"));
    }

    @Test(expected=IllegalArgumentException.class)
    public void testConstructorInvalidNgramLength() throws Exception {
        @SuppressWarnings("unused")
		NgramAnalyzer nga = new NgramAnalyzer(0);
		nga.close();
    }

    @Test
    public void getTokenizer() throws Exception {
        NgramAnalyzer nga = new NgramAnalyzer( 3);

        Reader r = new StringReader("Blah!");
        TokenStream ts = nga.tokenStream("fieldName", r);
        CharTermAttribute termAtt = ts.addAttribute(CharTermAttribute.class);

        ts.reset();
        assertTrue("1st token exists", ts.incrementToken());
        assertEquals("1st token", "bla", termAtt.toString());
        assertTrue("2nd token exists", ts.incrementToken());
        assertEquals("2nd token", "lah", termAtt.toString());
        assertTrue("3rd token exists", ts.incrementToken());
        assertEquals("3rd token", "ah!", termAtt.toString());
        assertFalse("4th token should not exist", ts.incrementToken());

        ts.end();
        nga.close();
    }

    @Test
    public void shortEntry() throws Exception {
        NgramAnalyzer nga = new NgramAnalyzer( 3);

        Reader r = new StringReader("an");
        TokenStream ts = nga.tokenStream("fieldName", r);
        CharTermAttribute termAtt = ts.addAttribute(CharTermAttribute.class);

        ts.reset();
        assertTrue("A short token should be generated iff input is shorter than N", ts.incrementToken());
        assertEquals("The short token of the original term", "an", termAtt.toString());
        assertFalse("No other token should exist", ts.incrementToken());
        ts.end();
        nga.close();
    }

}
