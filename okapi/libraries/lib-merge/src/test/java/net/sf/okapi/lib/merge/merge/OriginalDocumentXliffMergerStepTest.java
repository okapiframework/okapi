/*===========================================================================
  Copyright (C) 2013 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
===========================================================================*/

package net.sf.okapi.lib.merge.merge;

import net.sf.okapi.common.Event;
import net.sf.okapi.common.EventType;
import net.sf.okapi.common.FileLocation;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.annotation.Note;
import net.sf.okapi.common.annotation.NoteAnnotation;
import net.sf.okapi.common.filters.DefaultFilters;
import net.sf.okapi.common.filters.FilterConfigurationMapper;
import net.sf.okapi.common.filters.FilterTestDriver;
import net.sf.okapi.common.filters.IFilter;
import net.sf.okapi.common.filters.IFilterConfigurationMapper;
import net.sf.okapi.common.resource.ITextUnit;
import net.sf.okapi.common.resource.RawDocument;
import net.sf.okapi.filters.html.HtmlFilter;
import net.sf.okapi.filters.xliff.XLIFFFilter;
import net.sf.okapi.filters.xml.XMLFilter;
import net.sf.okapi.lib.merge.merge.Parameters.NoteMergeStrategy;
import net.sf.okapi.lib.merge.step.OriginalDocumentXliffMergerStep;
import net.sf.okapi.steps.common.RawDocumentWriterStep;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.xmlunit.matchers.CompareMatcher;

import java.io.IOException;
import java.io.Reader;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static java.nio.charset.StandardCharsets.UTF_8;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@RunWith(JUnit4.class)
public class OriginalDocumentXliffMergerStepTest {
	private HtmlFilter htmlFilter;
	private XLIFFFilter xliffFilter;
	private FileLocation root;
	private OriginalDocumentXliffMergerStep merger;
	private RawDocumentWriterStep writer;

	@Before
	public void setUp() {
		htmlFilter = new HtmlFilter();
		xliffFilter = new XLIFFFilter();
		merger = new OriginalDocumentXliffMergerStep();
		writer = new RawDocumentWriterStep();
		root = FileLocation.fromClass(getClass());
	}

	@After
	public void tearDown() {
		htmlFilter.close();
		merger.destroy();
		writer.destroy();
	}

	@SuppressWarnings("resource")
	@Test
	public void simpleMerge() {
		String input = "/simple.html";
		// Serialize the source file
		MergerUtil.writeXliffAndSkeleton(FilterTestDriver.getEvents(
					htmlFilter, 
					new RawDocument(root.in(input).asInputStream(), "UTF-8", LocaleId.ENGLISH), null), 
				root.out("").toString(), root.out(input+".xlf").toString());

		IFilterConfigurationMapper fcm = new FilterConfigurationMapper();
		DefaultFilters.setMappings(fcm, true, true);
		merger.setFilterConfigurationMapper(fcm);
		merger.setOutputEncoding("UTF-8");
		RawDocument rd = new RawDocument(root.in(input).asInputStream(),"UTF-8", LocaleId.ENGLISH);
		rd.setFilterConfigId("okf_html");
		merger.setSecondInput(rd);
		List<LocaleId> ts = new LinkedList<>();
		ts.add(LocaleId.FRENCH);
		merger.setTargetLocales(ts);
		URI tempXlf = root.out(input+".xlf").asUri();
		// initialization
		merger.handleEvent(Event.createStartBatchItemEvent());
		Event e = merger.handleEvent(new Event(EventType.RAW_DOCUMENT, 
						new RawDocument(tempXlf, "UTF-8", LocaleId.ENGLISH, LocaleId.FRENCH)));
		merger.close();
		writer.setOutputURI(root.out(input+".merged").asUri());
		writer.handleEvent(e);
		writer.destroy();

		URI tempMerged = root.out(input+".merged").asUri();
		RawDocument ord = new RawDocument(root.in(input).asInputStream(), "UTF-8", LocaleId.ENGLISH);
		RawDocument trd = new RawDocument(tempMerged, "UTF-8", LocaleId.ENGLISH);
		List<Event> o = MergerUtil.getTextUnitEvents(htmlFilter, ord);
		List<Event> t = MergerUtil.getTextUnitEvents(htmlFilter, trd);
		assertTrue(o.size() == t.size());
		assertTrue(FilterTestDriver.compareEvents(o, t, true));
	}

	@Test
	public void translatableAndNonTranslatableContentHandled() throws IOException {
		final String input = "/translatable-and-untranslatable.xml";
		final String filterConfiguration = "okf_xml@translatable-and-untranslatable";
		FilterConfigurationMapper fcm = new FilterConfigurationMapper();
		fcm.addConfigurations(XMLFilter.class.getName());
		fcm.setCustomConfigurationsDirectory(root.in("/").toString());
		fcm.updateCustomConfigurations();
		IFilter filter = fcm.createFilter(filterConfiguration);
		// Serialize the source file
		MergerUtil.writeXliffAndSkeleton(
			FilterTestDriver.getEvents(
				filter,
				new RawDocument(root.in(input).asInputStream(), UTF_8.name(), LocaleId.SPANISH, LocaleId.ENGLISH),
				null
			),
			root.out("").toString(), root.out(input + ".xlf").toString(),
			true
		);
		merger.setFilterConfigurationMapper(fcm);
		merger.setOutputEncoding(UTF_8.name());
		RawDocument rd = new RawDocument(root.in(input).asInputStream(), UTF_8.name(), LocaleId.SPANISH, LocaleId.ENGLISH);
		rd.setFilterConfigId(filterConfiguration);
		merger.setSecondInput(rd);
		merger.setTargetLocales(Collections.singletonList(LocaleId.ENGLISH));
		URI tempXlf = root.out(input + ".xlf").asUri();
		// initialization
		merger.handleEvent(Event.createStartBatchItemEvent());
		Event e = merger.handleEvent(new Event(EventType.RAW_DOCUMENT,
			new RawDocument(tempXlf, UTF_8.name(), LocaleId.SPANISH, LocaleId.ENGLISH)));
		merger.close();
		writer.setOutputURI(root.out(input + ".merged").asUri());
		writer.handleEvent(e);
		writer.destroy();
		final Path inputPath = root.in(input).asPath();
		final Path outputPath = root.out(input + ".merged").asPath();
		try (final Reader out = Files.newBufferedReader(outputPath, UTF_8);
			 final Reader gold = Files.newBufferedReader(inputPath, UTF_8)) {
			assertThat(gold, CompareMatcher.isIdenticalTo(out));
		}
	}

	@Test
	public void testReplaceNotesOnMerge() {
		List<Note> notes = testNoteMerge(NoteMergeStrategy.REPLACE, ".modified.xlf");
		assertEquals(1, notes.size());
		assertEquals("New note", notes.get(0).getNoteText());
	}

	@Test
	public void testMergeNotesOnMerge() {
		List<Note> notes = testNoteMerge(NoteMergeStrategy.MERGE, ".modified.xlf");
		assertEquals(2, notes.size());
		assertTrue(notes.stream().map(Note::getNoteText).filter(t -> "Original note".equals(t)).findFirst().isPresent());
		assertTrue(notes.stream().map(Note::getNoteText).filter(t -> "New note".equals(t)).findFirst().isPresent());
	}

	@Test
	public void testMergeNotesAndConsolidateDuplicatesOnMerge() {
		// The source file is unchanged, so we're "merging" the original note and
		// itself.  We want to make sure we only have one copy in the result.
		List<Note> notes = testNoteMerge(NoteMergeStrategy.MERGE, "");
		assertEquals(1, notes.size());
		assertEquals("Original note", notes.get(0).getNoteText());
	}

	@Test
	public void testDiscardNotesOnMerge() {
		List<Note> notes = testNoteMerge(NoteMergeStrategy.DISCARD, ".modified.xlf");
		assertEquals(1, notes.size());
		assertEquals("Original note", notes.get(0).getNoteText());
	}

	private List<Note> testNoteMerge(NoteMergeStrategy strategy, String translatedFileSuffix) {
		String input = "/test.xlf";
		// Serialize the source file
		MergerUtil.writeXliffAndSkeleton(FilterTestDriver.getEvents(
					xliffFilter,
					new RawDocument(root.in(input).asInputStream(), "UTF-8", LocaleId.ENGLISH, LocaleId.SPANISH), null),
				root.out("").toString(), root.out(input+".xlf").toString());

		IFilterConfigurationMapper fcm = new FilterConfigurationMapper();
		DefaultFilters.setMappings(fcm, true, true);
		merger.setFilterConfigurationMapper(fcm);
		merger.setOutputEncoding("UTF-8");
		RawDocument rd = new RawDocument(root.in(input).asInputStream(), "UTF-8", LocaleId.ENGLISH, LocaleId.SPANISH);
		rd.setFilterConfigId("okf_xliff");
		merger.setSecondInput(rd);
		List<LocaleId> ts = new LinkedList<>();
		ts.add(LocaleId.SPANISH);
		merger.setTargetLocales(ts);
		merger.getSkelMergerWriter().getParameters().setNoteMergeStrategy(strategy);
		// initialization
		merger.handleEvent(Event.createStartBatchItemEvent());
		Event e = merger.handleEvent(new Event(EventType.RAW_DOCUMENT,
						new RawDocument(root.in(input + translatedFileSuffix).asInputStream(),
								"UTF-8", LocaleId.ENGLISH, LocaleId.SPANISH)));

		merger.close();
		writer.setOutputURI(root.out(input+".merged").asUri());
		writer.handleEvent(e);
		writer.destroy();

		URI tempMerged = root.out(input+".merged").asUri();
		RawDocument trd = new RawDocument(tempMerged, "UTF-8", LocaleId.ENGLISH, LocaleId.SPANISH);
		List<Event> t = MergerUtil.getTextUnitEvents(xliffFilter, trd);
		assertEquals(1, t.size());
		ITextUnit tu = t.get(0).getTextUnit();
		NoteAnnotation anno = tu.getAnnotation(NoteAnnotation.class);
		assertNotNull(anno);
		return StreamSupport.stream(anno.spliterator(), false).collect(Collectors.toList());
	}
}
