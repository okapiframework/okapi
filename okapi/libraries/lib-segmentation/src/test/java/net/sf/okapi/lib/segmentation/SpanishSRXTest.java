/*===========================================================================
  Copyright (C) 2023 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
===========================================================================*/
package net.sf.okapi.lib.segmentation;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class SpanishSRXTest {
    private static OkapiSegmenter segmenter;

    @BeforeClass
    public static void init() {
        segmenter = new OkapiSegmenter("es");
    }

    @Test
    public void okapiSegmentTest() {
        segment();
    }

    // NOTE: sentences here need to end with a space character so they
    // have correct whitespace when appended:
    private void segment() {
        test("¡Buenas Tardes!");
        test("¿Y Tú?");
        test("¡Vaya Ud Derecho!", " ¡Pues Tuerza Ud por la Izquierda/ Derecha!");
        test("Son las diez en punto.", " Las siete y media.");
        test("Un área importante de los aeropuertos es el \"centro de control de área\", en el cual se desempeñan los controladores del tráfico aéreo; personas encargadas de dirigir y controlar el movimiento de aeronaves en el aeropuerto y en la zona bajo su jurisdicción.");
        test("El mayor aeropuerto del mundo es el Aeropuerto Rey Khalid, en Arabia Saudita con un área total de 225 kilómetros cuadrados.");
        test("He aquí cómo ocurrió:", " En esa época vivía en el Perú un joven príncipe, hermoso y gallardo.");
        test("La presentación estuvo a cargo del abogado Gabriel Claudio Chamarro y denunció los delitos de \"abuso de autoridad\", \"violación de los deberes de funcionario público\" y \"malversación de caudales públicos\".");
        test("Pues tuerza Ud. por la S.A. izquierda derecha!");
        test("¿Cómo está hoy?", " Espero que muy bien.");
        test("¡Hola señorita!", " Espero que muy bien.");
        test("Hola Srta. Ledesma.", " Buenos días, soy el Lic. Naser Pastoriza, y él es mi padre, el Dr. Naser.");
        test("¡La casa cuesta $170.500.000,00!", " ¡Muy costosa!", " Se prevé una disminución del 12.5% para el próximo año.");
        test("«Ninguna mente extraordinaria está exenta de un toque de demencia.», dijo Aristóteles.");
        test("«Ninguna mente extraordinaria está exenta de un toque de demencia», dijo Aristóteles.", " Pablo, ¿adónde vas?", " ¡¿Qué viste?!");
        test("Admón. es administración o me equivoco.");
        test("1. Busca atención prenatal desde el principio");
        test("¡Hola Srta. Ledesma!", " ¿Cómo está hoy?", " Espero que muy bien.");
        test("Buenos días, soy el Lic. Naser Pastoriza, y él es mi padre, el Dr. Naser.");
        test("He apuntado una cita para la siguiente fecha:", " Mar. 23 de Nov. de 2014.", " Gracias.");
        test("Núm. de tel: 351.123.465.4.", " Envíe mis saludos a la Sra. Rescia.");
        test("Cero en la escala Celsius o de grados centígrados (0 °C) se define como el equivalente a 273.15 K, con una diferencia de temperatura de 1 °C equivalente a una diferencia de 1 Kelvin.", " Esto significa que 100 °C, definido como el punto de ebullición del agua, se define como el equivalente a 373.15 K.");
        test("Durante la primera misión del Discovery (30 Ago. 1984 15:08.10) tuvo lugar el lanzamiento de dos satélites de comunicación, el nombre de esta misión fue STS-41-D.");
        test("Citando a Criss Jami «Prefiero ser un artista a ser un líder, irónicamente, un líder tiene que seguir las reglas.», lo cual parece muy acertado.");
        test("Cuando llegué, le estaba dando ejercicios a los niños, uno de los cuales era \"3 + (14/7).x = 5\".", " ¿Qué te parece?");
        test("Se le pidió a los niños que leyeran los párrf. 5 y 6 del art. 4 de la constitución de los EE. UU..");
        test("El volumen del cuerpo es 3m³.", " ¿Cuál es la superficie de cada cara del prisma?");
        test("El corredor No. 103 arrivó 4°.");
        test("Explora oportunidades de carrera en el área de Salud en el Hospital de Northern en Mt. Kisco.");
        test("N°. 1026.253.553");
    }

    private void test(String... sentences) {
        SrxSplitCompare.compare(sentences, segmenter);
    }
}
