/*===========================================================================
  Copyright (C) 2023 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
===========================================================================*/
package net.sf.okapi.lib.segmentation;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class PolishSRXTest {
	private static OkapiSegmenter segmenter;

	@BeforeClass
	public static void init() {
		segmenter = new OkapiSegmenter("pl");
	}

	@Test
	public void testOkapiSegmentTest() {
		segment();
	}

	private void segment() {

		test("To się wydarzyło 3.10.2000 i mam na to dowody.");

		test("To było 13.12 - nikt nie zapomni tego przemówienia.");
		test("Heute ist der 13.12.2004.");
		test("To jest np. ten debil spod jedynki.");
		test("To jest 1. wydanie.");
		test("Dziś jest 13. rocznica powstania wąchockiego.");

		test("Das in Punkt 3.9.1 genannte Verhalten.");

		test("To jest tzw. premier.");
		test("Jarek kupił sobie kurteczkę, tj. strój Marka.");

		test("„Prezydent jest niemądry”.", " Tak wyszło.");
		test("„Prezydent jest niemądry”, powiedział premier");

		// from user bug reports:
		test("Temperatura wody w systemie wynosi 30°C.", " W skład obiegu otwartego wchodzi zbiornik i armatura.");
		test("Zabudowano kolumny o długości 45 m.", " Woda z ujęcia jest dostarczana do zakładu.");

		// two-letter initials:
		test("Najlepszym polskim reżyserem był St. Różewicz.", " Chodzi o brata wielkiego poety.");

		// From the abbreviation list:
		test("Ks. Jankowski jest prof. teologii.");
		test("To wydarzyło się w 1939 r.", " To był burzliwy rok.");
		test("Prezydent jest popierany przez 20 proc. społeczeństwa.");
		test("Moje wystąpienie ma na celu zmobilizowanie zarządu partii do działań, które umożliwią uzyskanie 40 proc.",
				" Nie widzę dziś na scenie politycznej formacji, która lepiej by łączyła różne poglądy");
		test("To jest zmienna A.", " Zaś to jest zmienna B.");
		// SKROTY_BEZ_KROPKI in ENDABREVLIST
		test("Mam już 20 mln.", " To powinno mi wystarczyć");
		test("Mam już 20 mln. buraków.");
		// ellipsis
		//testSplit("Rytmem tej wiecznie przemijającej światowej egzystencji […] rytmem mesjańskiej natury jest szczęście.");
		// sic!
		test("W gazecie napisali, że pasy (sic!) pogryzły człowieka.");
		// Numbers with dots.
		test("Mam w magazynie dwie skrzynie LMD20.", " Jestem żołnierzem i wiem, jak można ich użyć");
	}

	private void test(final String... sentences) {
		SrxSplitCompare.compare(sentences, segmenter);
	}

}
